var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm y tế phường Cổ Nhuế 2",
   "address": "phường Cổ Nhuế, quận Bắc Từ Liêm, Hà Nội ",
   "Longtitude": 21.0660875,
   "Latitude": 105.7639698
 },
 {
   "STT": 2,
   "Name": "Trạm y tế phường Phúc Diễn",
   "address": "phường Phúc Diễn, quận Bắc Từ Liêm, Hà Nội ",
   "Longtitude": 21.04729,
   "Latitude": 105.7512549
 },
 {
   "STT": 3,
   "Name": "Trạm y tế phường Phương Canh",
   "address": "phường Phương Canh, quận Nam Từ Liêm, Hà Nội ",
   "Longtitude": 21.0426019,
   "Latitude": 105.7395182
 },
 {
   "STT": 4,
   "Name": "Trạm y tế phường Mỹ Đình 2",
   "address": "phương Mỹ Đình, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0221606,
   "Latitude": 105.771796
 },
 {
   "STT": 5,
   "Name": "Trạm y tế phường Khương Mai (TTYT Thanh Xuân)",
   "address": "phường Khương Mai, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9963153,
   "Latitude": 105.8304975
 },
 {
   "STT": 6,
   "Name": "Trạm y tế P.Thanh Xuân Trung (TTYT Thanh Xuân)",
   "address": "phường Thanh Xuân Trung, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9959212,
   "Latitude": 105.8040796
 },
 {
   "STT": 7,
   "Name": "Trạm y tế phường Phương Liệt (TTYT Thanh Xuân)",
   "address": "phường Phương Liệt, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9925823,
   "Latitude": 105.8393044
 },
 {
   "STT": 8,
   "Name": "Trạm y tế phường Hạ Đình (TTYT Thanh Xuân)",
   "address": "phường Hạ Đình,, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9866719,
   "Latitude": 105.8099499
 },
 {
   "STT": 9,
   "Name": "Trạm y tế phường Khương Đình (TTYT Thanh Xuân)",
   "address": "phường Khương Đình, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9887356,
   "Latitude": 105.8187558
 },
 {
   "STT": 10,
   "Name": "Trạm y tế P.Thanh Xuân Bắc (TTYT Thanh Xuân)",
   "address": "phường Thanh Xuân Bắc, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.993579,
   "Latitude": 105.7982094
 },
 {
   "STT": 11,
   "Name": "Trạm y tế P.Thanh Xuân Nam (TTYT Thanh Xuân)",
   "address": "phường Thanh Xuân Nam, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9855403,
   "Latitude": 105.7989432
 },
 {
   "STT": 12,
   "Name": "Trạm y tế phường Kim Giang (TTYT Thanh Xuân)",
   "address": "phường Kim Giang, Quận Thanh Xuân , Hà Nội",
   "Longtitude": 20.9828413,
   "Latitude": 105.8121513
 },
 {
   "STT": 13,
   "Name": "Trạm y tế thị trấn Sóc Sơn (TTYT H.Sóc Sơn)",
   "address": "Thị Trấn Sóc Sơn, Huyện Sóc Sơn , Hà Nội",
   "Longtitude": 21.2556659,
   "Latitude": 105.8532494
 },
 {
   "STT": 14,
   "Name": "Trạm y tế xã Bắc Sơn (TTYT h. Sóc Sơn)",
   "address": "Xã Bắc Sơn, Sóc Sơn , Hà Nội",
   "Longtitude": 21.3636125,
   "Latitude": 105.8231588
 },
 {
   "STT": 15,
   "Name": "Trạm y tế xã Minh Trí (TTYT h. Sóc Sơn)",
   "address": "Xã Minh Trí, Sóc Sơn , Hà Nội",
   "Longtitude": 21.3322109,
   "Latitude": 105.7879371
 },
 {
   "STT": 16,
   "Name": "Trạm y tế xã Hồng Kỳ (TTYT h. Sóc Sơn)",
   "address": "Xã Hồng Kỳ, Sóc Sơn , Hà Nội",
   "Longtitude": 21.3215666,
   "Latitude": 105.8550831
 },
 {
   "STT": 17,
   "Name": "Trạm y tế xã Nam Sơn (TTYT h. Sóc Sơn)",
   "address": "Xã Nam Sơn, Sóc Sơn , Hà Nội",
   "Longtitude": 21.3235995,
   "Latitude": 105.8172881
 },
 {
   "STT": 18,
   "Name": "Trạm y tế xã Trung Giã (TTYT h. Sóc Sơn)",
   "address": "Xã Trung Giã, Sóc Sơn , Hà Nội",
   "Longtitude": 21.3153395,
   "Latitude": 105.8730678
 },
 {
   "STT": 19,
   "Name": "Trạm y tế xã Tân Hưng (TTYT h. Sóc Sơn)",
   "address": "Xã Tân Hưng, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2951248,
   "Latitude": 105.9024322
 },
 {
   "STT": 20,
   "Name": "Trạm y tế xã Minh Phú (TTYT h. Sóc Sơn)",
   "address": "Xã Minh Phú, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2753602,
   "Latitude": 105.776198
 },
 {
   "STT": 21,
   "Name": "Trạm y tế xã Phù Linh (TTYT h. Sóc Sơn)",
   "address": "Xã Phù Linh, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2802077,
   "Latitude": 105.8466437
 },
 {
   "STT": 22,
   "Name": "Trạm y tế xã Bắc Phú (TTYT h. Sóc Sơn)",
   "address": "Xã Bắc Phú, Sóc Sơn , Hà Nội",
   "Longtitude": 21.27456,
   "Latitude": 105.9053689
 },
 {
   "STT": 23,
   "Name": "Trạm y tế xã Tân Minh (TTYT h. Sóc Sơn)",
   "address": "Xã Tân Minh, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2779513,
   "Latitude": 105.8701316
 },
 {
   "STT": 24,
   "Name": "Trạm y tế xã Quang Tiến (TTYT h. Sóc Sơn)",
   "address": "Xã Quang Tiến, Sóc Sơn , Hà Nội",
   "Longtitude": 21.251423,
   "Latitude": 105.8143528
 },
 {
   "STT": 25,
   "Name": "Trạm y tế xã Hiền Ninh (TTYT h. Sóc Sơn)",
   "address": "Xã Hiền Ninh, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2510499,
   "Latitude": 105.7879371
 },
 {
   "STT": 26,
   "Name": "Trạm y tế xã Tân Dân (TTYT h. Sóc Sơn)",
   "address": "Xã Tân Dân, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2529081,
   "Latitude": 105.7380511
 },
 {
   "STT": 27,
   "Name": "Trạm y tế xã Tiên Dược (TTYT h. Sóc Sơn)",
   "address": "Xã Tiên Dược, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2428157,
   "Latitude": 105.843708
 },
 {
   "STT": 28,
   "Name": "Trạm y tế xã Việt Long (TTYT h. Sóc Sơn)",
   "address": "Xã Việt Long, Sóc Sơn , Hà Nội",
   "Longtitude": 21.24707,
   "Latitude": 105.9200531
 },
 {
   "STT": 29,
   "Name": "Trạm y tế xã Xuân Giang (TTYT h. Sóc Sơn)",
   "address": "Xã Xuân Giang, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2427624,
   "Latitude": 105.8986169
 },
 {
   "STT": 30,
   "Name": "Trạm y tế xã Mai Đình (TTYT h. Sóc Sơn)",
   "address": "Xã Mai Đình, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2295659,
   "Latitude": 105.8160445
 },
 {
   "STT": 31,
   "Name": "Trạm y tế xã Đức Hòa (TTYT h. Sóc Sơn)",
   "address": "Xã Đức Hòa, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2304607,
   "Latitude": 105.8818766
 },
 {
   "STT": 32,
   "Name": "Trạm y tế xã Thanh Xuân (TTYT h. Sóc Sơn)",
   "address": "Xã Thanh Xuân, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2301005,
   "Latitude": 105.7644596
 },
 {
   "STT": 33,
   "Name": "Trạm y tế xã Đông Xuân (TTYT h. Sóc Sơn)",
   "address": "Xã Đông Xuân, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2173817,
   "Latitude": 105.8671954
 },
 {
   "STT": 34,
   "Name": "Trạm y tế xã Kim Lũ (TTYT h. Sóc Sơn)",
   "address": "Xã Kim Lũ, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2134343,
   "Latitude": 105.9083056
 },
 {
   "STT": 35,
   "Name": "Trạm y tế xã Phú Cường (TTYT h. Sóc Sơn)",
   "address": "Xã Phú Cường, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2046739,
   "Latitude": 105.7879371
 },
 {
   "STT": 36,
   "Name": "Trạm y tế xã Phú Minh (TTYT h. Sóc Sơn)",
   "address": "Xã Phú Minh, Sóc Sơn , Hà Nội",
   "Longtitude": 21.2024335,
   "Latitude": 105.8114175
 },
 {
   "STT": 37,
   "Name": "Trạm y tế xã Phù Lỗ (TTYT h. Sóc Sơn)",
   "address": "Xã Phù Lỗ, Sóc Sơn , Hà Nội",
   "Longtitude": 21.1990662,
   "Latitude": 105.8466437
 },
 {
   "STT": 38,
   "Name": "Trạm y tế xã Xuân Thu (TTYT h. Sóc Sơn)",
   "address": "Xã Xuân Thu, Sóc Sơn , Hà Nội",
   "Longtitude": 21.1971793,
   "Latitude": 105.896559
 },
 {
   "STT": 39,
   "Name": "Trạm y tế phường Phúc Xá (TTYT Ba Đình)",
   "address": "86 Nghĩa Dũng, phường Phúc Xá, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.0478961,
   "Latitude": 105.8469748
 },
 {
   "STT": 40,
   "Name": "Trạm y tế phường Trúc Bạch (TTYT Ba Đình)",
   "address": "2 - trúc Bạch - ba Đình - hà Nội , Hà Nội",
   "Longtitude": 21.0440723,
   "Latitude": 105.8435583
 },
 {
   "STT": 41,
   "Name": "Trạm y tế phường Cống Vị (TTYT Ba Đình)",
   "address": "Ngõ 518 Đội Cấn, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.0376899,
   "Latitude": 105.8081393
 },
 {
   "STT": 42,
   "Name": "Trạm y tế phường Nguyễn Trung Trực (TTYT B.Đ)",
   "address": "6 Ngõ Hàng Bún, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.041889,
   "Latitude": 105.8453858
 },
 {
   "STT": 43,
   "Name": "Trạm y tế phường Quán Thánh (TTYT Ba Đình)",
   "address": "phường Quán Thánh , Hà Nội",
   "Longtitude": 21.0389429,
   "Latitude": 105.8393044
 },
 {
   "STT": 44,
   "Name": "Trạm y tế phường Ngọc Hà (TTYT Ba Đình)",
   "address": "42 Ngách 55 Tổ 17 Ngọc Hà, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.0350806,
   "Latitude": 105.8318277
 },
 {
   "STT": 45,
   "Name": "Trạm y tế phường Điện Biên (TTYT Ba Đình)",
   "address": "142 - 144 Nguyễn Thái Học, P Điện Biên , Hà Nội",
   "Longtitude": 21.031928,
   "Latitude": 105.830891
 },
 {
   "STT": 46,
   "Name": "Trạm y tế phường Đội Cấn (TTYT Ba Đình)",
   "address": "193 Đội Cấn, phường Đội Cấn, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.035077,
   "Latitude": 105.823746
 },
 {
   "STT": 47,
   "Name": "Trạm y tế phường Ngọc Khánh (TTYT Ba Đình)",
   "address": "27 Nguyễn Chí Thanh, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.028973,
   "Latitude": 105.81263
 },
 {
   "STT": 48,
   "Name": "Trạm y tế phường Kim Mã (TTYT Ba Đình)",
   "address": "Ngõ 166 Kim Mã, Quận Ba Đình , Hà Nội",
   "Longtitude": 20.9823049,
   "Latitude": 105.8498609
 },
 {
   "STT": 49,
   "Name": "Trạm y tế phường Giảng Võ (TTYT Ba Đình)",
   "address": "148c Ngọc Khánh, phường Giảng Võ, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.0257551,
   "Latitude": 105.8182086
 },
 {
   "STT": 50,
   "Name": "Trạm y tế phường Thành Công (TTYT Ba Đình)",
   "address": "Gần Nhà B4 Thành Công, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.0220353,
   "Latitude": 105.8154306
 },
 {
   "STT": 51,
   "Name": "Trạm y tế phường Vĩnh Phúc (TTYT Ba Đình)",
   "address": "K1 Khu 7,2ha phường Vĩnh Phúc, Quận Ba Đình , Hà Nội",
   "Longtitude": 21.0207688,
   "Latitude": 105.8175724
 },
 {
   "STT": 52,
   "Name": "Trạm y tế phường Liễu Giai (TTYT Ba Đình)",
   "address": "22 Văn Cao - Ba Đình - hà Nội , Hà Nội",
   "Longtitude": 21.0413709,
   "Latitude": 105.8158892
 },
 {
   "STT": 53,
   "Name": "Trạm y tế phường Phúc Tân (TTYT Hoàn Kiếm)",
   "address": "phường Phúc Tân, Quận Hoàn Kiếm , Hà Nội",
   "Longtitude": 21.0372674,
   "Latitude": 105.8569193
 },
 {
   "STT": 54,
   "Name": "Trạm y tế phường Đồng Xuân (TTYT Hoàn Kiếm)",
   "address": "phường Đồng Xuân, Quận Hoàn Kiếm , Hà Nội",
   "Longtitude": 21.0392122,
   "Latitude": 105.8498353
 },
 {
   "STT": 55,
   "Name": "Trạm y tế phường Hàng Mã (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Mã , Hà Nội",
   "Longtitude": 21.0375904,
   "Latitude": 105.8459098
 },
 {
   "STT": 56,
   "Name": "Trạm y tế phường Hàng Buồm (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Buồm , Hà Nội",
   "Longtitude": 21.0355832,
   "Latitude": 105.8517814
 },
 {
   "STT": 57,
   "Name": "Trạm y tế phường Hàng Đào (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Đào , Hà Nội",
   "Longtitude": 21.0346712,
   "Latitude": 105.8499465
 },
 {
   "STT": 58,
   "Name": "Trạm y tế phường Hàng Bồ (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Bồ , Hà Nội",
   "Longtitude": 21.0348806,
   "Latitude": 105.8477447
 },
 {
   "STT": 59,
   "Name": "Trạm y tế phường Cửa Đông (TTYT Hoàn Kiếm)",
   "address": "phường Cửa Đông , Hà Nội",
   "Longtitude": 21.0329168,
   "Latitude": 105.8455428
 },
 {
   "STT": 60,
   "Name": "Trạm y tế phường Lý Thái Tổ (TTYT Hoàn Kiếm)",
   "address": "phường Lý Thái Tổ , Hà Nội",
   "Longtitude": 21.0309577,
   "Latitude": 105.8547173
 },
 {
   "STT": 61,
   "Name": "Trạm y tế phường Hàng Bạc (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Bạc , Hà Nội",
   "Longtitude": 21.0329432,
   "Latitude": 105.8528824
 },
 {
   "STT": 62,
   "Name": "Trạm y tế phường Hàng Gai (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Gai , Hà Nội",
   "Longtitude": 21.0319133,
   "Latitude": 105.8484786
 },
 {
   "STT": 63,
   "Name": "Trạm y tế phường Chương Dương (TTYT Hoàn Kiếm)",
   "address": "phường Chương Dương, Quận Hoàn Kiếm , Hà Nội",
   "Longtitude": 21.0265395,
   "Latitude": 105.8621601
 },
 {
   "STT": 64,
   "Name": "Trạm y tế phường Hàng Trống (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Trống , Hà Nội",
   "Longtitude": 21.0284792,
   "Latitude": 105.8503135
 },
 {
   "STT": 65,
   "Name": "Trạm y tế phường Cửa Nam (TTYT Hoàn Kiếm)",
   "address": "phường Cửa Nam , Hà Nội",
   "Longtitude": 21.0252277,
   "Latitude": 105.8426071
 },
 {
   "STT": 66,
   "Name": "Trạm y tế phường Hàng Bông (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Bông , Hà Nội",
   "Longtitude": 21.0288979,
   "Latitude": 105.8459098
 },
 {
   "STT": 67,
   "Name": "Trạm y tế phường Tràng Tiền (TTYT Hoàn Kiếm)",
   "address": "phường Tràng Tiền, Quận Hoàn Kiếm , Hà Nội",
   "Longtitude": 21.0251629,
   "Latitude": 105.8547173
 },
 {
   "STT": 68,
   "Name": "Trạm y tế phường Trần Hưng Đạo (TTYT HK)",
   "address": "phường Trần Hưng Đạo , Hà Nội",
   "Longtitude": 21.0229634,
   "Latitude": 105.8473777
 },
 {
   "STT": 69,
   "Name": "Trạm y tế phường Phan Chu Trinh (TTYT HK)",
   "address": "phường Phan Chu Trinh , Hà Nội",
   "Longtitude": 21.0205376,
   "Latitude": 105.8576533
 },
 {
   "STT": 70,
   "Name": "Trạm y tế phường Hàng Bài (TTYT Hoàn Kiếm)",
   "address": "phường Hàng Bài, Quận Hoàn Kiếm , Hà Nội",
   "Longtitude": 21.021096,
   "Latitude": 105.8517814
 },
 {
   "STT": 71,
   "Name": "Trạm y tế phường Phú Thượng,  Tây Hồ",
   "address": "3 Phú Gia, phường Phú Thượng, Tây Hồ , Hà Nội",
   "Longtitude": 21.0892082,
   "Latitude": 105.8085541
 },
 {
   "STT": 72,
   "Name": "Trạm y tế phường Nhật Tân, Tây Hồ",
   "address": "Ngõ 479 Đường Âu Cơ, P Nhật Tân, Tây Hồ , Hà Nội",
   "Longtitude": 21.0790167,
   "Latitude": 105.8192278
 },
 {
   "STT": 73,
   "Name": "Trạm y tế phường Tứ Liên,  Tây Hồ",
   "address": "68 Âu Cơ, phường Tứ Liên, Tây Hồ , Hà Nội",
   "Longtitude": 21.0657,
   "Latitude": 105.836387
 },
 {
   "STT": 74,
   "Name": "Trạm y tế phường Quảng An, Tây Hồ",
   "address": "9 Ngõ 31 Xuân Diệu, P Quảng An, Tây Hồ , Hà Nội",
   "Longtitude": 21.0627639,
   "Latitude": 105.8285857
 },
 {
   "STT": 75,
   "Name": "Trạm y tế phường Xuân La, Tây Hồ",
   "address": "Tổ 23 Cụm 3 Đường Xuân La, Tây Hồ , Hà Nội",
   "Longtitude": 21.0596756,
   "Latitude": 105.8040796
 },
 {
   "STT": 76,
   "Name": "Trạm y tế phường Yên Phụ, Tây Hồ",
   "address": "48 Yên Phụ, phường Yên Phụ, Tây Hồ , Hà Nội",
   "Longtitude": 21.052021,
   "Latitude": 105.838182
 },
 {
   "STT": 77,
   "Name": "Trạm y tế phường Bưởi, Tây Hồ",
   "address": "564 Thuỵ Khuê, phường Bưởi, Tây Hồ , Hà Nội",
   "Longtitude": 21.0484648,
   "Latitude": 105.8078724
 },
 {
   "STT": 78,
   "Name": "Trạm y tế phường Thụy Khuê ,Tây Hồ",
   "address": "271a Thuỵ Khuê, phường Thuỵ Khuê, Tây Hồ , Hà Nội",
   "Longtitude": 21.0440916,
   "Latitude": 105.8154205
 },
 {
   "STT": 79,
   "Name": "Trạm y tế phường Thượng Thanh (TTYTq.LB)",
   "address": "phường Thượng Thanh, Long Biên , Hà Nội",
   "Longtitude": 21.0655013,
   "Latitude": 105.8883137
 },
 {
   "STT": 80,
   "Name": "Trạm y tế phường Ngọc Thụy (TTYTq.LB)",
   "address": "phường Ngọc Thuỵ, Long Biên , Hà Nội",
   "Longtitude": 21.0588582,
   "Latitude": 105.8583873
 },
 {
   "STT": 81,
   "Name": "Trạm y tế phường Giang Biên (TTYTq.LB)",
   "address": "phường Giang Biên, Long Biên , Hà Nội",
   "Longtitude": 21.0674567,
   "Latitude": 105.9200531
 },
 {
   "STT": 82,
   "Name": "Trạm y tế phường Đức Giang (TTYTq.LB)",
   "address": "phường Đức Giang, Long Biên , Hà Nội",
   "Longtitude": 21.0701691,
   "Latitude": 105.9068373
 },
 {
   "STT": 83,
   "Name": "Trạm y tế phường Việt Hưng (TTYTq.LB)",
   "address": "phường Việt Hưng, Long Biên , Hà Nội",
   "Longtitude": 21.0575538,
   "Latitude": 105.9024322
 },
 {
   "STT": 84,
   "Name": "Trạm y tế phường Gia Thụy (TTYTq.LB)",
   "address": "phường Gia Thuỵ, Long Biên , Hà Nội",
   "Longtitude": 21.048956,
   "Latitude": 105.8862812
 },
 {
   "STT": 85,
   "Name": "Trạm y tế phường Ngọc Lâm (TTYTq.LB)",
   "address": "phường Ngọc Lâm, Long Biên , Hà Nội",
   "Longtitude": 21.0448412,
   "Latitude": 105.8686635
 },
 {
   "STT": 86,
   "Name": "Trạm y tế phường Phúc Lợi (TTYTq.LB)",
   "address": "phường Phúc Lợi, Long Biên , Hà Nội",
   "Longtitude": 21.0437208,
   "Latitude": 105.9259271
 },
 {
   "STT": 87,
   "Name": "Trạm y tế phường Bồ Đề (TTYTq.LB)",
   "address": "phường Bồ Đề, Long Biên , Hà Nội",
   "Longtitude": 21.0371784,
   "Latitude": 105.8730678
 },
 {
   "STT": 88,
   "Name": "Trạm y tế phường Sài Đồng (TTYTq.LB)",
   "address": "phường Sài Đồng, Long Biên , Hà Nội",
   "Longtitude": 21.0348453,
   "Latitude": 105.9127108
 },
 {
   "STT": 89,
   "Name": "Trạm y tế phường Long Biên (TTYTq.LB)",
   "address": "phường Long Biên, Long Biên , Hà Nội",
   "Longtitude": 21.0186765,
   "Latitude": 105.884813
 },
 {
   "STT": 90,
   "Name": "Trạm y tế phường Thạch Bàn (TTYTq.LB)",
   "address": "phường Thạch Bàn, Long Biên , Hà Nội",
   "Longtitude": 21.0216695,
   "Latitude": 105.9141793
 },
 {
   "STT": 91,
   "Name": "Trạm y tế phường Phúc Đồng (TTYTq.LB)",
   "address": "phường Phúc Đồng, Long Biên , Hà Nội",
   "Longtitude": 21.0407327,
   "Latitude": 105.896559
 },
 {
   "STT": 92,
   "Name": "Trạm y tế phường Cự Khối (TTYTq.LB)",
   "address": "phường Cự Khối, Long Biên , Hà Nội",
   "Longtitude": 21.0059693,
   "Latitude": 105.896559
 },
 {
   "STT": 93,
   "Name": "Trạm y tế phường Nghĩa Đô, Cầu Giấy",
   "address": "phường Nghĩa Đô, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0454641,
   "Latitude": 105.8011445
 },
 {
   "STT": 94,
   "Name": "Trạm y tế phường Nghĩa Tân, Cầu Giấy",
   "address": "phường Nghĩa Tân, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0450408,
   "Latitude": 105.7921796
 },
 {
   "STT": 95,
   "Name": "Trạm y tế phường Mai Dịch, Cầu Giấy",
   "address": "phường Mai Dịch, , Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0395659,
   "Latitude": 105.7740712
 },
 {
   "STT": 96,
   "Name": "Trạm y tế phường Dịch Vọng, Cầu Giấy",
   "address": "phường Dịch Vọng, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0347067,
   "Latitude": 105.7923394
 },
 {
   "STT": 97,
   "Name": "Trạm y tế phường Quan Hoa, Cầu Giấy",
   "address": "phường Quan Hoa, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0367702,
   "Latitude": 105.8011445
 },
 {
   "STT": 98,
   "Name": "Trạm y tế phường Yên Hoà, Cầu Giấy",
   "address": "phường Yên Hoà, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0218044,
   "Latitude": 105.790872
 },
 {
   "STT": 99,
   "Name": "Trạm y tế phường Trung Hoà,  Cầu Giấy",
   "address": "phường Trung Hoà, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0101531,
   "Latitude": 105.7988345
 },
 {
   "STT": 100,
   "Name": "Trạm y tế phường Dịch Vọng H_u, Cầu Giấy",
   "address": "phường Dịch Vọng Hậu, Quận Cầu Giấy , Hà Nội",
   "Longtitude": 21.0339527,
   "Latitude": 105.7850022
 },
 {
   "STT": 101,
   "Name": "Trạm y tế phường Cát Linh (TTYT Đống Đa)",
   "address": "phường Cát Linh , Hà Nội",
   "Longtitude": 21.0289833,
   "Latitude": 105.8297637
 },
 {
   "STT": 102,
   "Name": "Trạm y tế phường Văn Miếu (TTYT Đống Đa)",
   "address": "130 Nguyễn Khuyến, phường Văn Miếu , Hà Nội",
   "Longtitude": 21.028574,
   "Latitude": 105.8376119
 },
 {
   "STT": 103,
   "Name": "Trạm y tế phường Quốc Tử Giám (TTYT Đống Đa)",
   "address": "14 Thông Phong, phường Quốc Tử Giám , Hà Nội",
   "Longtitude": 21.0272556,
   "Latitude": 105.8326992
 },
 {
   "STT": 104,
   "Name": "Trạm y tế phường Láng Thượng (TTYT Đống Đa)",
   "address": "112 Chùa Láng phường Láng Thượng , Hà Nội",
   "Longtitude": 21.0234642,
   "Latitude": 105.8034229
 },
 {
   "STT": 105,
   "Name": "Trạm y tế phường Ô Chợ Dừa (TTYT Đống Đa)",
   "address": "197 Đông Các, phường Ô Chợ Dừa , Hà Nội",
   "Longtitude": 21.0203389,
   "Latitude": 105.8256506
 },
 {
   "STT": 106,
   "Name": "Trạm y tế phường Văn Chương (TTYT Đống Đa)",
   "address": "53 Ngõ Văn Chương, P. Văn Chương , Hà Nội",
   "Longtitude": 21.0230326,
   "Latitude": 105.8332622
 },
 {
   "STT": 107,
   "Name": "Trạm y tế phường Hàng Bột (TTYT Đống Đa)",
   "address": "107 Tôn Đức Thắng, phường Hàng Bột , Hà Nội",
   "Longtitude": 21.0238533,
   "Latitude": 105.8324841
 },
 {
   "STT": 108,
   "Name": "Trạm y tế phường Láng Hạ (TTYT Đống Đa)",
   "address": "9 Ngõ 107 Nguyễn Chí Thanh, P Láng Hạ , Hà Nội",
   "Longtitude": 21.0164132,
   "Latitude": 105.8063009
 },
 {
   "STT": 109,
   "Name": "Trạm y tế phường Khâm Thiên (TTYT Đống Đa)",
   "address": "9 Ngõ Đình Tương Thuận, P Khâm Thiên , Hà Nội",
   "Longtitude": 21.0189966,
   "Latitude": 105.8391845
 },
 {
   "STT": 110,
   "Name": "Trạm y tế phường Thổ Quan (TTYT Đống Đa)",
   "address": "phường Thổ Quan , Hà Nội",
   "Longtitude": 21.0169746,
   "Latitude": 105.834167
 },
 {
   "STT": 111,
   "Name": "Trạm y tế phường Nam Đồng (TTYT Đống Đa)",
   "address": "194 Nguyễn Lương Bằng, P Nam Đồng , Hà Nội",
   "Longtitude": 21.0149322,
   "Latitude": 105.8275847
 },
 {
   "STT": 112,
   "Name": "Trạm y tế phường Trung Phụng (TTYT Đống Đa)",
   "address": "86 Ngõ Lan Bá, phường Trung Phụng , Hà Nội",
   "Longtitude": 21.0155346,
   "Latitude": 105.8395222
 },
 {
   "STT": 113,
   "Name": "Trạm y tế phường Quang Trung (TTYT Đống Đa)",
   "address": "194 Nguyễn Lương Bằng, P.Quang Trung , Hà Nội",
   "Longtitude": 21.0149322,
   "Latitude": 105.8275847
 },
 {
   "STT": 114,
   "Name": "Trạm y tế phường Trung Liệt (TTYT Đống Đa)",
   "address": "18 Trung Liệt, phường Trung Liệt , Hà Nội",
   "Longtitude": 21.012894,
   "Latitude": 105.8218946
 },
 {
   "STT": 115,
   "Name": "Trạm y tế phường Phương Liên (TTYT Đống Đa)",
   "address": "80 Kim Hoa, phường Phương Liên , Hà Nội",
   "Longtitude": 21.009425,
   "Latitude": 105.839539
 },
 {
   "STT": 116,
   "Name": "Trạm y tế phường Thịnh Quang (TTYT Đống Đa)",
   "address": "10 Ngõ 122 Đường Láng, Pthịnh Quang , Hà Nội",
   "Longtitude": 21.00709,
   "Latitude": 105.818872
 },
 {
   "STT": 117,
   "Name": "Trạm y tế phường Trung Tự (TTYT Đống Đa)",
   "address": "2 Ngõ 4d Đặng Văn Ngữ, Ptrung Tự , Hà Nội",
   "Longtitude": 21.0091577,
   "Latitude": 105.8329426
 },
 {
   "STT": 118,
   "Name": "Trạm y tế phường Kim Liên (TTYT Đống Đa)",
   "address": "20b Phụ, phường Kim Liên , Hà Nội",
   "Longtitude": 21.0295835,
   "Latitude": 105.8477748
 },
 {
   "STT": 119,
   "Name": "Trạm y tế phường Phương Mai (TTYT Đống Đa)",
   "address": "28c Lương Định Của, P Phương Mai , Hà Nội",
   "Longtitude": 21.001559,
   "Latitude": 105.836871
 },
 {
   "STT": 120,
   "Name": "Trạm y tế phường Ngã Tư Sở (TTYT Đống Đa)",
   "address": "phường Ngã Tư Sở , Hà Nội",
   "Longtitude": 21.004722,
   "Latitude": 105.8220581
 },
 {
   "STT": 121,
   "Name": "Trạm y tế phường Khương Thượng (TTYT Đống Đa)",
   "address": "107 Khương Thượng, Pkhương Thượng , Hà Nội",
   "Longtitude": 21.005046,
   "Latitude": 105.8259157
 },
 {
   "STT": 122,
   "Name": "Trạm y tế phường Nguyễn Du (TTYT Hai Bà Trưng)",
   "address": "phường Nguyễn Du, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0187567,
   "Latitude": 105.8459098
 },
 {
   "STT": 123,
   "Name": "Trạm y tế phường Bạch Đằng (TTYT Hai Bà Trưng)",
   "address": "phường Bạch Đằng, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0132507,
   "Latitude": 105.8657274
 },
 {
   "STT": 124,
   "Name": "Trạm y tế phường Phạm Đình Hổ (TTYT HBT)",
   "address": "phường Phạm Đình Hổ, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0161916,
   "Latitude": 105.8576533
 },
 {
   "STT": 125,
   "Name": "Trạm y tế phường Bùi Thị Xuân (TTYT HBT)",
   "address": "phường Bùi Thị Xuân, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.014855,
   "Latitude": 105.8504718
 },
 {
   "STT": 126,
   "Name": "Trạm y tế phường Ngô Thì Nhậm (TTYT HBT)",
   "address": "phường Ngô Thì Nhậm, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0166103,
   "Latitude": 105.8532494
 },
 {
   "STT": 127,
   "Name": "Trạm y tế phường Lê Đại Hành (TTYT HBT)",
   "address": "phường Lê Đại Hành, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0123071,
   "Latitude": 105.8451758
 },
 {
   "STT": 128,
   "Name": "Trạm y tế phường Đồng Nhân (TTYT Hai Bà Trưng)",
   "address": "phường Đồng Nhân, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 20.9967604,
   "Latitude": 105.8432719
 },
 {
   "STT": 129,
   "Name": "Trạm y tế phường Phố Huế (TTYT  Hai Bà Trưng)",
   "address": "Phố Huế- Hai bà Trưng-Hà Nội , Hà Nội",
   "Longtitude": 21.0115168,
   "Latitude": 105.8533481
 },
 {
   "STT": 130,
   "Name": "Trạm y tế phường Đống Mác (TTYT Hai Bà Trưng)",
   "address": "phường Đống Mác, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0112392,
   "Latitude": 105.8602223
 },
 {
   "STT": 131,
   "Name": "Trạm y tế phường Thanh Lương (TTYT HBT)",
   "address": "phường Thanh Lương, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0068975,
   "Latitude": 105.8715997
 },
 {
   "STT": 132,
   "Name": "Trạm y tế phường Thanh Nhàn (TTYT HBT)",
   "address": "phường Thanh Nhàn, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0053964,
   "Latitude": 105.8569193
 },
 {
   "STT": 133,
   "Name": "Trạm y tế phường Cầu Dền (TTYT Hai Bà Trưng)",
   "address": "phường Cầu Dền, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0067485,
   "Latitude": 105.8503135
 },
 {
   "STT": 134,
   "Name": "Trạm y tế phường Bách Khoa (TTYT Hai Bà Trưng)",
   "address": "phường Bách Khoa, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0042694,
   "Latitude": 105.8459098
 },
 {
   "STT": 135,
   "Name": "Trạm y tế phường Đồng Tâm (TTYT Hai Bà Trưng)",
   "address": "phường Đồng Tâm, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 20.9967604,
   "Latitude": 105.8432719
 },
 {
   "STT": 136,
   "Name": "Trạm y tế phường Vĩnh Tuy (TTYT Hai Bà Trưng)",
   "address": "phường Vĩnh Tuy, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 20.9984853,
   "Latitude": 105.8686635
 },
 {
   "STT": 137,
   "Name": "Trạm y tế phường Bạch Mai (TTYT Hai Bà Trưng)",
   "address": "phường Bạch Mai, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0008143,
   "Latitude": 105.8517814
 },
 {
   "STT": 138,
   "Name": "Trạm y tế phường Quỳnh Mai (TTYT Hai Bà Trưng)",
   "address": "Quỳnh Mai- Hai Bà Trưng-Hà Nội , Hà Nội",
   "Longtitude": 20.9999773,
   "Latitude": 105.8605893
 },
 {
   "STT": 139,
   "Name": "Trạm y tế phường Quỳnh Lôi (TTYT Hai Bà Trưng)",
   "address": "phường Quỳnh Lôi, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 21.0003958,
   "Latitude": 105.8561853
 },
 {
   "STT": 140,
   "Name": "Trạm y tế phường Minh Khai (TTYT Hai Bà Trưng)",
   "address": "phường Minh Khai,, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 20.9959104,
   "Latitude": 105.8576533
 },
 {
   "STT": 141,
   "Name": "Trạm y tế phường Trương Định  (TTYT Hai Bà Trưng)",
   "address": "phường Trương Định, quận Hai Bà Trưng , Hà Nội",
   "Longtitude": 20.9937815,
   "Latitude": 105.8466875
 },
 {
   "STT": 142,
   "Name": "Trạm y tế phường Thanh Trì, Hoàng Mai",
   "address": "phường Thanh Trì, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.994941,
   "Latitude": 105.8906859
 },
 {
   "STT": 143,
   "Name": "Trạm y tế phường Vĩnh Hưng, Hoàng Mai",
   "address": "phường Vĩnh Hưng, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9892354,
   "Latitude": 105.8745359
 },
 {
   "STT": 144,
   "Name": "Trạm y tế phường Định Công,  Hoàng Mai",
   "address": "phường Định Công, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9831371,
   "Latitude": 105.8319653
 },
 {
   "STT": 145,
   "Name": "Trạm y tế phường Mai Động, Hoàng Mai",
   "address": "phường Mai Động, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9908669,
   "Latitude": 105.8649934
 },
 {
   "STT": 146,
   "Name": "Trạm y tế phường Tương Mai, Hoàng Mai",
   "address": "phường Tương Mai, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9885702,
   "Latitude": 105.8510475
 },
 {
   "STT": 147,
   "Name": "Trạm y tế phường Đại Kim, Hoàng Mai",
   "address": "phường Đại Kim, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9739692,
   "Latitude": 105.8216911
 },
 {
   "STT": 148,
   "Name": "Trạm y tế phường Tân Mai, Hoàng Mai",
   "address": "phường Tân Mai, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.983687,
   "Latitude": 105.848156
 },
 {
   "STT": 149,
   "Name": "Trạm y tế P.Hoàng Văn Thụ, Hoàng Mai",
   "address": "phường Hoàng Văn Thụ, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9848364,
   "Latitude": 105.8598553
 },
 {
   "STT": 150,
   "Name": "Trạm y tế phường Giáp Bát, Hoàng Mai",
   "address": "phường Giáp Bát, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9836114,
   "Latitude": 105.8422401
 },
 {
   "STT": 151,
   "Name": "Trạm y tế phường Lĩnh Nam, Hoàng Mai",
   "address": "phường Lĩnh Nam, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9770005,
   "Latitude": 105.896559
 },
 {
   "STT": 152,
   "Name": "Trạm y tế phường Thịnh Liệt, Hoàng Mai",
   "address": "phường Thịnh Liệt, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9751142,
   "Latitude": 105.8554513
 },
 {
   "STT": 153,
   "Name": "Trạm y tế phường Trần Phú, Hoàng Mai",
   "address": "phường Trần Phú, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9723243,
   "Latitude": 105.884813
 },
 {
   "STT": 154,
   "Name": "Trạm y tế phường Hoàng Liệt, Hoàng Mai",
   "address": "phường Hoàng Liệt, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9625316,
   "Latitude": 105.8399127
 },
 {
   "STT": 155,
   "Name": "Trạm y tế phường Yên Sở, Hoàng Mai",
   "address": "phường Yên Sở, Quận Hoàng Mai , Hà Nội",
   "Longtitude": 20.9618527,
   "Latitude": 105.8730678
 },
 {
   "STT": 156,
   "Name": "Trạm y tế thị trấn Đông Anh, Đông Anh",
   "address": "Thị Trấn Đông Anh, Đông Anh , Hà Nội",
   "Longtitude": 21.1611142,
   "Latitude": 105.8495796
 },
 {
   "STT": 157,
   "Name": "Trạm y tế xã Xuân Nộn, Đông Anh",
   "address": "Xã Xuân Nộn, Đông Anh , Hà Nội",
   "Longtitude": 21.1852265,
   "Latitude": 105.8701316
 },
 {
   "STT": 158,
   "Name": "Trạm y tế xã Thụy Lâm, Đông Anh",
   "address": "Xã Thụy Lâm, Đông Anh , Hà Nội",
   "Longtitude": 21.1713853,
   "Latitude": 105.8936224
 },
 {
   "STT": 159,
   "Name": "Trạm y tế xã Bắc Hồng, Đông Anh",
   "address": "Xã Bắc Hồng, Đông Anh , Hà Nội",
   "Longtitude": 21.1766298,
   "Latitude": 105.8084823
 },
 {
   "STT": 160,
   "Name": "Trạm y tế xã Nguyên Khê, Đông Anh",
   "address": "Xã Nguyên Khê, Đông Anh , Hà Nội",
   "Longtitude": 21.1796234,
   "Latitude": 105.8378366
 },
 {
   "STT": 161,
   "Name": "Trạm y tế xã Nam Hồng, Đông Anh",
   "address": "Xã Nam Hồng, Đông Anh , Hà Nội",
   "Longtitude": 21.1582993,
   "Latitude": 105.7879371
 },
 {
   "STT": 162,
   "Name": "Trạm y tế xã Tiên Dương, Đông Anh",
   "address": "Xã Tiên Dương, Đông Anh , Hà Nội",
   "Longtitude": 21.1538228,
   "Latitude": 105.8349009
 },
 {
   "STT": 163,
   "Name": "Trạm y tế xã Vân Hà, Đông Anh",
   "address": "Xã Vân Hà, Đông Anh , Hà Nội",
   "Longtitude": 21.1491333,
   "Latitude": 105.9141793
 },
 {
   "STT": 164,
   "Name": "Trạm y tế xã Uy Nỗ, Đông Anh",
   "address": "Xã Uy Nỗ, Đông Anh , Hà Nội",
   "Longtitude": 21.1431669,
   "Latitude": 105.8554513
 },
 {
   "STT": 165,
   "Name": "Trạm y tế xã Vân Nội, Đông Anh",
   "address": "Xã Vân Nội, Đông Anh , Hà Nội",
   "Longtitude": 21.1470888,
   "Latitude": 105.8143528
 },
 {
   "STT": 166,
   "Name": "Trạm y tế xã Liên Hà, Đông Anh",
   "address": "Xã Liên Hà, Đông Anh , Hà Nội",
   "Longtitude": 21.1482071,
   "Latitude": 105.8936224
 },
 {
   "STT": 167,
   "Name": "Trạm y tế xã Việt Hùng, Đông Anh",
   "address": "Xã Việt Hùng, Đông Anh , Hà Nội",
   "Longtitude": 21.140921,
   "Latitude": 105.8789403
 },
 {
   "STT": 168,
   "Name": "Trạm y tế xã Kim Nỗ, Đông Anh",
   "address": "Xã Kim Nỗ, Đông Anh , Hà Nội",
   "Longtitude": 21.1371732,
   "Latitude": 105.7967419
 },
 {
   "STT": 169,
   "Name": "Trạm y tế xã Kim Chung, Đông Anh",
   "address": "Xã Kim Chung, Đông Anh , Hà Nội",
   "Longtitude": 21.133051,
   "Latitude": 105.7791327
 },
 {
   "STT": 170,
   "Name": "Trạm y tế xã Dục Tú, Đông Anh",
   "address": "Xã Dục Tú, Đông Anh , Hà Nội",
   "Longtitude": 21.1134405,
   "Latitude": 105.8936224
 },
 {
   "STT": 171,
   "Name": "Trạm y tế xã Đại Mạch, Đông Anh",
   "address": "Xã Đại Mạch, Đông Anh , Hà Nội",
   "Longtitude": 21.1183715,
   "Latitude": 105.7606997
 },
 {
   "STT": 172,
   "Name": "Trạm y tế xã Vĩnh Ngọc, Đông Anh",
   "address": "Xã Vĩnh Ngọc, Đông Anh , Hà Nội",
   "Longtitude": 21.1196638,
   "Latitude": 105.8153566
 },
 {
   "STT": 173,
   "Name": "Trạm y tế xã Cổ Loa, Đông Anh",
   "address": "Xã Cổ Loa, Đông Anh , Hà Nội",
   "Longtitude": 21.1156865,
   "Latitude": 105.8701316
 },
 {
   "STT": 174,
   "Name": "Trạm y tế xã Hải Bối, Đông Anh",
   "address": "Xã Hải Bối, Đông Anh , Hà Nội",
   "Longtitude": 21.1108103,
   "Latitude": 105.7996769
 },
 {
   "STT": 175,
   "Name": "Trạm y tế xã Xuân Canh, Đông Anh",
   "address": "Xã Xuân Canh, Đông Anh , Hà Nội",
   "Longtitude": 21.0829044,
   "Latitude": 105.8475265
 },
 {
   "STT": 176,
   "Name": "Trạm y tế xã Võng La, Đông Anh",
   "address": "Xã Võng La, Đông Anh , Hà Nội",
   "Longtitude": 21.1106142,
   "Latitude": 105.7782478
 },
 {
   "STT": 177,
   "Name": "Trạm y tế xã Tầm Xá, Đông Anh",
   "address": "Xã Tàm Xá, Đông Anh , Hà Nội",
   "Longtitude": 21.0958665,
   "Latitude": 105.8349009
 },
 {
   "STT": 178,
   "Name": "Trạm y tế xã Mai Lâm, Đông Anh",
   "address": "Xã Mai Lâm, Đông Anh , Hà Nội",
   "Longtitude": 21.0876468,
   "Latitude": 105.8906859
 },
 {
   "STT": 179,
   "Name": "Trạm y tế xã Đông Hội, Đông Anh",
   "address": "Xã Đông Hội, Đông Anh , Hà Nội",
   "Longtitude": 21.0809178,
   "Latitude": 105.8701316
 },
 {
   "STT": 180,
   "Name": "Trạm y tế Thị trấn Yên Viên (TTYT Gia Lâm)",
   "address": "Thị trấn Yên Viên , Hà Nội",
   "Longtitude": 21.0844649,
   "Latitude": 105.9163819
 },
 {
   "STT": 181,
   "Name": "Trạm y tế xã Yên Thường (TTYT Huyện Gia Lâm)",
   "address": "Xã Yên Thường, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0976543,
   "Latitude": 105.9317349
 },
 {
   "STT": 182,
   "Name": "Trạm y tế Xã Yên Viên (TTYT Gia Lâm)",
   "address": "Xã Yên Viên , Hà Nội",
   "Longtitude": 21.079044,
   "Latitude": 105.9200531
 },
 {
   "STT": 183,
   "Name": "Trạm y tế xã Ninh Hiệp (TTYT Huyện Gia Lâm)",
   "address": "Xã Ninh Hiệp, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0762299,
   "Latitude": 105.9494248
 },
 {
   "STT": 184,
   "Name": "Trạm y tế Xã Đình Xuyên (TTYT Gia Lâm)",
   "address": "Xã Đình Xuyên , Hà Nội",
   "Longtitude": 21.0837124,
   "Latitude": 105.9318012
 },
 {
   "STT": 185,
   "Name": "Trạm y tế Xã Dương Hà (TTYT Gia Lâm)",
   "address": "Xã Dương Hà , Hà Nội",
   "Longtitude": 21.0647433,
   "Latitude": 105.9332698
 },
 {
   "STT": 186,
   "Name": "Trạm y tế Xã Phù Đổng (TTYT Gia Lâm)",
   "address": "Xã Phù Đổng , Hà Nội",
   "Longtitude": 21.060339,
   "Latitude": 105.9641124
 },
 {
   "STT": 187,
   "Name": "Trạm y tế xã Trung Mầu (TTYT Huyện Gia Lâm)",
   "address": "Xã Trung Mầu, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0606965,
   "Latitude": 105.9905529
 },
 {
   "STT": 188,
   "Name": "Trạm y tế xã Lệ Chi",
   "address": "Xã Lệ Chi, huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.047983,
   "Latitude": 106.0023054
 },
 {
   "STT": 189,
   "Name": "Trạm y tế xã Cổ Bi",
   "address": "Xã Cổ Bi, huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0303618,
   "Latitude": 105.9433771
 },
 {
   "STT": 190,
   "Name": "Trạm y tế Xã Đặng Xá (TTYT Gia Lâm)",
   "address": "Xã Đặng Xá , Hà Nội",
   "Longtitude": 21.0287612,
   "Latitude": 105.9611748
 },
 {
   "STT": 191,
   "Name": "Trạm y tế xã Phú Thị (TTYT Huyện Gia Lâm)",
   "address": "Xã Phú Thị, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0276351,
   "Latitude": 105.9729255
 },
 {
   "STT": 192,
   "Name": "Trạm y tế xã Kim Sơn (TTYT Huyện Gia Lâm)",
   "address": "Xã Kim Sơn, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0259443,
   "Latitude": 105.9905529
 },
 {
   "STT": 193,
   "Name": "Trạm y tế Trị trấn Trâu Quỳ (TTYT Gia Lâm)",
   "address": "Trị trấn Trâu Quỳ , Hà Nội",
   "Longtitude": 21.0078386,
   "Latitude": 105.9376756
 },
 {
   "STT": 194,
   "Name": "Trạm y tế Xã Dương Quang (TTYT Gia Lâm)",
   "address": "Xã Dương Quang , Hà Nội",
   "Longtitude": 21.0085686,
   "Latitude": 105.9905529
 },
 {
   "STT": 195,
   "Name": "Trạm y tế xã Dương Xá (TTYT Huyện Gia Lâm)",
   "address": "Xã Dương Xá, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 21.0024138,
   "Latitude": 105.9641124
 },
 {
   "STT": 196,
   "Name": "Trạm y tế xã Đông Dư (TTYT Huyện Gia Lâm)",
   "address": "Xã Đông Dư, Huyện Gia Lâm , Hà Nội",
   "Longtitude": 20.9927022,
   "Latitude": 105.9141793
 },
 {
   "STT": 197,
   "Name": "Trạm y tế Xã Đa Tốn (TTYT Gia Lâm)",
   "address": "Xã Đa Tốn , Hà Nội",
   "Longtitude": 20.9852277,
   "Latitude": 105.9318012
 },
 {
   "STT": 198,
   "Name": "Trạm y tế Xã Kiêu Kỵ (TTYT Gia Lâm)",
   "address": "Xã Kiêu Kỵ , Hà Nội",
   "Longtitude": 20.9803677,
   "Latitude": 105.9523622
 },
 {
   "STT": 199,
   "Name": "Trạm y tế Xã Bát Tràng (TTYT Gia Lâm)",
   "address": "Xã Bát Tràng , Hà Nội",
   "Longtitude": 20.9773791,
   "Latitude": 105.9136226
 },
 {
   "STT": 200,
   "Name": "Trạm y tế Xã Kim Lan (TTYT Gia Lâm)",
   "address": "Xã Kim Lan , Hà Nội",
   "Longtitude": 20.9603693,
   "Latitude": 105.9039005
 },
 {
   "STT": 201,
   "Name": "Trạm y tế xã Văn Đức",
   "address": "Xã Văn Đức, huyện Gia Lâm , Hà Nội",
   "Longtitude": 20.9396212,
   "Latitude": 105.8936224
 },
 {
   "STT": 202,
   "Name": "Trạm y tế phường Cầu Diễn",
   "address": "phường Cầu Diễn, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 21.0308291,
   "Latitude": 105.7639583
 },
 {
   "STT": 203,
   "Name": "Trạm y tế phường Thượng Cát",
   "address": "phườngthượng Cát, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0963665,
   "Latitude": 105.7380511
 },
 {
   "STT": 204,
   "Name": "Trạm y tế phường Liên Mạc",
   "address": "phường Liên Mạc, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0831053,
   "Latitude": 105.7556564
 },
 {
   "STT": 205,
   "Name": "Trạm y tế phường Đông Ngạc",
   "address": "phường Đông Ngạc, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0919168,
   "Latitude": 105.7850022
 },
 {
   "STT": 206,
   "Name": "Trạm y tế phường Thụy Phương",
   "address": "phường Thuỵ Phương, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0833035,
   "Latitude": 105.7688614
 },
 {
   "STT": 207,
   "Name": "Trạm y tế phường Tây Tựu",
   "address": "phường Tây Tựu, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0679338,
   "Latitude": 105.7321831
 },
 {
   "STT": 208,
   "Name": "Trạm y tế phường Xuân Đỉnh",
   "address": "phường Xuân Đỉnh, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0739704,
   "Latitude": 105.790872
 },
 {
   "STT": 209,
   "Name": "Trạm y tế phường Minh Khai",
   "address": "phường Minh Khai, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0604722,
   "Latitude": 105.7497878
 },
 {
   "STT": 210,
   "Name": "Trạm y tế phường Cổ Nhuế 1",
   "address": "phường Cổ Nhuế, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.0660875,
   "Latitude": 105.7639698
 },
 {
   "STT": 211,
   "Name": "Trạm y tế phường Phú Diễn",
   "address": "phường Phú Diễn, Quận Bắc Từ Liêm , Hà Nội",
   "Longtitude": 21.047768,
   "Latitude": 105.7615252
 },
 {
   "STT": 212,
   "Name": "Trạm y tế phường Xuân Phương",
   "address": "phường Xuân Phương, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 21.0378383,
   "Latitude": 105.7439194
 },
 {
   "STT": 213,
   "Name": "Trạm y tế phường Mỹ Đình 1",
   "address": "phường Mỹ Đình, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 21.0308291,
   "Latitude": 105.7639583
 },
 {
   "STT": 214,
   "Name": "Trạm y tế phường Tây Mỗ",
   "address": "phường Tây Mỗ, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 21.0088528,
   "Latitude": 105.7439194
 },
 {
   "STT": 215,
   "Name": "Trạm y tế phường Mễ Trì",
   "address": "phường Mễ Trì, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 21.0055267,
   "Latitude": 105.7791327
 },
 {
   "STT": 216,
   "Name": "Trạm y tế phường Đại Mỗ",
   "address": "phường Đại Mỗ, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 20.9898009,
   "Latitude": 105.7615252
 },
 {
   "STT": 217,
   "Name": "Trạm y tế phường Trung Văn",
   "address": "phường Trung Văn, Quận Nam Từ Liêm , Hà Nội",
   "Longtitude": 20.989171,
   "Latitude": 105.7835348
 },
 {
   "STT": 218,
   "Name": "Trạm y tế thị trấn Văn Điển (TTYT H.Thanh Trì)",
   "address": "Thị Trấn Văn Điển, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9493584,
   "Latitude": 105.8444419
 },
 {
   "STT": 219,
   "Name": "Trạm y tế xã Tân Triều (TTYT Huyện Thanh Trì)",
   "address": "Xã Tân Triều, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9712048,
   "Latitude": 105.8010533
 },
 {
   "STT": 220,
   "Name": "Trạm y tế xã Thanh Liệt (TTYT Huyện Thanh Trì)",
   "address": "Xã Thanh Liệt, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9674199,
   "Latitude": 105.8143528
 },
 {
   "STT": 221,
   "Name": "Trạm y tế xã Tả Thanh Oai (TTYT H.Thanh Trì)",
   "address": "Xã Tả Thanh Oai, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.933203,
   "Latitude": 105.8084823
 },
 {
   "STT": 222,
   "Name": "Trạm y tế xã Hữu Hoà (TTYT Huyện Thanh Trì)",
   "address": "Xã Hữu Hoà, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.945903,
   "Latitude": 105.7967419
 },
 {
   "STT": 223,
   "Name": "Trạm y tế xã Tam Hiệp (TTYT Huyện Thanh Trì)",
   "address": "Xã Tam Hiệp, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.948923,
   "Latitude": 105.8260943
 },
 {
   "STT": 224,
   "Name": "Trạm y tế xã Tứ Hiệp (TTYT Huyện Thanh Trì)",
   "address": "Xã Tứ Hiệp, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9461418,
   "Latitude": 105.8554513
 },
 {
   "STT": 225,
   "Name": "Trạm y tế xã Yên Mỹ (TTYT Huyện Thanh Trì)",
   "address": "Xã Yên Mỹ, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9444705,
   "Latitude": 105.8730678
 },
 {
   "STT": 226,
   "Name": "Trạm y tế xã Vĩnh Quỳnh (TTYT Huyện Thanh Trì)",
   "address": "Xã Vĩnh Quỳnh, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9315381,
   "Latitude": 105.8260943
 },
 {
   "STT": 227,
   "Name": "Trạm y tế xã Ngũ Hiệp (TTYT Huyện Thanh Trì)",
   "address": "Xã Ngũ Hiệp, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9282023,
   "Latitude": 105.8613233
 },
 {
   "STT": 228,
   "Name": "Trạm y tế xã Duyên Hà (TTYT Huyện Thanh Trì)",
   "address": "Xã Duyên Hà, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9259739,
   "Latitude": 105.884813
 },
 {
   "STT": 229,
   "Name": "Trạm y tế xã Ngọc Hồi (TTYT Huyện Thanh Trì)",
   "address": "Xã Ngọc Hồi, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9182821,
   "Latitude": 105.843708
 },
 {
   "STT": 230,
   "Name": "Trạm y tế xã Vạn Phúc (TTYT Huyện Thanh Trì)",
   "address": "Xã Vạn Phúc, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.918507,
   "Latitude": 105.9024322
 },
 {
   "STT": 231,
   "Name": "Trạm y tế xã Đại áng (TTYT Huyện Thanh Trì)",
   "address": "Xã Đại áng, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9115334,
   "Latitude": 105.8231588
 },
 {
   "STT": 232,
   "Name": "Trạm y tế xã Liên Ninh (TTYT Huyện Thanh Trì)",
   "address": "Xã Liên Ninh, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9061376,
   "Latitude": 105.8495796
 },
 {
   "STT": 233,
   "Name": "Trạm y tế xã Đông Mỹ (TTYT Huyện Thanh Trì)",
   "address": "Xã Đông Mỹ, Huyện Thanh Trì , Hà Nội",
   "Longtitude": 20.9221912,
   "Latitude": 105.8725107
 },
 {
   "STT": 234,
   "Name": "Trạm y tế phường Nguyễn Trãi, Hà Đông",
   "address": "phường Nguyễn Trãi, Hà Đông , Hà Nội",
   "Longtitude": 20.9699557,
   "Latitude": 105.7798664
 },
 {
   "STT": 235,
   "Name": "Trạm y tế phường Văn Mỗ, Hà Đông",
   "address": "Văn Mỗ- Hà Đông-Hà Nội , Hà Nội",
   "Longtitude": 20.9571298,
   "Latitude": 105.7314496
 },
 {
   "STT": 236,
   "Name": "Trạm y tế phường Vạn Phúc, Hà Đông",
   "address": "phường Vạn Phúc, Hà Đông , Hà Nội",
   "Longtitude": 20.9786876,
   "Latitude": 105.771796
 },
 {
   "STT": 237,
   "Name": "Trạm y tế phường Yết Kiêu, Hà Đông",
   "address": "phường Yết Kiêu, Hà Đông , Hà Nội",
   "Longtitude": 20.97458,
   "Latitude": 105.7769317
 },
 {
   "STT": 238,
   "Name": "Trạm y tế phường Quang Trung, Hà Đông",
   "address": "phường Quang Trung, Hà Đông , Hà Nội",
   "Longtitude": 20.9652675,
   "Latitude": 105.7681278
 },
 {
   "STT": 239,
   "Name": "Trạm y tế phường Phúc La, Hà Đông",
   "address": "phường Phúc La, Hà Đông , Hà Nội",
   "Longtitude": 20.9654323,
   "Latitude": 105.7894045
 },
 {
   "STT": 240,
   "Name": "Trạm y tế phường Hà Cầu, Quận Hà Đông",
   "address": "phường Hà Cầu, Hà Đông , Hà Nội",
   "Longtitude": 20.9636429,
   "Latitude": 105.7776653
 },
 {
   "STT": 241,
   "Name": "Trạm y tế xã Văn Khê, Hà Đông",
   "address": "Văn Khê-Hà Đông-Hà Nội , Hà Nội",
   "Longtitude": 20.9742616,
   "Latitude": 105.76332
 },
 {
   "STT": 242,
   "Name": "Trạm y tế xã Yên Nghĩa, Quận Hà Đông",
   "address": "Xã Yên Nghĩa, Hà Đông , Hà Nội",
   "Longtitude": 20.9540586,
   "Latitude": 105.7409852
 },
 {
   "STT": 243,
   "Name": "Trạm y tế xã Kiến Hưng, Quận Hà Đông",
   "address": "Xã Kiến Hưng, Hà Đông , Hà Nội",
   "Longtitude": 20.9488815,
   "Latitude": 105.7855703
 },
 {
   "STT": 244,
   "Name": "Trạm y tế xã Phú Lãm, Quận Hà Đông",
   "address": "Xã Phú Lãm, Hà Đông , Hà Nội",
   "Longtitude": 20.9453498,
   "Latitude": 105.7564014
 },
 {
   "STT": 245,
   "Name": "Trạm y tế xã Phú Lương, Quận Hà Đông",
   "address": "Xã Phú Lương, Hà Đông , Hà Nội",
   "Longtitude": 20.973473,
   "Latitude": 105.7781964
 },
 {
   "STT": 246,
   "Name": "Trạm y tế xã Dương Nội, Quận Hà Đông",
   "address": "Xã Dương Nội, Hà Đông , Hà Nội",
   "Longtitude": 20.979868,
   "Latitude": 105.7439194
 },
 {
   "STT": 247,
   "Name": "Trạm y tế xã Đồng Mai, Quận Hà Đông",
   "address": "Xã Đồng Mai, Hà Đông , Hà Nội",
   "Longtitude": 20.9308715,
   "Latitude": 105.7409852
 },
 {
   "STT": 248,
   "Name": "Trạm y tế xã Biên Giang, Quận Hà Đông",
   "address": "Xã Biên Giang, Hà Đông , Hà Nội",
   "Longtitude": 20.9312147,
   "Latitude": 105.7219145
 },
 {
   "STT": 249,
   "Name": "Trạm y tế phường Văn Quán, Quận Hà Đông",
   "address": "phường Văn Quán, Hà Đông , Hà Nội",
   "Longtitude": 20.9796446,
   "Latitude": 105.7923394
 },
 {
   "STT": 250,
   "Name": "Trạm y tế phường Mộ Lao, Quận Hà Đông",
   "address": "phường Mộ Lao, Hà Đông , Hà Nội",
   "Longtitude": 20.9833749,
   "Latitude": 105.7835348
 },
 {
   "STT": 251,
   "Name": "Trạm y tế phường Phú La, Quận Hà Đông",
   "address": "phường Phú La, Hà Đông , Hà Nội",
   "Longtitude": 20.9560561,
   "Latitude": 105.7659269
 },
 {
   "STT": 252,
   "Name": "Trạm y tế phường La Khê, Quận Hà Đông",
   "address": "phường La Khê, Hà Đông , Hà Nội",
   "Longtitude": 20.9724114,
   "Latitude": 105.7615252
 },
 {
   "STT": 253,
   "Name": "Trạm y tế phường Lê Lợi (TX Sơn Tây)",
   "address": "phường Lê Lợi, Sơn Tây , Hà Nội",
   "Longtitude": 21.1450319,
   "Latitude": 105.5056761
 },
 {
   "STT": 254,
   "Name": "Trạm y tế phường Phú Thịnh (TX Sơn Tây)",
   "address": "phường Phú Thịnh, Sơn Tây , Hà Nội",
   "Longtitude": 21.1537625,
   "Latitude": 105.4976181
 },
 {
   "STT": 255,
   "Name": "Trạm y tế phường Ngô Quyền (TX Sơn Tây)",
   "address": "phường Ngô Quyền, Sơn Tây , Hà Nội",
   "Longtitude": 21.1425408,
   "Latitude": 105.5012808
 },
 {
   "STT": 256,
   "Name": "Trạm y tế phường Quang Trung (TX Sơn Tây)",
   "address": "phường Quang Trung, Sơn Tây , Hà Nội",
   "Longtitude": 21.1347408,
   "Latitude": 105.5071413
 },
 {
   "STT": 257,
   "Name": "Trạm y tế phường Sơn Lộc (TX Sơn Tây)",
   "address": "phường Sơn Lộc, Sơn Tây , Hà Nội",
   "Longtitude": 21.1147313,
   "Latitude": 105.4961531
 },
 {
   "STT": 258,
   "Name": "Trạm y tế phường Xuân Khanh (TX Sơn Tây)",
   "address": "phường Xuân Khanh, Sơn Tây , Hà Nội",
   "Longtitude": 21.114733,
   "Latitude": 105.4393571
 },
 {
   "STT": 259,
   "Name": "Trạm y tế xã Đường Lâm (TX Sơn Tây)",
   "address": "Xã Đường Lâm, Sơn Tây , Hà Nội",
   "Longtitude": 21.1533196,
   "Latitude": 105.471249
 },
 {
   "STT": 260,
   "Name": "Trạm y tế xã Viên Sơn (TX Sơn Tây)",
   "address": "Xã Viên Sơn, Sơn Tây , Hà Nội",
   "Longtitude": 21.131937,
   "Latitude": 105.5124527
 },
 {
   "STT": 261,
   "Name": "Trạm y tế xã Xuân Sơn (TX Sơn Tây)",
   "address": "Xã Xuân Sơn, Sơn Tây , Hà Nội",
   "Longtitude": 21.1333747,
   "Latitude": 105.4360963
 },
 {
   "STT": 262,
   "Name": "Trạm y tế xã Trung Hưng (TX Sơn Tây)",
   "address": "Xã Trung Hưng, Sơn Tây , Hà Nội",
   "Longtitude": 21.1384668,
   "Latitude": 105.485374
 },
 {
   "STT": 263,
   "Name": "Trạm y tế xã Thanh Mỹ (TX Sơn Tây)",
   "address": "Xã Thanh Mỹ, Sơn Tây , Hà Nội",
   "Longtitude": 21.1301068,
   "Latitude": 105.471249
 },
 {
   "STT": 264,
   "Name": "Trạm y tế xãTrung Sơn Trầm (TX Sơn Tây)",
   "address": "Xã Trung Sơn Trầm, Sơn Tây , Hà Nội",
   "Longtitude": 21.1073416,
   "Latitude": 105.4976181
 },
 {
   "STT": 265,
   "Name": "Trạm y tế xã Kim Sơn (TX Sơn Tây)",
   "address": "Xã Kim Sơn, Sơn Tây , Hà Nội",
   "Longtitude": 21.0847705,
   "Latitude": 105.4595306
 },
 {
   "STT": 266,
   "Name": "Trạm y tế xã Sơn Đông (TX Sơn Tây)",
   "address": "Xã Sơn Đông, Sơn Tây , Hà Nội",
   "Longtitude": 21.0709878,
   "Latitude": 105.4829681
 },
 {
   "STT": 267,
   "Name": "Trạm y tế xã Cổ Đông (TX Sơn Tây)",
   "address": "Xã Cổ Đông, Sơn Tây , Hà Nội",
   "Longtitude": 21.0455992,
   "Latitude": 105.5064087
 },
 {
   "STT": 268,
   "Name": "Trạm y tế thị trấn Tây Đằng (TTYT H. Ba Vì)",
   "address": "Thị Trấn Tây Đằng, Ba Vì , Hà Nội",
   "Longtitude": 21.2067429,
   "Latitude": 105.4273093
 },
 {
   "STT": 269,
   "Name": "Trạm y tế xã Tân Đức (TTYT H. Ba Vì)",
   "address": "Xã Tân Đức, Ba Vì , Hà Nội",
   "Longtitude": 21.2998132,
   "Latitude": 105.3628846
 },
 {
   "STT": 270,
   "Name": "Trạm y tế xã Phú Cường (TTYT H. Ba Vì)",
   "address": "Xã Phú Cường, Ba Vì , Hà Nội",
   "Longtitude": 21.2875615,
   "Latitude": 105.4009508
 },
 {
   "STT": 271,
   "Name": "Trạm y tế xã Cổ Đô (TTYT H. Ba Vì)",
   "address": "Xã Cổ Đô, Ba Vì , Hà Nội",
   "Longtitude": 21.2813067,
   "Latitude": 105.3745964
 },
 {
   "STT": 272,
   "Name": "Trạm y tế xã Tản Hồng (TTYT H. Ba Vì)",
   "address": "Xã Tản Hồng, Ba Vì , Hà Nội",
   "Longtitude": 21.2737673,
   "Latitude": 105.4243804
 },
 {
   "STT": 273,
   "Name": "Trạm y tế xã Vạn Thắng (TTYT H. Ba Vì)",
   "address": "Xã Vạn Thắng, Ba Vì , Hà Nội",
   "Longtitude": 21.2484348,
   "Latitude": 105.3955142
 },
 {
   "STT": 274,
   "Name": "Trạm y tế xã Châu Sơn (TTYT H. Ba Vì)",
   "address": "Xã Châu Sơn, Ba Vì , Hà Nội",
   "Longtitude": 21.2636941,
   "Latitude": 105.4390254
 },
 {
   "STT": 275,
   "Name": "Trạm y tế xã Phong Vân (TTYT H. Ba Vì)",
   "address": "Xã Phong Vân, Ba Vì , Hà Nội",
   "Longtitude": 21.2568737,
   "Latitude": 105.3637928
 },
 {
   "STT": 276,
   "Name": "Trạm y tế xã Phú Đông (TTYT H. Ba Vì)",
   "address": "Xã Phú Đông, Ba Vì , Hà Nội",
   "Longtitude": 21.2479196,
   "Latitude": 105.4214515
 },
 {
   "STT": 277,
   "Name": "Trạm y tế xã Phú Phương (TTYT H. Ba Vì)",
   "address": "Xã Phú Phương, Ba Vì , Hà Nội",
   "Longtitude": 21.2479196,
   "Latitude": 105.4214515
 },
 {
   "STT": 278,
   "Name": "Trạm y tế xã Phú Châu (TTYT H. Ba Vì)",
   "address": "Xã Phú Châu, Ba Vì , Hà Nội",
   "Longtitude": 21.2378481,
   "Latitude": 105.4360963
 },
 {
   "STT": 279,
   "Name": "Trạm y tế xã Thái Hòa (TTYT H. Ba Vì)",
   "address": "Xã Thái Hòa, Ba Vì , Hà Nội",
   "Longtitude": 21.2295531,
   "Latitude": 105.371387
 },
 {
   "STT": 280,
   "Name": "Trạm y tế xã Đồng Thái (TTYT huyện Ba Vì)",
   "address": "Xã Đồng Thái - ba Vì - hà Nội , Hà Nội",
   "Longtitude": 21.2259936,
   "Latitude": 105.3968327
 },
 {
   "STT": 281,
   "Name": "Trạm y tế xã Phú Sơn (TTYT H. Ba Vì)",
   "address": "Xã Phú Sơn, Ba Vì , Hà Nội",
   "Longtitude": 21.2235815,
   "Latitude": 105.3790784
 },
 {
   "STT": 282,
   "Name": "Trạm y tế xã Minh Châu (TTYT H. Ba Vì)",
   "address": "Xã Minh Châu, Ba Vì , Hà Nội",
   "Longtitude": 21.2040143,
   "Latitude": 105.4566012
 },
 {
   "STT": 283,
   "Name": "Trạm y tế xã Vật Lại (TTYT huyện Ba Vì)",
   "address": "Xã Vật Lại - ba Vì - hà Nội , Hà Nội",
   "Longtitude": 21.2171301,
   "Latitude": 105.3936297
 },
 {
   "STT": 284,
   "Name": "Trạm y tế xã Chu Minh (TTYT H. Ba Vì)",
   "address": "Xã Chu Minh, Ba Vì , Hà Nội",
   "Longtitude": 21.1987605,
   "Latitude": 105.4381084
 },
 {
   "STT": 285,
   "Name": "Trạm y tế xã Tòng Bạt (TTYT H. Ba Vì)",
   "address": "Xã Tòng Bạt, Ba Vì , Hà Nội",
   "Longtitude": 21.1964493,
   "Latitude": 105.3270223
 },
 {
   "STT": 286,
   "Name": "Trạm y tế xã Cẩm Lĩnh (TTYT H. Ba Vì)",
   "address": "Xã Cẩm Lĩnh, Ba Vì , Hà Nội",
   "Longtitude": 21.1846018,
   "Latitude": 105.3626874
 },
 {
   "STT": 287,
   "Name": "Trạm y tế xã Sơn Đà (TTYT huyện Ba Vì)",
   "address": "Xã Sơn Đà - huyện Ba Vì - hà Nội , Hà Nội",
   "Longtitude": 21.1789702,
   "Latitude": 105.3049783
 },
 {
   "STT": 288,
   "Name": "Trạm y tế xã Đông Quang (TTYT H. Ba Vì)",
   "address": "Xã Đông Quang, Ba Vì , Hà Nội",
   "Longtitude": 21.1813452,
   "Latitude": 105.4507424
 },
 {
   "STT": 289,
   "Name": "Trạm y tế xã Tiên Phong (TTYT H. Ba Vì)",
   "address": "Xã Tiên Phong, Ba Vì , Hà Nội",
   "Longtitude": 21.1719173,
   "Latitude": 105.4273093
 },
 {
   "STT": 290,
   "Name": "Trạm y tế xã Thụy An (TTYT H. Ba Vì)",
   "address": "Xã Thụy An, Ba Vì , Hà Nội",
   "Longtitude": 21.1594785,
   "Latitude": 105.4153668
 },
 {
   "STT": 291,
   "Name": "Trạm y tế xã Cam Thượng (TTYT H. Ba Vì)",
   "address": "Xã Cam Thượng, Ba Vì , Hà Nội",
   "Longtitude": 21.1671748,
   "Latitude": 105.4614541
 },
 {
   "STT": 292,
   "Name": "Trạm y tế xã Thuần Mỹ (TTYT H. Ba Vì)",
   "address": "Xã Thuần Mỹ, Ba Vì , Hà Nội",
   "Longtitude": 21.1527992,
   "Latitude": 105.301984
 },
 {
   "STT": 293,
   "Name": "Trạm y tế xã Tản Lĩnh (TTYT H. Ba Vì)",
   "address": "Xã Tản Lĩnh, Ba Vì , Hà Nội",
   "Longtitude": 21.124523,
   "Latitude": 105.3907014
 },
 {
   "STT": 294,
   "Name": "Trạm y tế xã Ba Trại (TTYT H. Ba Vì)",
   "address": "Xã Ba Trại, Ba Vì , Hà Nội",
   "Longtitude": 21.1256787,
   "Latitude": 105.3467822
 },
 {
   "STT": 295,
   "Name": "Trạm y tế xã Minh Quang (TTYT H. Ba Vì)",
   "address": "Xã Minh Quang, Ba Vì , Hà Nội",
   "Longtitude": 21.0745356,
   "Latitude": 105.3189726
 },
 {
   "STT": 296,
   "Name": "Trạm y tế xã Ba Vì  (TTYT H. Ba Vì)",
   "address": "Xã Ba Vì, Ba Vì , Hà Nội",
   "Longtitude": 21.0713032,
   "Latitude": 105.3541013
 },
 {
   "STT": 297,
   "Name": "Trạm y tế xã Vân Hòa (TTYT H. Ba Vì)",
   "address": "Xã Vân Hòa, Ba Vì , Hà Nội",
   "Longtitude": 21.0669809,
   "Latitude": 105.4009508
 },
 {
   "STT": 298,
   "Name": "Trạm y tế xã Yên Bài (TTYT H. Ba Vì)",
   "address": "Xã Yên Bài, Ba Vì , Hà Nội",
   "Longtitude": 21.0341701,
   "Latitude": 105.4419546
 },
 {
   "STT": 299,
   "Name": "Trạm y tế xã Khánh Thượng (TTYT H. Ba Vì)",
   "address": "Xã Khánh Thượng, Ba Vì , Hà Nội",
   "Longtitude": 21.0259381,
   "Latitude": 105.3423909
 },
 {
   "STT": 300,
   "Name": "Trạm y tế thị trấn Phúc Thọ (TTYT H. Phúc Thọ)",
   "address": "Thị Trấn Phúc Thọ, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1029672,
   "Latitude": 105.5445064
 },
 {
   "STT": 301,
   "Name": "Trạm y tế xã Vân Hà (TTYT H. Phúc Thọ)",
   "address": "Xã Vân Hà, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1596292,
   "Latitude": 105.6207263
 },
 {
   "STT": 302,
   "Name": "Trạm y tế xã Vân Phúc  (TTYT H. Phúc Thọ)",
   "address": "Xã Vân Phúc, Phúc Thọ , Hà Nội",
   "Longtitude": 21.151335,
   "Latitude": 105.5855438
 },
 {
   "STT": 303,
   "Name": "Trạm y tế xã Vân Nam (TTYT H. Phúc Thọ)",
   "address": "Xã Vân Nam, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1491318,
   "Latitude": 105.6089981
 },
 {
   "STT": 304,
   "Name": "Trạm y tế xã Xuân Phú  (TTYT H. Phúc Thọ)",
   "address": "Xã Xuân Phú, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1397333,
   "Latitude": 105.5855438
 },
 {
   "STT": 305,
   "Name": "Trạm y tế xã Phương Độ  (TTYT H. Phúc Thọ)",
   "address": "Xã Phương Độ, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1499289,
   "Latitude": 105.5386447
 },
 {
   "STT": 306,
   "Name": "Trạm y tế xã Sen Chiểu (TTYT H. Phúc Thọ)",
   "address": "Xã Sen Chiểu, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1394212,
   "Latitude": 105.5269218
 },
 {
   "STT": 307,
   "Name": "Trạm y tế xã Cẩm Đình (TTYT H. Phúc Thọ)",
   "address": "Xã Cẩm Đình, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1482825,
   "Latitude": 105.5562304
 },
 {
   "STT": 308,
   "Name": "Trạm y tế xã Võng Xuyên  (TTYT H. Phúc Thọ)",
   "address": "Xã Võng Xuyên, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1329554,
   "Latitude": 105.5650239
 },
 {
   "STT": 309,
   "Name": "Trạm y tế xã Thọ Lộc",
   "address": "Xã Thọ Lộc, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1214676,
   "Latitude": 105.5327831
 },
 {
   "STT": 310,
   "Name": "Trạm y tế xã Long Xuyên (TTYT H. Phúc Thọ)",
   "address": "Xã Long Xuyên, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1170795,
   "Latitude": 105.5796808
 },
 {
   "STT": 311,
   "Name": "Trạm y tế xã Thượng Cốc (TTYT H. Phúc Thọ)",
   "address": "Xã Thượng Cốc, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1270314,
   "Latitude": 105.5972706
 },
 {
   "STT": 312,
   "Name": "Trạm y tế xã Hát Môn  (TTYT H. Phúc Thọ)",
   "address": "Xã Hát Môn, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1253794,
   "Latitude": 105.6148621
 },
 {
   "STT": 313,
   "Name": "Trạm y tế xã Tích Giang (TTYT H. Phúc Thọ)",
   "address": "Xã Tích Giang, Phúc Thọ , Hà Nội",
   "Longtitude": 21.111505,
   "Latitude": 105.5151997
 },
 {
   "STT": 314,
   "Name": "Trạm y tế xã Thanh Đa  (TTYT H. Phúc Thọ)",
   "address": "Xã Thanh Đa, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1132282,
   "Latitude": 105.6207263
 },
 {
   "STT": 315,
   "Name": "Trạm y tế xã Trạch Mỹ Lộc  (TTYT H. Phúc Thọ)",
   "address": "Xã Trạch Mỹ Lộc, Phúc Thọ , Hà Nội",
   "Longtitude": 21.0988074,
   "Latitude": 105.5269218
 },
 {
   "STT": 316,
   "Name": "Trạm y tế xã Phúc Hòa  (TTYT H. Phúc Thọ)",
   "address": "Xã Phúc Hòa, Phúc Thọ , Hà Nội",
   "Longtitude": 21.1071243,
   "Latitude": 105.5620927
 },
 {
   "STT": 317,
   "Name": "Trạm y tế xã Ngọc Tảo (TTYT H. Phúc Thọ)",
   "address": "Xã Ngọc Tảo, Phúc Thọ , Hà Nội",
   "Longtitude": 21.0974791,
   "Latitude": 105.6031342
 },
 {
   "STT": 318,
   "Name": "Trạm y tế xã Phụng Thượng (TTYT H. Phúc Thọ)",
   "address": "Xã Phụng Thượng, Phúc Thọ , Hà Nội",
   "Longtitude": 21.0933272,
   "Latitude": 105.5855438
 },
 {
   "STT": 319,
   "Name": "Trạm y tế xã Tam Thuấn  (TTYT H. Phúc Thọ)",
   "address": "Xã Tam Thuấn, Phúc Thọ , Hà Nội",
   "Longtitude": 21.0952776,
   "Latitude": 105.6265908
 },
 {
   "STT": 320,
   "Name": "Trạm y tế xã Tam Hiệp (TTYT H. Phúc Thọ)",
   "address": "Xã Tam Hiệp, Phúc Thọ , Hà Nội",
   "Longtitude": 21.0789787,
   "Latitude": 105.6148621
 },
 {
   "STT": 321,
   "Name": "Trạm y tế xã Hiệp Thuận (TTYT H. Phúc Thọ)",
   "address": "Xã Hiệp Thuận, Phúc Thọ , Hà Nội",
   "Longtitude": 21.0709773,
   "Latitude": 105.6383202
 },
 {
   "STT": 322,
   "Name": "Trạm y tế xã Liên Hiệp  (TTYT H. Phúc Thọ)",
   "address": "Xã Liên Hiệp, Phúc Thọ , Hà Nội",
   "Longtitude": 21.053579,
   "Latitude": 105.6383202
 },
 {
   "STT": 323,
   "Name": "Trạm y tế thị trấn Phùng (TTYT h. Đan Phượng)",
   "address": "Thị Trấn Phùng, Đan Phượng , Hà Nội",
   "Longtitude": 21.0877573,
   "Latitude": 105.6603149
 },
 {
   "STT": 324,
   "Name": "Trạm y tế xã Trung Châu (TTYT h. Đan Phượng)",
   "address": "Xã Trung Châu, Đan Phượng , Hà Nội",
   "Longtitude": 21.1495488,
   "Latitude": 105.6353878
 },
 {
   "STT": 325,
   "Name": "Trạm y tế xã Thọ An (TTYT h. Đan Phượng)",
   "address": "Xã Thọ An, Đan Phượng , Hà Nội",
   "Longtitude": 21.1342209,
   "Latitude": 105.6441852
 },
 {
   "STT": 326,
   "Name": "Trạm y tế xã Thọ Xuân (TTYT h. Đan Phượng)",
   "address": "Xã Thọ Xuân, Đan Phượng , Hà Nội",
   "Longtitude": 21.1220695,
   "Latitude": 105.6500504
 },
 {
   "STT": 327,
   "Name": "Trạm y tế xã Hồng Hà (TTYT h. Đan Phượng)",
   "address": "Xã Hồng Hà, Đan Phượng , Hà Nội",
   "Longtitude": 21.1335253,
   "Latitude": 105.6823124
 },
 {
   "STT": 328,
   "Name": "Trạm y tế xã Liên Hồng (TTYT h. Đan Phượng)",
   "address": "Xã Liên Hồng, Đan Phượng , Hà Nội",
   "Longtitude": 21.1223332,
   "Latitude": 105.7087128
 },
 {
   "STT": 329,
   "Name": "Trạm y tế xã Liên Hà (TTYT h. Đan Phượng)",
   "address": "Xã Liên Hà, Đan Phượng , Hà Nội",
   "Longtitude": 21.1109055,
   "Latitude": 105.7090807
 },
 {
   "STT": 330,
   "Name": "Trạm y tế xã Hạ Mỗ (TTYT h. Đan Phượng)",
   "address": "Xã Hạ Mỗ, Đan Phượng , Hà Nội",
   "Longtitude": 21.1129527,
   "Latitude": 105.6852455
 },
 {
   "STT": 331,
   "Name": "Trạm y tế xã Liên Trung (TTYT h. Đan Phượng)",
   "address": "Xã Liên Trung, Đan Phượng , Hà Nội",
   "Longtitude": 21.1085171,
   "Latitude": 105.7321831
 },
 {
   "STT": 332,
   "Name": "Trạm y tế xã Phương Đình (TTYT h. Đan Phượng)",
   "address": "Xã Phương Đình, Đan Phượng , Hà Nội",
   "Longtitude": 21.1052231,
   "Latitude": 105.6441852
 },
 {
   "STT": 333,
   "Name": "Trạm y tế xã Thượng Mỗ (TTYT h. Đan Phượng)",
   "address": "Xã Thượng Mỗ, Đan Phượng , Hà Nội",
   "Longtitude": 21.1066725,
   "Latitude": 105.6749796
 },
 {
   "STT": 334,
   "Name": "Trạm y tế xã Tân Hội  (TTYT h. Đan Phượng)",
   "address": "Xã Tân Hội, Đan Phượng , Hà Nội",
   "Longtitude": 21.0938962,
   "Latitude": 105.7028457
 },
 {
   "STT": 335,
   "Name": "Trạm y tế xã Tân Lập  (TTYT h. Đan Phượng)",
   "address": "Xã Tân Lập, Đan Phượng , Hà Nội",
   "Longtitude": 21.0811921,
   "Latitude": 105.7145801
 },
 {
   "STT": 336,
   "Name": "Trạm y tế xã Đan Phượng  (TTYT h. Đan Phượng)",
   "address": "Xã Đan Phượng, Đan Phượng , Hà Nội",
   "Longtitude": 21.0930042,
   "Latitude": 105.6661806
 },
 {
   "STT": 337,
   "Name": "Trạm y tế xã Đồng Tháp  (TTYT h. Đan Phượng)",
   "address": "Xã Đồng Tháp, Đan Phượng , Hà Nội",
   "Longtitude": 21.0814739,
   "Latitude": 105.6500504
 },
 {
   "STT": 338,
   "Name": "Trạm y tế xã Song Phượng  (TTYT h. Đan Phượng)",
   "address": "Xã Song Phượng, Đan Phượng , Hà Nội",
   "Longtitude": 21.0798184,
   "Latitude": 105.6676471
 },
 {
   "STT": 339,
   "Name": "Trạm y tế thị trấn Trạm Trôi (TTYT h. Hoài Đức)",
   "address": "Thị Trấn Trạm Trôi, Hoài Đức , Hà Nội",
   "Longtitude": 21.068562,
   "Latitude": 105.7101796
 },
 {
   "STT": 340,
   "Name": "Trạm y tế xã Đức Thượng (TTYT h. Hoài Đức)",
   "address": "Xã Đức Thượng, Hoài Đức , Hà Nội",
   "Longtitude": 21.0776081,
   "Latitude": 105.6911121
 },
 {
   "STT": 341,
   "Name": "Trạm y tế xã Minh Khai (TTYT h. Hoài Đức)",
   "address": "Xã Minh Khai, Hoài Đức , Hà Nội",
   "Longtitude": 21.066357,
   "Latitude": 105.6720466
 },
 {
   "STT": 342,
   "Name": "Trạm y tế xã Dương Liễu (TTYT h. Hoài Đức)",
   "address": "Xã Dương Liễu, Hoài Đức , Hà Nội",
   "Longtitude": 21.0579347,
   "Latitude": 105.6691136
 },
 {
   "STT": 343,
   "Name": "Trạm y tế xã Di Trạch (TTYT h. Hoài Đức)",
   "address": "Xã Di Trạch, Hoài Đức , Hà Nội",
   "Longtitude": 21.0458514,
   "Latitude": 105.7204476
 },
 {
   "STT": 344,
   "Name": "Trạm y tế xã Đức Giang (TTYT h. Hoài Đức)",
   "address": "Xã Đức Giang, Hoài Đức , Hà Nội",
   "Longtitude": 21.0585539,
   "Latitude": 105.7087128
 },
 {
   "STT": 345,
   "Name": "Trạm y tế xã Cát Quế  (Hoài Đức)",
   "address": "Xã Cát Quế, Hoài Đức , Hà Nội",
   "Longtitude": 21.0502725,
   "Latitude": 105.6735131
 },
 {
   "STT": 346,
   "Name": "Trạm y tế xã Kim Chung  (TTYT h. Hoài Đức)",
   "address": "Xã Kim Chung, Hoài Đức , Hà Nội",
   "Longtitude": 21.046958,
   "Latitude": 105.7087128
 },
 {
   "STT": 347,
   "Name": "Trạm y tế xã Yên Sở  (TTYT h. Hoài Đức)",
   "address": "Xã Yên Sở, Hoài Đức , Hà Nội",
   "Longtitude": 21.0392266,
   "Latitude": 105.6676471
 },
 {
   "STT": 348,
   "Name": "Trạm y tế xã Sơn Đồng (TTYT h. Hoài Đức)",
   "address": "Xã Sơn Đồng, Hoài Đức , Hà Nội",
   "Longtitude": 21.0422656,
   "Latitude": 105.6969788
 },
 {
   "STT": 349,
   "Name": "Trạm y tế xã Vân Canh (TTYT h. Hoài Đức)",
   "address": "Xã Vân Canh, Hoài Đức , Hà Nội",
   "Longtitude": 21.033149,
   "Latitude": 105.7321831
 },
 {
   "STT": 350,
   "Name": "Trạm y tế xã Đắc Sở (TTYT h. Hoài Đức)",
   "address": "Xã Đắc Sở, Hoài Đức , Hà Nội",
   "Longtitude": 21.0281142,
   "Latitude": 105.6779127
 },
 {
   "STT": 351,
   "Name": "Trạm y tế xã Lại Yên  (TTYT h. Hoài Đức)",
   "address": "Xã Lại Yên, Hoài Đức , Hà Nội",
   "Longtitude": 21.0237665,
   "Latitude": 105.7087128
 },
 {
   "STT": 352,
   "Name": "Trạm y tế xã Tiền Yên  (TTYT h. Hoài Đức)",
   "address": "Xã Tiền Yên, Hoài Đức , Hà Nội",
   "Longtitude": 21.0259751,
   "Latitude": 105.6852455
 },
 {
   "STT": 353,
   "Name": "Trạm y tế xã Song Phương  (TTYT h. Hoài Đức)",
   "address": "Xã Song Phương, Hoài Đức , Hà Nội",
   "Longtitude": 21.0138269,
   "Latitude": 105.6911121
 },
 {
   "STT": 354,
   "Name": "Trạm y tế xã An Khánh (TTYT h. Hoài Đức)",
   "address": "Xã An Khánh, Hoài Đức , Hà Nội",
   "Longtitude": 20.9994708,
   "Latitude": 105.7204476
 },
 {
   "STT": 355,
   "Name": "Trạm y tế xã An Thượng  (TTYT h. Hoài Đức)",
   "address": "Xã An Thượng, Hoài Đức , Hà Nội",
   "Longtitude": 20.9895318,
   "Latitude": 105.7028457
 },
 {
   "STT": 356,
   "Name": "Trạm y tế xã Vân Côn (TTYT h. Hoài Đức)",
   "address": "Xã Vân Côn, Hoài Đức , Hà Nội",
   "Longtitude": 20.9917365,
   "Latitude": 105.6793792
 },
 {
   "STT": 357,
   "Name": "Trạm y tế xã La Phù  (TTYT h. Hoài Đức)",
   "address": "Xã La Phù, Hoài Đức , Hà Nội",
   "Longtitude": 20.979915,
   "Latitude": 105.7325582
 },
 {
   "STT": 358,
   "Name": "Trạm y tế xã Đông La (TTYT h. Hoài Đức)",
   "address": "Xã Đông La, Hoài Đức , Hà Nội",
   "Longtitude": 20.9704838,
   "Latitude": 105.7204476
 },
 {
   "STT": 359,
   "Name": "Trạm y tế thị trấn Quốc Oai (TTYT H. Quốc Oai)",
   "address": "Thị Trấn Quốc Oai, Quốc Oai , Hà Nội",
   "Longtitude": 20.9950369,
   "Latitude": 105.6441852
 },
 {
   "STT": 360,
   "Name": "Trạm y tế xã Sài Sơn (TTYT H. Quốc Oai)",
   "address": "Xã Sài Sơn, Quốc Oai , Hà Nội",
   "Longtitude": 21.032456,
   "Latitude": 105.6471178
 },
 {
   "STT": 361,
   "Name": "Trạm y tế xã Phượng Cách  (TTYT H. Quốc Oai)",
   "address": "Xã Phượng Cách, Quốc Oai , Hà Nội",
   "Longtitude": 21.0123711,
   "Latitude": 105.6603149
 },
 {
   "STT": 362,
   "Name": "Trạm y tế xã Yên Sơn  (TTYT H. Quốc Oai)",
   "address": "Xã Yên Sơn, Quốc Oai , Hà Nội",
   "Longtitude": 21.0010487,
   "Latitude": 105.6573821
 },
 {
   "STT": 363,
   "Name": "Trạm y tế xã Ngọc Liệp  (TTYT H. Quốc Oai)",
   "address": "Xã Ngọc Liệp, Quốc Oai , Hà Nội",
   "Longtitude": 20.9961897,
   "Latitude": 105.5959406
 },
 {
   "STT": 364,
   "Name": "Trạm y tế xã Ngọc Mỹ (TTYT H. Quốc Oai)",
   "address": "Xã Ngọc Mỹ, Quốc Oai , Hà Nội",
   "Longtitude": 20.9886646,
   "Latitude": 105.6299812
 },
 {
   "STT": 365,
   "Name": "Trạm y tế xã Liệp Tuyết (TTYT H. Quốc Oai)",
   "address": "Xã Liệp Tuyết, Quốc Oai , Hà Nội",
   "Longtitude": 20.9812759,
   "Latitude": 105.6003509
 },
 {
   "STT": 366,
   "Name": "Trạm y tế xã Thạch Thán (TTYT H. Quốc Oai)",
   "address": "Xã Thạch Thán, Quốc Oai , Hà Nội",
   "Longtitude": 20.989531,
   "Latitude": 105.6348491
 },
 {
   "STT": 367,
   "Name": "Trạm y tế xã Đồng Quang (TTYT H. Quốc Oai)",
   "address": "Xã Đồng Quang, Quốc Oai , Hà Nội",
   "Longtitude": 20.974466,
   "Latitude": 105.6471178
 },
 {
   "STT": 368,
   "Name": "Trạm y tế xã Phú Cát (TTYT H. Quốc Oai)",
   "address": "Xã Phú Cát, Quốc Oai , Hà Nội",
   "Longtitude": 20.9716211,
   "Latitude": 105.5532993
 },
 {
   "STT": 369,
   "Name": "Trạm y tế xã Tuyết Nghĩa (TTYT H. Quốc Oai)",
   "address": "Xã Tuyết Nghĩa, Quốc Oai , Hà Nội",
   "Longtitude": 20.9718888,
   "Latitude": 105.5880415
 },
 {
   "STT": 370,
   "Name": "Trạm y tế xã Nghĩa Hương (TTYT H. Quốc Oai)",
   "address": "Xã Nghĩa Hương, Quốc Oai , Hà Nội",
   "Longtitude": 20.9745828,
   "Latitude": 105.6148621
 },
 {
   "STT": 371,
   "Name": "Trạm y tế xã Cộng Hòa (TTYT H. Quốc Oai)",
   "address": "Xã Cộng Hòa, Quốc Oai , Hà Nội",
   "Longtitude": 20.9638452,
   "Latitude": 105.6676471
 },
 {
   "STT": 372,
   "Name": "Trạm y tế xã Tân Phú (TTYT H. Quốc Oai)",
   "address": "Xã Tân Phú, Quốc Oai , Hà Nội",
   "Longtitude": 20.9726897,
   "Latitude": 105.6969788
 },
 {
   "STT": 373,
   "Name": "Trạm y tế xã Đại Thành (TTYT H. Quốc Oai)",
   "address": "Xã Đại Thành, Quốc Oai , Hà Nội",
   "Longtitude": 20.9657896,
   "Latitude": 105.7087128
 },
 {
   "STT": 374,
   "Name": "Trạm y tế xã Phú Mãn (TTYT H. Quốc Oai)",
   "address": "Xã Phú Mãn, Quốc Oai , Hà Nội",
   "Longtitude": 20.9427091,
   "Latitude": 105.5210607
 },
 {
   "STT": 375,
   "Name": "Trạm y tế xã Cấn Hữu (TTYT H. Quốc Oai)",
   "address": "Xã Cấn Hữu, Quốc Oai , Hà Nội",
   "Longtitude": 20.9545582,
   "Latitude": 105.6119301
 },
 {
   "STT": 376,
   "Name": "Trạm y tế xã Tân Hòa (TTYT H. Quốc Oai)",
   "address": "Xã Tân Hòa, Quốc Oai , Hà Nội",
   "Longtitude": 20.9627455,
   "Latitude": 105.6793792
 },
 {
   "STT": 377,
   "Name": "Trạm y tế xã Hòa Thạch (TTYT H. Quốc Oai)",
   "address": "Xã Hòa Thạch, Quốc Oai , Hà Nội",
   "Longtitude": 20.9473282,
   "Latitude": 105.5650239
 },
 {
   "STT": 378,
   "Name": "Trạm y tế xã Đông Yên (TTYT H. Quốc Oai)",
   "address": "Xã Đông Yên, Quốc Oai , Hà Nội",
   "Longtitude": 20.9335457,
   "Latitude": 105.5884754
 },
 {
   "STT": 379,
   "Name": "Trạm y tế xã Đông Xuân (TTYT H. Quốc Oai)",
   "address": "Xã Đông Xuân, Quốc Oai , Hà Nội",
   "Longtitude": 20.963176,
   "Latitude": 105.5135949
 },
 {
   "STT": 380,
   "Name": "Trạm y tế thị trấn Liên Quan (TTYT H. Thạch Thất)",
   "address": "Thị Trấn Liên Quan, Thạch Thất , Hà Nội",
   "Longtitude": 21.0548585,
   "Latitude": 105.578215
 },
 {
   "STT": 381,
   "Name": "Trạm y tế xã Đại Đồng (TTYT H. Thạch Thất)",
   "address": "Xã Đại Đồng, Thạch Thất , Hà Nội",
   "Longtitude": 21.0897208,
   "Latitude": 105.5620927
 },
 {
   "STT": 382,
   "Name": "Trạm y tế xã Cẩm Yên (TTYT H. Thạch Thất)",
   "address": "Xã Cẩm Yên, Thạch Thất , Hà Nội",
   "Longtitude": 21.0861099,
   "Latitude": 105.5386447
 },
 {
   "STT": 383,
   "Name": "Trạm y tế xã Lại Thượng (TTYT H. Thạch Thất)",
   "address": "Xã Lại Thượng, Thạch Thất , Hà Nội",
   "Longtitude": 21.0683716,
   "Latitude": 105.5570547
 },
 {
   "STT": 384,
   "Name": "Trạm y tế xã Phú Kim (TTYT H. Thạch Thất)",
   "address": "Xã Phú Kim, Thạch Thất , Hà Nội",
   "Longtitude": 21.062464,
   "Latitude": 105.5778405
 },
 {
   "STT": 385,
   "Name": "Trạm y tế xã Hương Ngải (TTYT H. .Thạch Thất)",
   "address": "Xã Hương Ngải, Thạch Thất , Hà Nội",
   "Longtitude": 21.0574264,
   "Latitude": 105.5972706
 },
 {
   "STT": 386,
   "Name": "Trạm y tế xã Canh Nậu (TTYT H. Thạch Thất)",
   "address": "Xã Canh Nậu, Thạch Thất , Hà Nội",
   "Longtitude": 21.0563282,
   "Latitude": 105.6089981
 },
 {
   "STT": 387,
   "Name": "Trạm y tế xã Kim Quan (TTYT H. Thạch Thất)",
   "address": "Xã Kim Quan, Thạch Thất , Hà Nội",
   "Longtitude": 21.0505586,
   "Latitude": 105.5740998
 },
 {
   "STT": 388,
   "Name": "Trạm y tế xã Dị Nậu (TTYT H. Thạch Thất)",
   "address": "Xã Dị Nậu, Thạch Thất , Hà Nội",
   "Longtitude": 21.0488797,
   "Latitude": 105.6265908
 },
 {
   "STT": 389,
   "Name": "Trạm y tế xã Bình Yên (TTYT H. Thạch Thất)",
   "address": "Xã Bình Yên, Thạch Thất , Hà Nội",
   "Longtitude": 20.9919225,
   "Latitude": 105.4595306
 },
 {
   "STT": 390,
   "Name": "Trạm y tế xã Chàng Sơn (TTYT H. Thạch Thất)",
   "address": "Xã Chàng Sơn, Thạch Thất , Hà Nội",
   "Longtitude": 21.0333851,
   "Latitude": 105.5925999
 },
 {
   "STT": 391,
   "Name": "Trạm y tế xã Thạch Hoà (TTYT H. Thạch Thất)",
   "address": "Xã Thạch Hoà, Thạch Thất , Hà Nội",
   "Longtitude": 20.9829917,
   "Latitude": 105.5311811
 },
 {
   "STT": 392,
   "Name": "Trạm y tế xã Cần Kiệm (TTYT H. Thạch Thất)",
   "address": "Xã Cần Kiệm, Thạch Thất , Hà Nội",
   "Longtitude": 21.0259721,
   "Latitude": 105.5743918
 },
 {
   "STT": 393,
   "Name": "Trạm y tế xã Hữu Bằng (TTYT H. Thạch Thất)",
   "address": "Xã Hữu Bằng, Thạch Thất , Hà Nội",
   "Longtitude": 21.0268766,
   "Latitude": 105.6193406
 },
 {
   "STT": 394,
   "Name": "Trạm y tế xã Phùng Xá (TTYT H. Thạch Thất)",
   "address": "Xã Phùng Xá, Thạch Thất , Hà Nội",
   "Longtitude": 21.0146314,
   "Latitude": 105.6207263
 },
 {
   "STT": 395,
   "Name": "Trạm y tế xã Tân Xã (TTYT H. Thạch Thất)",
   "address": "Xã Tân Xã, Thạch Thất , Hà Nội",
   "Longtitude": 21.0277701,
   "Latitude": 105.557696
 },
 {
   "STT": 396,
   "Name": "Trạm y tế xã Thạch Xá (TTYT H. Thạch Thất)",
   "address": "Xã Thạch Xá, Thạch Thất , Hà Nội",
   "Longtitude": 21.0318587,
   "Latitude": 105.6030076
 },
 {
   "STT": 397,
   "Name": "Trạm y tế xã Bình Phú (TTYT H. Thạch Thất)",
   "address": "Xã Bình Phú, Thạch Thất , Hà Nội",
   "Longtitude": 21.0273237,
   "Latitude": 105.6065328
 },
 {
   "STT": 398,
   "Name": "Trạm y tế xã Hạ Bằng (Thạch Thất)",
   "address": "Xã Hạ Bằng, Thạch Thất , Hà Nội",
   "Longtitude": 21.0016661,
   "Latitude": 105.557696
 },
 {
   "STT": 399,
   "Name": "Trạm y tế xã Đồng Trúc (TTYT H. Thạch Thất)",
   "address": "Xã Đồng Trúc, Thạch Thất , Hà Nội",
   "Longtitude": 20.9934926,
   "Latitude": 105.5697941
 },
 {
   "STT": 400,
   "Name": "Trạm y tế xã Tiến Xuân (TTYT H. Thạch Thất)",
   "address": "Xã Tiến Xuân, Thạch Thất , Hà Nội",
   "Longtitude": 20.9665439,
   "Latitude": 105.4829681
 },
 {
   "STT": 401,
   "Name": "Trạm y tế xã Yên Bình (TTYT H. Thạch Thất)",
   "address": "Xã Yên Bình, Thạch Thất , Hà Nội",
   "Longtitude": 20.9919225,
   "Latitude": 105.4595306
 },
 {
   "STT": 402,
   "Name": "Trạm y tế xã Yên Trung (TTYT H. Thạch Thất)",
   "address": "Xã Yên Trung, Thạch Thất , Hà Nội",
   "Longtitude": 21.0041459,
   "Latitude": 105.4214515
 },
 {
   "STT": 403,
   "Name": "Trạm y tế thị trấn Chúc Sơn (TTYT  Chương Mỹ)",
   "address": "Thị Trấn Chúc Sơn, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9246569,
   "Latitude": 105.7145801
 },
 {
   "STT": 404,
   "Name": "Trạm y tế thị trấn Xuân Mai (TTYT h. Chương Mỹ)",
   "address": "Thị Trấn Xuân Mai, Chương Mỹ , Hà Nội",
   "Longtitude": 20.896292,
   "Latitude": 105.5787167
 },
 {
   "STT": 405,
   "Name": "Trạm y tế xã Phụng Châu (TTYT h. Chương Mỹ)",
   "address": "Xã Phụng Châu, Chương Mỹ , Hà Nội",
   "Longtitude": 20.948948,
   "Latitude": 105.7028457
 },
 {
   "STT": 406,
   "Name": "Trạm y tế xã Tiên Phương (TTYT h. Chương Mỹ)",
   "address": "Xã Tiên Phương, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9363793,
   "Latitude": 105.6823124
 },
 {
   "STT": 407,
   "Name": "Trạm y tế xã Đông Sơn (TTYT h. Chương Mỹ)",
   "address": "Xã Đông Sơn, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9218414,
   "Latitude": 105.6207263
 },
 {
   "STT": 408,
   "Name": "Trạm y tế xã Đông Phương Yên (TTYT Chương Mỹ)",
   "address": "Xã Đông Phương Yên, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9317973,
   "Latitude": 105.6383202
 },
 {
   "STT": 409,
   "Name": "Trạm y tế xã Phú Nghĩa (TTYT h. Chương Mỹ)",
   "address": "Xã Phú Nghĩa, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9359513,
   "Latitude": 105.6559158
 },
 {
   "STT": 410,
   "Name": "Trạm y tế xã Trường Yên (TTYT h. Chương Mỹ)",
   "address": "Xã Trường Yên, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9127576,
   "Latitude": 105.6559158
 },
 {
   "STT": 411,
   "Name": "Trạm y tế xã Ngọc Hòa (TTYT h. Chương Mỹ)",
   "address": "Xã Ngọc Hòa, Chương Mỹ , Hà Nội",
   "Longtitude": 20.915812,
   "Latitude": 105.6852455
 },
 {
   "STT": 412,
   "Name": "Trạm y tế xã Thủy Xuân Tiên (TTYT h. Chương Mỹ)",
   "address": "Xã Thủy Xuân Tiên, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8845203,
   "Latitude": 105.5855438
 },
 {
   "STT": 413,
   "Name": "Trạm y tế xã Thanh Bình (TTYT h. Chương Mỹ)",
   "address": "Xã Thanh Bình, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9044441,
   "Latitude": 105.6207263
 },
 {
   "STT": 414,
   "Name": "Trạm y tế xã Trung Hòa (TTYT h. Chương Mỹ)",
   "address": "Xã Trung Hòa, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8959102,
   "Latitude": 105.6500504
 },
 {
   "STT": 415,
   "Name": "Trạm y tế xã Đại Yên (TTYT h. Chương Mỹ)",
   "address": "Xã Đại Yên, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8978697,
   "Latitude": 105.6911121
 },
 {
   "STT": 416,
   "Name": "Trạm y tế xã Thụy Hương (TTYT h. Chương Mỹ)",
   "address": "Xã Thụy Hương, Chương Mỹ , Hà Nội",
   "Longtitude": 20.9072654,
   "Latitude": 105.7145801
 },
 {
   "STT": 417,
   "Name": "Trạm y tế xã Tốt Động (TTYT h. Chương Mỹ)",
   "address": "Xã Tốt Động, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8821226,
   "Latitude": 105.6735131
 },
 {
   "STT": 418,
   "Name": "Trạm y tế xã Lam Điền (TTYT h. Chương Mỹ)",
   "address": "Xã Lam Điền, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8867006,
   "Latitude": 105.7175138
 },
 {
   "STT": 419,
   "Name": "Trạm y tế xã Tân Tiến (TTYT h. Chương Mỹ)",
   "address": "Xã Tân Tiến, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8744588,
   "Latitude": 105.6002024
 },
 {
   "STT": 420,
   "Name": "Trạm y tế xã Nam Phương Tiến (TTYT  Chương Mỹ)",
   "address": "Xã Nam Phương Tiến, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8807019,
   "Latitude": 105.6265908
 },
 {
   "STT": 421,
   "Name": "Trạm y tế xã Hợp Đồng (Chương Mỹ)",
   "address": "Xã Hợp Đồng, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8799282,
   "Latitude": 105.6969788
 },
 {
   "STT": 422,
   "Name": "Trạm y tế xã Hoàng Văn Thụ (TTYT h. Chương Mỹ)",
   "address": "Xã Hoàng Văn Thụ, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8595873,
   "Latitude": 105.6353878
 },
 {
   "STT": 423,
   "Name": "Trạm y tế xã Hoàng Diệu (TTYT h. Chương Mỹ)",
   "address": "Xã Hoàng Diệu, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8592403,
   "Latitude": 105.7321831
 },
 {
   "STT": 424,
   "Name": "Trạm y tế xã Hữu Văn (TTYT h. Chương Mỹ)",
   "address": "Xã Hữu Văn, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8658243,
   "Latitude": 105.6617813
 },
 {
   "STT": 425,
   "Name": "Trạm y tế xã Quảng Bị (TTYT h. Chương Mỹ)",
   "address": "Xã Quảng Bị, Chương Mỹ , Hà Nội",
   "Longtitude": 20.856739,
   "Latitude": 105.6969788
 },
 {
   "STT": 426,
   "Name": "Trạm y tế xã Mỹ Lương (TTYT h. Chương Mỹ)",
   "address": "Xã Mỹ Lương, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8536811,
   "Latitude": 105.6676471
 },
 {
   "STT": 427,
   "Name": "Trạm y tế xã Thượng Vực (TTYT h. Chương Mỹ)",
   "address": "Xã Thượng Vực, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8487464,
   "Latitude": 105.7204476
 },
 {
   "STT": 428,
   "Name": "Trạm y tế xã Hồng Phong (TTYT h. Chương Mỹ)",
   "address": "Xã Hồng Phong, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8393475,
   "Latitude": 105.6969788
 },
 {
   "STT": 429,
   "Name": "Trạm y tế xã Đồng Phú (TTYT h. Chương Mỹ)",
   "address": "Xã Đồng Phú, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8319052,
   "Latitude": 105.7145801
 },
 {
   "STT": 430,
   "Name": "Trạm y tế xã Trần Phú (TTYT h. Chương Mỹ)",
   "address": "Xã Trần Phú, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8331155,
   "Latitude": 105.6705801
 },
 {
   "STT": 431,
   "Name": "Trạm y tế xã Văn Võ (TTYT h. Chương Mỹ)",
   "address": "Xã Văn Võ, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8297086,
   "Latitude": 105.7380511
 },
 {
   "STT": 432,
   "Name": "Trạm y tế xã Đồng Lạc (TTYT h. Chương Mỹ)",
   "address": "Xã Đồng Lạc, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8178012,
   "Latitude": 105.6793792
 },
 {
   "STT": 433,
   "Name": "Trạm y tế xã Hòa Chính (TTYT h. Chương Mỹ)",
   "address": "Xã Hòa Chính, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8139667,
   "Latitude": 105.7204476
 },
 {
   "STT": 434,
   "Name": "Trạm y tế xã Phú Nam An (TTYT h. Chương Mỹ)",
   "address": "Xã Phú Nam An, Chương Mỹ , Hà Nội",
   "Longtitude": 20.8175668,
   "Latitude": 105.7439194
 },
 {
   "STT": 435,
   "Name": "Trạm y tế thị trấn Kim Bài (TTYT H. Thanh Oai)",
   "address": "Thị Trấn Kim Bài, Thanh Oai , Hà Nội",
   "Longtitude": 20.8559363,
   "Latitude": 105.7673942
 },
 {
   "STT": 436,
   "Name": "Trạm y tế xã Cự Khê (TTYT H. Thanh Oai)",
   "address": "Xã Cự Khê, Thanh Oai , Hà Nội",
   "Longtitude": 20.9238282,
   "Latitude": 105.7850022
 },
 {
   "STT": 437,
   "Name": "Trạm y tế xã Bích Hòa (TTYT H. Thanh Oai)",
   "address": "Xã Bích Hòa, Thanh Oai , Hà Nội",
   "Longtitude": 20.9170698,
   "Latitude": 105.7644596
 },
 {
   "STT": 438,
   "Name": "Trạm y tế xã Mỹ Hưng (TTYT H. Thanh Oai)",
   "address": "Xã Mỹ Hưng, Thanh Oai , Hà Nội",
   "Longtitude": 20.9021599,
   "Latitude": 105.7996769
 },
 {
   "STT": 439,
   "Name": "Trạm y tế xã Cao Viên (TTYT H. Thanh Oai)",
   "address": "Xã Cao Viên, Thanh Oai , Hà Nội",
   "Longtitude": 20.9045109,
   "Latitude": 105.7439194
 },
 {
   "STT": 440,
   "Name": "Trạm y tế xã Bình Minh (TTYT H. Thanh Oai)",
   "address": "Xã Bình Minh, Thanh Oai , Hà Nội",
   "Longtitude": 20.8959551,
   "Latitude": 105.7732633
 },
 {
   "STT": 441,
   "Name": "Trạm y tế xã Tam Hưng (TTYT H. Thanh Oai)",
   "address": "Xã Tam Hưng, Thanh Oai , Hà Nội",
   "Longtitude": 20.8800846,
   "Latitude": 105.7879371
 },
 {
   "STT": 442,
   "Name": "Trạm y tế xã Thanh Cao (TTYT H. Thanh Oai)",
   "address": "Xã Thanh Cao, Thanh Oai , Hà Nội",
   "Longtitude": 20.8865704,
   "Latitude": 105.7497878
 },
 {
   "STT": 443,
   "Name": "Trạm y tế xã Thanh Thùy (TTYT H. Thanh Oai)",
   "address": "Xã Thanh Thùy, Thanh Oai , Hà Nội",
   "Longtitude": 20.8752513,
   "Latitude": 105.8084823
 },
 {
   "STT": 444,
   "Name": "Trạm y tế xã Thanh Mai (TTYT H. Thanh Oai)",
   "address": "Xã Thanh Mai, Thanh Oai , Hà Nội",
   "Longtitude": 20.8691816,
   "Latitude": 105.7497878
 },
 {
   "STT": 445,
   "Name": "Trạm y tế xã Thanh Văn (TTYT H. Thanh Oai)",
   "address": "Xã Thanh Văn, Thanh Oai , Hà Nội",
   "Longtitude": 20.8520715,
   "Latitude": 105.8084823
 },
 {
   "STT": 446,
   "Name": "Trạm y tế xã Đỗ Động (TTYT H. Thanh Oai)",
   "address": "Xã Đỗ Động, Thanh Oai , Hà Nội",
   "Longtitude": 20.8453123,
   "Latitude": 105.7879371
 },
 {
   "STT": 447,
   "Name": "Trạm y tế xã Kim An (TTYT H. Thanh Oai)",
   "address": "Xã Kim An, Thanh Oai , Hà Nội",
   "Longtitude": 20.8407512,
   "Latitude": 105.7439194
 },
 {
   "STT": 448,
   "Name": "Trạm y tế xã Kim Thư (TTYT H. Thanh Oai)",
   "address": "Xã Kim Thư, Thanh Oai , Hà Nội",
   "Longtitude": 20.8385491,
   "Latitude": 105.7673942
 },
 {
   "STT": 449,
   "Name": "Trạm y tế xã Phương Trung (TTYT H. Thanh Oai)",
   "address": "Xã Phương Trung, Thanh Oai , Hà Nội",
   "Longtitude": 20.8269579,
   "Latitude": 105.7673942
 },
 {
   "STT": 450,
   "Name": "Trạm y tế xã Tân Ước (TTYT H. Thanh Oai)",
   "address": "Xã Tân Ước, Thanh Oai , Hà Nội",
   "Longtitude": 20.8315134,
   "Latitude": 105.8114175
 },
 {
   "STT": 451,
   "Name": "Trạm y tế xã Dân Hòa (TTYT H. Thanh Oai)",
   "address": "Xã Dân Hòa, Thanh Oai , Hà Nội",
   "Longtitude": 20.8131631,
   "Latitude": 105.790872
 },
 {
   "STT": 452,
   "Name": "Trạm y tế xã Liên Châu (TTYT H. Thanh Oai)",
   "address": "Xã Liên Châu, Thanh Oai , Hà Nội",
   "Longtitude": 20.8104037,
   "Latitude": 105.8202234
 },
 {
   "STT": 453,
   "Name": "Trạm y tế xã Cao Dương (TTYT H. Thanh Oai)",
   "address": "Xã Cao Dương, Thanh Oai , Hà Nội",
   "Longtitude": 20.8101214,
   "Latitude": 105.7615252
 },
 {
   "STT": 454,
   "Name": "Trạm y tế xã Xuân Dương (TTYT H. Thanh Oai)",
   "address": "Xã Xuân Dương, Thanh Oai , Hà Nội",
   "Longtitude": 20.7996295,
   "Latitude": 105.7497878
 },
 {
   "STT": 455,
   "Name": "Trạm y tế xã Hồng Dương (TTYT H. Thanh Oai)",
   "address": "Xã Hồng Dương, Thanh Oai , Hà Nội",
   "Longtitude": 20.7989511,
   "Latitude": 105.7879371
 },
 {
   "STT": 456,
   "Name": "Trạm y tế thị trấn Thường Tín (TTYT h. Thường Tín)",
   "address": "Thị Trấn Thường Tín, Thường Tín , Hà Nội",
   "Longtitude": 20.8715715,
   "Latitude": 105.8627913
 },
 {
   "STT": 457,
   "Name": "Trạm y tế xã Ninh Sở (TTYT h. Thường Tín)",
   "address": "Xã Ninh Sở, Thường Tín , Hà Nội",
   "Longtitude": 20.8970059,
   "Latitude": 105.884813
 },
 {
   "STT": 458,
   "Name": "Trạm y tế xã Nhị Khê (TTYT h. Thường Tín)",
   "address": "Xã Nhị Khê, Thường Tín , Hà Nội",
   "Longtitude": 20.8951043,
   "Latitude": 105.843708
 },
 {
   "STT": 459,
   "Name": "Trạm y tế xã Duyên Thái (TTYT h. Thường Tín)",
   "address": "Xã Duyên Thái, Thường Tín , Hà Nội",
   "Longtitude": 20.8986756,
   "Latitude": 105.8671954
 },
 {
   "STT": 460,
   "Name": "Trạm y tế xã Khánh Hà (TTYT h. Thường Tín)",
   "address": "Xã Khánh Hà, Thường Tín , Hà Nội",
   "Longtitude": 20.8909742,
   "Latitude": 105.8260943
 },
 {
   "STT": 461,
   "Name": "Trạm y tế xã Hòa Bình (TTYT h. Thường Tín)",
   "address": "Xã Hòa Bình, Thường Tín , Hà Nội",
   "Longtitude": 20.878276,
   "Latitude": 105.8378366
 },
 {
   "STT": 462,
   "Name": "Trạm y tế xã Văn Bình (TTYT h. Thường Tín)",
   "address": "Xã Văn Bình, Thường Tín , Hà Nội",
   "Longtitude": 20.8850245,
   "Latitude": 105.8583873
 },
 {
   "STT": 463,
   "Name": "Trạm y tế xã Hiền Giang (TTYT h. Thường Tín)",
   "address": "Xã Hiền Giang, Thường Tín , Hà Nội",
   "Longtitude": 20.8741441,
   "Latitude": 105.8202234
 },
 {
   "STT": 464,
   "Name": "Trạm y tế xã Hồng Vân (TTYT h. Thường Tín)",
   "address": "Xã Hồng Vân, Thường Tín , Hà Nội",
   "Longtitude": 20.8773976,
   "Latitude": 105.9083056
 },
 {
   "STT": 465,
   "Name": "Trạm y tế xã Vân Tảo (TTYT h. Thường Tín)",
   "address": "Xã Vân Tảo, Thường Tín , Hà Nội",
   "Longtitude": 20.8727188,
   "Latitude": 105.896559
 },
 {
   "STT": 466,
   "Name": "Trạm y tế xã Liên Phương (TTYT h. Thường Tín)",
   "address": "Xã Liên Phương, Thường Tín , Hà Nội",
   "Longtitude": 20.8585549,
   "Latitude": 105.8855471
 },
 {
   "STT": 467,
   "Name": "Trạm y tế xã Văn Phú (TTYT h. Thường Tín)",
   "address": "Xã Văn Phú, Thường Tín , Hà Nội",
   "Longtitude": 20.8801818,
   "Latitude": 105.8789403
 },
 {
   "STT": 468,
   "Name": "Trạm y tế xã Tự Nhiên (TTYT h. Thường Tín)",
   "address": "Xã Tự Nhiên, Thường Tín , Hà Nội",
   "Longtitude": 20.8583468,
   "Latitude": 105.9259271
 },
 {
   "STT": 469,
   "Name": "Trạm y tế xã Tiền Phong (TTYT h. Thường Tín)",
   "address": "Xã Tiền Phong, Thường Tín , Hà Nội",
   "Longtitude": 20.8556526,
   "Latitude": 105.8319653
 },
 {
   "STT": 470,
   "Name": "Trạm y tế xã Hà Hồi (TTYT h. Thường Tín)",
   "address": "Xã Hà Hồi, Thường Tín , Hà Nội",
   "Longtitude": 20.8628013,
   "Latitude": 105.8789403
 },
 {
   "STT": 471,
   "Name": "Trạm y tế xã Thư Phú (TTYT h. Thường Tín)",
   "address": "Xã Thư Phú, Thường Tín , Hà Nội",
   "Longtitude": 20.8600189,
   "Latitude": 105.9083056
 },
 {
   "STT": 472,
   "Name": "Trạm y tế xã Nguyễn Trãi (TTYT h. Thường Tín)",
   "address": "Xã Nguyễn Trãi, Thường Tín , Hà Nội",
   "Longtitude": 20.8476414,
   "Latitude": 105.8554513
 },
 {
   "STT": 473,
   "Name": "Trạm y tế xã Quất Động (TTYT h. Thường Tín)",
   "address": "Xã Quất Động, Thường Tín , Hà Nội",
   "Longtitude": 20.8401829,
   "Latitude": 105.8730678
 },
 {
   "STT": 474,
   "Name": "Trạm y tế xã Chương Dương (TTYT h. Thường Tín)",
   "address": "Xã Chương Dương, Thường Tín , Hà Nội",
   "Longtitude": 20.8484333,
   "Latitude": 105.9083056
 },
 {
   "STT": 475,
   "Name": "Trạm y tế xã Tân Minh (TTTYT h. hường Tín)",
   "address": "Xã Tân Minh, Thường Tín , Hà Nội",
   "Longtitude": 20.8377159,
   "Latitude": 105.8378366
 },
 {
   "STT": 476,
   "Name": "Trạm y tế xã Lê Lợi (TTYT h. Thường Tín)",
   "address": "Xã Lê Lợi, Thường Tín , Hà Nội",
   "Longtitude": 20.8316114,
   "Latitude": 105.9024322
 },
 {
   "STT": 477,
   "Name": "Trạm y tế xã Thắng Lợi (TTYT h. Thường Tín)",
   "address": "Xã Thắng Lợi, Thường Tín , Hà Nội",
   "Longtitude": 20.8248668,
   "Latitude": 105.8818766
 },
 {
   "STT": 478,
   "Name": "Trạm y tế xã Dũng Tiến (TTYT h. Thường Tín)",
   "address": "Xã Dũng Tiến, Thường Tín , Hà Nội",
   "Longtitude": 20.8186724,
   "Latitude": 105.8554513
 },
 {
   "STT": 479,
   "Name": "Trạm y tế xã Thống Nhất (TTYT h. Thường Tín)",
   "address": "Xã Thống Nhất, Thường Tín , Hà Nội",
   "Longtitude": 20.8020921,
   "Latitude": 105.9083056
 },
 {
   "STT": 480,
   "Name": "Trạm y tế xã Nghiêm Xuyên (TTYT h. Thường Tín)",
   "address": "Xã Nghiêm Xuyên, Thường Tín , Hà Nội",
   "Longtitude": 20.807085,
   "Latitude": 105.8554513
 },
 {
   "STT": 481,
   "Name": "Trạm y tế xã Tô Hiệu (TTYT h. Thường Tín)",
   "address": "Xã Tô Hiệu, Thường Tín , Hà Nội",
   "Longtitude": 20.8037584,
   "Latitude": 105.8906859
 },
 {
   "STT": 482,
   "Name": "Trạm y tế xã Văn Tự (TTYT h. Thường Tín)",
   "address": "Xã Văn Tự, Thường Tín , Hà Nội",
   "Longtitude": 20.7869345,
   "Latitude": 105.884813
 },
 {
   "STT": 483,
   "Name": "Trạm y tế xã Vạn Điểm (TTYT h. Thường Tín)",
   "address": "Xã Vạn Điểm, Thường Tín , Hà Nội",
   "Longtitude": 20.7847147,
   "Latitude": 105.9083056
 },
 {
   "STT": 484,
   "Name": "Trạm y tế xã Minh Cường (TTYT h. Thường Tín)",
   "address": "Xã Minh Cường, Thường Tín , Hà Nội",
   "Longtitude": 20.7621001,
   "Latitude": 105.9024322
 },
 {
   "STT": 485,
   "Name": "Trạm y tế thị trấn Phú Minh (TTYT h. Phú Xuyên)",
   "address": "Thị Trấn Phú Minh, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7825722,
   "Latitude": 105.9156477
 },
 {
   "STT": 486,
   "Name": "Trạm y tế thị trấn Phú Xuyên (TTYT h. Phú Xuyên)",
   "address": "Thị Trấn Phú Xuyên, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7383765,
   "Latitude": 105.9083056
 },
 {
   "STT": 487,
   "Name": "Trạm y tế xã Hồng Minh (TTYT h. Phú Xuyên)",
   "address": "Xã Hồng Minh, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7924684,
   "Latitude": 105.8260943
 },
 {
   "STT": 488,
   "Name": "Trạm y tế xã Phượng Dực (TTYT h. Phú Xuyên)",
   "address": "Xã Phượng Dực, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7962276,
   "Latitude": 105.8451937
 },
 {
   "STT": 489,
   "Name": "Trạm y tế xã Văn Nhân (TTYT h. Phú Xuyên)",
   "address": "Xã Văn Nhân, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7714636,
   "Latitude": 105.9259271
 },
 {
   "STT": 490,
   "Name": "Trạm y tế xã Thụy Phú (TTYT h. Phú Xuyên)",
   "address": "Xã Thụy Phú, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7761432,
   "Latitude": 105.9376756
 },
 {
   "STT": 491,
   "Name": "Trạm y tế xã Tri Trung (TTYT h. Phú Xuyên)",
   "address": "Xã Tri Trung, Phú Xuyên , Hà Nội",
   "Longtitude": 20.775086,
   "Latitude": 105.8260943
 },
 {
   "STT": 492,
   "Name": "Trạm y tế xã Đại Thắng (TTYT h. Phú Xuyên)",
   "address": "Xã Đại Thắng, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7706637,
   "Latitude": 105.8730678
 },
 {
   "STT": 493,
   "Name": "Trạm y tế xã Phú Túc (TTYT h. Phú Xuyên)",
   "address": "Xã Phú Túc, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7619792,
   "Latitude": 105.8114175
 },
 {
   "STT": 494,
   "Name": "Trạm y tế xã Văn Hoàng (TTYT h. Phú Xuyên)",
   "address": "Xã Văn Hoàng, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7723237,
   "Latitude": 105.8554513
 },
 {
   "STT": 495,
   "Name": "Trạm y tế xã Hồng Thái (TTYT h. Phú Xuyên)",
   "address": "Xã Hồng Thái, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7602732,
   "Latitude": 105.9523622
 },
 {
   "STT": 496,
   "Name": "Trạm y tế xã Hoàng Long (TTYT h. Phú Xuyên)",
   "address": "Xã Hoàng Long, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7481857,
   "Latitude": 105.8349009
 },
 {
   "STT": 497,
   "Name": "Trạm y tế xã Quang Trung (TTYT h. Phú Xuyên)",
   "address": "Xã Quang Trung, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7637631,
   "Latitude": 105.884813
 },
 {
   "STT": 498,
   "Name": "Trạm y tế xã Nam Phong (TTYT h. Phú Xuyên)",
   "address": "Xã Nam Phong, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7540878,
   "Latitude": 105.9259271
 },
 {
   "STT": 499,
   "Name": "Trạm y tế xã Nam Triều (TTYT h. Phú Xuyên)",
   "address": "Xã Nam Triều, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7471849,
   "Latitude": 105.9376756
 },
 {
   "STT": 500,
   "Name": "Trạm y tế xã Tân Dân (TTYT h. Phú Xuyên)",
   "address": "Xã Tân Dân, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7474917,
   "Latitude": 105.8730678
 },
 {
   "STT": 501,
   "Name": "Trạm y tế xã Sơn Hà (TTYT h. Phú Xuyên)",
   "address": "Xã Sơn Hà, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7510696,
   "Latitude": 105.896559
 },
 {
   "STT": 502,
   "Name": "Trạm y tế xã Chuyên Mỹ (TTYT h. Phú Xuyên)",
   "address": "Xã Chuyên Mỹ, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7393911,
   "Latitude": 105.8664484
 },
 {
   "STT": 503,
   "Name": "Trạm y tế xã Khai Thái (TTYT h. Phú Xuyên)",
   "address": "Xã Khai Thái, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7223523,
   "Latitude": 105.9552997
 },
 {
   "STT": 504,
   "Name": "Trạm y tế xã Phúc Tiến (TTYT h. Phú Xuyên)",
   "address": "Xã Phúc Tiến, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7251289,
   "Latitude": 105.9259271
 },
 {
   "STT": 505,
   "Name": "Trạm y tế xã Vân Từ (TTYT h. Phú Xuyên)",
   "address": "Xã Vân Từ, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7168688,
   "Latitude": 105.8906859
 },
 {
   "STT": 506,
   "Name": "Trạm y tế xã Tri Thủy (TTYT h. Phú Xuyên)",
   "address": "Xã Tri Thủy, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7113258,
   "Latitude": 105.9494248
 },
 {
   "STT": 507,
   "Name": "Trạm y tế xã Đại Xuyên (TTYT h. Phú Xuyên)",
   "address": "Xã Đại Xuyên, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7003229,
   "Latitude": 105.9157274
 },
 {
   "STT": 508,
   "Name": "Trạm y tế xã Phú Yên (TTYT h. Phú Xuyên)",
   "address": "Xã Phú Yên, Phú Xuyên , Hà Nội",
   "Longtitude": 20.6989389,
   "Latitude": 105.896559
 },
 {
   "STT": 509,
   "Name": "Trạm y tế xã Bạch Hạ (TTYT h. Phú Xuyên)",
   "address": "Xã Bạch Hạ, Phú Xuyên , Hà Nội",
   "Longtitude": 20.7352032,
   "Latitude": 105.9112424
 },
 {
   "STT": 510,
   "Name": "Trạm y tế xã Quang Lãng (TTYT h. Phú Xuyên)",
   "address": "Xã Quang Lãng, Phú Xuyên , Hà Nội",
   "Longtitude": 20.6928796,
   "Latitude": 105.9839916
 },
 {
   "STT": 511,
   "Name": "Trạm y tế xã Châu Can (TTYT h. Phú Xuyên)",
   "address": "Xã Châu Can, Phú Xuyên , Hà Nội",
   "Longtitude": 20.6836293,
   "Latitude": 105.9053689
 },
 {
   "STT": 512,
   "Name": "Trạm y tế xã Minh Tân (TTYT h. Phú Xuyên)",
   "address": "Xã Minh Tân, Phú Xuyên , Hà Nội",
   "Longtitude": 20.674452,
   "Latitude": 105.9659467
 },
 {
   "STT": 513,
   "Name": "Trạm y tế thị trấn Vân Đình (TTYT h. ứng Hoà)",
   "address": "Thị Trấn Vân Đình, ứng Hoà , Hà Nội",
   "Longtitude": 20.7337728,
   "Latitude": 105.7704551
 },
 {
   "STT": 514,
   "Name": "Trạm y tế xã Viên An  (TTYT h. ứng Hoà)",
   "address": "Xã Viên An, ứng Hoà , Hà Nội",
   "Longtitude": 20.8018254,
   "Latitude": 105.7263152
 },
 {
   "STT": 515,
   "Name": "Trạm y tế xã Viên Nội  (TTYT h. ứng Hoà)",
   "address": "Xã Viên Nội, ứng Hoà , Hà Nội",
   "Longtitude": 20.7902328,
   "Latitude": 105.7263152
 },
 {
   "STT": 516,
   "Name": "Trạm y tế xã Hoa Sơn  (TTYT h. ứng Hoà)",
   "address": "Xã Hoa Sơn, ứng Hoà , Hà Nội",
   "Longtitude": 20.7764465,
   "Latitude": 105.7497878
 },
 {
   "STT": 517,
   "Name": "Trạm y tế xã Quảng Phú Cầu (TTYT h. ứng Hoà)",
   "address": "Xã Quảng Phú Cầu, ứng Hoà , Hà Nội",
   "Longtitude": 20.7748261,
   "Latitude": 105.7704148
 },
 {
   "STT": 518,
   "Name": "Trạm y tế xã Trường Thịnh  (TTYT h. ứng Hoà)",
   "address": "Xã Trường Thịnh, ứng Hoà , Hà Nội",
   "Longtitude": 20.758511,
   "Latitude": 105.7556564
 },
 {
   "STT": 519,
   "Name": "Trạm y tế xã Cao Thành  (TTYT h. ứng Hoà)",
   "address": "Xã Cao Thành, ứng Hoà , Hà Nội",
   "Longtitude": 20.7665002,
   "Latitude": 105.7321831
 },
 {
   "STT": 520,
   "Name": "Trạm y tế xã Liên Bạt  (TTYT h. ứng Hoà)",
   "address": "Xã Liên Bạt, ứng Hoà , Hà Nội",
   "Longtitude": 20.7536915,
   "Latitude": 105.776198
 },
 {
   "STT": 521,
   "Name": "Trạm y tế xã Sơn Công  (TTYT h. ứng Hoà)",
   "address": "Xã Sơn Công, ứng Hoà , Hà Nội",
   "Longtitude": 20.7475827,
   "Latitude": 105.7175138
 },
 {
   "STT": 522,
   "Name": "Trạm y tế xã Đồng Tiến  (TTYT h. ứng Hoà)",
   "address": "Xã Đồng Tiến, ứng Hoà , Hà Nội",
   "Longtitude": 20.7453929,
   "Latitude": 105.7409852
 },
 {
   "STT": 523,
   "Name": "Trạm y tế xã Phương Tú  (TTYT h. ứng Hoà)",
   "address": "Xã Phương Tú, ứng Hoà , Hà Nội",
   "Longtitude": 20.7399029,
   "Latitude": 105.7996769
 },
 {
   "STT": 524,
   "Name": "Trạm y tế xã Trung Tú  (TTYT h. ứng Hoà)",
   "address": "Xã Trung Tú, ứng Hoà , Hà Nội",
   "Longtitude": 20.7281835,
   "Latitude": 105.8319653
 },
 {
   "STT": 525,
   "Name": "Trạm y tế xã Đồng Tân  (TTYT h. ứng Hoà)",
   "address": "Xã Đồng Tân, ứng Hoà , Hà Nội",
   "Longtitude": 20.7097007,
   "Latitude": 105.843708
 },
 {
   "STT": 526,
   "Name": "Trạm y tế xã Tảo Dương Văn  (TTYT h. ứng Hoà)",
   "address": "Xã Tảo Dương Văn, ứng Hoà , Hà Nội",
   "Longtitude": 20.708858,
   "Latitude": 105.790872
 },
 {
   "STT": 527,
   "Name": "Trạm y tế xã Vạn Thái (TTYT h. ứng Hoà)",
   "address": "Xã Vạn Thái, ứng Hoà , Hà Nội",
   "Longtitude": 20.6989147,
   "Latitude": 105.7732633
 },
 {
   "STT": 528,
   "Name": "Trạm y tế xã Minh Đức (TTYT h. ứng Hoà)",
   "address": "Xã Minh Đức, ứng Hoà , Hà Nội",
   "Longtitude": 20.7017014,
   "Latitude": 105.8671954
 },
 {
   "STT": 529,
   "Name": "Trạm y tế xã Hòa Lâm  (TTYT h. ứng Hoà)",
   "address": "Xã Hòa Lâm, ứng Hoà , Hà Nội",
   "Longtitude": 20.6827048,
   "Latitude": 105.8201839
 },
 {
   "STT": 530,
   "Name": "Trạm y tế xã Hòa Xá  (TTYT h. ứng Hoà)",
   "address": "Xã Hòa Xá, ứng Hoà , Hà Nội",
   "Longtitude": 20.696075,
   "Latitude": 105.7571236
 },
 {
   "STT": 531,
   "Name": "Trạm y tế xã Trầm Lộng (TTYT h. ứng Hoà)",
   "address": "Xã Trầm Lộng, ứng Hoà , Hà Nội",
   "Longtitude": 20.6902502,
   "Latitude": 105.8349009
 },
 {
   "STT": 532,
   "Name": "Trạm y tế xã Kim Đường  (TTYT h. ứng Hoà)",
   "address": "Xã Kim Đường, ứng Hoà , Hà Nội",
   "Longtitude": 20.6753586,
   "Latitude": 105.8701316
 },
 {
   "STT": 533,
   "Name": "Trạm y tế xã Hòa Nam (TTYT h. ứng Hoà)",
   "address": "Xã Hòa Nam, ứng Hoà , Hà Nội",
   "Longtitude": 20.6826258,
   "Latitude": 105.7615252
 },
 {
   "STT": 534,
   "Name": "Trạm y tế xã Hòa Phú  (TTYT h. ứng Hoà)",
   "address": "Xã Hòa Phú, ứng Hoà , Hà Nội",
   "Longtitude": 20.6746404,
   "Latitude": 105.7850022
 },
 {
   "STT": 535,
   "Name": "Trạm y tế xã Đội Bình  (TTYT h. ứng Hoà)",
   "address": "Xã Đội Bình, ứng Hoà , Hà Nội",
   "Longtitude": 20.6603098,
   "Latitude": 105.8143528
 },
 {
   "STT": 536,
   "Name": "Trạm y tế xã Đại Hùng  (TTYT h. ứng Hoà)",
   "address": "Xã Đại Hùng, ứng Hoà , Hà Nội",
   "Longtitude": 20.6696986,
   "Latitude": 105.8378366
 },
 {
   "STT": 537,
   "Name": "Trạm y tế xã Đông Lỗ  (TTYT h. ứng Hoà)",
   "address": "Xã Đông Lỗ, ứng Hoà , Hà Nội",
   "Longtitude": 20.6594987,
   "Latitude": 105.884813
 },
 {
   "STT": 538,
   "Name": "Trạm y tế xã Phù Lưu  (TTYT h. ứng Hoà)",
   "address": "Xã Phù Lưu, ứng Hoà , Hà Nội",
   "Longtitude": 20.6625038,
   "Latitude": 105.790872
 },
 {
   "STT": 539,
   "Name": "Trạm y tế xã Đại Cường  (TTYT h. ứng Hoà)",
   "address": "Xã Đại Cường, ứng Hoà , Hà Nội",
   "Longtitude": 20.6564618,
   "Latitude": 105.8554513
 },
 {
   "STT": 540,
   "Name": "Trạm y tế xã Lưu Hoàng (TTYT h. ứng Hoà)",
   "address": "Xã Lưu Hoàng, ứng Hoà , Hà Nội",
   "Longtitude": 20.6492712,
   "Latitude": 105.8084823
 },
 {
   "STT": 541,
   "Name": "Trạm y tế xã Hồng Quang  (TTYT h. ứng Hoà)",
   "address": "Xã Hồng Quang, ứng Hoà , Hà Nội",
   "Longtitude": 20.6213986,
   "Latitude": 105.7967419
 },
 {
   "STT": 542,
   "Name": "Trạm y tế thị trấn Đại Nghĩa (TTYT h. Mỹ Đức)",
   "address": "Thị Trấn Đại Nghĩa, Mỹ Đức , Hà Nội",
   "Longtitude": 20.685899,
   "Latitude": 105.7421069
 },
 {
   "STT": 543,
   "Name": "Trạm y tế xã Đồng Tâm (TTYT h. Mỹ Đức)",
   "address": "Xã Đồng Tâm, Mỹ Đức, , Hà Nội",
   "Longtitude": 20.8008616,
   "Latitude": 105.6764671
 },
 {
   "STT": 544,
   "Name": "Trạm y tế xã Thượng Lâm (TTYT h. Mỹ Đức)",
   "address": "Xã Thượng Lâm, Mỹ Đức , Hà Nội",
   "Longtitude": 20.789361,
   "Latitude": 105.6735131
 },
 {
   "STT": 545,
   "Name": "Trạm y tế xã Tuy Lai (TTYT h. Mỹ Đức)",
   "address": "Xã Tuy Lai, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7635461,
   "Latitude": 105.6705801
 },
 {
   "STT": 546,
   "Name": "Trạm y tế xã Phúc Lâm (TTYT h. Mỹ Đức)",
   "address": "Xã Phúc Lâm, Mỹ Đức , Hà Nội",
   "Longtitude": 20.8156114,
   "Latitude": 105.7028457
 },
 {
   "STT": 547,
   "Name": "Trạm y tế xã Mỹ Thành (TTYT h. Mỹ Đức)",
   "address": "Xã Mỹ Thành, Mỹ Đức , Hà Nội",
   "Longtitude": 20.775581,
   "Latitude": 105.6969788
 },
 {
   "STT": 548,
   "Name": "Trạm y tế xã Bột Xuyên (TTYT h. Mỹ Đức)",
   "address": "Xã Bột Xuyên, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7739396,
   "Latitude": 105.7145801
 },
 {
   "STT": 549,
   "Name": "Trạm y tế xã An Mỹ (TTYT h. Mỹ Đức)",
   "address": "Xã An Mỹ, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7529404,
   "Latitude": 105.6911121
 },
 {
   "STT": 550,
   "Name": "Trạm y tế xã Hồng Sơn (TTYT h. Mỹ Đức)",
   "address": "Xã Hồng Sơn, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7277224,
   "Latitude": 105.7085338
 },
 {
   "STT": 551,
   "Name": "Trạm y tế xã Lê Thanh (TTYT h. Mỹ Đức)",
   "address": "Xã Lê Thanh, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7270229,
   "Latitude": 105.7204476
 },
 {
   "STT": 552,
   "Name": "Trạm y tế xã Xuy Xá (TTYT h. Mỹ Đức)",
   "address": "Xã Xuy Xá, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7137907,
   "Latitude": 105.7380511
 },
 {
   "STT": 553,
   "Name": "Trạm y tế xã Phùng Xá (TTYT h. Mỹ Đức)",
   "address": "Xã Phùng Xá, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7222567,
   "Latitude": 105.762346
 },
 {
   "STT": 554,
   "Name": "Trạm y tế xã Phù Lưu Tế (TTYT h. Mỹ Đức)",
   "address": "Xã Phù Lưu Tế, Mỹ Đức , Hà Nội",
   "Longtitude": 20.7137907,
   "Latitude": 105.7380511
 },
 {
   "STT": 555,
   "Name": "Trạm y tế xã Đại Hưng (TTYT h. Mỹ Đức)",
   "address": "Xã Đại Hưng, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6613764,
   "Latitude": 105.7599735
 },
 {
   "STT": 556,
   "Name": "Trạm y tế xã Vạn Kim (TTYT h. Mỹ Đức)",
   "address": "Xã Vạn Kim, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6462166,
   "Latitude": 105.7791327
 },
 {
   "STT": 557,
   "Name": "Trạm y tế xã Đốc Tín (TTYT h. Mỹ Đức)",
   "address": "Xã Đốc Tín, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6346282,
   "Latitude": 105.7791327
 },
 {
   "STT": 558,
   "Name": "Trạm y tế xã Hương Sơn (TTYT h. Mỹ Đức)",
   "address": "Xã Hương Sơn, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6041258,
   "Latitude": 105.7644596
 },
 {
   "STT": 559,
   "Name": "Trạm y tế xã Hùng Tiến (TTYT h. Mỹ Đức)",
   "address": "Xã Hùng Tiến, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6368144,
   "Latitude": 105.7556564
 },
 {
   "STT": 560,
   "Name": "Trạm y tế xã An Tiến (TTYT h. Mỹ Đức)",
   "address": "Xã An Tiến, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6539736,
   "Latitude": 105.7376347
 },
 {
   "STT": 561,
   "Name": "Trạm y tế xã Hợp Tiến (TTYT h. Mỹ Đức)",
   "address": "Xã Hợp Tiến, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6869244,
   "Latitude": 105.6970532
 },
 {
   "STT": 562,
   "Name": "Trạm y tế xã Hợp Thanh (TTYT h. Mỹ Đức)",
   "address": "Xã Hợp Thanh, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6690058,
   "Latitude": 105.702982
 },
 {
   "STT": 563,
   "Name": "Trạm y tế xã An Phú (TTYT h. Mỹ Đức)",
   "address": "Xã An Phú, Mỹ Đức , Hà Nội",
   "Longtitude": 20.6257876,
   "Latitude": 105.6848576
 },
 {
   "STT": 564,
   "Name": "Trạm y tế xã Đại Thịnh (TTYT Huyện.Mê Linh)",
   "address": "Xã Đại Thịnh, Huyện Mê Linh , Hà Nội",
   "Longtitude": 21.1850027,
   "Latitude": 105.7204476
 },
 {
   "STT": 565,
   "Name": "Trạm y tế xã Kim Hoa (TTYT h. Mê Linh)",
   "address": "Xã Kim Hoa, Mê Linh , Hà Nội",
   "Longtitude": 21.2207398,
   "Latitude": 105.7409852
 },
 {
   "STT": 566,
   "Name": "Trạm y tế xã Thạch Đà(TTYT h. Mê Linh)",
   "address": "Xã Thạch Đà, Mê Linh , Hà Nội",
   "Longtitude": 21.1720499,
   "Latitude": 105.6735131
 },
 {
   "STT": 567,
   "Name": "Trạm y tế xã Tiến Thắng (TTYT h. Mê Linh)",
   "address": "Xã Tiến Thắng, Mê Linh , Hà Nội",
   "Longtitude": 21.2242433,
   "Latitude": 105.6735131
 },
 {
   "STT": 568,
   "Name": "Trạm y tế xã Tự Lập (TTYT h. Mê Linh)",
   "address": "Xã Tự Lập, Mê Linh , Hà Nội",
   "Longtitude": 21.207955,
   "Latitude": 105.6617813
 },
 {
   "STT": 569,
   "Name": "Trạm y tế thị trấn Quang Minh (TTYT h. Mê Linh)",
   "address": "Thị Trấn Quang Minh, Mê Linh , Hà Nội",
   "Longtitude": 21.1915793,
   "Latitude": 105.7732633
 },
 {
   "STT": 570,
   "Name": "Trạm y tế xã Thanh Lâm (TTYT h. Mê Linh)",
   "address": "Xã Thanh Lâm, Mê Linh , Hà Nội",
   "Longtitude": 21.2113735,
   "Latitude": 105.7175138
 },
 {
   "STT": 571,
   "Name": "Trạm y tế xã Tam Đồng (TTYT h. Mê Linh)",
   "address": "Xã Tam Đồng, Mê Linh , Hà Nội",
   "Longtitude": 21.1999357,
   "Latitude": 105.6852455
 },
 {
   "STT": 572,
   "Name": "Trạm y tế xã Liên Mạc (TTYT h. Mê Linh)",
   "address": "Xã Liên Mạc, Mê Linh , Hà Nội",
   "Longtitude": 21.1937334,
   "Latitude": 105.6588485
 },
 {
   "STT": 573,
   "Name": "Trạm y tế xã Vạn Yên (TTYT h. Mê Linh)",
   "address": "Xã Vạn Yên, Mê Linh , Hà Nội",
   "Longtitude": 21.1996782,
   "Latitude": 105.6265908
 },
 {
   "STT": 574,
   "Name": "Trạm y tế xã Chu Phan (TTYT h. Mê Linh)",
   "address": "Xã Chu Phan, Mê Linh , Hà Nội",
   "Longtitude": 21.1589365,
   "Latitude": 105.6588485
 },
 {
   "STT": 575,
   "Name": "Trạm y tế xã Tiến Thinh (TTYT h. Mê Linh)",
   "address": "Xã Tiến Thịnh, Mê Linh , Hà Nội",
   "Longtitude": 21.1753719,
   "Latitude": 105.6383202
 },
 {
   "STT": 576,
   "Name": "Trạm y tế xã Mê Linh (TTYT h. Mê Linh)",
   "address": "Xã Mê Linh, Mê Linh , Hà Nội",
   "Longtitude": 21.1601408,
   "Latitude": 105.7380511
 },
 {
   "STT": 577,
   "Name": "Trạm y tế xã Văn Khê (TTYT h. Mê Linh)",
   "address": "Xã Văn Khê, Mê Linh , Hà Nội",
   "Longtitude": 21.1660984,
   "Latitude": 105.7057792
 },
 {
   "STT": 578,
   "Name": "Trạm y tế xã Hoàng Kim (TTYT h. Mê Linh)",
   "address": "Xã Hoàng Kim, Mê Linh , Hà Nội",
   "Longtitude": 21.1587884,
   "Latitude": 105.6911121
 },
 {
   "STT": 579,
   "Name": "Trạm y tế xã Tiền Phong (TTYT h. Mê Linh)",
   "address": "Xã Tiền Phong, Mê Linh , Hà Nội",
   "Longtitude": 21.1605321,
   "Latitude": 105.7644596
 },
 {
   "STT": 580,
   "Name": "Trạm y tế xã Tráng Việt (TTYT h. Mê Linh)",
   "address": "Xã Tráng Việt, Mê Linh , Hà Nội",
   "Longtitude": 21.1406828,
   "Latitude": 105.7292491
 },
 {
   "STT": 581,
   "Name": "Trạm y tế thị trấn Chi Đông (TTYT h. Mê Linh)",
   "address": "Thị Trấn Chi Đông, Mê Linh , Hà Nội",
   "Longtitude": 21.2048498,
   "Latitude": 105.7556564
 }
];