var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Dược Phú Gia Khang",
   "address": "Số 12 Kim Mã, phường Kim Mã, Ba Đình, Hà Nội",
   "Longtitude": 21.031675,
   "Latitude": 105.8223349
 },
 {
   "STT": 2,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cp Y Học Công Nghệ Cao",
   "address": "Số 59 phố Khương Trung, Khương Trung, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9999162,
   "Latitude": 105.8187318
 },
 {
   "STT": 3,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Đầu Tư Tân Thiên Hòa",
   "address": "Số 73 đường Trần Duy Hưng, Trung Hòa, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0115175,
   "Latitude": 105.8010152
 },
 {
   "STT": 4,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thế Duy",
   "address": "Số 1073 đường Giải Phóng, tổ 7, Thịnh Liệt, Hoàng Mai, Hà Nội",
   "Longtitude": 20.9739806,
   "Latitude": 105.8412922
 },
 {
   "STT": 5,
   "Name": "Phòng khám Chuyên khoa - Tai Mũi Họng",
   "address": "Số nhà 188 đường Hoàng Quốc Việt, xã Cổ Nhuế, Từ Liêm, Hà Nội",
   "Longtitude": 21.0462774,
   "Latitude": 105.7882601
 },
 {
   "STT": 6,
   "Name": "Nha Khoa Hà Thành",
   "address": "Số 03 phố Dương Quảng Hàm, Quan Hoa, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0344305,
   "Latitude": 105.799752
 },
 {
   "STT": 7,
   "Name": "Phòng khám Chuyên khoa Ngoại Tổng Hợp Vũ Hải",
   "address": "Số 311 Tây Sơn, Ngã Tư Sở, Đống Đa, Hà Nội",
   "Longtitude": 21.004206,
   "Latitude": 105.82169
 },
 {
   "STT": 8,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đỗ Xuân Tòng",
   "address": "Thị trấn Xuân Mai, Chương Mỹ, Hà Nội",
   "Longtitude": 20.896292,
   "Latitude": 105.5787167
 },
 {
   "STT": 9,
   "Name": "Phòng khám 175 Đường Giải Phóng",
   "address": "Số 175 đường Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.001069,
   "Latitude": 105.841469
 },
 {
   "STT": 10,
   "Name": "Phòng khám Chuyên khoa Nội Đặng Xuân Lâm",
   "address": "Số 20 dãy F2 ngõ 40 phố Tạ Quang Bửu, Bách Khoa, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0034137,
   "Latitude": 105.8457675
 },
 {
   "STT": 11,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 15A ngõ 119 Tây Sơn, Quang Trung, Đống Đa, Hà Nội",
   "Longtitude": 21.01181,
   "Latitude": 105.825912
 },
 {
   "STT": 12,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 103 - C1 khu tập thể Nghĩa Tân, Nghĩa Tân, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0434009,
   "Latitude": 105.7923394
 },
 {
   "STT": 13,
   "Name": "Phòng khám Chuyên khoa Tai - Mũi - Họng",
   "address": "Số 30 phố Thọ Lão, Đống Mác, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0116129,
   "Latitude": 105.8566496
 },
 {
   "STT": 14,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Tiến Lóng",
   "address": "Tổ 8, Định Công, Hoàng Mai, Hà Nội",
   "Longtitude": 20.9831371,
   "Latitude": 105.8319653
 },
 {
   "STT": 15,
   "Name": "Phòng khám Da Liễu 5 E8 Phương Mai",
   "address": "Số 5 E8 Phương Mai, Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 21.004265,
   "Latitude": 105.839187
 },
 {
   "STT": 16,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Hồng Quyên",
   "address": "Số 408 C9, Tân Mai, Hoàng Mai, Hà Nội",
   "Longtitude": 20.9836985,
   "Latitude": 105.8508339
 },
 {
   "STT": 17,
   "Name": "Nha Khoa Liên Thanh",
   "address": "Số 30A phố Hạ Hồi, Trần Hưng Đạo, Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.021521,
   "Latitude": 105.846991
 },
 {
   "STT": 18,
   "Name": "Nha Khoa Minh Thành",
   "address": "Số 143 Trung Tả, Văn Chương, Đống Đa, Hà Nội",
   "Longtitude": 21.021262,
   "Latitude": 105.8348
 },
 {
   "STT": 19,
   "Name": "Phòng khám 12",
   "address": "Số 12 Điện Biên Phủ, Điện Biên, Ba Đình, Hà Nội",
   "Longtitude": 21.0294863,
   "Latitude": 105.8424497
 },
 {
   "STT": 20,
   "Name": "Nha Khoa Nam Minh",
   "address": "Số 161 Nguyễn Lương Bằng, Quang Trung, Đống Đa, Hà Nội",
   "Longtitude": 21.01398,
   "Latitude": 105.827288
 },
 {
   "STT": 21,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Phạm Minh Tuấn",
   "address": "Số 71 A Phố Tuệ Tĩnh, Nguyễn Du, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151809,
   "Latitude": 105.848967
 },
 {
   "STT": 22,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Thị Chiến",
   "address": "Số 1 tổ 87, Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.0194294,
   "Latitude": 105.8283358
 },
 {
   "STT": 23,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Lê Ngọc Huyến",
   "address": "Xóm 5 Phú Xuyên, Phú Châu, Ba Vì, Hà Nội",
   "Longtitude": 21.2309496,
   "Latitude": 105.4244533
 },
 {
   "STT": 24,
   "Name": "Phòng Siêu Âm Khánh - Phương",
   "address": "Số 74 phố Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0314836,
   "Latitude": 105.7779625
 },
 {
   "STT": 25,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Ngọc Châm",
   "address": "Số 89 đường Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002779,
   "Latitude": 105.8415127
 },
 {
   "STT": 26,
   "Name": "Phòng khám Bệnh Hàng Bột - Hà Nội Xưa",
   "address": "Số 36 Tôn Đức Thắng, Cát Linh, Đống Đa, Hà Nội",
   "Longtitude": 21.0289812,
   "Latitude": 105.8348608
 },
 {
   "STT": 27,
   "Name": "Công Ty Tnhh Y - Cao Cầu Giấy",
   "address": "262 đường Cầu Giấy, Quan Hoa, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0345491,
   "Latitude": 105.7959956
 },
 {
   "STT": 28,
   "Name": "Nha Khoa Anh Đào",
   "address": "Số 40 phố Bát Đàn, Cửa Đông, Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0335626,
   "Latitude": 105.8463811
 },
 {
   "STT": 29,
   "Name": "Phòng khám Chuyên khoa Ung Bướu",
   "address": "11 ngõ 41 ngách 41/5 phố Đông Tác, Kim Liên, Đống Đa, Hà Nội",
   "Longtitude": 21.005407,
   "Latitude": 105.8327409
 },
 {
   "STT": 30,
   "Name": "Phòng khám Đa Khoa Nguyễn Trọng Thọ",
   "address": "Ngã Tư, Sơn Đồng, Hoài Đức, Hà Nội",
   "Longtitude": 21.047343,
   "Latitude": 105.703896
 },
 {
   "STT": 31,
   "Name": "Phòng khám Chuyên khoa Ngoại Tạ Văn Tư",
   "address": "Số 38 ngõ 167 đường Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.001228,
   "Latitude": 105.84148
 },
 {
   "STT": 32,
   "Name": "Công Ty Tnhh Y Tế Hồng Ngọc",
   "address": "B050 - Tầng B1, tòa nhà Keangnam, đường Phạm Hùng, Mễ Trì, Từ Liêm, Hà Nội",
   "Longtitude": 21.0168415,
   "Latitude": 105.7837524
 },
 {
   "STT": 33,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 34 đường Phùng Hưng, Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 34,
   "Name": "Phòng khám Chuyên khoa Nhi 76 Ngô Xuân Quảng",
   "address": "Số 1B ngõ 76 đường Ngô Xuân Quảng, thị trấn Trâu Quỳ, Gia Lâm, Hà Nội",
   "Longtitude": 21.0057327,
   "Latitude": 105.9357403
 },
 {
   "STT": 35,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "P105-IF14 khu Thành Công II, Đống Đa, Hà Nội",
   "Longtitude": 21.020889,
   "Latitude": 105.8158204
 },
 {
   "STT": 36,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 90 ngõ chợ Khâm Thiên, Trung Phụng, Đống Đa, Hà Nội",
   "Longtitude": 21.016964,
   "Latitude": 105.83847
 },
 {
   "STT": 37,
   "Name": "Phòng khám Đa Khoa Quảng Tây-Chi Nhánh Công Ty Cổ Phần Quảng Tây",
   "address": "Khu Hưng Đạo, thị trấn Tây Đằng, Ba Vì, Hà Nội",
   "Longtitude": 21.198391,
   "Latitude": 105.4245115
 },
 {
   "STT": 38,
   "Name": "Nha Khoa Khánh Vân",
   "address": "Số 101-B8 phố Tô Hiệu, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0428942,
   "Latitude": 105.796637
 },
 {
   "STT": 39,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Thu Phương",
   "address": "Nhà 1 Dãy 38 tập thể Bách Khoa, Bách Khoa, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0005974,
   "Latitude": 105.84763
 },
 {
   "STT": 40,
   "Name": "Nha Khoa Việt Sing",
   "address": "Số 37 phố Trần Đại Nghĩa, Trương Định, Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9961746,
   "Latitude": 105.8472988
 },
 {
   "STT": 41,
   "Name": "Nha Khoa Thẩm Mỹ Răng Hàm Mặt 108",
   "address": "Số 3H phố Trần Hưng Đạo, Bạch Đằng, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0186048,
   "Latitude": 105.8602519
 },
 {
   "STT": 42,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Văn Hinh",
   "address": "Số 103-H4 tập thể Thành Công, Thành Công, Ba Đình, Hà Nội",
   "Longtitude": 21.0201571,
   "Latitude": 105.8168742
 },
 {
   "STT": 43,
   "Name": "Nha Khoa 108",
   "address": "Số 107, tổ 7, thị trấn Cầu Diễn, Từ Liêm, Hà Nội",
   "Longtitude": 21.0403832,
   "Latitude": 105.7629924
 },
 {
   "STT": 44,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Thị Thanh Huyền",
   "address": "Số 80 ngõ 285 phố Đội Cấn, Liễu Giai, Ba Đình, Hà Nội",
   "Longtitude": 21.0347348,
   "Latitude": 105.8193646
 },
 {
   "STT": 45,
   "Name": "Phòng khám Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Tnhh Viện Thẩm Mỹ Hà Nội",
   "address": "Số 14 đường Yên Phụ, Trúc Bạch, Ba Đình, Hà Nội",
   "Longtitude": 21.0499227,
   "Latitude": 105.8403491
 },
 {
   "STT": 46,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả ",
   "address": "Số 50b ngõ 264 Ngọc Thụy, Ngọc Thụy, Long Biên, Hà Nội",
   "Longtitude": 21.0555737,
   "Latitude": 105.8668562
 },
 {
   "STT": 47,
   "Name": "Phòng khám Nội Khoa Tư Nhân Bác sĩ Lâm Thị Hải",
   "address": "Số 5 ngách 75 Ngõ 35, Cát Linh, Đống Đa, Hà Nội",
   "Longtitude": 21.038721,
   "Latitude": 105.8168971
 },
 {
   "STT": 48,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Xuân Sanh",
   "address": "Số 117 đường Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0023418,
   "Latitude": 105.8413058
 },
 {
   "STT": 49,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt- Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Cao Y Dược Minh Tâm.",
   "address": "P2, Số 1B Trần Hưng Đạo, Bạch Đằng, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0187507,
   "Latitude": 105.8595539
 },
 {
   "STT": 50,
   "Name": "Phòng khám Bệnh Văn Sơn",
   "address": "Tổ 5, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538645,
   "Latitude": 105.8498585
 },
 {
   "STT": 51,
   "Name": "Phòng khám Bệnh Kim Chi",
   "address": "Tổ 3, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538889,
   "Latitude": 105.8494444
 },
 {
   "STT": 52,
   "Name": "Phòng khám Bệnh Văn Nhiệm",
   "address": "Kim Thượng, Kim Lũ, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2034352,
   "Latitude": 105.9068373
 },
 {
   "STT": 53,
   "Name": "Phòng khám Bệnh M Ai Hoa",
   "address": "Khu Tập thể Z117, Đông Xuân, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2122221,
   "Latitude": 105.87173
 },
 {
   "STT": 54,
   "Name": "Phòng khám Chuyên khoa Phụ Sản -60 Nguyễn Viết Xuân",
   "address": "Số nhà 60 phố Nguyễn Viết Xuân, Khương Mai, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9979105,
   "Latitude": 105.8271385
 },
 {
   "STT": 55,
   "Name": "Phòng khám Chuyên khoa Hữu Nghị",
   "address": "Số 91 Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002696,
   "Latitude": 105.841329
 },
 {
   "STT": 56,
   "Name": "Phòng khám Chuyên khoa Hữu Nghị",
   "address": "Số 91 Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002696,
   "Latitude": 105.841329
 },
 {
   "STT": 57,
   "Name": "Phòng khám Chuyên khoa Hữu Nghị",
   "address": "Số 91 Giải Phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002696,
   "Latitude": 105.841329
 },
 {
   "STT": 58,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 32, ngách 42, ngõ 291, tổ 3, đường Lạc Long Quân, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0489081,
   "Latitude": 105.7946138
 },
 {
   "STT": 59,
   "Name": "Phòng khám Chuyên khoa Giải Phẫu Thẩm Mỹ",
   "address": "Số 2 Ngõ 52 Giang Văn Minh, Đội Cấn, Ba Đình, Hà Nội",
   "Longtitude": 21.034023,
   "Latitude": 105.827281
 },
 {
   "STT": 60,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng  Văn Khải",
   "address": "Phố Ba Thá, xã Yên Viên, Ứng Hòa, Hà Nội",
   "Longtitude": 20.8066279,
   "Latitude": 105.7098129
 },
 {
   "STT": 61,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số nhà 2, ngõ 1, Nguyễn Thái Học, Hà Đông, Hà Nội",
   "Longtitude": 20.9717104,
   "Latitude": 105.7761936
 },
 {
   "STT": 62,
   "Name": "Phòng khám Đa Khoa Số 07 Quang Vinh",
   "address": "Số 07 Thanh Ấm, thị trấn Vân Đình, Ứng Hòa, Hà Nội",
   "Longtitude": 20.7248375,
   "Latitude": 105.772542
 },
 {
   "STT": 63,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Dương Xuân Thức",
   "address": "Phố Ba Thá, xã Viên An, Ứng Hòa, Hà Nội",
   "Longtitude": 20.8056281,
   "Latitude": 105.7091024
 },
 {
   "STT": 64,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Văn Hữu",
   "address": "Số 21, tổ 7, khu Xuân Hà, thị trấn Xuân Mai, Chương Mỹ, Hà Nội",
   "Longtitude": 20.90033,
   "Latitude": 105.578998
 },
 {
   "STT": 65,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 107, thị trấn Vân Đình, Ứng Hòa, Hà Nội",
   "Longtitude": 20.7339482,
   "Latitude": 105.7703
 },
 {
   "STT": 66,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 26 phố Chùa Láng, phường Láng Thượng, Đống Đa, Hà Nội",
   "Longtitude": 21.0235829,
   "Latitude": 105.8052281
 },
 {
   "STT": 67,
   "Name": "Nha Khoa  Tuấn Vân",
   "address": "Số 67A phố Hàng Cót, phường Hàng Mã, Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0383591,
   "Latitude": 105.8472286
 },
 {
   "STT": 68,
   "Name": "Phòng khám Phương Thanh",
   "address": "Số 85 đường Giải phóng, Đồng Tâm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002862,
   "Latitude": 105.841341
 },
 {
   "STT": 69,
   "Name": "Phòng khám Chuyên khoa Nội Đào Huy Liệu",
   "address": "Thôn Canh Hoạch, xã Dân Hòa, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.810128,
   "Latitude": 105.77765
 },
 {
   "STT": 70,
   "Name": "Phòng khám Cường Long",
   "address": "Số 29, tổ 2, khu Xuân Hà, Xuân Mai, Chương Mỹ, Hà Nội",
   "Longtitude": 20.90033,
   "Latitude": 105.578998
 },
 {
   "STT": 71,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Bùi Tuấn Việt",
   "address": "Số 130 phố Mai Hắc Đế, Lê Đại Hành, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.009652,
   "Latitude": 105.850719
 },
 {
   "STT": 72,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Nha Khoa- Thanh Phong",
   "address": "Số nhà 238 Lê Lợi, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.9660609,
   "Latitude": 105.7819075
 },
 {
   "STT": 73,
   "Name": "Phòng khám Y Cao - Hà Nội",
   "address": "Thôn Canh Hoạch, xã Dân Hòa, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.810128,
   "Latitude": 105.77765
 },
 {
   "STT": 74,
   "Name": "Phòng khám Đa Khoa Quốc Tế Bảo Sơn Trực Thuộc Công Ty Tnhh Bệnh Viện Đa Khoa Bảo Sơn",
   "address": "Số 52 Nguyễn Chí Thanh, phường Láng Hạ, Đống Đa, Hà Nội",
   "Longtitude": 21.0183324,
   "Latitude": 105.8074751
 },
 {
   "STT": 75,
   "Name": "Nha Khoa Như Ngọc",
   "address": "Số 34 đường Trần Quang Diệu, Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.016033,
   "Latitude": 105.8248536
 },
 {
   "STT": 76,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 79 Thụy Khuê, Thụy Khuê, Tây Hồ, Hà Nội",
   "Longtitude": 21.0435258,
   "Latitude": 105.8165783
 },
 {
   "STT": 77,
   "Name": "Nha Khoa Á Châu",
   "address": "Số 3, đường Trần Duy Hưng, Tổ 19, Trung Hòa, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0148074,
   "Latitude": 105.8013715
 },
 {
   "STT": 78,
   "Name": "Nha Khoa - Nhật Minh",
   "address": "Số 180 phố Ngọc Trì, Thạch Bàn, Long Biên, Hà Nội",
   "Longtitude": 21.0207965,
   "Latitude": 105.911215
 },
 {
   "STT": 79,
   "Name": "Phòng khám Chuyên khoa Nhi Bùi Thị Ngọc Bích",
   "address": "Số 52 ngõ 19 phố Lạc Trung, Vĩnh Tuy, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002848,
   "Latitude": 105.8621928
 },
 {
   "STT": 80,
   "Name": "Phòng khám Tư Nhân Chuyên khoa Nhi - Bác sĩ: Lê Thị Thanh",
   "address": "Số 01 phố Nguyễn Văn Tố, Cửa Đông, Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0316892,
   "Latitude": 105.8454516
 },
 {
   "STT": 81,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "P113 nhà A3 tập thể Phương Mai, Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 21.0055084,
   "Latitude": 105.8384107
 },
 {
   "STT": 82,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 42 phố Văn Cao, Liễu Giai, Ba Đình, Hà Nội",
   "Longtitude": 21.0408281,
   "Latitude": 105.8163138
 },
 {
   "STT": 83,
   "Name": "Phòng khám Chuyên khoa Điều Dưỡng Và Phục Hồi Chức Năng Và Vật Lý Trị Liệu - Công Ty Tnhh Phát Triển Sức Khỏe Bền Vững Viethealth",
   "address": "Số 39, đường Xuân Diệu, Quảng An, Tây Hồ, Hà Nội",
   "Longtitude": 21.0662362,
   "Latitude": 105.8262993
 },
 {
   "STT": 84,
   "Name": "Phòng khám Tư Nhân  Bình Minh Khám Chuyên khoa Tâm Thần",
   "address": "Số 01 phố Lê Phụng Hiểu, Tràng Tiền, Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0263132,
   "Latitude": 105.8580558
 },
 {
   "STT": 85,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Phạm Thị Thục",
   "address": "Số 134 phố Kim Ngưu, Thanh Nhàn, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0079197,
   "Latitude": 105.8602184
 },
 {
   "STT": 86,
   "Name": "Phòng khám Sản Phụ Khoa Chuyên Nghiệp",
   "address": "Tầng 1,2, Số nhà 2, ngõ 36, phố Trung Hòa, Trung Hòa, Cầu Giấy, Hà Nội",
   "Longtitude": 21.017429,
   "Latitude": 105.801762
 },
 {
   "STT": 87,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng Việt Hùng",
   "address": "Số 65 phố Lê Văn Hưu, Ngô Thì Nhậm, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0110931,
   "Latitude": 105.8607478
 },
 {
   "STT": 88,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nha Khoa Đức",
   "address": "Số 116A phố Tuệ Tĩnh, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151325,
   "Latitude": 105.8492108
 },
 {
   "STT": 89,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 41 phố Võ Văn Dũng, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0158949,
   "Latitude": 105.824005
 },
 {
   "STT": 90,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Đức Thuấn",
   "address": "Số nhà 50, ngõ 255, đường Lĩnh Nam, phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.984417,
   "Latitude": 105.8736757
 },
 {
   "STT": 91,
   "Name": "Cơ Sở Dịch Vụ Y Tế Tư Nhân Lê Xuân Liến",
   "address": "Số 33 -D4 Tập thể dệt kim Đông Xuân, phường Đồng Nhân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0134599,
   "Latitude": 105.856577
 },
 {
   "STT": 92,
   "Name": "Phòng khám Chuyên khoa Mắt Bùi Tố Nga",
   "address": "Số 132 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0176734,
   "Latitude": 105.8491723
 },
 {
   "STT": 93,
   "Name": "Phòng khám Tai Mũi Họng - Bác sĩ : Lê Công Định",
   "address": "Số 67 phố Nam Ngư, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0264776,
   "Latitude": 105.8419525
 },
 {
   "STT": 94,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 2 ngách 25/59 Vũ Ngọc Phan ( cũ: số 2 tổ 4), phường Láng Hạ, Đống Đa, Hà Nội",
   "Longtitude": 21.0139046,
   "Latitude": 105.8121372
 },
 {
   "STT": 95,
   "Name": "Phòng khám Đa Khoa Hoa Hồng - Công Ty Cổ Phần Y Dược Hoa Hồng",
   "address": "Số 2 Triệu Quốc Đạt, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.026954,
   "Latitude": 105.847757
 },
 {
   "STT": 96,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Thị Ngân",
   "address": "Đầu cầu Đa Phúc, Sông Công, Trung Giã, Sóc Sơn, Hà Nội",
   "Longtitude": 21.3273761,
   "Latitude": 105.8772916
 },
 {
   "STT": 97,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Đăng Thắng",
   "address": "Phố Mã, Phù Ninh, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2684245,
   "Latitude": 105.8561853
 },
 {
   "STT": 98,
   "Name": "Phòng khám Nha Khoa Trang Hường",
   "address": "Số 23-đường Cổ Nhuế, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0745715,
   "Latitude": 105.7772907
 },
 {
   "STT": 99,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đinh Tuân Vũ",
   "address": "Số nhà 56, tổ 11, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0403832,
   "Latitude": 105.7629924
 },
 {
   "STT": 100,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 15 đường 40m, tổ 23, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0186765,
   "Latitude": 105.884813
 },
 {
   "STT": 101,
   "Name": "Phòng khám Chuyên khoa Mắt Lê Quang Kính",
   "address": "Tại nhà bà Nguyễn Thị Thoa, xóm Ngoài, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.0378336,
   "Latitude": 105.8457613
 },
 {
   "STT": 102,
   "Name": "Phòng khám Chuyên khoa Mắt Phạm Văn Hiệu",
   "address": "Số 24B, tổ 40, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 103,
   "Name": "Phòng khám Đa Khoa Nam Hồng Trực Thuộc Công Ty Cổ Phầm Đầu Tư Xây Dựng Và Thương Mại Bách Khoa.",
   "address": "Khu Cầu Lớn, xã Nam Hồng, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1603582,
   "Latitude": 105.7948557
 },
 {
   "STT": 104,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "46 Thái Thịnh, phường Ngã Tư Sở, Đống Đa, Hà Nội",
   "Longtitude": 21.006515,
   "Latitude": 105.821851
 },
 {
   "STT": 105,
   "Name": "Phòng khám Chuyên khoa Nội Phan Chúc Lâm",
   "address": "Số 6, ngách 34A/16 Trần Phú, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.031067,
   "Latitude": 105.8429759
 },
 {
   "STT": 106,
   "Name": "Phòng khám Anh Sơn",
   "address": "Số 245 Phố Vọng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995806,
   "Latitude": 105.842924
 },
 {
   "STT": 107,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Hào Nam",
   "address": "Số 14 phố Vũ Thạnh, phường Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.0263802,
   "Latitude": 105.8266901
 },
 {
   "STT": 108,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ- Siêu Âm Sản Phụ Khoa 83",
   "address": "Số 18 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026833,
   "Latitude": 105.8083804
 },
 {
   "STT": 109,
   "Name": "Nha Khoa Nụ Cười",
   "address": "Số 6 Võng Thị, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0516746,
   "Latitude": 105.8100627
 },
 {
   "STT": 110,
   "Name": "Phòng khám Chuyên khoa Nội Ngô Ngọc Quyên",
   "address": "Tổ dân phố 11, thị trấn Quang Minh, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.20036,
   "Latitude": 105.7742
 },
 {
   "STT": 111,
   "Name": "Nha Khoa Trang Dung",
   "address": "Số 3K đường Trần Hưng Đạo, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.018902,
   "Latitude": 105.859218
 },
 {
   "STT": 112,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "163 phố Phương Mai, phường Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 21.003318,
   "Latitude": 105.8364789
 },
 {
   "STT": 113,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 172 Mai Anh Tuấn, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0191759,
   "Latitude": 105.8201846
 },
 {
   "STT": 114,
   "Name": "Phòng khám Sản Phụ Khoa Kế Hoạch Hóa Gia Đình Hà Nội I",
   "address": "Căn hộ số 2, tầng 1, khu nhà cao tầng A4, làng quốc tế Thăng Long, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0382654,
   "Latitude": 105.7941113
 },
 {
   "STT": 115,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "88 Hoàng Hoa Thám, Thụy Khuê, Tây Hồ, Hà Nội",
   "Longtitude": 21.040473,
   "Latitude": 105.825565
 },
 {
   "STT": 116,
   "Name": "Phòng khám Nha Khoa Khánh",
   "address": "Số 277D Âu Cơ, Nhật Tân, Tây Hồ, Hà Nội",
   "Longtitude": 21.0716259,
   "Latitude": 105.8255805
 },
 {
   "STT": 117,
   "Name": "Công Ty Tnhh Phòng khám Đa Khoa Thẩm Mỹ Thanh Bình",
   "address": "Tầng 2 và 5, tòa nhà số 23, phố Cửa Đông, phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0272307,
   "Latitude": 105.8248559
 },
 {
   "STT": 118,
   "Name": "Phòng khám Bảo Gia",
   "address": "Số 34 tổ 1 ( Số mới: 58) phố Vĩnh Tuy, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0009489,
   "Latitude": 105.8729436
 },
 {
   "STT": 119,
   "Name": "Nha Khoa Đông Á",
   "address": "Số 111 Vũ Xuân Thiều, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0329112,
   "Latitude": 105.9164026
 },
 {
   "STT": 120,
   "Name": "Phòng khám Sản Phụ Khoa Hương Thủy",
   "address": "Số 77 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 121,
   "Name": "Khám Xét Nghiệm Hóa Sinh Hương Thủy",
   "address": "Số 77 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 122,
   "Name": "Phòng khám Chuyên khoa Nội Hương Thủy",
   "address": "Số 77 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 123,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 70 ( cũ: số 19C) ngõ Cẩm Văn, phường Hàng Bột, quận Đống Đa, Hà Nội",
   "Longtitude": 21.020477,
   "Latitude": 105.828706
 },
 {
   "STT": 124,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Binh",
   "address": "Số 75 đường Thanh Bình, Mộ Lao, Hà Đông, Hà Nội",
   "Longtitude": 20.9802252,
   "Latitude": 105.7828195
 },
 {
   "STT": 125,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Chu Văn Thọ",
   "address": "Số 111, A4, Tập thể cơ khí Hà Nội, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.000189,
   "Latitude": 105.816442
 },
 {
   "STT": 126,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Ngọc Lan",
   "address": "Số 35, ngõ 283, phố Đội Cấn, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0362307,
   "Latitude": 105.8191366
 },
 {
   "STT": 127,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Trung Tâm Bác sĩ Gia Đình Hà Nội",
   "address": "75 đường Hồ Mễ Trì, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0009189,
   "Latitude": 105.7930008
 },
 {
   "STT": 128,
   "Name": "Phòng khám Đa Khoa Việt Nhật- Công Ty Cổ Phần Thiết Bị Y Tế Việt Nhật",
   "address": "Biệt thự số 17 BT15, khu đô thị Pháp Vân- Tứ Hiệp, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9537375,
   "Latitude": 105.8498226
 },
 {
   "STT": 129,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Nhà số 10, tập thể Viện quy hoạch đất đai, tổ 59, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0218044,
   "Latitude": 105.790872
 },
 {
   "STT": 130,
   "Name": "Phòng khám Chuyên khoa Nội Đỗ Thúy Mai",
   "address": "Thôn Kim Hoàng, xã Vân Canh, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.033008,
   "Latitude": 105.7351523
 },
 {
   "STT": 131,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Đăng Sơn",
   "address": "Thôn Minh Hòa 4, xã Minh Khai, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.064384,
   "Latitude": 105.6740995
 },
 {
   "STT": 132,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Ngô Thị Thanh",
   "address": "Xóm Minh Khai, xã La Phù, huyện Hoài Đức, Hà Nội",
   "Longtitude": 20.9832706,
   "Latitude": 105.7310801
 },
 {
   "STT": 133,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 29-31 ngõ Thái Hà ( cũ: P103-104-F15 khu IF Thành Công), phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014895,
   "Latitude": 105.815474
 },
 {
   "STT": 134,
   "Name": "Phòng khám Chuyên khoa Hoàng Thị Minh Thu",
   "address": "Số 34A Cao Bá Quát, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0304956,
   "Latitude": 105.8380315
 },
 {
   "STT": 135,
   "Name": "Phòng khám Bệnh Thái Hà",
   "address": "Thôn Chấu, Bắc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.363571,
   "Latitude": 105.823218
 },
 {
   "STT": 136,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Đặng Thị Minh",
   "address": "Phố Chợ, Phù Lỗ, Sóc Sơn, Hà Nội",
   "Longtitude": 21.20061,
   "Latitude": 105.847
 },
 {
   "STT": 137,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Thị Hà - 75",
   "address": "Số 51 A phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995719,
   "Latitude": 105.852044
 },
 {
   "STT": 138,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Khhgđ - Siêu Âm Sản Phụ Khoa",
   "address": "Số 236 đường Nguyễn Trãi, Thôn Phùng Khoang, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 139,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Phòng 106-A25, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0452008,
   "Latitude": 105.7927793
 },
 {
   "STT": 140,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Thôn Đông Bảng, xã Đồng Thái, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2267978,
   "Latitude": 105.3990891
 },
 {
   "STT": 141,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng Minh Cự",
   "address": "Số 75 phố Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1162867,
   "Latitude": 105.4949277
 },
 {
   "STT": 142,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 121 phố Nguyễn Lương Bằng, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0157955,
   "Latitude": 105.8282941
 },
 {
   "STT": 143,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Kim Liên",
   "address": "Thôn Sen, xã Hữu Bằng, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0247936,
   "Latitude": 105.6112243
 },
 {
   "STT": 144,
   "Name": "Nha Khoa Dũng Tuyên",
   "address": "Xã Vật Lại, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2171301,
   "Latitude": 105.3936297
 },
 {
   "STT": 145,
   "Name": "Phòng khám Chuyên khoa Nội Bạch Mai - Doanh Nghiệp Tư Nhân Trung Tâm Khám Bệnh Đa Khoa Nhân Dân",
   "address": "Phố Mới, thị trấn Quốc Oai, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.9927752,
   "Latitude": 105.6401432
 },
 {
   "STT": 146,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Quách Văn Lệ",
   "address": "Xã Ba Trại, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1256787,
   "Latitude": 105.3467822
 },
 {
   "STT": 147,
   "Name": "Nha Khoa Nhật Anh",
   "address": "Tử Dương, Tô Hiệu, Thường Tín, Hà Nội",
   "Longtitude": 20.8051918,
   "Latitude": 105.8804405
 },
 {
   "STT": 148,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Kế Hoạch Hóa Gia Đình Đỗ Thị Liên",
   "address": "Cụm 8, thị trấn Phúc Thọ, Phúc Thọ, Hà Nội",
   "Longtitude": 21.1032387,
   "Latitude": 105.5441093
 },
 {
   "STT": 149,
   "Name": "Phòng khám Chuyên khoa Nội Lê Xuân Trang",
   "address": "Tổ dân phố 6, phường Yên Nghĩa, Hà Đông, Hà Nội",
   "Longtitude": 20.9537772,
   "Latitude": 105.7338133
 },
 {
   "STT": 150,
   "Name": "Phòng khám Bệnh Tư Nhân",
   "address": "Số 392 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.064142,
   "Latitude": 105.896768
 },
 {
   "STT": 151,
   "Name": "Phòng khám Chuyên khoa Nội Trần Khánh",
   "address": "Số 6, Tổ 55, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 152,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Phạm Thị Lý",
   "address": "Xóm 3, Thôn Cổ Điển, xã Hải Bối, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1112858,
   "Latitude": 105.7909905
 },
 {
   "STT": 153,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Văn Hạnh",
   "address": "Thửa số 41, khu Hồ Vạn Lộc, ven Quốc lộ 3, xã Xuân Canh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1044584,
   "Latitude": 105.8558029
 },
 {
   "STT": 154,
   "Name": "Phòng khám Tư Vấn Sức Khỏe Sinh Sản Đức Minh",
   "address": "Kiốt 02-CT 2A khu đô thị Xa La, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.962578,
   "Latitude": 105.795367
 },
 {
   "STT": 155,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Đức Đản",
   "address": "Số 56, tổ 24, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 156,
   "Name": "Nha Khoa Thăng Long - Hà Nội",
   "address": "Số 43, đường Cao Lỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1383992,
   "Latitude": 105.8565335
 },
 {
   "STT": 157,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Dãy D- Lô 2 khu 74 đường Lạc Long Quân, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0559228,
   "Latitude": 105.8089701
 },
 {
   "STT": 158,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Lê Thị Hoàn Châu",
   "address": "Số 2 phố Đội Cung, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0099613,
   "Latitude": 105.8485851
 },
 {
   "STT": 159,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Văn Nhâm",
   "address": "Số 605 Hoàng Hoa Thám, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.046382,
   "Latitude": 105.810201
 },
 {
   "STT": 160,
   "Name": "Phòng khám Chuyên khoa Nhi Trần Trí Bình",
   "address": "Tiểu khu Phú Thịnh, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7826366,
   "Latitude": 105.9143337
 },
 {
   "STT": 161,
   "Name": "Phòng khám Chuyên khoa Nội Đỗ Sỹ Đạt",
   "address": "Thôn Nhân Vực, xã Văn Nhân, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7695,
   "Latitude": 105.9237243
 },
 {
   "STT": 162,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Chu Quốc Thắng",
   "address": "Số 330 đường Bưởi, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0397757,
   "Latitude": 105.8071147
 },
 {
   "STT": 163,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Nguyễn Duy Linh",
   "address": "Số 384 Hoàng Hoa Thám, phường Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.043126,
   "Latitude": 105.814441
 },
 {
   "STT": 164,
   "Name": "Phòng khám Bác sĩ Lương - Chuyên khoa Sản",
   "address": "Phòng 502 N2D khu đô thị Trung Hòa- Nhân Chính, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0027566,
   "Latitude": 105.8002731
 },
 {
   "STT": 165,
   "Name": "Nha Khoa Thành Thu",
   "address": "Số 75 phố Sơn Tây, phường Quang Trung, Đống Đa, Hà Nội",
   "Longtitude": 21.013141,
   "Latitude": 105.826775
 },
 {
   "STT": 166,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trịnh Thị Minh Chung",
   "address": "Tiểu khu Phú Thịnh, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7826366,
   "Latitude": 105.9143337
 },
 {
   "STT": 167,
   "Name": "Nha Khoa  Hoàng Gia",
   "address": "Số 32 ( gác 3+4) phố đường Thành, phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0320763,
   "Latitude": 105.8466654
 },
 {
   "STT": 168,
   "Name": "Phòng khám - Siêu Âm: Sản Phụ Khoa  Đào Hoa",
   "address": "Số 14B phố Hội Vũ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0287281,
   "Latitude": 105.8453695
 },
 {
   "STT": 169,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Thanh",
   "address": "Tổ dân phố 4, phường La Khê, Hà Đông, Hà Nội",
   "Longtitude": 20.9724114,
   "Latitude": 105.7615252
 },
 {
   "STT": 170,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thu Thủy",
   "address": "Số 34 ngách 12/36 phố Nguyễn Văn Trỗi, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.987294,
   "Latitude": 105.8374328
 },
 {
   "STT": 171,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Đăng Thuyết",
   "address": " Đội 7, Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1267692,
   "Latitude": 105.7764757
 },
 {
   "STT": 172,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 39 ngõ 151 phố Láng Hạ, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0111973,
   "Latitude": 105.8118243
 },
 {
   "STT": 173,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 17 Trần Nhật Duật, phường Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9730327,
   "Latitude": 105.7744496
 },
 {
   "STT": 174,
   "Name": "Phòng khám Chuyên khoa Nội Đỗ Văn Siêng",
   "address": "Thôn Giẽ Thượng, xã Phú Yên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.69373,
   "Latitude": 105.8954
 },
 {
   "STT": 175,
   "Name": "Phòng khám Chuyên khoa Nội Ngô Thanh Hào",
   "address": "Thôn Đại Nghiệp, xã Tân Dân, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7399183,
   "Latitude": 105.8729599
 },
 {
   "STT": 176,
   "Name": "Phòng khám Chuyên khoa Nội Phan Thị Mỹ Hạnh",
   "address": "Số 14A ngõ Thịnh Yên, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0104104,
   "Latitude": 105.8528146
 },
 {
   "STT": 177,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Căn số 9/98 ngõ xã Đàn II, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0157203,
   "Latitude": 105.8309732
 },
 {
   "STT": 178,
   "Name": "Nha Khoa Trịnh",
   "address": "Số 46 A Trần Phú, phường Mộ Lao, Hà Đông, Hà Nội",
   "Longtitude": 20.9814108,
   "Latitude": 105.7891881
 },
 {
   "STT": 179,
   "Name": "Nha Khoa Liên",
   "address": "A38-thị trấn 5 đô thị Văn Quán, Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.972046,
   "Latitude": 105.7885049
 },
 {
   "STT": 180,
   "Name": "Đỗ Thị Đức Mai - Phòng khám Chuyên khoa Nhi",
   "address": "Số 128E phố Quan Nhân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0034035,
   "Latitude": 105.8110102
 },
 {
   "STT": 181,
   "Name": "Phòng khám Việt Mai",
   "address": "Số 25/126 phố Hoàng Văn Thái, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9965037,
   "Latitude": 105.8261259
 },
 {
   "STT": 182,
   "Name": "Dịch Vụ Y Tế Tư Nhân",
   "address": "Số 21 ngõ 99 phố Đức Giang, tổ 29 phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0701691,
   "Latitude": 105.9068373
 },
 {
   "STT": 183,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 01 Phan Đình Phùng, Yết Kiêu, Hà Đông, Hà Nội",
   "Longtitude": 21.0418153,
   "Latitude": 105.835132
 },
 {
   "STT": 184,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Tâm Phúc",
   "address": "Số 28 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026774,
   "Latitude": 105.808356
 },
 {
   "STT": 185,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 204H, phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349255,
   "Latitude": 105.8252758
 },
 {
   "STT": 186,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng - Lg",
   "address": "Số 10, ngõ 267 Hoàng Hoa Thám, phường Liễu Giai, Ba Đình, Hà Nội",
   "Longtitude": 21.0412188,
   "Latitude": 105.8189216
 },
 {
   "STT": 187,
   "Name": "Phòng khám Chuyên khoa Răng hàm mặt Nguyễn Thị Thanh Tâm",
   "address": "Số 18 nhà 10A ngõ 186 Ngọc Hà, phường Ngọc Hà, quận Ba Đình, Hà Nội",
   "Longtitude": 21.039168,
   "Latitude": 105.8287579
 },
 {
   "STT": 188,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Thị Thực 59",
   "address": "Số 742 phố Minh Khai, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0018323,
   "Latitude": 105.8703514
 },
 {
   "STT": 189,
   "Name": "Phòng khám Răng Hàm Mặt 28",
   "address": "Số 28 phố Vạn Bảo, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0341986,
   "Latitude": 105.8172682
 },
 {
   "STT": 190,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Tạ Thị Bảo Ngọc",
   "address": "P114-C7A tập thể Quỳnh Mai, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0009366,
   "Latitude": 105.8591528
 },
 {
   "STT": 191,
   "Name": "Nha Khoa Phúc An",
   "address": "Số 39 tổ 1, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0030438,
   "Latitude": 105.8282959
 },
 {
   "STT": 192,
   "Name": "Phòng khám Chuyên khoa Mắt Hoàng Thị Ngát",
   "address": "Số 165 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 193,
   "Name": "Phòng khám Nội Tim Mạch Thăng Long",
   "address": "Số 106 phố Lê Thanh Nghị, phường Bách Khoa, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 194,
   "Name": "Phòng khám Chuyên khoa Da Liễu Đinh Doãn Thạch",
   "address": "Tổ 3 Văn Phú, phường Phú La, Hà Đông, Hà Nội",
   "Longtitude": 20.9632877,
   "Latitude": 105.7682341
 },
 {
   "STT": 195,
   "Name": "Phòng khám Chuyên khoa - Sản Phụ Khoa Vân Trang",
   "address": "Số 42 đường Văn Cao, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0408281,
   "Latitude": 105.8163138
 },
 {
   "STT": 196,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Bác sĩ Hoàng Quốc Kỷ",
   "address": "Số 75 phố Tràng Thi, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0279,
   "Latitude": 105.844443
 },
 {
   "STT": 197,
   "Name": "Phòng khám Tư Nhân Chuyên khoa Răng Hàm Mặt - Bác sĩ Nguyễn Thị Ngọc Diễm",
   "address": "Số 70 phố Hàng Bồ, phường Hàng Bồ, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0337914,
   "Latitude": 105.8471712
 },
 {
   "STT": 198,
   "Name": "Thẩm Mỹ Viện Mika Vũ Thái",
   "address": "Số 2B phố Thái Phiên, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0105137,
   "Latitude": 105.8476013
 },
 {
   "STT": 199,
   "Name": "Phòng khám Nha Khoa Imed",
   "address": "Số 38 ngõ 98 Thái Hà, phường Trung Liệt, Đống Đa, Hà Nội",
   "Longtitude": 21.011665,
   "Latitude": 105.822001
 },
 {
   "STT": 200,
   "Name": "Nha Khoa Châu Thành",
   "address": "Số 36 phố Hàm Long, phường Hàng Bài, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.019413,
   "Latitude": 105.8526742
 },
 {
   "STT": 201,
   "Name": "Xét Nghiệm Vân Yến",
   "address": "Số 136 phố Lò Đúc, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0109907,
   "Latitude": 105.8596752
 },
 {
   "STT": 202,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "P107E3 Thái Thịnh, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0090826,
   "Latitude": 105.819615
 },
 {
   "STT": 203,
   "Name": "Phòng khám Chuyên khoa Nhi Nguyễn Văn Lộc - 44",
   "address": "Số 69 ngõ 19 phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002848,
   "Latitude": 105.8621928
 },
 {
   "STT": 204,
   "Name": "Thẩm Mỹ Viện Vân Yến",
   "address": "Số 136 phố Lò Đúc, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0109907,
   "Latitude": 105.8596752
 },
 {
   "STT": 205,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Thị Thu Thủy",
   "address": "Số 2XT tập thể Đại học Xây dựng, phường Bách Khoa, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0033135,
   "Latitude": 105.8433207
 },
 {
   "STT": 206,
   "Name": "Nha Khoa Hoàn Mỹ",
   "address": "Số 16 phố Lê Thanh Nghị, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 207,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Mai Anh",
   "address": "Số 42 phố Ngô Thì Nhậm, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0035913,
   "Latitude": 105.863893
 },
 {
   "STT": 208,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 59 ngõ 119 phố Hồ Đắc Di (cũ:P103A-Đ14 tập thể Nam Đồng), phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0117119,
   "Latitude": 105.8280376
 },
 {
   "STT": 209,
   "Name": "Nha Khoa Giải Phóng",
   "address": "Số 21 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 210,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Khu vực nhà để xe tầng 01-Nhà B3 Làng quốc tế Thăng Long, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0396642,
   "Latitude": 105.7921101
 },
 {
   "STT": 211,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 2B ngõ Thịnh Hào 1- Tôn Đức Thắng, phường Hàng Bột, Đống Đa, Hà Nội",
   "Longtitude": 21.0234395,
   "Latitude": 105.8321304
 },
 {
   "STT": 212,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Tạ Thị Loan",
   "address": "Số 267 phố Vĩnh Hưng, phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9891316,
   "Latitude": 105.87775
 },
 {
   "STT": 213,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đỗ Thị Song Hằng",
   "address": "Số 41-C8-thị trấn Viện Kỹ thuật- Ngõ 106- đường Hoàng Quốc Việt, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0483397,
   "Latitude": 105.7950018
 },
 {
   "STT": 214,
   "Name": "Phòng khám Sản Phụ Khoa - Khhgđ 22A",
   "address": "Số 72 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 215,
   "Name": "Phòng khám Đa Khoa Việt Hàn 2- Quầy Thuốc Việt Hàn Công Ty Cổ Phần Công Nghiệ Y Học Hồng Đức",
   "address": "Tầng 1&2, tòa nhà FLC Landmark Tower, đường Lê Đức Thọ, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0346104,
   "Latitude": 105.7714759
 },
 {
   "STT": 216,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đặng Triệu Hùng",
   "address": "Số 33/69A phố Hoàng Văn Thái, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9965037,
   "Latitude": 105.8261259
 },
 {
   "STT": 217,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Bùi Duy Sáu",
   "address": "Số 128, phố Nguyễn Ngọc Nại, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9984408,
   "Latitude": 105.8257024
 },
 {
   "STT": 218,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Mạnh Hùng",
   "address": "Phố Gốt, xã Đông Sơn, Chương Mỹ, Hà Nội",
   "Longtitude": 20.9123761,
   "Latitude": 105.6133961
 },
 {
   "STT": 219,
   "Name": "Nha Khoa Khôi Nguyên",
   "address": "Số 273 phố Bùi Xương Trạch, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9887717,
   "Latitude": 105.8180319
 },
 {
   "STT": 220,
   "Name": "Phòng khám : Tuấn - Thuyên",
   "address": "Số 26 D1 khu giãn dân Yên Phúc, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.969244,
   "Latitude": 105.788715
 },
 {
   "STT": 221,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Ngô Thị Lý",
   "address": "Số 39 Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9712372,
   "Latitude": 105.7836435
 },
 {
   "STT": 222,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đinh Thị Vĩnh",
   "address": "Số 1, ngõ 49, phố Triều Khúc, phường Thanh Xuân Nam, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9860933,
   "Latitude": 105.7983636
 },
 {
   "STT": 223,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Kim Nghĩa",
   "address": "Số 3 C2 tập thể Khương Trung, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9944556,
   "Latitude": 105.8175357
 },
 {
   "STT": 224,
   "Name": "Phòng khám 19A Hội An",
   "address": "Số 165 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 225,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Vũ Thị Sin",
   "address": "Số 167 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 226,
   "Name": "Phòng khám Chuyên khoa Nội Đỗ Huy Khánh",
   "address": "Thôn Tân Phương, Đông Phương Yên, Chương Mỹ, Hà Nội",
   "Longtitude": 20.9175933,
   "Latitude": 105.6412053
 },
 {
   "STT": 227,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Kế Hoạch Hóa Gia Đình Nguyễn Thu Hải",
   "address": "Số 23D6 khu giãn dân Yên Phúc, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.969863,
   "Latitude": 105.7883419
 },
 {
   "STT": 228,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Phạm Lệ Thu",
   "address": "Số 733 đường Quang Trung, Phú La, Hà Đông, Hà Nội",
   "Longtitude": 20.9600149,
   "Latitude": 105.7620891
 },
 {
   "STT": 229,
   "Name": "Phòng khám Sản Phụ Khoa",
   "address": "Lô 204+205 khu giãn dân Yên Phúc, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9688797,
   "Latitude": 105.7885742
 },
 {
   "STT": 230,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Trịnh Anh Hải",
   "address": "Lô 204-205 khu giãn dân Yên Phúc, Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9687813,
   "Latitude": 105.7883482
 },
 {
   "STT": 231,
   "Name": "Phòng khám Sản Phụ Khoa Số 15",
   "address": "Số 35 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 232,
   "Name": "Phòng khám Chuyên khoa Phụ Sản- Kế Hoạch Hóa Gia Đình Ngô Thị Thuyên",
   "address": "Số 26 D1 khu giãn dân Yên Phúc, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.969244,
   "Latitude": 105.788715
 },
 {
   "STT": 233,
   "Name": "Phòng khám Chuyên khoa Nội Đinh Thúy Dung",
   "address": "Phòng 111-B4, Thanh Xuân Bắc, Thanh Xuân, Hà Nội",
   "Longtitude": 20.993579,
   "Latitude": 105.7982094
 },
 {
   "STT": 234,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Thị Đông",
   "address": "Thôn Đản Dị, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1447794,
   "Latitude": 105.8527907
 },
 {
   "STT": 235,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Anh Tuấn",
   "address": "Số 13 đường Tô Hiệu, phường Nguyễn Trãi, Hà Đông, Hà Nội",
   "Longtitude": 20.9685829,
   "Latitude": 105.779849
 },
 {
   "STT": 236,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Tuấn Nam",
   "address": ", xã Vạn Điểm, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.7847147,
   "Latitude": 105.9083056
 },
 {
   "STT": 237,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Văn Nhật",
   "address": "Thôn Do Hạ, xã Tiền Phong, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1544849,
   "Latitude": 105.7701981
 },
 {
   "STT": 238,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Tập thể Bệnh viện E, tổ 54, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.050147,
   "Latitude": 105.7897569
 },
 {
   "STT": 239,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Nhà 2 phòng 18 tập thể Nam Đồng, phường Nam Đồng, Đống Đa, Hà Nội",
   "Longtitude": 21.0162421,
   "Latitude": 105.8303517
 },
 {
   "STT": 240,
   "Name": "Phòng khám Răng Hàm Mặt Bác sĩ : Lê Thị Thơm",
   "address": "Số 5 phố Quang Trung, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0240412,
   "Latitude": 105.8494612
 },
 {
   "STT": 241,
   "Name": "Nha Khoa Ba Đình",
   "address": "Số 57 phố Linh Lang, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0341385,
   "Latitude": 105.8102234
 },
 {
   "STT": 242,
   "Name": "Nha Khoa Hà Nội",
   "address": "Số 56B phố Kim Ngưu, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0043966,
   "Latitude": 105.8611706
 },
 {
   "STT": 243,
   "Name": "Nha Khoa Smile",
   "address": "Số 106-A6 Ngõ 29, phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0028514,
   "Latitude": 105.8658014
 },
 {
   "STT": 244,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phan Xuân Tần",
   "address": "Số 345, C6, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.983687,
   "Latitude": 105.848156
 },
 {
   "STT": 245,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Kế Hoạch Hóa Gia Đình Tạ Thị Xuân Lan",
   "address": "Số 18, ngách 97/24 đường Văn Cao, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0387937,
   "Latitude": 105.8166067
 },
 {
   "STT": 246,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Phạm Thị Thu - Khám Chữa Bệnh Ngoài Giờ",
   "address": "Số 24 ngõ 15 đường An Dương Vương, Tổ 45 cụm 7, phường Phú Thượng, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0872229,
   "Latitude": 105.8142936
 },
 {
   "STT": 247,
   "Name": "Phòng khám Chuyên khoa Mắt Đặng Thanh Huyền",
   "address": "Số nhà 4, ngõ 115, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0167467,
   "Latitude": 105.8024473
 },
 {
   "STT": 248,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Sỹ Khang",
   "address": "Số 45, phố Tô Hiệu, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0449773,
   "Latitude": 105.7964897
 },
 {
   "STT": 249,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 195, phố Hoa Bằng, tổ 29, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0249595,
   "Latitude": 105.795396
 },
 {
   "STT": 250,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 125, phố Phan Văn Trường, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0385645,
   "Latitude": 105.7859219
 },
 {
   "STT": 251,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 290 Thụy Khuê, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0448432,
   "Latitude": 105.8143065
 },
 {
   "STT": 252,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 2 Xuân La, Tổ 21, Cụm 3, Xuân La, Tây Hồ, Hà Nội",
   "Longtitude": 21.0596756,
   "Latitude": 105.8040796
 },
 {
   "STT": 253,
   "Name": "Nha Khoa Anh Quân",
   "address": "Số 33, đường Khương Đình, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9974282,
   "Latitude": 105.8136516
 },
 {
   "STT": 254,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế Thành Tây",
   "address": "Số 311, tổ 9, phường Kiến Hưng, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9528073,
   "Latitude": 105.7850022
 },
 {
   "STT": 255,
   "Name": "Phòng khám Chuyên khoa Nội Trịnh Quốc Cường",
   "address": "Khu tập thể Trường sỹ quan Pháo binh, xã Thanh Mỹ, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1134857,
   "Latitude": 105.4846538
 },
 {
   "STT": 256,
   "Name": "Phòng khám Chuyên khoa Nội Hà Thị Lãm",
   "address": "Số 8 - BT3 - Khu đô thị Xa La, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9622571,
   "Latitude": 105.7923394
 },
 {
   "STT": 257,
   "Name": "Phòng khám Sản Phụ Khoa Và Dịch Vụ Khhgđ- Bác sĩ : Triệu Thị Hòa",
   "address": "Số 49B phố Trần Quốc Toản, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0197147,
   "Latitude": 105.8489708
 },
 {
   "STT": 258,
   "Name": "Phòng khám Tai Mũi Họng - Bác sĩ : Đỗ Viên",
   "address": "Số 11 phố Nguyễn Gia Thiều, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.019956,
   "Latitude": 105.846628
 },
 {
   "STT": 259,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Quang Thanh",
   "address": "Phòng 103-C21, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.993579,
   "Latitude": 105.7982094
 },
 {
   "STT": 260,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phùng Hương Thầm",
   "address": "Số 93 Quang Trung, phường Quang Trung, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.134864,
   "Latitude": 105.5025028
 },
 {
   "STT": 261,
   "Name": "Phòng khám Tư Nhân Nguyễn Thị Lai",
   "address": "Số 8 phố Thịnh Yên, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0102052,
   "Latitude": 105.8534788
 },
 {
   "STT": 262,
   "Name": "Phòng khám Đa Khoa Tư Nhân Việt Life - Công Ty Cổ Phần Cẩm Hà",
   "address": "Villa 05 -thị trấn 4, Trần Văn Lai, khu Sông Đà - Mỹ Đình, phường Mỹ Đình 1, quận namtừ Liêm, Hà Nội",
   "Longtitude": 21.0169346,
   "Latitude": 105.7775273
 },
 {
   "STT": 263,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thu Hà",
   "address": "Số 39 ngõ 105 phố Thụy Khuê, phường Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0414471,
   "Latitude": 105.8242979
 },
 {
   "STT": 264,
   "Name": "Phòng khám Nha Khoa Phương Anh",
   "address": "Kiốt số 19/21 tòa nhà CT5 khu đấu giá quyền sử dụng đất Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.965788,
   "Latitude": 105.794266
 },
 {
   "STT": 265,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "189 Đặng Tiến Đông, phường Trung Liệt, Đống Đa, Hà Nội",
   "Longtitude": 21.014542,
   "Latitude": 105.821415
 },
 {
   "STT": 266,
   "Name": "Nha Khoa Dũng Tuyên",
   "address": "Số 92, đường Xuân Thủy, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0365018,
   "Latitude": 105.7855108
 },
 {
   "STT": 267,
   "Name": "Phòng khám Nha Khoa Úc Châu",
   "address": "Số 3 phố Nguyễn Du, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0181911,
   "Latitude": 105.8512654
 },
 {
   "STT": 268,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Phương",
   "address": "Số 63 đường Trương Định, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9946633,
   "Latitude": 105.8496907
 },
 {
   "STT": 269,
   "Name": "Phòng khám Chuyên khoa Nội Phú Hà",
   "address": ", xã Vật Lại, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2171301,
   "Latitude": 105.3936297
 },
 {
   "STT": 270,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Thị Liên",
   "address": "Số 56 ngõ 110 phố Kim Hoa, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0123495,
   "Latitude": 105.8371026
 },
 {
   "STT": 271,
   "Name": "Phòng khám Chuyên khoa Nội Hải Tuyết",
   "address": "Số 17 ( số cũ 102) Hoàng Cầu, phường Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.0195479,
   "Latitude": 105.8225807
 },
 {
   "STT": 272,
   "Name": "Phòng khám Chuyên khoa Nội Trần Hà",
   "address": "Số 17 phố Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.116425,
   "Latitude": 105.495867
 },
 {
   "STT": 273,
   "Name": "Phòng khám Sản Phụ Khoa Kim Chi",
   "address": "Phố Vặt, xã Vật Lại, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2171301,
   "Latitude": 105.3936297
 },
 {
   "STT": 274,
   "Name": "Phòng khám Bác sĩ Liên",
   "address": "Số 10 ngách 37/100 phố Sài Đồng, tổ 18, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0317345,
   "Latitude": 105.7950595
 },
 {
   "STT": 275,
   "Name": "Phòng khám Sản Hồng Hà",
   "address": "Tổ Bình Minh, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.01985,
   "Latitude": 105.9412
 },
 {
   "STT": 276,
   "Name": "Phòng khám Quang Minh",
   "address": "Số 124, đường Ngô Xuân Quảng, tổ dân phố Chính Trung, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0179888,
   "Latitude": 105.937002
 },
 {
   "STT": 277,
   "Name": "Nha Khoa Vương Đức",
   "address": "Số 97 phố Nguyễn Sơn, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0456924,
   "Latitude": 105.8805986
 },
 {
   "STT": 278,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 2, phố Phùng Chí Kiên, tập thể Học viện quốc phòng, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0414661,
   "Latitude": 105.8008074
 },
 {
   "STT": 279,
   "Name": "Nha Khoa Thanh Tâm",
   "address": "Kiốt số 03 tầng 1 nhà K6 khu đô thị Việt Hưng, phường Giang Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0655256,
   "Latitude": 105.9085351
 },
 {
   "STT": 280,
   "Name": "Răng Hàm Mặt Đông Đô",
   "address": "Số 32 phố Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 281,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "18 ngõ 91 Lương Đình Của (1/2 P3-B2 tập thể khu B nhà máy cơ khí nông nghiệp 1), phường Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 21.0000171,
   "Latitude": 105.8375859
 },
 {
   "STT": 282,
   "Name": "Phòng khám Sản Phụ Khoa Bác sĩ Diêm Thủy",
   "address": "P110 Nhà T2 ngõ 2 Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.003318,
   "Latitude": 105.8364789
 },
 {
   "STT": 283,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "1A-D2 tập thể Trung Tự, phường Trung Tự, Đống Đa, Hà Nội",
   "Longtitude": 21.0101838,
   "Latitude": 105.8314318
 },
 {
   "STT": 284,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trịnh Xuân Thắng",
   "address": "Số 155 Thanh Bình, phường Mộ Lao, Hà Đông, Hà Nội",
   "Longtitude": 20.9804673,
   "Latitude": 105.7789117
 },
 {
   "STT": 285,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Thị Hồng Nga",
   "address": "Số 234, phố Quan Nhân, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0000822,
   "Latitude": 105.8099233
 },
 {
   "STT": 286,
   "Name": "Răng Hàm Mặt Nha Khoa Thu Thủy",
   "address": "Số 141 Nguyễn Lương Bằng, phường Quang Trung, quận Đống Đa, Hà Nội",
   "Longtitude": 21.01426,
   "Latitude": 105.827453
 },
 {
   "STT": 287,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đinh Thị Hiền",
   "address": "Số 11B, tổ 40, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 288,
   "Name": "Phòng Xét Nghiệm Hà Thành",
   "address": "Số 383 đường Giải Phóng, phường Phương Liệt, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9925823,
   "Latitude": 105.8393044
 },
 {
   "STT": 289,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Hồ Thị Bích Nguyệt",
   "address": "Số 04 Phạm Hồng Thái, phường Quang Trung, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1391395,
   "Latitude": 105.5075756
 },
 {
   "STT": 290,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 199 tầng 4 phố Chùa Láng, phường Láng Thượng, Đống Đa, Hà Nội",
   "Longtitude": 21.0235829,
   "Latitude": 105.8052281
 },
 {
   "STT": 291,
   "Name": "Phòng khám Chuyên khoa Nội  Thận Hà Thành",
   "address": "Số 383 đường Giải Phóng, phường Phương Liệt, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9925823,
   "Latitude": 105.8393044
 },
 {
   "STT": 292,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Tổ 3, phường Láng Hạ (mới 17 ngách 51 tổ 3), Đống Đa, Hà Nội",
   "Longtitude": 21.0153717,
   "Latitude": 105.8128851
 },
 {
   "STT": 293,
   "Name": "Phòng khám 58 Chuyên khoa Sản Phụ - Khhgđ",
   "address": "Số 141 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 294,
   "Name": "Phòng khám 58 Chuyên khoa Nội",
   "address": "Số 141 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 295,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "P103-E2 Phương Mai, phường Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 21.0038817,
   "Latitude": 105.8377787
 },
 {
   "STT": 296,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Số 12 ngách 18 ngõ Hàng Bột (cũ: số 3 Phan Phú Tiên), phường Cát Linh, Đống Đa, Hà Nội",
   "Longtitude": 21.0291456,
   "Latitude": 105.8343197
 },
 {
   "STT": 297,
   "Name": "Phòng khám Chuyên khoa Nhi - Bác sĩ : Nguyễn Gia Khánh",
   "address": "Số 20 phố Hàng Hòm, phường Hàng Gai, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.032091,
   "Latitude": 105.848665
 },
 {
   "STT": 298,
   "Name": "Phòng khám Răng Hàm Mặt Niềm Tin",
   "address": "Số 153 phố Phúc Tân, phường Phúc Tân, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0353887,
   "Latitude": 105.8555889
 },
 {
   "STT": 299,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "P26-N4 Vĩnh Hồ, phường Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.0067122,
   "Latitude": 105.8205443
 },
 {
   "STT": 300,
   "Name": "Nha Khoa Hạnh Sinh",
   "address": "Số nhà 113, tổ 2, đường bờ sông Tô Lịch, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0367702,
   "Latitude": 105.8011445
 },
 {
   "STT": 301,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "P107B-B3 Trung Tự, phường Trung Tự, Đống Đa, Hà Nội",
   "Longtitude": 21.0100029,
   "Latitude": 105.833525
 },
 {
   "STT": 302,
   "Name": "Phòng khám Tư Nhân Răng Hàm Mặt Pgs-Ts: Mai Đình Hưng",
   "address": "Số 51 phố Phủ Doãn, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0296022,
   "Latitude": 105.8479929
 },
 {
   "STT": 303,
   "Name": "Phòng khám Răng Hàm Mặt -Bác sĩ Vũ Tuấn Hùng",
   "address": "Số 20 phố Hàng Muối, phường Lý Thái Tổ, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0345687,
   "Latitude": 105.8546386
 },
 {
   "STT": 304,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Việt Hải",
   "address": "Số 106 phố Ngọc Khánh, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0253816,
   "Latitude": 105.8185611
 },
 {
   "STT": 305,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Cẩm Vân",
   "address": "Số 22B, phố Chính Kinh, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.999348,
   "Latitude": 105.811382
 },
 {
   "STT": 306,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Mận",
   "address": "Số 38A ngõ 445 Lạc Long Quân, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0573795,
   "Latitude": 105.8064268
 },
 {
   "STT": 307,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Hoàng Anh",
   "address": "Phòng 106, nhà B3, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0218293,
   "Latitude": 105.8154472
 },
 {
   "STT": 308,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đào Sơn Hà",
   "address": "Số 125A phố Núi Trúc, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0288099,
   "Latitude": 105.8235621
 },
 {
   "STT": 309,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 102A phố Ngọc Hà, Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.037261,
   "Latitude": 105.830984
 },
 {
   "STT": 310,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Thị Phương Hà",
   "address": "Số 9, ngõ 170 phố Ngọc Hà, phường Ngọc Hà, quận Ba Đình, Hà Nội",
   "Longtitude": 21.038541,
   "Latitude": 105.829027
 },
 {
   "STT": 311,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 16, đường Trần Thái Tông, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0216956,
   "Latitude": 105.7753372
 },
 {
   "STT": 312,
   "Name": "Phòng khám Chuyên khoa Nhi Vũ Thị Thanh Hương",
   "address": "Số 18 Ngọc Khánh (tập thể đường sắt Ngọc Khánh), phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.029599,
   "Latitude": 105.817409
 },
 {
   "STT": 313,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Lê Minh Châu",
   "address": "Phòng 103, nhà Y2, tập thể Bộ Y tế, Số 138A, phố Giảng Võ, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0287684,
   "Latitude": 105.824832
 },
 {
   "STT": 314,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Đoàn Thị Tuấn",
   "address": "Số 8 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026812,
   "Latitude": 105.808413
 },
 {
   "STT": 315,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Phạm Thị Thúy - Số 20",
   "address": "Số 20, dốc Viện Phụ sản, đường Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268324,
   "Latitude": 105.8083744
 },
 {
   "STT": 316,
   "Name": "Phòng khám Đa Khoa Hiện Đại - Công Ty Tnhh Quốc Tế Nhật Quang",
   "address": "Số 88, ngõ Giáp Bát, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9856209,
   "Latitude": 105.841413
 },
 {
   "STT": 317,
   "Name": "Phòng Xét Nghiệm Tân Việt C.D.C",
   "address": "Số 60 phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 318,
   "Name": "Phòng khám Chuyên khoa Nội - Bác sĩ : Nguyễn Hữu Soát",
   "address": "Số 60 phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 319,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội Tim Mạch Hàn Thành Long",
   "address": "Số 106 phố Lê Thanh Nghị, phường Bách Khoa, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 320,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Ngọc Chính",
   "address": "Số 9B phố Hương Viên, phường Đồng Nhân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0115727,
   "Latitude": 105.8560889
 },
 {
   "STT": 321,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Xuân Thực",
   "address": "Số 1 ngõ 93 phố 8/3, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0000185,
   "Latitude": 105.8591846
 },
 {
   "STT": 322,
   "Name": "Nha Khoa Trung Kính",
   "address": "Số 121, phố Trung Kính, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0197774,
   "Latitude": 105.7908172
 },
 {
   "STT": 323,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Thu Thủy",
   "address": "Số 49 phố Giang Văn Minh, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0327392,
   "Latitude": 105.826978
 },
 {
   "STT": 324,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Đào",
   "address": "Số 06 ngõ 3 Nguyễn Thái Học, Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9717104,
   "Latitude": 105.7761936
 },
 {
   "STT": 325,
   "Name": "Phòng khám Nha Khoa Phước An",
   "address": "Số 43, phố Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0130095,
   "Latitude": 105.7999183
 },
 {
   "STT": 326,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Thị Hương Giang",
   "address": "Số 26 phố Ngọc Khánh, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0301393,
   "Latitude": 105.8099499
 },
 {
   "STT": 327,
   "Name": "Phòng khám Tư Nhân- Nha Khoa Thắng Lợi",
   "address": "P115-A1 tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.021683,
   "Latitude": 105.8150866
 },
 {
   "STT": 328,
   "Name": "Phòng khám Nha Khoa Anh Minh",
   "address": "107/766 đường Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0174218,
   "Latitude": 105.8037688
 },
 {
   "STT": 329,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "22 Khu A Hoàng Cầu, phường Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.0175866,
   "Latitude": 105.8238258
 },
 {
   "STT": 330,
   "Name": " Phòng khám Nhi Trương Thị Mai Hồng",
   "address": "Số 03 Võ Văn Dũng (số cũ 11 lô C tập thể Công ty Hà Thủy), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0168234,
   "Latitude": 105.8299631
 },
 {
   "STT": 331,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Viết Mão",
   "address": "Số 21 Nguyễn Thái Học, phường Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9724195,
   "Latitude": 105.7754908
 },
 {
   "STT": 332,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Đặng Thanh Vân",
   "address": "Số 189C phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349255,
   "Latitude": 105.8252758
 },
 {
   "STT": 333,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 41A phố Phan Đình Phùng, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.040567,
   "Latitude": 105.8420625
 },
 {
   "STT": 334,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Văn Tự",
   "address": "Cầu Ngăm, Phú Hạ, Minh Phú, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2625218,
   "Latitude": 105.7937151
 },
 {
   "STT": 335,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Phạm Thị Hồi Xuân",
   "address": "Quốc Lộ 2, Phú Cường, Sóc Sơn, Hà Nội",
   "Longtitude": 21.205973,
   "Latitude": 105.794447
 },
 {
   "STT": 336,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Trần Thanh Hải",
   "address": "Dược Thượng, Tiên Dược, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2562787,
   "Latitude": 105.8393044
 },
 {
   "STT": 337,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Văn Nghiêm",
   "address": "Dược Thượng, Tiên Dược, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2562787,
   "Latitude": 105.8393044
 },
 {
   "STT": 338,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Vũ Thị Liệu",
   "address": "Đường 2, Phú Minh, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2082491,
   "Latitude": 105.7879061
 },
 {
   "STT": 339,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Quốc Chấn",
   "address": "Xuân Dục, Tân Minh, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2620775,
   "Latitude": 105.8807755
 },
 {
   "STT": 340,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Trần Hữu Can",
   "address": "Xóm 3, Phù Lỗ, Sóc Sơn, Hà Nội",
   "Longtitude": 21.213715,
   "Latitude": 105.8486795
 },
 {
   "STT": 341,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Văn Tự",
   "address": "Xóm 7, Phú Xá Đoài, Phú Minh, Sóc Sơn, Hà Nội",
   "Longtitude": 21.1918695,
   "Latitude": 105.8158204
 },
 {
   "STT": 342,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Văn Minh",
   "address": "Môn Tự, Tân Dân, Sóc Sơn, Hà Nội",
   "Longtitude": 21.246338,
   "Latitude": 105.7340359
 },
 {
   "STT": 343,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Nguyễn Thị Thúy",
   "address": "Bình An, Trung Giã, Sóc Sơn, Hà Nội",
   "Longtitude": 21.319962,
   "Latitude": 105.869436
 },
 {
   "STT": 344,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Trần Trung Kiên",
   "address": "Mai Nội, Mai Đình, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2149123,
   "Latitude": 105.8400383
 },
 {
   "STT": 345,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Việt Mỹ",
   "address": "Số 158, đường Đa Tốn, Thôn Khoan Tế, xã Đa Tốn, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9803239,
   "Latitude": 105.9299022
 },
 {
   "STT": 346,
   "Name": "Nha Khoa Hồng Phong",
   "address": "Số 490 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0710799,
   "Latitude": 105.903746
 },
 {
   "STT": 347,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lưu Thị Hồng",
   "address": "Số 380 phố Ngọc Lâm, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0499119,
   "Latitude": 105.8808414
 },
 {
   "STT": 348,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 01 phố Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 349,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Kiêu Kỵ, xã Kiêu Kỵ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9809951,
   "Latitude": 105.9504318
 },
 {
   "STT": 350,
   "Name": "Phòng khám Đa Khoa - Chi Nhánh Công Ty Tnhh International Sos Việt Nam Tại Hà Nội",
   "address": "Số 51 Xuân Diệu, phường Quảng An, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0637088,
   "Latitude": 105.8274747
 },
 {
   "STT": 351,
   "Name": "Phòng khám Chuyên khoa Nhi Trần Thị Nguyệt Nga",
   "address": "Số 211 Tân Mai, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9810634,
   "Latitude": 105.849847
 },
 {
   "STT": 352,
   "Name": "Phòng khám Anh Vũ",
   "address": "Số 133 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.059086,
   "Latitude": 105.8911339
 },
 {
   "STT": 353,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Thị An",
   "address": "Tổ 3, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538889,
   "Latitude": 105.8494444
 },
 {
   "STT": 354,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Tạ Văn Sứng",
   "address": "Tổ 8- Khu C, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538889,
   "Latitude": 105.8494444
 },
 {
   "STT": 355,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Huyền",
   "address": "Tổ 6, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538916,
   "Latitude": 105.8491233
 },
 {
   "STT": 356,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 23, tổ 4, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0231982,
   "Latitude": 105.906053
 },
 {
   "STT": 357,
   "Name": "Phòng khám Chuyên khoa Nội Nghĩa Vinh",
   "address": "Tổ 06, phường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0628557,
   "Latitude": 105.8599073
 },
 {
   "STT": 358,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Huy Tú",
   "address": "Số nhà 168, tổ 1, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538445,
   "Latitude": 105.8495796
 },
 {
   "STT": 359,
   "Name": "Phòng khám Chuyên khoa Nội Phạm Thanh Trì",
   "address": "Lập Trí, Minh Trí, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2797502,
   "Latitude": 105.7453864
 },
 {
   "STT": 360,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Văn Thành",
   "address": "Đạc Tài, Mai Đình, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2441296,
   "Latitude": 105.822425
 },
 {
   "STT": 361,
   "Name": "Phòng khám Chuyên khoa Da Liễu Nguyễn Văn Tằng",
   "address": "Xóm Núi Đôi, Xuân Dục, Tân Minh, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2578339,
   "Latitude": 105.8646264
 },
 {
   "STT": 362,
   "Name": "Nha Khoa 61",
   "address": "Số 61, tổ 12, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538445,
   "Latitude": 105.8495796
 },
 {
   "STT": 363,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Trần Ngọc Cường",
   "address": "Số 88 Lê Lợi, Nguyễn Trãi, Hà Đông, Hà Nội",
   "Longtitude": 20.969989,
   "Latitude": 105.7786169
 },
 {
   "STT": 364,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 72+74 phố Hàng Bông, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0302892,
   "Latitude": 105.8470209
 },
 {
   "STT": 365,
   "Name": "Phòng khám Chuyên khoa Nhật - Việt 3",
   "address": "Số 266 tầng 1 phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0019001,
   "Latitude": 105.8416736
 },
 {
   "STT": 366,
   "Name": "Phòng khám Chuyên khoa Nhật - Việt 4",
   "address": "Số 266 tầng 2+3 phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 367,
   "Name": "Medlatec Mỹ Đình - Công Ty Tnhh Công Nghệ Và Xét Nghiệm Y Học",
   "address": "Số 118 Võ Quý Huân, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0484999,
   "Latitude": 105.749854
 },
 {
   "STT": 368,
   "Name": "Phòng khám Răng Hàm Mặt Bác sĩ Nguyễn Thị Hồng Minh",
   "address": "Số 32 phố Hàng Mành, phường Hàng Gai, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.031718,
   "Latitude": 105.848332
 },
 {
   "STT": 369,
   "Name": "Nha Khoa Liên Minh",
   "address": "P11 ngõ 30/18 phố Tạ Quang Bửu (số cũ: N3B tập thể Bách Khoa), phường Bách Khoa, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0034137,
   "Latitude": 105.8457675
 },
 {
   "STT": 370,
   "Name": "Phòng khám Chuyên khoa Nhật - Việt 1",
   "address": "Số 266 (Tầng 3) phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025988,
   "Latitude": 105.8417109
 },
 {
   "STT": 371,
   "Name": "Phòng khám Chuyên khoa Nhật - Việt 2",
   "address": "Số 266 tầng 4 phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025988,
   "Latitude": 105.8417109
 },
 {
   "STT": 372,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thanh Thái",
   "address": "Số 2 phố Yecxanh, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0156166,
   "Latitude": 105.8514261
 },
 {
   "STT": 373,
   "Name": "Nha Khoa Phú Liên",
   "address": "Số 3B phố Nguyễn Cao, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0122052,
   "Latitude": 105.860483
 },
 {
   "STT": 374,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Lợi",
   "address": "Số 482 đường Quang Trung, La Khê, Hà Đông, Hà Nội",
   "Longtitude": 20.9609437,
   "Latitude": 105.7629347
 },
 {
   "STT": 375,
   "Name": "Phòng khám Chuyên khoa Phụ Sản, Siêu Âm Sản Phụ Khoa-Bác sĩ : Dương Hồng Chương",
   "address": "Số 15 phố Liên Trì, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0215655,
   "Latitude": 105.8465606
 },
 {
   "STT": 376,
   "Name": "Phòng khám Kim Ngưu 2",
   "address": "Số 54 phố Nguyễn Xiển, phường Thanh Xuân Nam, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9903025,
   "Latitude": 105.8037143
 },
 {
   "STT": 377,
   "Name": "Khám Đa Khoa Kim Ngưu I",
   "address": "Số 124 phố Lò Đúc, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0114769,
   "Latitude": 105.859342
 },
 {
   "STT": 378,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Anh Tuấn - 76",
   "address": "Số 3 phố Trần Hưng Đạo, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0187187,
   "Latitude": 105.8594982
 },
 {
   "STT": 379,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế An Bình",
   "address": "Tầng 1 nhà C2 Mỹ Đình I, Mỹ Đình, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0320258,
   "Latitude": 105.7646889
 },
 {
   "STT": 380,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Văn Kỳ",
   "address": "Phòng 19, nhà G1, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0224017,
   "Latitude": 105.8158599
 },
 {
   "STT": 381,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số E1, Thôn cầu 7, xã Thụy Phương, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0886131,
   "Latitude": 105.7767789
 },
 {
   "STT": 382,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Đường",
   "address": "Thôn Liên Ngạc, xã Đông Ngạc, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0906849,
   "Latitude": 105.7779405
 },
 {
   "STT": 383,
   "Name": "Khám Chữa Bệnh Nội Khoa - Bác sĩ : Mai Đức Thảo",
   "address": "Số 76 phố Quán Sứ, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0257931,
   "Latitude": 105.845454
 },
 {
   "STT": 384,
   "Name": "Phòng khám Siêu Âm Mầu - 4D- Bác sĩ Chuyên khoa i Nguyễn Anh Sơn",
   "address": "Số nhà 70 đường Trần Cung, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0490446,
   "Latitude": 105.7899299
 },
 {
   "STT": 385,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Xóm Chợ, xã Đại Mỗ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.998376,
   "Latitude": 105.7589576
 },
 {
   "STT": 386,
   "Name": "Phòng khám Sản Phụ Khoa - Bác sĩ : Nguyễn Quốc Tuấn",
   "address": "Số 20, ngõ Hội Vũ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.028755,
   "Latitude": 105.8453301
 },
 {
   "STT": 387,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 10 ngõ 42 Vũ Ngọc Phan, phường Láng Hạ, Đống Đa, Hà Nội",
   "Longtitude": 21.0147603,
   "Latitude": 105.8103454
 },
 {
   "STT": 388,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "P104 - C7B Quỳnh Mai, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0012183,
   "Latitude": 105.8601535
 },
 {
   "STT": 389,
   "Name": "Nha Khoa Hà Anh",
   "address": "Số 12A ngõ Trạm, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0307638,
   "Latitude": 105.8455896
 },
 {
   "STT": 390,
   "Name": "Phòng khám Răng Hàm Mặt",
   "address": "Số nhà 70 đường Trần Cung, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0490446,
   "Latitude": 105.7899299
 },
 {
   "STT": 391,
   "Name": "Phòng khám Chuyên khoa Nhi Đào Minh Tuấn",
   "address": "Số 155B phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349255,
   "Latitude": 105.8252758
 },
 {
   "STT": 392,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 6 ngõ Thái Thịnh II, tổ 40, phường Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.0107867,
   "Latitude": 105.8178644
 },
 {
   "STT": 393,
   "Name": "Phòng khám Răng Hàm Mặt - Hoàng Dũng",
   "address": "Số 20, Đặng Trần Côn, phường Quốc Tử Giám, Đống Đa, Hà Nội",
   "Longtitude": 21.0270245,
   "Latitude": 105.8328785
 },
 {
   "STT": 394,
   "Name": "Nha Khoa Sao Việt",
   "address": "Số 469, phố Trương Định, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9847108,
   "Latitude": 105.8461613
 },
 {
   "STT": 395,
   "Name": "Phòng khám Hoàng Nhuận",
   "address": "Số 105 B7, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.983687,
   "Latitude": 105.848156
 },
 {
   "STT": 396,
   "Name": "Nha Khoa Doãn Cậy",
   "address": "Số 30 phố Mai Động, phường Mai Động, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9906005,
   "Latitude": 105.8614585
 },
 {
   "STT": 397,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Đình Kế",
   "address": "Số 104/50B phố Châu Long, phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0446123,
   "Latitude": 105.8423376
 },
 {
   "STT": 398,
   "Name": "Nha Khoa Mozart",
   "address": "Tòa nhà TID, số 4 Liễu Giai, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0368416,
   "Latitude": 105.8145075
 },
 {
   "STT": 399,
   "Name": "Nha Khoa Phương Thanh",
   "address": "Số 288A đường Giải Phóng, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9956233,
   "Latitude": 105.8405218
 },
 {
   "STT": 400,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phương Ngà",
   "address": "Số 15B phố Phan Đình Phùng, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0398355,
   "Latitude": 105.845249
 },
 {
   "STT": 401,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Đỗ Thị Hà",
   "address": "Xóm Tháp, xã Đại Mỗ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9945639,
   "Latitude": 105.7571236
 },
 {
   "STT": 402,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Trần Văn Thiệu",
   "address": "Khu tập thể Công ty Bê tông xây dựng Hà Nội, Xóm 8, xã Đông Ngạc, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0910389,
   "Latitude": 105.784756
 },
 {
   "STT": 403,
   "Name": "Phòng khám Bệnh Tư Nhân 5/78",
   "address": "5/78 đường Giải Phóng, phường Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 20.9990499,
   "Latitude": 105.8409891
 },
 {
   "STT": 404,
   "Name": "Phòng khám Răng Hàm Mặt - Bác sĩ : Nguyễn Minh Đức",
   "address": "Số 2B phố Tạ Hiện, phường Hàng Buồm, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0346841,
   "Latitude": 105.8521616
 },
 {
   "STT": 405,
   "Name": "Phòng khám Răng Hàm Mặt - Bác sĩ : Chu Thị Quỳnh Hương",
   "address": "Số 19B phố Trần Hưng Đạo, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0196151,
   "Latitude": 105.8562656
 },
 {
   "STT": 406,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đồng Văn Biểu",
   "address": "Số 18-B1 khu Đầm Trấu, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0118555,
   "Latitude": 105.8651409
 },
 {
   "STT": 407,
   "Name": "Phòng khám Bệnh Tư Nhân Số 73",
   "address": "Số 73, ngõ 189 Hoàng Hoa Thám, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.04176,
   "Latitude": 105.8207709
 },
 {
   "STT": 408,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 70, phố Yên Hòa, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.029457,
   "Latitude": 105.784013
 },
 {
   "STT": 409,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Văn Chùy",
   "address": "Số 11 hẻm 460/7/39 Thụy Khuê, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0465265,
   "Latitude": 105.8121924
 },
 {
   "STT": 410,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Thường - 46",
   "address": "Số 321A phố Bạch Mai, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 411,
   "Name": "Nha Khoa Thành Công",
   "address": "Phòng 112, nhà H1, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0200923,
   "Latitude": 105.8170588
 },
 {
   "STT": 412,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Trần Thị Xuân Thanh",
   "address": "Số 100 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9955449,
   "Latitude": 105.8520615
 },
 {
   "STT": 413,
   "Name": "Răng Hàm Mặt Minh Thu",
   "address": "Số nhà 92, phố Hoàng Ngân, tổ 37, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0088442,
   "Latitude": 105.8077008
 },
 {
   "STT": 414,
   "Name": "Phòng khám Răng Hàm Mặt - Ts: Phạm Như Hải",
   "address": "Số 01 phố Hàng Tre, phường Lý Thái Tổ, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0336069,
   "Latitude": 105.8549866
 },
 {
   "STT": 415,
   "Name": "Nha Khoa Răng Xinh",
   "address": "Số 63 phố Tạ Quang Bửu, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0018655,
   "Latitude": 105.84932
 },
 {
   "STT": 416,
   "Name": "Nha Khoa Hữu Nghị",
   "address": "Số 133 Giảng Võ, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.028723,
   "Latitude": 105.8263946
 },
 {
   "STT": 417,
   "Name": "Phòng khám Bệnh Tư Nhân Sản Phụ Khoa",
   "address": "Số 4 ngách 30 ngõ 117 phố Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0118123,
   "Latitude": 105.8195617
 },
 {
   "STT": 418,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Thị Minh Phụng",
   "address": "Số 122/18, ngõ 1, đường Khuất Duy Tiến, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.017702,
   "Latitude": 105.8114965
 },
 {
   "STT": 419,
   "Name": "Phòng khám Chuyên khoa Nội Bùi Văn Thơng",
   "address": "Thôn Minh Hòa, xã Minh Khai, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0644826,
   "Latitude": 105.6760284
 },
 {
   "STT": 420,
   "Name": "Phòng khám Tư Nhân Thụy Ứng",
   "address": "Số nhà 196 phố Tây Sơn, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0840483,
   "Latitude": 105.6711291
 },
 {
   "STT": 421,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 3 ngõ 78 đường Giải Phóng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021881,
   "Latitude": 105.8412069
 },
 {
   "STT": 422,
   "Name": "Phòng khám Nội Tiết Bác sĩ Tô Thị Kim Thoa",
   "address": "Số 26 ngách 26 Thái Thịnh 2, phường Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.0101229,
   "Latitude": 105.8187456
 },
 {
   "STT": 423,
   "Name": "Nha Khoa Thanh Xuân",
   "address": "106 - B5 tập thể Trung Tự (Cũ 31-B5), phường Trung Tự, Đống Đa, Hà Nội",
   "Longtitude": 21.0090668,
   "Latitude": 105.8336803
 },
 {
   "STT": 424,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Quỳnh Lan",
   "address": "Số 99 phố xã Đàn(cũ: số 187 tổ 47), phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0128979,
   "Latitude": 105.8354966
 },
 {
   "STT": 425,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Y Tế Việt Phúc",
   "address": "Số 69, phố Trần Xuân Soạn, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0169506,
   "Latitude": 105.8540824
 },
 {
   "STT": 426,
   "Name": "Phòng khám Chuyên khoa Nhi Trực Thuộc Công Ty Tnhh Y Tế Việt Phúc",
   "address": "Số 69, phố Trần Xuân Soạn, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0169506,
   "Latitude": 105.8540824
 },
 {
   "STT": 427,
   "Name": "Phòng khám Chuyên khoa Ngoại Trực Thuộc Công Ty Tnhh Y Tế Việt Phúc",
   "address": "Số 69, phố Trần Xuân Soạn, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0169506,
   "Latitude": 105.8540824
 },
 {
   "STT": 428,
   "Name": "Phòng khám Chuyên khoa Mắt Trịnh Thị Bích Ngọc",
   "address": "Số 117 Phố Huế, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0154714,
   "Latitude": 105.8516499
 },
 {
   "STT": 429,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình ",
   "address": "Số 54 phố Nguyễn Văn Tố, phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0318831,
   "Latitude": 105.8462947
 },
 {
   "STT": 430,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả -Vũ Thị Bích Thủy",
   "address": "Số 194 đường Nguyễn Trãi, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 431,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Văn Xiêm",
   "address": "Nhà A11- Khu đấu giá quyền sử dụng đất Liên cơ Mỹ Đình, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0389831,
   "Latitude": 105.7662088
 },
 {
   "STT": 432,
   "Name": "Phòng khám Nha Khoa 89",
   "address": "Kiốt số 6 tòa nhà CT2 khu đô thị Linh Đàm, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9708151,
   "Latitude": 105.8271747
 },
 {
   "STT": 433,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Phòng 18-A29, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0450408,
   "Latitude": 105.7921796
 },
 {
   "STT": 434,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phan Hữu Chác ",
   "address": " Số 3 phố Trần Hưng Đạo, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0187187,
   "Latitude": 105.8594982
 },
 {
   "STT": 435,
   "Name": "Nha Khoa Phúc Nguyên",
   "address": "Số 259, đường Lĩnh Nam, phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9843149,
   "Latitude": 105.8737682
 },
 {
   "STT": 436,
   "Name": "Phòng khám Chuyên khoa Nội Dư Đình Tiến",
   "address": "Số 65 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1133152,
   "Latitude": 105.4954221
 },
 {
   "STT": 437,
   "Name": "Phòng khám Răng Hàm Mặt",
   "address": "Số 58 Trần Phú, Mộ Lao, Hà Đông, Hà Nội",
   "Longtitude": 20.9811654,
   "Latitude": 105.7888931
 },
 {
   "STT": 438,
   "Name": "Phòng khám Quang Minh",
   "address": "Tổ 3, phường Yên Nghĩa, Hà Đông, Hà Nội",
   "Longtitude": 20.9584257,
   "Latitude": 105.733221
 },
 {
   "STT": 439,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả 45A Lê Thị Bưởi",
   "address": "Số 70 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 440,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả 44A Lê Thị Lợi",
   "address": "Số 181 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 441,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Quốc Chí",
   "address": "Số 83 Trương Công Định, phường Yết Kiêu, Hà Đông, Hà Nội",
   "Longtitude": 20.9734248,
   "Latitude": 105.7759397
 },
 {
   "STT": 442,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Việt - Xô",
   "address": "Số 108 đường Lương Thế Vinh, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.989774,
   "Latitude": 105.797342
 },
 {
   "STT": 443,
   "Name": "Phòng khám Nha Khoa Ngân Phượng",
   "address": "Số 100 phố Doãn Kế Thiện, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0406401,
   "Latitude": 105.7789838
 },
 {
   "STT": 444,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt  Vũ Vi Minh",
   "address": "Số 1 phố Trần Thánh Tông (Số cũ:13 dãy D Tập thể Bệnh viện 108), phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0186412,
   "Latitude": 105.858907
 },
 {
   "STT": 445,
   "Name": "Nha Khoa Hưng Phong",
   "address": "Số 01, tập thể Tổng công ty dược Việt Nam, tổ 51, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0367702,
   "Latitude": 105.8011445
 },
 {
   "STT": 446,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Đức Minh",
   "address": "Thôn Bình Đà, xã Bình Minh, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8978183,
   "Latitude": 105.7688614
 },
 {
   "STT": 447,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Hữu Niên",
   "address": "Tổ 3, thị trấn Kim Bài, Thanh Oai, Hà Nội",
   "Longtitude": 20.8559363,
   "Latitude": 105.7673942
 },
 {
   "STT": 448,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ - 83Phụ sản",
   "address": "Số 83 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0267047,
   "Latitude": 105.809491
 },
 {
   "STT": 449,
   "Name": "Phòng khám 83 Chuyên Sản Phụ Khoa, Khhgđ, Siêu Âm SPhòng khám ",
   "address": "Số 387 phố Kim Mã, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0297473,
   "Latitude": 105.8103271
 },
 {
   "STT": 450,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 67 Xuân La, Cụm 2, Xuân La, Tây Hồ, Hà Nội",
   "Longtitude": 21.0627689,
   "Latitude": 105.8073692
 },
 {
   "STT": 451,
   "Name": "Phòng khám Chuyên khoa  Răng Hàm Mặt ",
   "address": "Số 694 Hoàng Hoa Thám, Bưởi, Tây Hồ, Hà Nội",
   "Longtitude": 21.0475427,
   "Latitude": 105.8071943
 },
 {
   "STT": 452,
   "Name": "Phòng khám Nha Khoa Răng Hàm Mặt Minh Sinh",
   "address": "Số 316 Nghi Tàm, Quảng An, Tây Hồ, Hà Nội",
   "Longtitude": 21.0586757,
   "Latitude": 105.8348769
 },
 {
   "STT": 453,
   "Name": "Phòng khám Đông Hưng",
   "address": "Số nhà 41, tổ 15, phố Võ Quý Huân, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0526869,
   "Latitude": 105.7485575
 },
 {
   "STT": 454,
   "Name": "Răng Hàm Mặt Hương Sơn",
   "address": "Số 2C, ngõ 106, đường Trần Duy Hưng, tổ 5, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0107492,
   "Latitude": 105.799529
 },
 {
   "STT": 455,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 40, phố Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0165283,
   "Latitude": 105.7939102
 },
 {
   "STT": 456,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Quốc Trung",
   "address": "Số 2, lô I, tổ 76, phường Phúc Xá, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0467979,
   "Latitude": 105.8481116
 },
 {
   "STT": 457,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Đức Dũng",
   "address": "Số 97 phố Quán Thánh, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0426201,
   "Latitude": 105.841167
 },
 {
   "STT": 458,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Đinh Thị Quý",
   "address": "101-C5 Trần Huy Liệu, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026806,
   "Latitude": 105.822644
 },
 {
   "STT": 459,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 25 B7, tổ 16, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0407327,
   "Latitude": 105.896559
 },
 {
   "STT": 460,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Như Ngọc",
   "address": "Số nhà 112b, nhà A4, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0224017,
   "Latitude": 105.8158599
 },
 {
   "STT": 461,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng - 103 -H4 Thành Công",
   "address": "Phòng 103-H4 tập thể thành công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.021683,
   "Latitude": 105.8150866
 },
 {
   "STT": 462,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh",
   "address": "Số 23, phố Đốc Ngữ, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0411832,
   "Latitude": 105.813469
 },
 {
   "STT": 463,
   "Name": "Phòng khám Đa Khoa Bình Minh",
   "address": "Số 101 - 103 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025061,
   "Latitude": 105.8415395
 },
 {
   "STT": 464,
   "Name": "Phòng khám Đa Khoa Tuyết Thái - Công Ty Cổ Phần Phòng khám Tuyết Thái",
   "address": "Số 92 Thợ Nhuộm, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.022489,
   "Latitude": 105.849125
 },
 {
   "STT": 465,
   "Name": "Phòng khám Phẫu Thuật Thẩm Mỹ",
   "address": "Số 76 phố Đại Cồ Việt, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.008286,
   "Latitude": 105.847256
 },
 {
   "STT": 466,
   "Name": "Phòng Siêu Âm Chẩn Đoán - Nguyễn Văn Sang",
   "address": "Số 17, ngõ 198, đường Trần Cung, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0498837,
   "Latitude": 105.7909006
 },
 {
   "STT": 467,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 387 phố Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 468,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Ngọc Lan",
   "address": "Số 4 khu đô thị 4A, phường La Khê, Hà Đông, Hà Nội",
   "Longtitude": 20.9745524,
   "Latitude": 105.7541892
 },
 {
   "STT": 469,
   "Name": "Phòng khám Tư Nhân Phủ Lỗ",
   "address": "Số nhà 6, đường Quốc lộ 3, Cầu Vuông, xóm Đông, Phù Lỗ, Sóc Sơn, Hà Nội",
   "Longtitude": 21.1688203,
   "Latitude": 105.9134199
 },
 {
   "STT": 470,
   "Name": "Phòng khám Đa Khoa Việt - Sing Trực Thuộc Công Ty Cổ Phần Y Học Rạng Đông",
   "address": "Số 83B Lý Thường Kiệt, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0249117,
   "Latitude": 105.8433693
 },
 {
   "STT": 471,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 20 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9698814,
   "Latitude": 105.7847973
 },
 {
   "STT": 472,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Võ Hoàng Anh",
   "address": "Số 15 Trần Phú, phường Văn Quán, Hà Đông, Hà Nội",
   "Longtitude": 20.9827231,
   "Latitude": 105.7908028
 },
 {
   "STT": 473,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Thị Thanh Nhơn",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 474,
   "Name": "Phòng khám Răng - Hàm - Mặt 296 Ngọc Lâm",
   "address": "Số 296 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0465837,
   "Latitude": 105.8712837
 },
 {
   "STT": 475,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": " Số 14, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 476,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 58 Vũ Xuân Thiều, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.037445,
   "Latitude": 105.921835
 },
 {
   "STT": 477,
   "Name": "Phòng khám Chuyên khoa Nội Lê Văn Bào",
   "address": "Thôn Tiên Hội, xã Đông Hội, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1010566,
   "Latitude": 105.8632064
 },
 {
   "STT": 478,
   "Name": "Phòng khám Bệnh Tư Nhân",
   "address": "Số 843 đường Nguyễn Văn Linh, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0311794,
   "Latitude": 105.9121031
 },
 {
   "STT": 479,
   "Name": "Phòng khám Chuyên khoa Nội Dương Mạnh Hùng",
   "address": "Số 69, khu Hòa Sơn, Chúc Sơn, Chương Mỹ, Hà Nội",
   "Longtitude": 20.91966,
   "Latitude": 105.7003
 },
 {
   "STT": 480,
   "Name": "Phòng khám Thành Ngọc",
   "address": "Số 05 ngõ 366 phố Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0480328,
   "Latitude": 105.8743121
 },
 {
   "STT": 481,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Đức Tựa",
   "address": "Số 39 A đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 482,
   "Name": "Phòng khám Chuyên khoa Nội Sông Nhuệ",
   "address": "Số 143 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 483,
   "Name": "Phòng khám Bệnh Tư Nhân",
   "address": "Số nhà 36 ngõ 118 tổ 5, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.0371784,
   "Latitude": 105.8730678
 },
 {
   "STT": 484,
   "Name": "Phòng khám Chuyên khoa Nhi Hồ Sỹ Công",
   "address": "Đường 3, Phù Lỗ, Sóc Sơn, Hà Nội",
   "Longtitude": 21.1964493,
   "Latitude": 105.843708
 },
 {
   "STT": 485,
   "Name": "Phòng khám Chuyên khoa Ngoại 146",
   "address": "Số 146 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9683829,
   "Latitude": 105.7863474
 },
 {
   "STT": 486,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Dương Lộc",
   "address": "Số 9 ngõ Giao Thông, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.998129,
   "Latitude": 105.7989541
 },
 {
   "STT": 487,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Thành Long - 63",
   "address": "Số 18 phố Yên Lạc, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001023,
   "Latitude": 105.8633526
 },
 {
   "STT": 488,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đặng Thị Vỹ",
   "address": "Số 97 phố Sơn Tây, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0328154,
   "Latitude": 105.8303167
 },
 {
   "STT": 489,
   "Name": "Nha Khoa Minh Huệ",
   "address": "Số 60B phố Lê Duẩn, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0247546,
   "Latitude": 105.8414851
 },
 {
   "STT": 490,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thành Công",
   "address": "Số 202 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 491,
   "Name": "Phòng khám Bệnh Chuyên khoa Nội - Bác sĩ : Đào Ngọc Ca",
   "address": "B9 tập thể Bờ Sông, phường Chương Dương, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0217307,
   "Latitude": 105.8537251
 },
 {
   "STT": 492,
   "Name": "Phòng khám Bệnh Tư Nhân - Bác sĩ: Quách Đình Tuấn",
   "address": "Số 92 phố Hàng Bạc, phường Hàng Bạc, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0335455,
   "Latitude": 105.852324
 },
 {
   "STT": 493,
   "Name": "Phòng khám Chuyên khoa Mắt Đỗ Quang Ngọc",
   "address": "Số 97 phố Sơn Tây, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0328154,
   "Latitude": 105.8303167
 },
 {
   "STT": 494,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Tuấn Anh - 59",
   "address": "P102-C3 Quỳnh Mai, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0010204,
   "Latitude": 105.8597735
 },
 {
   "STT": 495,
   "Name": "Phòng khám Chuyên khoa Phụ Sản, Siêu Âm Sản Phụ Khoa - Bác sĩ : Phạm Thanh Nga",
   "address": "Ngõ 42C (cổng sau số 5 phố Quang Trung) phố Lý Thường Kiệt, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0246893,
   "Latitude": 105.8495056
 },
 {
   "STT": 496,
   "Name": "Phòng khám Chuyên khoa Nội Đặng Phòng",
   "address": "Số 223 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 497,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Chi Nhánh Công Ty Cổ Phần Nha Khoa Bắc Nam",
   "address": "Số 42 Cửa Đông, phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0348909,
   "Latitude": 105.845135
 },
 {
   "STT": 498,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 19, hẻm 27, ngách 26, ngõ 61, đường Trần Duy Hưng, tổ 41, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0107492,
   "Latitude": 105.799529
 },
 {
   "STT": 499,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Văn Sơn",
   "address": "Số 18B Hồ Giám (cũ: số 18 đường 215 Tôn Đức Thắng), phường Quốc Tử Giám, Đống Đa, Hà Nội",
   "Longtitude": 21.021715,
   "Latitude": 105.83149
 },
 {
   "STT": 500,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "200A tổ 6, Khương Thượng, Đống Đa, Hà Nội",
   "Longtitude": 21.0180725,
   "Latitude": 105.8299495
 },
 {
   "STT": 501,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Quốc Dũng",
   "address": "Số 54, tổ 53, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0339527,
   "Latitude": 105.7850022
 },
 {
   "STT": 502,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 91 ngõ 508 đường Láng, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104095,
   "Latitude": 105.812094
 },
 {
   "STT": 503,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Xuân Hòa",
   "address": "Lô 149 Tổ 8, phường Mộ Lao, Hà Đông, Hà Nội",
   "Longtitude": 20.9854492,
   "Latitude": 105.7836265
 },
 {
   "STT": 504,
   "Name": "Phòng khám Chuyên khoa Sản Phụ",
   "address": "Tổ 5, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0630011,
   "Latitude": 105.894919
 },
 {
   "STT": 505,
   "Name": "Phòng khám Sản Phụ Khoa Thành Công",
   "address": "P104 - G4 tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0205372,
   "Latitude": 105.8169535
 },
 {
   "STT": 506,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình ",
   "address": "Số nhà 3, ngõ 381, ngách 81, đường Nguyễn Khang, tổ 12, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0272916,
   "Latitude": 105.7970669
 },
 {
   "STT": 507,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Trần Thị Thành - 25",
   "address": "Số 48 phố Cửa Bắc, phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0442948,
   "Latitude": 105.8429581
 },
 {
   "STT": 508,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Và Kế Hoạch Hóa Gia Đình",
   "address": " Tổ 7, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0311075,
   "Latitude": 105.8546737
 },
 {
   "STT": 509,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 7 Thái Hà, phường Trung Liệt, Đống Đa, Hà Nội",
   "Longtitude": 21.0103319,
   "Latitude": 105.8224546
 },
 {
   "STT": 510,
   "Name": "Phòng khám 36 Tuệ Tĩnh - Công Ty Cổ Phần Viện Mắt Quốc Tế Việt - Nga",
   "address": "Số 36 phố Tuệ Tĩnh, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.015125,
   "Latitude": 105.8503804
 },
 {
   "STT": 511,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 41 ngách 37 ngõ 167 Tây Sơn (cũ: Số 41 nhà I tập thể Đại học Công đoàn), phường Quang Trung, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0105499,
   "Latitude": 105.8269658
 },
 {
   "STT": 512,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Nội Tiết",
   "address": "P2-B20 tập thể Yên Lãng, phường Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.0114953,
   "Latitude": 105.8166664
 },
 {
   "STT": 513,
   "Name": "Khám Chữa Bệnh Chuyên khoa Nội Hồng Hải",
   "address": "Số 25/159 Pháo Đài Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.018102,
   "Latitude": 105.804155
 },
 {
   "STT": 514,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 1 -5 nhà A1 - T5C tổ 59, phường Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.0200535,
   "Latitude": 105.8246266
 },
 {
   "STT": 515,
   "Name": "Phòng Xét Nghiệm Hóa Sinh Và Phòng Xét Nghiệm Huyết Học - Công Ty Tnhh Thương Mại Và Dịch Vụ Quang Ngọc",
   "address": "Số 2, ngõ 167, đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0015414,
   "Latitude": 105.841679
 },
 {
   "STT": 516,
   "Name": "Phòng Xét Nghiệm Hóa Sinh Và Phòng Xét Nghiệm Huyết Học - Công Ty Tnhh Thương Mại Và Dịch Vụ Quang Ngọc",
   "address": "Số 2, ngõ 167, đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0015414,
   "Latitude": 105.841679
 },
 {
   "STT": 517,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Nội Tiết",
   "address": "Số 60 ngách 26 ngõ Thái Thịnh 2, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104507,
   "Latitude": 105.8174745
 },
 {
   "STT": 518,
   "Name": "Nha Khoa Việt - Hàn",
   "address": "Số 199 phố Lò Đúc, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0098191,
   "Latitude": 105.8608161
 },
 {
   "STT": 519,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trịnh Hùng Mạnh",
   "address": "Số 135 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.99545,
   "Latitude": 105.854828
 },
 {
   "STT": 520,
   "Name": "Xét Nghiệm Huyết Học",
   "address": "Số 106 phố Lê Thanh Nghị, phường Bách Khoa, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 521,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Cao Phong",
   "address": "P101 nhà C4 tập thể Quỳnh Mai, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0008348,
   "Latitude": 105.8606695
 },
 {
   "STT": 522,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Văn Hải",
   "address": "Số 01 ngõ 231 phố Khâm Thiên, phường Thổ Quan, quận Đống Đa, Hà Nội",
   "Longtitude": 20.9978926,
   "Latitude": 105.8463412
 },
 {
   "STT": 523,
   "Name": "Phòng khám Chuyên khoa Mắt Vũ Đình Hy",
   "address": "Số 134 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 524,
   "Name": "Nha Khoa Minh Tâm",
   "address": "Số 46 đường Kim Giang, phường Kim Giang, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.98457,
   "Latitude": 105.814254
 },
 {
   "STT": 525,
   "Name": "Nha Khoa Thúy Đức",
   "address": "Số 64 phố Vọng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 20.998667,
   "Latitude": 105.842125
 },
 {
   "STT": 526,
   "Name": "Phòng khám Nha Khoa West Coast - Hà Nội Trực Thuộc Chi Nhánh Công Ty Tnhh Một Thành Viên Chăm Sóc Sức Khỏe Bờ Biển Tây Hà Nội",
   "address": "Khu A, tầng 2, tòa nhà Syrena, số 51 phố Xuân Diệu, phường Quảng An, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0641172,
   "Latitude": 105.8280828
 },
 {
   "STT": 527,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Tế Tiêu",
   "address": "Ngã năm chợ Tế Tiêu, thị trấn Đại Nghĩa, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6852929,
   "Latitude": 105.7414287
 },
 {
   "STT": 528,
   "Name": "Phòng khám Răng Số 1 - Công Ty Tnhh Nga Hải",
   "address": "Số 8 ngõ 27 Xuân Diệu, phường Quảng An, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0628635,
   "Latitude": 105.8284271
 },
 {
   "STT": 529,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 25A, đường Trần Duy Hưng, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0089898,
   "Latitude": 105.7981182
 },
 {
   "STT": 530,
   "Name": "Phòng khám (Số 11) Chuyên khoa : Răng - Hàm - Mặt",
   "address": "Nhà số 11, phố Doãn Kế Thiện, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0414805,
   "Latitude": 105.7772575
 },
 {
   "STT": 531,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 101, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0285645,
   "Latitude": 105.7995097
 },
 {
   "STT": 532,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số nhà 12A, ngõ 2, tổ 18, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0403832,
   "Latitude": 105.7629924
 },
 {
   "STT": 533,
   "Name": "Phòng khám Nội Khoa - Nội Soi Tiêu Hóa",
   "address": "Số 17, ngõ 198, phố Trần Cung, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0500238,
   "Latitude": 105.7919326
 },
 {
   "STT": 534,
   "Name": "Nha Khoa Đức Toàn I",
   "address": "Số 87 phố Thái Thịnh, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.010521,
   "Latitude": 105.818389
 },
 {
   "STT": 535,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Trust Health Care",
   "address": "Số nhà A26-1, ngõ 91 phố Trần Duy Hưng, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0103034,
   "Latitude": 105.8012152
 },
 {
   "STT": 536,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 41 Tôn Đức Thắng, phường Quốc Tử Giám, Đống Đa, Hà Nội",
   "Longtitude": 21.026686,
   "Latitude": 105.833758
 },
 {
   "STT": 537,
   "Name": "Phòng khám Chữa Bệnh Chuyên khoa Sản Phụ Khoa",
   "address": "Số 83 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0267047,
   "Latitude": 105.809491
 },
 {
   "STT": 538,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Long Đỏ",
   "address": "192 Khâm Thiên, phường Thổ Quan, Đống Đa, Hà Nội",
   "Longtitude": 21.0192746,
   "Latitude": 105.8357261
 },
 {
   "STT": 539,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "17 ngõ 35 Nguyễn Như Đổ, phường Văn Miếu, Đống Đa, Hà Nội",
   "Longtitude": 21.02654,
   "Latitude": 105.8390919
 },
 {
   "STT": 540,
   "Name": "Phòng khám Chuyên khoa Nội Đức Minh",
   "address": "Số 48 ngách 26 ngõ Thái Thịnh 2, phường Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.0104507,
   "Latitude": 105.8174745
 },
 {
   "STT": 541,
   "Name": "Phòng khám Nha Khoa Thanh Phương",
   "address": "Số 148 phố Nguyễn Khuyến, phường Văn Miếu, quận Đống Đa, Hà Nội",
   "Longtitude": 21.028719,
   "Latitude": 105.83668
 },
 {
   "STT": 542,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nụ Cười (Smile Dental Clinic)",
   "address": "Số 1 phố Hạ Đình, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9882578,
   "Latitude": 105.8085515
 },
 {
   "STT": 543,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Quốc Toàn",
   "address": "Số 193 phố Vũ Hữu, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9989308,
   "Latitude": 105.7954338
 },
 {
   "STT": 544,
   "Name": "Nha Khoa Bảo Việt - Công Ty Tnhh Chăm Sóc Sức Khỏe Bảo Việt",
   "address": "Tầng 1, tòa nhà HACINCO B3.7, phố Hoàng Đạo Thúy, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0043234,
   "Latitude": 105.8053186
 },
 {
   "STT": 545,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Văn Phú",
   "address": "Số 8, phố Nguyễn An Ninh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9892555,
   "Latitude": 105.8458991
 },
 {
   "STT": 546,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Thị Hạnh - 67",
   "address": "Số 2 M16, ngõ 104 Nguyễn An Ninh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9910325,
   "Latitude": 105.8460933
 },
 {
   "STT": 547,
   "Name": "Răng Hàm Mặt Gia Phương 24",
   "address": "Số 24, ngách 136/8 phố Thịnh Liệt, phường Thịnh Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9758621,
   "Latitude": 105.8461391
 },
 {
   "STT": 548,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Thành",
   "address": "Số 2-CT9 khu Thương Mại CH tổ 52, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.985563,
   "Latitude": 105.8340753
 },
 {
   "STT": 549,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lò Thị Hà",
   "address": "Kiốt số 03, tầng 1, nhà chung cư A5, 15 tầng, chung cư Đại Kim - Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.974829,
   "Latitude": 105.825415
 },
 {
   "STT": 550,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Xuân Thủy",
   "address": "Số 7 M3 thị trấn 6 Bắc Linh Đàm, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9696874,
   "Latitude": 105.830065
 },
 {
   "STT": 551,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Khanh",
   "address": "Số nhà 55, tổ dân phố Ga, thị trấn Thường Tín, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8721772,
   "Latitude": 105.8645141
 },
 {
   "STT": 552,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Văn Ninh",
   "address": ", xã Nhị Khê, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8951043,
   "Latitude": 105.843708
 },
 {
   "STT": 553,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Kiều Văn Oanh",
   "address": "Số nhà 45, khu chung cư Quân đội, Thôn Mi Hạ, xã Thanh Mai, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8611719,
   "Latitude": 105.7470078
 },
 {
   "STT": 554,
   "Name": "Phòng khám Đa Khoa Thiên Tâm - Công Ty Cổ Phần Dịch Vụ Y Tế Thiên Tâm",
   "address": "Số 212 Nguyễn Lương Bằng, phường Quang Trung, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0143246,
   "Latitude": 105.8273184
 },
 {
   "STT": 555,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Phạm Việt Hồng",
   "address": "Số 7 ngách 1/26 ngõ 1 phố Phan Đình Giót, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9899314,
   "Latitude": 105.840493
 },
 {
   "STT": 556,
   "Name": "Phòng khám Chuyên khoa Nội Đỗ Thế Hùng",
   "address": "Số 27 ngõ 345 phố Khương Trung, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9931513,
   "Latitude": 105.8180283
 },
 {
   "STT": 557,
   "Name": "Cơ Sở Dịch Vụ Y Tế - Khắc Thạnh",
   "address": "P12 ngõ 3 phố Hoàng Đạo Thành, phường Kim Giang, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0348064,
   "Latitude": 105.8163337
 },
 {
   "STT": 558,
   "Name": "Phòng khám Kim Ngưu 2",
   "address": "Số 54 phố Nguyễn Xiển, phường Thanh Xuân Nam, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9903025,
   "Latitude": 105.8037143
 },
 {
   "STT": 559,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Trịnh Thị Lan",
   "address": "Số 5, phố Nguyễn Quý Đức, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9912458,
   "Latitude": 105.8000904
 },
 {
   "STT": 560,
   "Name": "Phòng khám Chuyên khoa Nhi Trực Thuộc Công Ty Cổ Phần Thông Minh Cha Và Con",
   "address": "Số nhà 45 phố Hoàng Ngân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0069561,
   "Latitude": 105.8108616
 },
 {
   "STT": 561,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng An Bình",
   "address": "Phòng 107 - nhà E5 tập thể Thanh Xuân Bắc, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.990202,
   "Latitude": 105.799538
 },
 {
   "STT": 562,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Phạm Tiến Dũng",
   "address": "A4 thị trấn 2 Bắc Linh Đàm, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.983845,
   "Latitude": 105.8574239
 },
 {
   "STT": 563,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Lưu Vân Anh",
   "address": "Số 4K20 ngõ 63 Nguyễn An Ninh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9903942,
   "Latitude": 105.8450993
 },
 {
   "STT": 564,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Nội Tiết",
   "address": "Số A6 lô 15 Khu đô thị mới Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9877167,
   "Latitude": 105.8315992
 },
 {
   "STT": 565,
   "Name": "Phòng khám Chuyên  Khoa Nội",
   "address": "Thôn Ngọc Hồi, xã Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.921933,
   "Latitude": 105.8510475
 },
 {
   "STT": 566,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Dự Quy",
   "address": "Số nhà 151 tổ 3, thị trấn Kim Bài, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8559363,
   "Latitude": 105.7673942
 },
 {
   "STT": 567,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 9 phố Đặng Tiến Đông, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0108183,
   "Latitude": 105.8249416
 },
 {
   "STT": 568,
   "Name": "Phòng khám Đa Khoa Thanh Trì Hà Nội - Chi Nhánh Công Ty Tnhh Duy Thịnh",
   "address": "Km 12, quốc lộ 1, xã Tứ Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.949083,
   "Latitude": 105.843798
 },
 {
   "STT": 569,
   "Name": "Bác sĩ Nguyễn Văn Dĩ - Phòng khám Chữa Răng Hàm Mặt",
   "address": "Số 23A phố Hội Vũ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0287281,
   "Latitude": 105.8453695
 },
 {
   "STT": 570,
   "Name": "Nha Khoa Huệ Xuyên",
   "address": "Nhà số 110, đường Hồ Tùng Mậu, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.036957,
   "Latitude": 105.776906
 },
 {
   "STT": 571,
   "Name": "Phòng khám Chuyên khoa Mắt Mai Thịnh",
   "address": "Số 110 (cũ: số 34A) nhà B1 tập thể Khương Thượng, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021606,
   "Latitude": 105.8311626
 },
 {
   "STT": 572,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 112 ngõ Thịnh Quang (cũ: tổ 13C), phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.007474,
   "Latitude": 105.8182525
 },
 {
   "STT": 573,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Đức Chuyên",
   "address": "Số 105 nhà C1 tập thể Vĩnh Hồ, Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.007201,
   "Latitude": 105.8198005
 },
 {
   "STT": 574,
   "Name": "Phòng khám Bảo Tâm",
   "address": "Số 208 ngõ 354 Trường Chinh, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0015208,
   "Latitude": 105.826149
 },
 {
   "STT": 575,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Ngọc Phấn",
   "address": "Phòng 116-H2 tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.020229,
   "Latitude": 105.815989
 },
 {
   "STT": 576,
   "Name": "Phòng khám Chuyên khoa Nhi Nguyễn Thị Quý Linh",
   "address": "Số 6 ngõ 314 đường Lạc Long Quân, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0627926,
   "Latitude": 105.8100872
 },
 {
   "STT": 577,
   "Name": "Phòng khám Sản Phụ Khoa - 16 Dốc Phụ Sản",
   "address": "Số 16, dốc Bệnh viện phụ sản, Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0267649,
   "Latitude": 105.8073191
 },
 {
   "STT": 578,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Hòa - 83",
   "address": "Số 86 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268932,
   "Latitude": 105.8087146
 },
 {
   "STT": 579,
   "Name": "Phòng khám Sản Phụ Khoa Tuyệt Đối Chuyên Nghiệp",
   "address": "Số 89B, dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268939,
   "Latitude": 105.8092321
 },
 {
   "STT": 580,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Phạm Tuấn Cảnh",
   "address": "Số 32 phố Cao Bá Quát, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.030468,
   "Latitude": 105.838447
 },
 {
   "STT": 581,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Dương Thị Chân Phương",
   "address": "Số 178D đường Trương Định, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9946633,
   "Latitude": 105.8496907
 },
 {
   "STT": 582,
   "Name": "Nha Khoa Bạch Mai",
   "address": "Số 124, phố Trần Huy Liệu, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0297562,
   "Latitude": 105.8199593
 },
 {
   "STT": 583,
   "Name": "Nha Khoa Toàn Khánh",
   "address": "Số 81A đường Âu Cơ, phường Tứ Liên, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0618861,
   "Latitude": 105.8324194
 },
 {
   "STT": 584,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 121 phố Đốc Ngữ, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0411832,
   "Latitude": 105.813469
 },
 {
   "STT": 585,
   "Name": "Nha Khoa Hòa Phương",
   "address": "Số 293 Hoàng Hoa Thám, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0420771,
   "Latitude": 105.8186031
 },
 {
   "STT": 586,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Ngọc Quang",
   "address": "Phòng 3, nhà B5, số 28 Điện Biên Phủ, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0325614,
   "Latitude": 105.8394679
 },
 {
   "STT": 587,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Tạ Quốc Đại",
   "address": "Số 70 phố Phúc Xá, phường Phúc Xá, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0478999,
   "Latitude": 105.8468498
 },
 {
   "STT": 588,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trương Uyên Thái",
   "address": "Số 66B, phố Nguyễn Thái Học, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.030332,
   "Latitude": 105.836481
 },
 {
   "STT": 589,
   "Name": "Phòng khám Đa Khoa Elise - Công Ty Cổ Phần Y Tế Phú Hưng",
   "address": "Khu đất 3A, dự án KTKĐ 62, đường Trường Chinh, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.997838,
   "Latitude": 105.840966
 },
 {
   "STT": 590,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 40, tổ 58, phố Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.018517,
   "Latitude": 105.7928367
 },
 {
   "STT": 591,
   "Name": "Phòng khám Tai Mũi Họng Thanh Thủy",
   "address": "F102 nhà B4 (số cũ căn hộ 35-B4) tập thể Trung Tự, phường Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0013021,
   "Latitude": 105.8316213
 },
 {
   "STT": 592,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Công Nghệ Y Học Hồng Đức",
   "address": "Số 9, Ngô Thì Nhậm, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.017988,
   "Latitude": 105.8529609
 },
 {
   "STT": 593,
   "Name": "Phòng khám Răng Tuấn Vân",
   "address": "Số 53 phố Tràng Thi, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 594,
   "Name": "Phòng khám Việt An",
   "address": "Số 54C phố Trần Quốc Toản, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0203359,
   "Latitude": 105.8466325
 },
 {
   "STT": 595,
   "Name": "Phòng khám Răng Hàm Mặt - Bác sĩ : Lương Quốc Ninh",
   "address": "Số 53 (cổng sau 54 Quán Sứ) phố Tràng Thi, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0272674,
   "Latitude": 105.8455488
 },
 {
   "STT": 596,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 50 phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 597,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Trực Thuộc Công Ty Tnhh Phát Triển Sức Khỏe Bền Vững Viethealth",
   "address": "Số 114 đường Đê La Thành, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0138973,
   "Latitude": 105.8350499
 },
 {
   "STT": 598,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Số 54C phố Trần Quốc Toản, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0203359,
   "Latitude": 105.8466325
 },
 {
   "STT": 599,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Vũ Thị Hảo - 52",
   "address": "Số 95 phố Mai Hắc Đế, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0129699,
   "Latitude": 105.8511059
 },
 {
   "STT": 600,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả - Ktv: Lê Thị Liên",
   "address": "Số 66 phố Hàng Bạc, phường Hàng Bạc, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0329432,
   "Latitude": 105.8528824
 },
 {
   "STT": 601,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Lê Thị Phương Lan",
   "address": "Số 193G phố Bà Triệu, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0105679,
   "Latitude": 105.8492663
 },
 {
   "STT": 602,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "P1/111 Láng Hạ (cũ 206A-N21 Láng Hạ), phường Láng Hạ, Đống Đa, Hà Nội",
   "Longtitude": 21.011757,
   "Latitude": 105.812401
 },
 {
   "STT": 603,
   "Name": "Phòng khám Chữa Bệnh Nội Khoa - Bác sĩ : Nguyễn Mạnh Tài",
   "address": "Số 2 Cổng Đục, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.036444,
   "Latitude": 105.84666
 },
 {
   "STT": 604,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Như Phúc",
   "address": "Số 4 ngõ 117 Phố 8/3, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9991731,
   "Latitude": 105.8612391
 },
 {
   "STT": 605,
   "Name": "Phòng khám Tai - Mũi - Họng",
   "address": "Số 44/283 phố Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.009253,
   "Latitude": 105.8567869
 },
 {
   "STT": 606,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 17 ngõ 55 phố Huỳnh Thúc Kháng (cũ: 27 đường số 1 khu A Nam Thành Công), phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.017898,
   "Latitude": 105.811936
 },
 {
   "STT": 607,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Ngô Phong",
   "address": "Số 16 phố Phù Đổng Thiên Vương, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016308,
   "Latitude": 105.8535709
 },
 {
   "STT": 608,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 5 ngách 26 ngõ Thái Thịnh II, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104507,
   "Latitude": 105.8174745
 },
 {
   "STT": 609,
   "Name": "Phòng khám Chuyên khoa Mắt Hồng Lê",
   "address": "19 khu thị trấn 26 Trần Quý Cáp, phường Văn Miếu, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0117571,
   "Latitude": 105.8300285
 },
 {
   "STT": 610,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 19B ngách 4/14 Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.005619,
   "Latitude": 105.83898
 },
 {
   "STT": 611,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Thị Bạch Dương",
   "address": "Số 4 - A5 tập thể Nguyễn Công Trứ, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0122126,
   "Latitude": 105.8530332
 },
 {
   "STT": 612,
   "Name": "Nha Khoa Bảo Đức",
   "address": "Số 1A ngõ 03 Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0132867,
   "Latitude": 105.8195983
 },
 {
   "STT": 613,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Trần Hùng",
   "address": "Số 51 phố Bạch Mai, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 614,
   "Name": "Chuyên khoa Răng - Hàm - Mặt",
   "address": "Số 38 đường Trương Định, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9973932,
   "Latitude": 105.8463841
 },
 {
   "STT": 615,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Bác sĩ : Nguyễn Hữu Liên",
   "address": "Số 3 phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0362348,
   "Latitude": 105.8455604
 },
 {
   "STT": 616,
   "Name": "Phòng khám Chữa Bệnh Răng Hàm Mặt - Bác sĩ : Vũ Thị Chức",
   "address": "Số 07 phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0371448,
   "Latitude": 105.8454154
 },
 {
   "STT": 617,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Trần Ngọc Cường",
   "address": "Số 90 Lê Lợi, phường Nguyễn Trãi, Hà Đông, Hà Nội",
   "Longtitude": 20.9698514,
   "Latitude": 105.7786757
 },
 {
   "STT": 618,
   "Name": "Phòng khám Ngoại Khoa Hoàng Vũ",
   "address": "Số 225 đường Quang Trung, phường Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9683335,
   "Latitude": 105.7729206
 },
 {
   "STT": 619,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp - Nguyễn Thị Thảo - 48",
   "address": "Số 41 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9805709,
   "Latitude": 105.8412424
 },
 {
   "STT": 620,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "276 đường Trường Chinh (cũ 157B), Khương Thượng, Đống Đa, Hà Nội",
   "Longtitude": 21.00122,
   "Latitude": 105.827335
 },
 {
   "STT": 621,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Lyon",
   "address": "Số 5DK (cũ: số 15DK) Công ty chế biến và kinh doanh dầu mỏ, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0153717,
   "Latitude": 105.8128851
 },
 {
   "STT": 622,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Văn Phú, phường Phú La, Hà Đông, Hà Nội",
   "Longtitude": 20.9586775,
   "Latitude": 105.7688614
 },
 {
   "STT": 623,
   "Name": "Phòng khám 41 Đặng Văn Ngữ",
   "address": "Số 41 Đặng Văn Ngữ, phường Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.008306,
   "Latitude": 105.832102
 },
 {
   "STT": 624,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 18 Hoàng Hoa Thám, phường Nguyễn Trãi, Hà Đông, Hà Nội",
   "Longtitude": 20.9722456,
   "Latitude": 105.7783426
 },
 {
   "STT": 625,
   "Name": "Phòng khám Răng Hàm Mặt - Bác sĩ Nguyễn Văn Bài",
   "address": "Số 56 Trúc Khê (cũ: Số 56 đường 6 tổ 97), phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0162043,
   "Latitude": 105.8083472
 },
 {
   "STT": 626,
   "Name": "Nha Khoa Đức Thành",
   "address": "Phòng 101A-E6 tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0212996,
   "Latitude": 105.8124711
 },
 {
   "STT": 627,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 81 ngõ Thái Thịnh 1, phường Thịnh Quang, Đống Đa, Hà Nội",
   "Longtitude": 21.0097398,
   "Latitude": 105.8166644
 },
 {
   "STT": 628,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "P308-A7 tập thể đường sắt Ngọc Khánh, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.029599,
   "Latitude": 105.817409
 },
 {
   "STT": 629,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Mạnh Hà",
   "address": "Số 23D phố Quán Thánh, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0416738,
   "Latitude": 105.841164
 },
 {
   "STT": 630,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt -Bác sĩ : Nguyễn Việt Phương",
   "address": "Số 18 phố Đình Ngang, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0283227,
   "Latitude": 105.842337
 },
 {
   "STT": 631,
   "Name": "Phòng khám Răng Hàm Mặt - Công Ty Tnhh Nha Khoa Nguyễn Du",
   "address": "Số 2-4 Nguyễn Du, phường Hàng Bài, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.018337,
   "Latitude": 105.851433
 },
 {
   "STT": 632,
   "Name": "Phòng khám Chuyên khoa Da Liễu - Phan Phương",
   "address": "Tầng 2 số 169 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0013582,
   "Latitude": 105.8417479
 },
 {
   "STT": 633,
   "Name": "Phòng khám Chuyên khoa Nội Vũ Bảo",
   "address": "Tầng 1 số 169 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0013582,
   "Latitude": 105.8417479
 },
 {
   "STT": 634,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 108 - Thành Công",
   "address": "Số 6 phố Trần Hưng Đạo, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0194881,
   "Latitude": 105.85756
 },
 {
   "STT": 635,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Bác sĩ : Lê Trung Hiếu",
   "address": "Số 34 phố Bà Triệu, phường Hàng Bài, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0215642,
   "Latitude": 105.8502781
 },
 {
   "STT": 636,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt  Đào Thị Dung",
   "address": "Số 4 ngõ 18 phố Nguyễn Đình Chiểu, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0178931,
   "Latitude": 105.8110489
 },
 {
   "STT": 637,
   "Name": "Nha Khoa Việt Anh",
   "address": "Số 6 ngách 51 xã Đàn 2, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.017592,
   "Latitude": 105.830172
 },
 {
   "STT": 638,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Nha Khoa Hòa An",
   "address": "Số 83, tổ 27 Phương Liên, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0123495,
   "Latitude": 105.8371026
 },
 {
   "STT": 639,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Đăng  Quảng",
   "address": "Số 67 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 640,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Răng Thẩm Mỹ 108",
   "address": "Số 106 phố Hồng Mai, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9988954,
   "Latitude": 105.8532977
 },
 {
   "STT": 641,
   "Name": "Phòng khám Tai Mũi Họng - Bác sĩ : Nghiêm Quang Bình",
   "address": "Số 89 phố Thợ Nhuộm, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0230176,
   "Latitude": 105.8485025
 },
 {
   "STT": 642,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Văn Dũng",
   "address": "Tại nhà ông Nguyễn Đình Mít, xã Vân Canh, Hoài Đức, Hà Nội",
   "Longtitude": 21.033149,
   "Latitude": 105.7321831
 },
 {
   "STT": 643,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Huy Chí",
   "address": "Số nhà 31A, tổ 3, khu Chiến Thắng, thị trấn Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.893522,
   "Latitude": 105.571545
 },
 {
   "STT": 644,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Đức Đanh",
   "address": "Số 2/9 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1146467,
   "Latitude": 105.4955281
 },
 {
   "STT": 645,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Gia",
   "address": "Số 177 đường Nguyễn Trãi, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 646,
   "Name": "Phòng khám Chuyên khoa Nội Trương Phúc Dậu",
   "address": "Thôn 7, xã Phú Cát, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.961081,
   "Latitude": 105.536732
 },
 {
   "STT": 647,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 15 Yên Bình, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709464,
   "Latitude": 105.78594
 },
 {
   "STT": 648,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "LK 55A khu Bắc Hà, Mộ Lao, Hà Đông, Hà Nội",
   "Longtitude": 20.9833461,
   "Latitude": 105.7895879
 },
 {
   "STT": 649,
   "Name": "Phòng khám Chuyên khoa Nội 15 Nguyễn Viết Xuân",
   "address": "Số 15 Nguyễn Viết Xuân, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.998894,
   "Latitude": 105.82758
 },
 {
   "STT": 650,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thanh Phú",
   "address": "46B Phùng Hưng, phường Ngô Quyền, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.14265,
   "Latitude": 105.5020185
 },
 {
   "STT": 651,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 15 phố Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1162867,
   "Latitude": 105.4949277
 },
 {
   "STT": 652,
   "Name": "Phòng khám Sản Phụ Khoa - Khhgđ - Siêu Âm",
   "address": "Số 282 đường Nguyễn Trãi, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 653,
   "Name": "Phòng khám Chuyên khoa Ngoại Trần Thanh Nguyên",
   "address": "71 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1132037,
   "Latitude": 105.4955235
 },
 {
   "STT": 654,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình 37A",
   "address": "Số 187 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 655,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Số 1 Dốc Phụ Sản Hà Nội",
   "address": "Số 85B Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0240097,
   "Latitude": 105.8159367
 },
 {
   "STT": 656,
   "Name": "Phòng khám Chuyên khoa Nội Lê Thanh Hải - 472",
   "address": "Số 472, phố Đội Cấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0377178,
   "Latitude": 105.8089805
 },
 {
   "STT": 657,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Trần Lâm",
   "address": "Số 42, phố Cầu Giấy, phường Ngọc Khánh, Ba Đình, Hà Nội",
   "Longtitude": 21.0288403,
   "Latitude": 105.8046013
 },
 {
   "STT": 658,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Tống Xuân Thắng",
   "address": "Số 472, phố Đội Cấn, phường Cống Vị, Ba Đình, Hà Nội",
   "Longtitude": 21.0377178,
   "Latitude": 105.8089805
 },
 {
   "STT": 659,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Mai Ngọc",
   "address": "Phòng 102 nhà H1 thị trấn Quân Đội, phường Phương Mai, Đống Đa, Hà Nội",
   "Longtitude": 21.0200923,
   "Latitude": 105.8170588
 },
 {
   "STT": 660,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 27 ngõ 41, phố Vũ Ngọc Phan (cũ: tổ 5), Láng Hạ, Đống Đa, Hà Nội",
   "Longtitude": 21.0141774,
   "Latitude": 105.8108406
 },
 {
   "STT": 661,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 109",
   "address": "Số 109-H1 Láng Hạ, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.019671,
   "Latitude": 105.817116
 },
 {
   "STT": 662,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Số nhà 103, đường Trần Cung, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0490797,
   "Latitude": 105.7899256
 },
 {
   "STT": 663,
   "Name": "Nha Khoa Mạnh Chiến",
   "address": "Căn 3D chung cư Biển Bắc, 1070 Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0275911,
   "Latitude": 105.809578
 },
 {
   "STT": 664,
   "Name": "Phòng khám Nha Khoa Quỳnh Anh",
   "address": "Số 43B, ngõ 79, đường Cầu Giấy, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0303628,
   "Latitude": 105.7999819
 },
 {
   "STT": 665,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 183, phố Tô Hiệu, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0409763,
   "Latitude": 105.7931441
 },
 {
   "STT": 666,
   "Name": "Phòng khám Phụ Sản Số 9",
   "address": "Số 99 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268251,
   "Latitude": 105.8082922
 },
 {
   "STT": 667,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Nguyễn Ngọc Minh",
   "address": "Số 18 ngõ 242 Lạc Long Quân, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0538659,
   "Latitude": 105.8092246
 },
 {
   "STT": 668,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Phương Mai",
   "address": "Số 11 ngách 4/14 ngõ 4 Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0058694,
   "Latitude": 105.8389349
 },
 {
   "STT": 669,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Vũ Văn Nghị",
   "address": "Số 6 dốc Bệnh viện Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268582,
   "Latitude": 105.8087468
 },
 {
   "STT": 670,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 21 ngõ 172 Lạc Long Quân, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0512714,
   "Latitude": 105.8091384
 },
 {
   "STT": 671,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Ngọc Anh",
   "address": "Số 379 Kim Mã, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.030432,
   "Latitude": 105.81622
 },
 {
   "STT": 672,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Bình",
   "address": "Số 67 tổ 44B, cụm 7, phường Ngọc Hà, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0378157,
   "Latitude": 105.8282959
 },
 {
   "STT": 673,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phan Văn Việt- Le Bordelais",
   "address": "Số 14, nhà C3, tập thể Giảng Võ, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0271097,
   "Latitude": 105.8227919
 },
 {
   "STT": 674,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - 102",
   "address": "Số 102 phố Kim Mã, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0311643,
   "Latitude": 105.8187085
 },
 {
   "STT": 675,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh",
   "address": "Số 167, đường Lạc Long Quân, tổ 09, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0508828,
   "Latitude": 105.8077128
 },
 {
   "STT": 676,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc",
   "address": "Số 372 đường Ngọc Lâm, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0497307,
   "Latitude": 105.8775932
 },
 {
   "STT": 677,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 14 ngõ 67 phố Đức Giang, tổ 21, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0701691,
   "Latitude": 105.9068373
 },
 {
   "STT": 678,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Phạm Thị Phượng",
   "address": "Thôn Liên Ngạc, xã Đông Ngạc, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0906849,
   "Latitude": 105.7779405
 },
 {
   "STT": 679,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Bác sĩ : Phạm Dương Châu",
   "address": "Số 52 (tầng 5) phố Bà Triệu, phường Hàng Bài, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0215642,
   "Latitude": 105.8502781
 },
 {
   "STT": 680,
   "Name": "Phòng khám Chuyên khoa  Răng Hàm Mặt Trần Cao Bính",
   "address": "Số 43C/461 đường Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.009253,
   "Latitude": 105.8567869
 },
 {
   "STT": 681,
   "Name": "Phòng khám Nha Khoa Thăng Long",
   "address": "Số 389 đường Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0088111,
   "Latitude": 105.854339
 },
 {
   "STT": 682,
   "Name": "Nha Khoa Anh Thư",
   "address": "Nhà số 101, Trần Phú, thị trấn Thường Tín, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8676233,
   "Latitude": 105.8604655
 },
 {
   "STT": 683,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Đình Kiên",
   "address": "Số 186, đường Trần Cung, xóm 1C, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0542975,
   "Latitude": 105.7871202
 },
 {
   "STT": 684,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 322 đường Ngô Gia Tự, tổ 8, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0650095,
   "Latitude": 105.8974448
 },
 {
   "STT": 685,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Thị Ánh Diệp",
   "address": "Số 13 ngõ 481 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0465837,
   "Latitude": 105.8712837
 },
 {
   "STT": 686,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Thôn Trùng Quán, xã Yên Thường, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0916081,
   "Latitude": 105.9325355
 },
 {
   "STT": 687,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Bách Khang Viên",
   "address": "Kiốt số 10, NO 3, bán đảo Linh Đàm, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.963428,
   "Latitude": 105.828569
 },
 {
   "STT": 688,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Vũ Đức Khánh",
   "address": "Số 68 đường Trương Định, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9937815,
   "Latitude": 105.8466875
 },
 {
   "STT": 689,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 32 tập thể UBND huyện Từ Liêm, Thôn Hoàng 4, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0583202,
   "Latitude": 105.7835931
 },
 {
   "STT": 690,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ, Siêu Âm Sản Phụ Khoa",
   "address": "Số 43, tổ 18, phường Thượng Thanh, quận Long Biên, Hà Nội",
   "Longtitude": 21.0703795,
   "Latitude": 105.88063
 },
 {
   "STT": 691,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số nhà 27, tổ 52, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0339527,
   "Latitude": 105.7850022
 },
 {
   "STT": 692,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Số 6",
   "address": "Số 6, dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268357,
   "Latitude": 105.8084106
 },
 {
   "STT": 693,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Lê Huy Tuấn",
   "address": "Số 19, Thôn Hoàng 4, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0583202,
   "Latitude": 105.7835931
 },
 {
   "STT": 694,
   "Name": "Phòng khám Chuyên khoa Nội An Bình - 1",
   "address": "Thôn Hậu Dưỡng, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1227594,
   "Latitude": 105.7706677
 },
 {
   "STT": 695,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tập thể Viện thổ nhưỡng Nông hóa, xã Đông Ngạc, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0750711,
   "Latitude": 105.7764631
 },
 {
   "STT": 696,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 53 phố Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 697,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Lê Văn Sáng",
   "address": "Đội 7, Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1267692,
   "Latitude": 105.7764757
 },
 {
   "STT": 698,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Phát Triển Dịch Vụ Y Tế Hoàng Anh",
   "address": "Số 400 đường Hồ Tùng Mậu, tổ 11, phường Phú Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0367229,
   "Latitude": 105.7788626
 },
 {
   "STT": 699,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Tnhh Kính Mắt Quang Hưng",
   "address": "Số 20, phố Tôn Thất Tùng, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0022945,
   "Latitude": 105.829711
 },
 {
   "STT": 700,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình ",
   "address": "Số 26 ngõ 104 phố Nguyễn Phúc Lai (cũ: 25 lô B tập thể Tổng cục Hậu cần Hoàng Cầu), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0103686,
   "Latitude": 105.8155181
 },
 {
   "STT": 701,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Nguyễn Minh Nguyệt",
   "address": "Số 6 ngõ 23 phố Cát Linh, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0289645,
   "Latitude": 105.8315681
 },
 {
   "STT": 702,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Bác sĩ Nguyễn Văn Dần",
   "address": "Phòng 106 nhà I1 tập thể Phương Mai, ngách 91/2 đường Lương Đình Của, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0031985,
   "Latitude": 105.8360115
 },
 {
   "STT": 703,
   "Name": "Phòng khám Đa Khoa Hà Nội 1",
   "address": "Số 54, Thôn Đản Dị, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1447794,
   "Latitude": 105.8527907
 },
 {
   "STT": 704,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Thái Thịnh",
   "address": "Số 19 ngách 95 ngõ Thái Thịnh 2, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0094871,
   "Latitude": 105.8163267
 },
 {
   "STT": 705,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 56 ngõ Trung Tiền, phường Khâm Thiên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0200116,
   "Latitude": 105.8380346
 },
 {
   "STT": 706,
   "Name": "Nha Khoa 3D",
   "address": "Số 520 đường Trường Chinh, phường Ngã Tư Sở, quận Đống Đa, Hà Nội",
   "Longtitude": 21.002177,
   "Latitude": 105.82358
 },
 {
   "STT": 707,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 297 Bắc Biên, phường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0576444,
   "Latitude": 105.8635253
 },
 {
   "STT": 708,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đức Lộc",
   "address": "Số 36, tập thể Trung tâm Nhiệt đới Việt Nga - Thôn Hoàng 5, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0444957,
   "Latitude": 105.7986087
 },
 {
   "STT": 709,
   "Name": "Nha Khoa Thẩm Mỹ Thanh Xuân",
   "address": "Số 342 đường Nguyễn Trãi, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 710,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Kiốt số 5 nhà CT2-ĐN2 khu đô thị Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9861693,
   "Latitude": 105.8281583
 },
 {
   "STT": 711,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 28 ngõ 178 Tây Sơn, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104521,
   "Latitude": 105.8247756
 },
 {
   "STT": 712,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Lê Thị Thu Hà",
   "address": "Số 72A, ngõ 245 phố Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9822372,
   "Latitude": 105.8323932
 },
 {
   "STT": 713,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Trần Xuân Bách",
   "address": "Phòng 110-C21, phố Nguyễn Quý Đức, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9916574,
   "Latitude": 105.8000167
 },
 {
   "STT": 714,
   "Name": "Phòng khám Chuyên khoa Nhi Mai Thịnh",
   "address": "Số 110 (cũ: số 34A) nhà B1 tập thể Khương Thượng, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021606,
   "Latitude": 105.8311626
 },
 {
   "STT": 715,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 26 phố Chương Dương, phường Chương Dương, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0299501,
   "Latitude": 105.8580217
 },
 {
   "STT": 716,
   "Name": "Nha Khoa Quốc Tế Việt Đức",
   "address": "Số 166 Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0127208,
   "Latitude": 105.8206011
 },
 {
   "STT": 717,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 117 phố Kim Ngưu, phường Thanh Lương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0043966,
   "Latitude": 105.8611706
 },
 {
   "STT": 718,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Minh Tú",
   "address": "Số nhà 44, phố Nhân Hòa, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0023549,
   "Latitude": 105.8089674
 },
 {
   "STT": 719,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Trịnh Xuân Hưởng",
   "address": "Số 30 Trưng Nhị, phường Nguyễn Trãi, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9706667,
   "Latitude": 105.7784271
 },
 {
   "STT": 720,
   "Name": "Phòng khám Đa Khoa Ngọc Khánh",
   "address": "Số 140 phố Chùa Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0230198,
   "Latitude": 105.8012521
 },
 {
   "STT": 721,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Chi Nhánh Công Ty Tnhh International Sos Việt Nam Tại Hà Nội",
   "address": "Số 51 Xuân Diệu, phường Quảng An, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0637088,
   "Latitude": 105.8274747
 },
 {
   "STT": 722,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Lê Thế Vũ",
   "address": "Số 16, ngõ 313, phố Quan Nhân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0025437,
   "Latitude": 105.8518741
 },
 {
   "STT": 723,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Đình Thông",
   "address": "Tầng 1 số 189 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0010237,
   "Latitude": 105.8416146
 },
 {
   "STT": 724,
   "Name": "Phòng khám Chuyên khoa Nội Ngô Thế Kỷ",
   "address": "Số 28, ngõ 22, phố Phan Đình Giót, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9910864,
   "Latitude": 105.8392758
 },
 {
   "STT": 725,
   "Name": "Phòng khám Chuyên khoa Phụ Sản ",
   "address": "Số 99 ngõ 185 phố Chùa Láng (số cũ 260, tổ 13), phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0235829,
   "Latitude": 105.8052281
 },
 {
   "STT": 726,
   "Name": "Phòng khám Chuyên khoa Da Liễu - Bác sĩ : Hoàng Thị Hiếu",
   "address": "Số 37 phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0357809,
   "Latitude": 105.8450256
 },
 {
   "STT": 727,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Hòa Bình",
   "address": "Số nhà 60, phố Hoàng Diệu, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0355063,
   "Latitude": 105.8392342
 },
 {
   "STT": 728,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội Dị Ứng",
   "address": "Số 19B ngách 4/14 Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.005619,
   "Latitude": 105.83898
 },
 {
   "STT": 729,
   "Name": "Phòng khám Chuyên khoa Nội Huy Cường",
   "address": "Số 1 ngõ 133 Thái Hà, phường Trung Liệt, n, Hà Nội",
   "Longtitude": 21.0133519,
   "Latitude": 105.819327
 },
 {
   "STT": 730,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Răng Xinh",
   "address": "Số 150, đường Trần Duy Hưng, tổ 12, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0107492,
   "Latitude": 105.799529
 },
 {
   "STT": 731,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 06 phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0371448,
   "Latitude": 105.8454154
 },
 {
   "STT": 732,
   "Name": "Nha Khoa Ngọc Linh",
   "address": "Số 26 Hoàng Hoa Thám, phường Thụy Khuê, Tây Hồ, Hà Nội",
   "Longtitude": 21.0418139,
   "Latitude": 105.8351402
 },
 {
   "STT": 733,
   "Name": "Nha Khoa Bảo Nhân",
   "address": "Căn hộ tại nhà A2-5B Hoàng Hoa Thám, phường Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0421911,
   "Latitude": 105.8214638
 },
 {
   "STT": 734,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Tiến Sĩ: Lê Thanh - Hạ Hồi",
   "address": "Số 23B phố Hạ Hồi, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0212236,
   "Latitude": 105.846876
 },
 {
   "STT": 735,
   "Name": "Nha Khoa Lạc Việt",
   "address": "Số 280 phố Khương Trung, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9924667,
   "Latitude": 105.8171664
 },
 {
   "STT": 736,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Quốc Dân",
   "address": "Số 46, ngõ 69A, phố Hoàng Văn Thái, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.995432,
   "Latitude": 105.8285708
 },
 {
   "STT": 737,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Phạm Thị Minh Nguyệt",
   "address": "Số 42 phố Phan Đình Giót, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9910864,
   "Latitude": 105.8392758
 },
 {
   "STT": 738,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Tnhh Kính Mắt Việt Nam",
   "address": "Số 231, đường Nguyễn Trãi, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 739,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Thị Thu Hiền 11",
   "address": "Số 11, ngách 45, ngõ 88, phố Ngọc Hà, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0371132,
   "Latitude": 105.8310476
 },
 {
   "STT": 740,
   "Name": "Phòng khám Chuyên khoa Nội Bùi Thị Khắp",
   "address": "101E -C5 tập thể Giảng Võ, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0271097,
   "Latitude": 105.8227919
 },
 {
   "STT": 741,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Beauty Medi Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Dịch Vụ Trường Giang",
   "address": "Số 169 Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0125534,
   "Latitude": 105.8499695
 },
 {
   "STT": 742,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ 72",
   "address": "Số 72 tổ 4, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0108448,
   "Latitude": 105.822425
 },
 {
   "STT": 743,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Thăng Long - Bác sĩ : Nguyễn Thị Phương",
   "address": "Số 121 phố Phủ Doãn, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0287882,
   "Latitude": 105.8479312
 },
 {
   "STT": 744,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trần Thị Vân Xuyên",
   "address": "Số 38 phố Vĩnh Hưng (tổ 25), phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9891316,
   "Latitude": 105.87775
 },
 {
   "STT": 745,
   "Name": "Phòng khám Chuyên khoa Nội Lê Thị Kim",
   "address": "Số 38 phố Vĩnh Hưng (tổ 25), phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9891316,
   "Latitude": 105.87775
 },
 {
   "STT": 746,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Phát Triển Công Nghệ Y Học Vĩnh Hà",
   "address": "Số 55B Hàng Cót, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.036915,
   "Latitude": 105.847197
 },
 {
   "STT": 747,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Quốc Tế Nha Khoa Việt Pháp",
   "address": "Số 24 Trần Duy Hưng, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0135599,
   "Latitude": 105.802398
 },
 {
   "STT": 748,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà A8 ngõ 217 Đê La Thành, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.021759,
   "Latitude": 105.8245555
 },
 {
   "STT": 749,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Văn Đông",
   "address": "Xóm Tháp, xã Đại Mỗ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9945639,
   "Latitude": 105.7571236
 },
 {
   "STT": 750,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đàm Thị Hồng Thúy",
   "address": "Số 155 phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 751,
   "Name": "Nha Khoa Như Ngọc",
   "address": "Số 42 phố Kim Mã, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0321915,
   "Latitude": 105.826188
 },
 {
   "STT": 752,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt  Hà Nội",
   "address": "Số 117 (cũ: số 19) phố Láng Hạ, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0170062,
   "Latitude": 105.8150862
 },
 {
   "STT": 753,
   "Name": "Phòng khám Đa Khoa Quốc Tế Aa Trực Thuộc Công Ty Cổ Phần Xây Dựng Và Đầu Tư Hà Nội",
   "address": "B18 và B19 khu đấu giá Vạn Phúc, phường Vạn Phúc, quận Hà Đông, Hà Nội",
   "Longtitude": 20.982151,
   "Latitude": 105.771255
 },
 {
   "STT": 754,
   "Name": "Phòng khám Đa Khoa Tư Nhân - 3A",
   "address": "Số 01 Nguyễn Chánh, đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 21.0088494,
   "Latitude": 105.7947647
 },
 {
   "STT": 755,
   "Name": "Phòng khám Đa Khoa",
   "address": "62 Thái Thịnh, Ngã Tư Sở, Đống Đa, Hà Nội",
   "Longtitude": 21.006739,
   "Latitude": 105.821665
 },
 {
   "STT": 756,
   "Name": "Phòng khám Đa Khoa - Pgs: Trịnh Văn Quang",
   "address": "Số 24+26 phố Thợ Nhuộm, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0269161,
   "Latitude": 105.8447634
 },
 {
   "STT": 757,
   "Name": "Phòng khám Đa Khoa Việt An Trực Thuộc Công Ty Tnhh Liên Việt - Hoa",
   "address": "Số 620, đường Hoàng Hoa Thám, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0478902,
   "Latitude": 105.8084337
 },
 {
   "STT": 758,
   "Name": "Phòng khám Đa Khoa Yecxanh Trực Thuộc Công Ty Cổ Phần Dịch Vụ Giáo Dục Và Y Tế Hà Vân",
   "address": "Số 221, phố Vọng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.996206,
   "Latitude": 105.842919
 },
 {
   "STT": 759,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "230 phố Trần Đăng Ninh, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7305099,
   "Latitude": 105.7744709
 },
 {
   "STT": 760,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Phát Triển Y Tế Nam Khánh",
   "address": "Số 66 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0172281,
   "Latitude": 105.8065569
 },
 {
   "STT": 761,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 86 đường Chiến Thắng, phường Văn Quán, Hà Đông, Hà Nội",
   "Longtitude": 20.9779976,
   "Latitude": 105.7975432
 },
 {
   "STT": 762,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Chiến Hoàng",
   "address": "Đội 3, Thôn Tế Tiêu, thị trấn Đại Nghĩa, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.685899,
   "Latitude": 105.7421069
 },
 {
   "STT": 763,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Súy",
   "address": "Số 48 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 764,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Giang Thanh Giảng",
   "address": "Khu tập thể phân hiệu 1, Thôn My Hạ, xã Thanh Mai, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8643542,
   "Latitude": 105.7703287
 },
 {
   "STT": 765,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Triển",
   "address": "Xóm Quê, xã Dương Liễu, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0579347,
   "Latitude": 105.6691136
 },
 {
   "STT": 766,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "46 phố Nguyễn Thượng Hiền, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7348856,
   "Latitude": 105.7758
 },
 {
   "STT": 767,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "130 phố Trần Đăng Ninh, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7305099,
   "Latitude": 105.7744709
 },
 {
   "STT": 768,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Lương Hồng Liên",
   "address": "Đường 430 Khối Chiến Thắng, Vạn Phúc, Hà Đông, Hà Nội",
   "Longtitude": 20.978441,
   "Latitude": 105.773795
 },
 {
   "STT": 769,
   "Name": "Phòng khám Chuyên khoa Mắt Trực Thuộc Công Ty Tnhh Kính Mắt Việt Nam",
   "address": "138B Giảng Võ, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0287964,
   "Latitude": 105.8256764
 },
 {
   "STT": 770,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Tnhh Kính Mắt Việt Nam",
   "address": "138B Giảng Võ, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0287964,
   "Latitude": 105.8256764
 },
 {
   "STT": 771,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Cổ Phần Dược Thiết Bị Y Tế Biphartek",
   "address": "Số 61E, đường Đê La Thành, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0155086,
   "Latitude": 105.8335127
 },
 {
   "STT": 772,
   "Name": "Phòng khám Chuyên khoa Nhi Nhi Việt Trực Thuộc Công Ty Cổ Phần Dược Thiết Bị Y Tế Biphartek",
   "address": "Số 61E, đường Đê La Thành, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0155086,
   "Latitude": 105.8335127
 },
 {
   "STT": 773,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Trực Thuộc Công Ty Cổ Phần Dược Thiết Bị Y Tế Biphartek",
   "address": "Số 61E, đường Đê La Thành, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0155086,
   "Latitude": 105.8335127
 },
 {
   "STT": 774,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Thanh Bình",
   "address": "Tập thể tiểu đoàn 8, cục hậu cần, Bộ tư lệnh Thông tin liên lạc, xã Tứ Hiệp, Thanh Trì, Hà Nội",
   "Longtitude": 20.9640702,
   "Latitude": 105.8429611
 },
 {
   "STT": 775,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà Nguyên",
   "address": "Xóm 15, Thôn Trù, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0640578,
   "Latitude": 105.7788074
 },
 {
   "STT": 776,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nha Khoa Pháp",
   "address": "Số 154, đường Hoàng Quốc Việt (số cũ:A28- Khu nhà ở), xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0461113,
   "Latitude": 105.7933552
 },
 {
   "STT": 777,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 61, tổ 15, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0403832,
   "Latitude": 105.7629924
 },
 {
   "STT": 778,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 214 Nguyễn Lương Bằng, phường Quang Trung, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014484,
   "Latitude": 105.827461
 },
 {
   "STT": 779,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Việt Nga",
   "address": "Số 115 Vũ Xuân Thiều, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0329086,
   "Latitude": 105.9163998
 },
 {
   "STT": 780,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 26 phố Đức Giang, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0667572,
   "Latitude": 105.8836675
 },
 {
   "STT": 781,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 16 phố Hoa Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 782,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 123 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0422127,
   "Latitude": 105.8696791
 },
 {
   "STT": 783,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 108 phố Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0455361,
   "Latitude": 105.8690714
 },
 {
   "STT": 784,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Minh Tâm",
   "address": "Số 267 Nguyễn Văn Linh, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0368996,
   "Latitude": 105.9003134
 },
 {
   "STT": 785,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Phú Mai, phường Phú Thịnh, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1537625,
   "Latitude": 105.4976181
 },
 {
   "STT": 786,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà Nội - Hoa Kỳ",
   "address": "Số nhà 213, đường Trần Đăng Ninh, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0388719,
   "Latitude": 105.7923185
 },
 {
   "STT": 787,
   "Name": "Nha Khoa Việt",
   "address": "Tầng 1, lô số A12, khu giãn dân phường Yên Hòa, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0197966,
   "Latitude": 105.7842633
 },
 {
   "STT": 788,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 179 phố Tây Sơn (Cũ: số 10 phố Khương Thượng), phường Ngã Tư Sở, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0062159,
   "Latitude": 105.823554
 },
 {
   "STT": 789,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Châu Á Thái Bình Dương",
   "address": "Số 148 phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349255,
   "Latitude": 105.8252758
 },
 {
   "STT": 790,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thúy Anh",
   "address": "Số 74, Đội 8, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0339371,
   "Latitude": 105.7662822
 },
 {
   "STT": 791,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thị Kim Hòa",
   "address": "Số 19a phố Phan Đình Phùng, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.040567,
   "Latitude": 105.8420625
 },
 {
   "STT": 792,
   "Name": "Phòng khám Chuyên khoa Nội Dương Đình Đạc",
   "address": "Số 50 ngõ 187 phố Hồng Mai, phường Quỳnh Lôi, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9994437,
   "Latitude": 105.855664
 },
 {
   "STT": 793,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 14/558 đường Nguyễn Văn Cừ, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0495394,
   "Latitude": 105.8834124
 },
 {
   "STT": 794,
   "Name": "Phòng khám Chuyên khoa Nội Đức Minh",
   "address": "Xóm 18A, Thôn Viên II, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0655404,
   "Latitude": 105.7792473
 },
 {
   "STT": 795,
   "Name": "Phòng khám Chuyên khoa Nội Hồng Lam",
   "address": "Số 01/M7, Khu tập thể Cục đo lường chất lượng, Thôn Trù 2, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0685137,
   "Latitude": 105.7624422
 },
 {
   "STT": 796,
   "Name": "Phòng khám Chuyên khoa Nội Vũ Hoàng Tuyết Minh",
   "address": "Số 19, Thôn Hoàng 4, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0583202,
   "Latitude": 105.7835931
 },
 {
   "STT": 797,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 203 phố Hoa Lâm, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.055006,
   "Latitude": 105.896879
 },
 {
   "STT": 798,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Quý Sinh",
   "address": "Số 41 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1139105,
   "Latitude": 105.495528
 },
 {
   "STT": 799,
   "Name": "Phòng khám Chuyên khoa Nhi Chu Lâm Khuê",
   "address": "Số 8 ngõ Cẩm An, phường Ngô Quyền, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1425408,
   "Latitude": 105.5012808
 },
 {
   "STT": 800,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Vũ Duy Thịnh",
   "address": "Số nhà 151 tổ 3, thị trấn Kim Bài, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8559363,
   "Latitude": 105.7673942
 },
 {
   "STT": 801,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 121 phố Nguyễn Thượng Hiền, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7277897,
   "Latitude": 105.7698558
 },
 {
   "STT": 802,
   "Name": "Phòng khám Chuyên khoa Phụ Sản- Khhgđ 86A",
   "address": "Số 38 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.971288,
   "Latitude": 105.7834882
 },
 {
   "STT": 803,
   "Name": "Phòng khám Chuyên khoa Nội Đoàn Thịnh Trường",
   "address": "Xóm Chàng Chợ, xã Dương Liễu, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0579347,
   "Latitude": 105.6691136
 },
 {
   "STT": 804,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Trần Thị Kim Thanh",
   "address": "Số 8 ngõ Lạc Sơn, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1401459,
   "Latitude": 105.5076992
 },
 {
   "STT": 805,
   "Name": "Phòng khám Chuyên khoa Nội Lê Phúc Cầu",
   "address": "Thôn 2, Thống Nhất, xã Kim Lan, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9603693,
   "Latitude": 105.9039005
 },
 {
   "STT": 806,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 164 tổ 12, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.0371784,
   "Latitude": 105.8730678
 },
 {
   "STT": 807,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng Đạt",
   "address": "Xuân Sơn, Trung Giã, Sóc Sơn, Hà Nội",
   "Longtitude": 21.3030059,
   "Latitude": 105.8657274
 },
 {
   "STT": 808,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 130/604 tổ 21, phường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0588582,
   "Latitude": 105.8583873
 },
 {
   "STT": 809,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng Hữu Đốc",
   "address": "Xóm Nhồi dưới, xã Cổ Loa, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1170352,
   "Latitude": 105.869757
 },
 {
   "STT": 810,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Đức Minh",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 811,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Tấn Lợi",
   "address": "Tổ 8, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0626555,
   "Latitude": 105.8947706
 },
 {
   "STT": 812,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Lê Thị Quất",
   "address": "Số 105, C33, phố Mai Động, phường Mai Động, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9890701,
   "Latitude": 105.8611987
 },
 {
   "STT": 813,
   "Name": "Phòng khám Chuyên khoa Phụ Sản 6 Sao",
   "address": "Số 6 dốc Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268357,
   "Latitude": 105.8084106
 },
 {
   "STT": 814,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 14, dốc Bệnh viện phụ sản, đường Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0267649,
   "Latitude": 105.8073191
 },
 {
   "STT": 815,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Thường - 46",
   "address": "Số 321A phố Bạch Mai, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 816,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 203 phố Hoa Lâm, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.055006,
   "Latitude": 105.896879
 },
 {
   "STT": 817,
   "Name": "Phòng khám Chuyên khoa Da Liễu Thúy Hiệp",
   "address": "Tổ 26, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.161664,
   "Latitude": 105.854389
 },
 {
   "STT": 818,
   "Name": "Phòng khám Chuyên khoa Da Liễu Nguyễn Lan Hương",
   "address": "Số 19, ngõ 567, phố Hoàng Hoa Thám, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0053575,
   "Latitude": 105.833574
 },
 {
   "STT": 819,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Cổ Phần Công Nghệ Sinh Học Bionet Việt Nam",
   "address": "Tầng 3+4 số nhà 30 đường Hoàng Cầu, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0161524,
   "Latitude": 105.8211628
 },
 {
   "STT": 820,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Trần Thị Minh  Nguyệt",
   "address": "Thôn Đình Thôn, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.020236,
   "Latitude": 105.7772989
 },
 {
   "STT": 821,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Nguyễn Thanh Hải",
   "address": "Số 77, đường Trần Duy Hưng, tổ 14, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0107492,
   "Latitude": 105.799529
 },
 {
   "STT": 822,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Tạ Thị Minh Phương",
   "address": "Nhà số 3, ngõ 268, đường Xuân Đỉnh, xã Xuân Đỉnh, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.070562,
   "Latitude": 105.7887407
 },
 {
   "STT": 823,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Thị Thảo",
   "address": "Số 41 Đặng Văn Ngữ, phường Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.008306,
   "Latitude": 105.832102
 },
 {
   "STT": 824,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 40 đường Lương Thế Vinh, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.98895,
   "Latitude": 105.798141
 },
 {
   "STT": 825,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Tại phòng 409 - B7, Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.993579,
   "Latitude": 105.7982094
 },
 {
   "STT": 826,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 6 phố Nguyễn Biểu, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.04263,
   "Latitude": 105.8401
 },
 {
   "STT": 827,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Hạnh",
   "address": "Số 16 Vũ Thạnh, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0263139,
   "Latitude": 105.8270051
 },
 {
   "STT": 828,
   "Name": "Nha Khoa Bạch Mai",
   "address": "Số 63 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 829,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Sáu",
   "address": "Số 4/461 phố Minh Khai (Số cũ: 60 tổ 36), phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.999058,
   "Latitude": 105.871463
 },
 {
   "STT": 830,
   "Name": "Nha Khoa 443",
   "address": "Số 443, tổ 19, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0213245,
   "Latitude": 105.9002168
 },
 {
   "STT": 831,
   "Name": "Nha Khoa Thẩm Mỹ Hiếu Quyền",
   "address": "Số 2 Nguyễn An Ninh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9886382,
   "Latitude": 105.8473599
 },
 {
   "STT": 832,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Lê Hưng",
   "address": "Số 476 phố Bạch Mai, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 833,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 589 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.06618,
   "Latitude": 105.897997
 },
 {
   "STT": 834,
   "Name": "Nha Khoa Nụ Cười Hạnh Phúc",
   "address": "Số 43 Đào Tấn, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0327737,
   "Latitude": 105.807223
 },
 {
   "STT": 835,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Anh Tú",
   "address": "Số 226 phố Hoàng Văn Thái, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9965037,
   "Latitude": 105.8261259
 },
 {
   "STT": 836,
   "Name": "Nha Khoa Trang Dung",
   "address": "Số 3B phố Trần Hưng Đạo, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0186048,
   "Latitude": 105.8602519
 },
 {
   "STT": 837,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Mạnh Hà",
   "address": "Số 55A, ngõ 228, phố Lê Trọng Tấn, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9822842,
   "Latitude": 105.8442401
 },
 {
   "STT": 838,
   "Name": "Phòng khám Đa Khoa Tư Nhân Bạch Mai",
   "address": "Số nhà 26, tổ 11, thị trấn Sóc Sơn, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538445,
   "Latitude": 105.8495796
 },
 {
   "STT": 839,
   "Name": "Phòng khám Đa Khoa Quân Dân",
   "address": "Số 173 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0013582,
   "Latitude": 105.8417479
 },
 {
   "STT": 840,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Cổ Phần Kính Mắt Thành phố Hồ Chí Minh",
   "address": "Số nhà 138 phố Giảng Võ, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0290986,
   "Latitude": 105.8262639
 },
 {
   "STT": 841,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 16 phố Lạc Trung, phường Thanh Lương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0031073,
   "Latitude": 105.8619436
 },
 {
   "STT": 842,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Ngọc Sơn",
   "address": "Số 3, Cầu Lủ, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9804588,
   "Latitude": 105.8193195
 },
 {
   "STT": 843,
   "Name": "Nha Khoa Hồng Vân",
   "address": "Số 52, phố Nghĩa Tân, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0441046,
   "Latitude": 105.7907277
 },
 {
   "STT": 844,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 246, đường Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9825027,
   "Latitude": 105.8338684
 },
 {
   "STT": 845,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Quý Quân",
   "address": "Xóm Đình, xã Đại Mỗ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.998376,
   "Latitude": 105.7589576
 },
 {
   "STT": 846,
   "Name": "Phòng khám Chuyên khoa Nội Hùng Sơn",
   "address": "Số 225 phố chợ Khâm Thiên, phường Trung Phụng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0160597,
   "Latitude": 105.8386075
 },
 {
   "STT": 847,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Đầu Tư Y Tế Và Xây Dựng An Phát",
   "address": "Số 5, phố Nhân Hòa, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0019942,
   "Latitude": 105.8097293
 },
 {
   "STT": 848,
   "Name": "Phòng khám Chuyên khoa Mắt Phạm Hồng Điệp",
   "address": "Số 148 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 849,
   "Name": "Nha Khoa Thành Công",
   "address": "17 Hoàng Ngọc Phách (Cũ 101 C1), phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0153064,
   "Latitude": 105.8128606
 },
 {
   "STT": 850,
   "Name": "Phòng khám Chuyên khoa Mắt Hà Thị Thu Hà",
   "address": "Số nhà 98, tổ 2, xóm Bến, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9493584,
   "Latitude": 105.8444419
 },
 {
   "STT": 851,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 197, đường Trần Đăng Ninh, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0389401,
   "Latitude": 105.7925462
 },
 {
   "STT": 852,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 15 tổ 12, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0348453,
   "Latitude": 105.9127108
 },
 {
   "STT": 853,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Phương Hạnh",
   "address": "Tập thể A76- Thôn Hoàng 5, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0448574,
   "Latitude": 105.7863088
 },
 {
   "STT": 854,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 4, ngõ 99, phố Trung Kính, tổ 1, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0168067,
   "Latitude": 105.7940437
 },
 {
   "STT": 855,
   "Name": "Phòng khám Chuyên khoa Ngoại",
   "address": "Số 60 phố Hào Nam, phường Ô Chợ Dừa, Đống Đa, Hà Nội",
   "Longtitude": 21.0256865,
   "Latitude": 105.8271682
 },
 {
   "STT": 856,
   "Name": "Phòng khám Chuyên khoa Mắt Tân An",
   "address": "C5-P2 phố Lương Định Của, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.004543,
   "Latitude": 105.835694
 },
 {
   "STT": 857,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 57 Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0624315,
   "Latitude": 105.8973137
 },
 {
   "STT": 858,
   "Name": "Nha Khoa Hồng Vân",
   "address": "Kiốt số 5, tầng 1, tòa nhà CT2A, khu đô thị Mỹ Đình II, huyện từ Liêm, Hà Nội",
   "Longtitude": 21.0312482,
   "Latitude": 105.7694511
 },
 {
   "STT": 859,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Quang Tuấn",
   "address": "Số 173 phố Lò Đúc, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0101475,
   "Latitude": 105.8605194
 },
 {
   "STT": 860,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 59 phố Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0621167,
   "Latitude": 105.8969806
 },
 {
   "STT": 861,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 11 Quốc Tử Giám, phường Văn Chương, quận Đống Đa, Hà Nội",
   "Longtitude": 21.026201,
   "Latitude": 105.837579
 },
 {
   "STT": 862,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 9 ngách 117/48 phố Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0129876,
   "Latitude": 105.8200121
 },
 {
   "STT": 863,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thanh Kỳ",
   "address": "Số 39 ngách 61/15 ngõ 61, phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0018,
   "Latitude": 105.8667577
 },
 {
   "STT": 864,
   "Name": "Phòng khám Sản Phụ Khoa Phương Hạnh",
   "address": "Tập thể A76, Thôn Hoàng 5, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0448574,
   "Latitude": 105.7863088
 },
 {
   "STT": 865,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 06, phố Đỗ Quang, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0111175,
   "Latitude": 105.8019279
 },
 {
   "STT": 866,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Ngọc Vũ",
   "address": "Số 76 đường Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9556265,
   "Latitude": 105.7559969
 },
 {
   "STT": 867,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Chi Nhánh Công Ty Tnhh Phát Triển Kỹ Thuật Y Học",
   "address": "Số 230, Nghi Tàm, phường Yên Phụ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.055492,
   "Latitude": 105.836667
 },
 {
   "STT": 868,
   "Name": "Phòng khám Chuyên khoa Nhi Hải Bảo",
   "address": "Số 1 ngõ 231 phố Khâm Thiên, phường Thổ Quan, quận Đống Đa, Hà Nội",
   "Longtitude": 20.9978926,
   "Latitude": 105.8463412
 },
 {
   "STT": 869,
   "Name": "Phòng khám Chuyên khoa Nội Đặng Quốc Kỷ",
   "address": "Số 191 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 870,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Thị Mai Huyên",
   "address": "Số 8 ngõ 192 phố Hạ Đình, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9866719,
   "Latitude": 105.8099499
 },
 {
   "STT": 871,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Nha Khoa Việt Đức",
   "address": "Đường Ngũ Hiệp, Thôn Tự Khoát, xã Ngũ Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9234334,
   "Latitude": 105.8594137
 },
 {
   "STT": 872,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Huy Hà",
   "address": "Số 295 đường Ngọc Hồi, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.948715,
   "Latitude": 105.8443869
 },
 {
   "STT": 873,
   "Name": "Nha Khoa Phan Vân",
   "address": "Số 179 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0011009,
   "Latitude": 105.8415086
 },
 {
   "STT": 874,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Hoàn Mỹ",
   "address": "Ô 2, Lô No 4A, Khu đô thị Đền Lừ II, phường Hoàng Văn Thụ, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9865719,
   "Latitude": 105.8582315
 },
 {
   "STT": 875,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 234 đường Lương Thế Vinh, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.992345,
   "Latitude": 105.7952844
 },
 {
   "STT": 876,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Phùng Minh Phương",
   "address": "TK Thao Chính, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7422333,
   "Latitude": 105.9091794
 },
 {
   "STT": 877,
   "Name": "Phòng khám Chuyên khoa Ngoại Vũ Đức Tuấn",
   "address": "Tiểu khu Thao Chính, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.74565,
   "Latitude": 105.911
 },
 {
   "STT": 878,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Phòng 109, E5, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.992434,
   "Latitude": 105.801811
 },
 {
   "STT": 879,
   "Name": "Phòng khám Chuyên khoa Nội Minh Thuận",
   "address": "Số 6 ngõ 250 phố Khương Trung, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9929281,
   "Latitude": 105.8168982
 },
 {
   "STT": 880,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Hoàng Phúc ",
   "address": "Số 23 phố Trần Hưng Đạo, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0192045,
   "Latitude": 105.8582939
 },
 {
   "STT": 881,
   "Name": "Phòng khám Chuyên khoa Mắt Minh Hà - 339",
   "address": "Số 339 phố Huế, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.009866,
   "Latitude": 105.851563
 },
 {
   "STT": 882,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Huy Điệp",
   "address": "Khu 2, Thôn Nội Đồng, xã Đại Thịnh, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1843871,
   "Latitude": 105.7203792
 },
 {
   "STT": 883,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Tổ 3, thị trấn Sóc Sơn, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538889,
   "Latitude": 105.8494444
 },
 {
   "STT": 884,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Thanh Nhàn",
   "address": "Khu Thá, Xuân Giang, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2427145,
   "Latitude": 105.8865436
 },
 {
   "STT": 885,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Bích Thiện",
   "address": "Số 41, tổ 33, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 886,
   "Name": "Phòng khám Đa Khoa An Việt Trực Thuộc Công Ty Cổ Phần Dịch Vụ Và Thương Mại Dương Ánh Châu",
   "address": "Số 6, đường Tây cao tốc, Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1280905,
   "Latitude": 105.7799803
 },
 {
   "STT": 887,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Medelab Việt Nam",
   "address": "Số 1B Yết Kiêu, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.020689,
   "Latitude": 105.8432034
 },
 {
   "STT": 888,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Bệnh Viện Tâm Anh",
   "address": "Số 30A, phố Lý Nam Đế, phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0318859,
   "Latitude": 105.8443155
 },
 {
   "STT": 889,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ",
   "address": "Số 35 phố Tràng Thi, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0311543,
   "Latitude": 105.8491764
 },
 {
   "STT": 890,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thành Lê",
   "address": "Thôn Tráng Việt, xã Tráng Việt, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1394882,
   "Latitude": 105.7404068
 },
 {
   "STT": 891,
   "Name": "Nha Khoa Khanh",
   "address": "P109 - B1 Trung Tự, phường Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0095739,
   "Latitude": 105.8351123
 },
 {
   "STT": 892,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đào Đình Thi",
   "address": "Số 33A ngõ 38 Phương Mai (cũ: Căn hộ 1+2 - D23 tập thể Kim Liên), phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0053433,
   "Latitude": 105.836525
 },
 {
   "STT": 893,
   "Name": "Phòng khám Sản Phụ Khoa Và Siêu Âm Sản Phụ Khoa Bác sĩ Phạm Thị Thanh Hiền",
   "address": "Số 98A phố Hàng Bông, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.030178,
   "Latitude": 105.84638
 },
 {
   "STT": 894,
   "Name": "Phòng khám Mẹ Và Bé Trực Thuộc Công Ty Tnhh Thương Mại Và Dịch Vụ Thịnh Thịnh",
   "address": "Lô số 6, đường Nguyễn Hữu Thọ, tổ 12, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9625316,
   "Latitude": 105.8399127
 },
 {
   "STT": 895,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Bác sĩ Nguyễn Thị Yến",
   "address": "Căn hộ số 1- 1 nhà A1 T5C khu Tập thể quân đội, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0200535,
   "Latitude": 105.8246266
 },
 {
   "STT": 896,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Toàn Nha",
   "address": "Số 157 phố Đông Các (cũ: số 93 tập thể lao động Thịnh Hào), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0186862,
   "Latitude": 105.8286709
 },
 {
   "STT": 897,
   "Name": "Phòng khám Chuyên khoa Da Liễu Thảo Anh",
   "address": "Số 119 Đặng Văn Ngữ, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013278,
   "Latitude": 105.833549
 },
 {
   "STT": 898,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Nano",
   "address": "Kiốt số 2 tòa nhà B14 Kim Liên, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0083873,
   "Latitude": 105.8343435
 },
 {
   "STT": 899,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Phòng 106A-D14B khu tập thể Quỳnh Mai, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0008348,
   "Latitude": 105.8606695
 },
 {
   "STT": 900,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 22 phố Triệu Việt Vương, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0130973,
   "Latitude": 105.8502331
 },
 {
   "STT": 901,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 61 Hồng Mai",
   "address": "Số 61 phố Hồng Mai, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 902,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đoàn Thu Hương",
   "address": "Số 71B phố Nguyễn Du (Số 5A Yết Kiêu cửa bên), phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.020032,
   "Latitude": 105.842654
 },
 {
   "STT": 903,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Thị Thu Hải",
   "address": "Số 5, M3 thị trấn 6, Bắc Linh Đàm, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9696874,
   "Latitude": 105.830065
 },
 {
   "STT": 904,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Vũ Thị Thanh Bình",
   "address": "P101 nhà A1, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.990774,
   "Latitude": 105.799728
 },
 {
   "STT": 905,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Bác sĩ Dũng",
   "address": "Số 25 ngõ 183 phố Hoàng Văn Thái, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9966951,
   "Latitude": 105.8229427
 },
 {
   "STT": 906,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Hoàng Thị Yến",
   "address": "1/2 căn hộ số 112, nhà B5, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0216112,
   "Latitude": 105.8146509
 },
 {
   "STT": 907,
   "Name": "Phòng khám Chuyên khoa Nội Phạm Công Hội",
   "address": "1/2 căn hộ số 112-B5, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0215104,
   "Latitude": 105.8144009
 },
 {
   "STT": 908,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Thị Lan Anh",
   "address": "Số 45 phố Nam Tràng (30 Trần Kế Xương), phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0455546,
   "Latitude": 105.8404684
 },
 {
   "STT": 909,
   "Name": "Nha Khoa Phương Nam",
   "address": "Số nhà 36 ,đường Hồ Tùng Mậu, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.036649,
   "Latitude": 105.7791529
 },
 {
   "STT": 910,
   "Name": "Phòng khám Sản Phụ Khoa Ngọc Oanh",
   "address": "Số 5 ngõ 1 phố Hoàng Ngọc Phách, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0150707,
   "Latitude": 105.8131295
 },
 {
   "STT": 911,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Cao Y Dược Minh Tâm",
   "address": "Số 1B Trần Hưng Đạo, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0187507,
   "Latitude": 105.8595539
 },
 {
   "STT": 912,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Cao Y Dược Minh Tâm",
   "address": "Số 21 Trần Khánh Dư, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.020726,
   "Latitude": 105.861548
 },
 {
   "STT": 913,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Ngọc Hà",
   "address": "Số 321A phố Bạch Mai, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 914,
   "Name": "Phòng khám Chuyên khoa Nhi Cây Thông Xanh",
   "address": "Số 39 ngõ 255 phố Vọng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.994847,
   "Latitude": 105.844044
 },
 {
   "STT": 915,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Bác sĩ Trương Trọng Hậu",
   "address": "Số 60 phố Hàng Bông, phường Hàng Gai, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.030599,
   "Latitude": 105.847596
 },
 {
   "STT": 916,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế Vip 12",
   "address": "Tầng 3 tòa nhà Hòa Phát, số 257 đường Giải Phóng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 20.9984981,
   "Latitude": 105.8417115
 },
 {
   "STT": 917,
   "Name": "Phòng khám Chuyên khoa Da Liễu Trần Văn Thọ",
   "address": "Số 125 phố Núi Trúc, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0288099,
   "Latitude": 105.8235621
 },
 {
   "STT": 918,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Khánh Phương",
   "address": "Số nhà 74, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 919,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Linh Anh",
   "address": "Số 119 Đặng Văn Ngữ, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013278,
   "Latitude": 105.833549
 },
 {
   "STT": 920,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Văn Nghĩa",
   "address": "Số 52 Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1423342,
   "Latitude": 105.5055891
 },
 {
   "STT": 921,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Hàn Hoàng Hải",
   "address": "Số 54C Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1425478,
   "Latitude": 105.5056588
 },
 {
   "STT": 922,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Trịnh Đình Vượng",
   "address": "Số 106, tổ 6, phường Xuân Khanh, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.114733,
   "Latitude": 105.4393571
 },
 {
   "STT": 923,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 90 Cửa Bắc, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.041726,
   "Latitude": 105.842416
 },
 {
   "STT": 924,
   "Name": "Nha Khoa Hùng Quế",
   "address": "Số 53 lô C khu 7,2 ha, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0417308,
   "Latitude": 105.8099499
 },
 {
   "STT": 925,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Phát Triển Công Nghệ Cao Y Dược Minh Tâm",
   "address": "Số 12 Đinh Công Tráng, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0189347,
   "Latitude": 105.8597817
 },
 {
   "STT": 926,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Công Nghệ Y Tế Xuân Hồng",
   "address": "Nhà B1 khu dự án nhà ở Thôn Hoàng 4, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0345864,
   "Latitude": 105.7629924
 },
 {
   "STT": 927,
   "Name": "Phòng khám Đa Khoa Vĩnh Phúc Trực Thuộc Công Ty Tnhh Y Dược Sơn Tùng",
   "address": "Số 595, đường Hoàng Hoa Thám, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0470472,
   "Latitude": 105.8092905
 },
 {
   "STT": 928,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Mỹ Hưng",
   "address": "Số 40A, phố Trần Hưng Đạo, phường Hàng Bài, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0209907,
   "Latitude": 105.8522201
 },
 {
   "STT": 929,
   "Name": "Phòng khám Sản Phụ Khoa Ngọc Oanh",
   "address": "Số 5 ngõ 1 phố Hoàng Ngọc Phách, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0150707,
   "Latitude": 105.8131295
 },
 {
   "STT": 930,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Thăng Long - Thuận An",
   "address": "Số 02 (tầng 1 + tầng 4 phòng 402) phố Triệu Quốc Đạt, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0263812,
   "Latitude": 105.8475597
 },
 {
   "STT": 931,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Minh Khanh",
   "address": "Số 37 ngõ 183 phố Đặng Tiến Đông, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014239,
   "Latitude": 105.821736
 },
 {
   "STT": 932,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Kế Hoạch Hóa Gia Đình Nguyễn Thị Nhã",
   "address": "Số 198 - A1 phố Bạch Mai, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 933,
   "Name": "Phòng khám Đa Khoa Thiên Phúc Trực Thuộc Công Ty Cổ Phần Thương Mại Và Dịch Vụ Đồng Hưng",
   "address": "Số 37 phố Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1425846,
   "Latitude": 105.5054087
 },
 {
   "STT": 934,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Châu",
   "address": "Số 21 (cũ 59B tổ 4), ngõ 444, phố Đội Cấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0368183,
   "Latitude": 105.8093267
 },
 {
   "STT": 935,
   "Name": "Phòng khám Quốc Tế Việt Minh Trực Thuộc Công Ty Cổ Phần Y Tế Quốc Tế Việt Minh",
   "address": "Số 130 Nguyễn Đức Cảnh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.986655,
   "Latitude": 105.8520591
 },
 {
   "STT": 936,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Trách Nhiệm Hữu Hạn Jclv",
   "address": "Phòng 203, tầng 2, tòa nhà Hàn Việt, số 203 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9968369,
   "Latitude": 105.8615514
 },
 {
   "STT": 937,
   "Name": "Phòng khám Chuyên khoa Nội Chữ Thập Đỏ",
   "address": "Km 29+700, Quốc lộ 6A, Thôn Đồi Chè, xã Thanh Bình, Chương Mỹ, Hà Nội",
   "Longtitude": 20.9603564,
   "Latitude": 105.7625314
 },
 {
   "STT": 938,
   "Name": "Phòng khám Chuyên khoa X-Quang Chữ Thập Đỏ",
   "address": "Km 29+700, Quốc lộ 6A, Thôn Đồi Chè, xã Thanh Bình, Chương Mỹ, Hà Nội",
   "Longtitude": 20.9603564,
   "Latitude": 105.7625314
 },
 {
   "STT": 939,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Âu Lạc",
   "address": "Số 178 Lạc Long Quân, phường Bưởi, Tây Hồ, Hà Nội",
   "Longtitude": 21.0525508,
   "Latitude": 105.8093794
 },
 {
   "STT": 940,
   "Name": "Phòng khám Siêu Âm An Nhi",
   "address": "Số 49 ngõ 54 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0238903,
   "Latitude": 105.8101971
 },
 {
   "STT": 941,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Bác sĩ Nguyễn Thị Hoài Thu",
   "address": "Số 90A Phố Vọng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 20.997919,
   "Latitude": 105.842424
 },
 {
   "STT": 942,
   "Name": "Nha Khoa Đức",
   "address": "Số 90 phố Lò Đúc, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0143594,
   "Latitude": 105.8572806
 },
 {
   "STT": 943,
   "Name": "Phòng khám Răng Hàm Mặt Nam Trung",
   "address": "Số 78 phố Trần Hưng Đạo, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0233256,
   "Latitude": 105.8476583
 },
 {
   "STT": 944,
   "Name": "Phòng khám Chuyên khoa Nội Trần Duy Cường",
   "address": "Số 54 phố Vân Đồn, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0139967,
   "Latitude": 105.8640307
 },
 {
   "STT": 945,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Thanh",
   "address": "Đội 2, xã Cát Quế, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0503802,
   "Latitude": 105.6733918
 },
 {
   "STT": 946,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 07 ngách 26/17 Nguyên Hồng (cũ D4 khu A Nam Thành Công), phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0174075,
   "Latitude": 105.8113527
 },
 {
   "STT": 947,
   "Name": "Nha Khoa Việt Hàn",
   "address": "Số 253 đường Giải Phóng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021881,
   "Latitude": 105.8412069
 },
 {
   "STT": 948,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Dịch Vụ Khám Chữa Bệnh Thăng Long Hà Nội",
   "address": "Số 101 nhà N23 thị trấn Quân Đội, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0172632,
   "Latitude": 105.8073191
 },
 {
   "STT": 949,
   "Name": "Nha Khoa Quang Khánh",
   "address": "Số 76 đường Lạc Long Quân, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.054726,
   "Latitude": 105.839749
 },
 {
   "STT": 950,
   "Name": "Nha Khoa Minh Sinh 2",
   "address": "Số 46C phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 951,
   "Name": "Nha Khoa Minh Sinh",
   "address": "Số 174 phố Hàng Bông, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.029093,
   "Latitude": 105.844744
 },
 {
   "STT": 952,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lý Hán Thành",
   "address": "Nhà 11 lô A khu 7,2ha, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0417308,
   "Latitude": 105.8099499
 },
 {
   "STT": 953,
   "Name": "Phòng khám Răng Hàm Mặt - Nha Khoa Nhật Tân- Công Ty Cổ Phần Đầu Tư Thương Mại Toàn Khánh",
   "address": "Số 525 Âu Cơ, phường Nhật Tân, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0804165,
   "Latitude": 105.818532
 },
 {
   "STT": 954,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ",
   "address": "Đội 2, xã Cát Quế, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0503802,
   "Latitude": 105.6733918
 },
 {
   "STT": 955,
   "Name": "Phòng khám Đa Khoa Trung Hòa -  Nhân Chính Trực Thuộc Công Ty Tnhh Bệnh Viện Việt Pháp Hà Nội",
   "address": "Tầng 1, nhà 24T1, khu đô thị Trung Hòa - Nhân Chính, phố Hoàng Đạo Thúy, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0084666,
   "Latitude": 105.8020233
 },
 {
   "STT": 956,
   "Name": "Phòng khám Răng Hàm Mặt Hà Nội",
   "address": "Số 36 phố Sài Đồng, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0363131,
   "Latitude": 105.9095684
 },
 {
   "STT": 957,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh",
   "address": "Thôn Đoàn Kết, xã Cổ Đông, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.04679,
   "Latitude": 105.505885
 },
 {
   "STT": 958,
   "Name": "Phòng khám Nha Khoa Hồng",
   "address": "Ki ốt số 146, chợ Bún, Thôn Thuận Tốn, xã Đa Tốn, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9855858,
   "Latitude": 105.9287931
 },
 {
   "STT": 959,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Thăng Long",
   "address": "Số nhà 86, phố Phan Đình Phùng, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.085828,
   "Latitude": 105.6679938
 },
 {
   "STT": 960,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Thượng, xã Đông Dư, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9949348,
   "Latitude": 105.9167862
 },
 {
   "STT": 961,
   "Name": "Phòng khám Chuyên khoa Nội Hà Khắc Phụ",
   "address": "53 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1136262,
   "Latitude": 105.4955165
 },
 {
   "STT": 962,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Việt Nhật",
   "address": "Số 55 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1135443,
   "Latitude": 105.4955648
 },
 {
   "STT": 963,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thái Sơn",
   "address": "Khu thị trấn 73 Lục Quân 1, Thôn Đoàn Kết, xã Cổ Đông, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.04679,
   "Latitude": 105.505885
 },
 {
   "STT": 964,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 93 đường Cổ Linh, tổ 23, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0280599,
   "Latitude": 105.8926853
 },
 {
   "STT": 965,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Văn Lực",
   "address": "Xóm Tự, Thôn Phù Đổng 1, xã Phù Đổng, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.060339,
   "Latitude": 105.9641124
 },
 {
   "STT": 966,
   "Name": "Phòng khám Chuyên khoa Nội 10A Quang Vinh",
   "address": "Thôn Chúc Lý, xã Ngọc Hòa, Chương Mỹ, Hà Nội",
   "Longtitude": 20.9202554,
   "Latitude": 105.6958162
 },
 {
   "STT": 967,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 34 ngõ 135 Nguyễn Văn Cừ, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.042514,
   "Latitude": 105.870493
 },
 {
   "STT": 968,
   "Name": "Phòng khám Chuyên khoa Nội An Khánh",
   "address": "Số 69 ngách 355/41 Nguyễn Văn Linh, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.036165,
   "Latitude": 105.901661
 },
 {
   "STT": 969,
   "Name": "Phòng khám Chuyên khoa Nội Bình An",
   "address": "Số 1/107, đường S, tổ dân phố Nông Lâm, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0058696,
   "Latitude": 105.9370305
 },
 {
   "STT": 970,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Bàng",
   "address": "Số 01 Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1419526,
   "Latitude": 105.5054356
 },
 {
   "STT": 971,
   "Name": "Phòng khám Đa Khoa Thăng Long",
   "address": "Thôn 9, xã Thạch Hòa, huyện Thạch Thất, Hà Nội",
   "Longtitude": 20.9829917,
   "Latitude": 105.5311811
 },
 {
   "STT": 972,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Lê Thị Thu Lan",
   "address": "Số 193 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9709127,
   "Latitude": 105.7838737
 },
 {
   "STT": 973,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Nguyễn Thị Quế",
   "address": "Khu Cây Sau, phường Trung Hưng, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1313193,
   "Latitude": 105.5049436
 },
 {
   "STT": 974,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Kim Phượng",
   "address": "Số 05 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9676061,
   "Latitude": 105.7879355
 },
 {
   "STT": 975,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Văn Tuấn",
   "address": "Số 230 Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1492483,
   "Latitude": 105.5054864
 },
 {
   "STT": 976,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Hàn Hoàng Sơn",
   "address": "Số 17 Phạm Ngũ Lão, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1396779,
   "Latitude": 105.5069554
 },
 {
   "STT": 977,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Lê Thị Thanh",
   "address": "Số 4 ngách 3, ngõ 351 đường Lĩnh Nam, phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9833181,
   "Latitude": 105.8763653
 },
 {
   "STT": 978,
   "Name": "Phòng khám Nha Khoa Minh Cường",
   "address": "Số 4 Khu tập thể In may Bộ Công an, đường 19/5, phường Văn Quán, Hà Đông, Hà Nội",
   "Longtitude": 20.975595,
   "Latitude": 105.794928
 },
 {
   "STT": 979,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nha Khoa 16",
   "address": "Số 16 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9699147,
   "Latitude": 105.784905
 },
 {
   "STT": 980,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Tổ 1, phường Yên Nghĩa, Hà Đông, Hà Nội",
   "Longtitude": 20.9537772,
   "Latitude": 105.7338133
 },
 {
   "STT": 981,
   "Name": "Phòng khám Nha Khoa Xuân Giang",
   "address": "61, phố Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9809888,
   "Latitude": 105.8230378
 },
 {
   "STT": 982,
   "Name": "Phòng khám Đa Khoa Y Cao Hà Nội",
   "address": "Thôn Thanh Ấm, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.728887,
   "Latitude": 105.771362
 },
 {
   "STT": 983,
   "Name": "Phòng khám Chuyên khoa Nội Bùi Chí Thiện",
   "address": "Số 24 - D5 Khu giãn dân Yên Phúc, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9691696,
   "Latitude": 105.7879333
 },
 {
   "STT": 984,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Đoàn Xá, xã Đồng Tiến, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7337858,
   "Latitude": 105.7455836
 },
 {
   "STT": 985,
   "Name": "Phòng khám Chuyên khoa Nội Ban Mai - Bác sĩ Mai",
   "address": "Số 3, ngõ 69 phố Vọng Hà, phường Chương Dương, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0315939,
   "Latitude": 105.8572074
 },
 {
   "STT": 986,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trịnh Thị Hồng",
   "address": "Số 301 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0448412,
   "Latitude": 105.8686635
 },
 {
   "STT": 987,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 20A",
   "address": "Số 12 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.965712,
   "Latitude": 105.790734
 },
 {
   "STT": 988,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Hoàng Thị Cúc",
   "address": "Số nhà 54, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 989,
   "Name": "Cơ Sở Dịch Vụ Tiêm, Thay Băng Lã Văn Thắng",
   "address": "Thôn 1, xã Vạn Phúc, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9232706,
   "Latitude": 105.8980272
 },
 {
   "STT": 990,
   "Name": "Phòng khám Chuyên khoa Nội An Việt",
   "address": "Số 202 phố Lê Trọng Tấn, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9938395,
   "Latitude": 105.8318009
 },
 {
   "STT": 991,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 367 đường Giải Phóng, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.996327,
   "Latitude": 105.841397
 },
 {
   "STT": 992,
   "Name": "Phòng khám Đa Khoa Nguyễn Lệ Thúy",
   "address": "Số 159 phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 993,
   "Name": "Nha Khoa Hoa Bằng",
   "address": "Số 46, phố Hoa Bằng, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0249595,
   "Latitude": 105.795396
 },
 {
   "STT": 994,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Toàn Nha",
   "address": "138 phố Nguyễn Lương Bằng, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.015876,
   "Latitude": 105.8283101
 },
 {
   "STT": 995,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 111B-B6, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0066938,
   "Latitude": 105.8356348
 },
 {
   "STT": 996,
   "Name": "Phòng khám Đa Khoa Đông Đô Trực Thuộc Công Ty Cổ Phần Y Tế Thăng Long",
   "address": "Số 61 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0037782,
   "Latitude": 105.8376342
 },
 {
   "STT": 997,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 7, H13, tập thể Đại học Sư phạm Hà Nội, tổ 22, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0384289,
   "Latitude": 105.7855525
 },
 {
   "STT": 998,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Xquang",
   "address": "Số nhà 44, phố Trần Bình, tổ 40, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0350848,
   "Latitude": 105.7787168
 },
 {
   "STT": 999,
   "Name": "Nha Khoa Hà Anh",
   "address": "Tầng 1, số 191, đường Hoàng Quốc Việt, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0462978,
   "Latitude": 105.7994211
 },
 {
   "STT": 1000,
   "Name": "Phòng khám Chuyên khoa Nội Phạm Thái Hồng",
   "address": "Số 95 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1001,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Hào Nam",
   "address": "Số 2 ngách 23 ngõ 168 Hào Nam (tổ 117), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0261203,
   "Latitude": 105.8284003
 },
 {
   "STT": 1002,
   "Name": "Phòng khám Chuyên khoa Nội Bác sĩ Thúy",
   "address": "Số nhà 38, ngõ 291, đường Lạc Long Quân, tổ 6, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0652024,
   "Latitude": 105.8101498
 },
 {
   "STT": 1003,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Trần Thị Thanh Nga - 49",
   "address": "Số 264 Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.001607,
   "Latitude": 105.841682
 },
 {
   "STT": 1004,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Cảnh Hùng",
   "address": "Số 7 - M14 Mai Hương, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0007632,
   "Latitude": 105.8508937
 },
 {
   "STT": 1005,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 39, ngõ 575, phố Kim Mã, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0297473,
   "Latitude": 105.8103271
 },
 {
   "STT": 1006,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Tim Mạch Tâm An",
   "address": "Số 110 nhà D, tổ 35, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9925823,
   "Latitude": 105.8393044
 },
 {
   "STT": 1007,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 75 tổ 27, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0448412,
   "Latitude": 105.8686635
 },
 {
   "STT": 1008,
   "Name": "Phòng khám Chuyên khoa Nội Vy Linh",
   "address": "Số 32 phố Đức Giang, tổ 21, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 1009,
   "Name": "Phòng khám Chuyên khoa Nội Bình Hiền",
   "address": "Số 104, tổ dân phố Cửu Việt, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0083504,
   "Latitude": 105.939247
 },
 {
   "STT": 1010,
   "Name": "Phòng khám Chuyên khoa Nội Quang Vinh",
   "address": "Thôn Ngọc Chi, xã Vĩnh Ngọc, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1157878,
   "Latitude": 105.8306352
 },
 {
   "STT": 1011,
   "Name": "Phòng khám Chuyên khoa Nội Trần Thị Nga",
   "address": "Số 40, tổ 40, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1012,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 25 ngõ 116 tổ 7, phường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0588582,
   "Latitude": 105.8583873
 },
 {
   "STT": 1013,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Y Tế Tâm Việt",
   "address": "Km 28 + 500, xã Trường Yên, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9127576,
   "Latitude": 105.6559158
 },
 {
   "STT": 1014,
   "Name": "Phòng khám Chuyên khoa Da Liễu Nguyễn Thị Vỹ",
   "address": "Tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.17064,
   "Latitude": 105.8424
 },
 {
   "STT": 1015,
   "Name": "Phòng khám Chuyên khoa Ngoại - Thái Sơn",
   "address": "Số 02, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1611142,
   "Latitude": 105.8495796
 },
 {
   "STT": 1016,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Trương Quang Tuyết",
   "address": "Số 01N 7C tập thể Học viện Quân Y, tổ dân phố 14, phường Kiến Hưng, Hà Đông, Hà Nội",
   "Longtitude": 20.9665272,
   "Latitude": 105.7884224
 },
 {
   "STT": 1017,
   "Name": "Nha Khoa Duy Thanh",
   "address": "Khu tập thể bệnh viện khu vực 1, xã Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9210189,
   "Latitude": 105.8542059
 },
 {
   "STT": 1018,
   "Name": "Phòng khám Đa Khoa Liên Kết Y Khoa (Medlink) Trực Thuộc Công Ty Cổ Phần Khám Chữa Bệnh Quốc Tế",
   "address": "Nhà A6, đường số 3, phố Trần Thái Tông, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0325873,
   "Latitude": 105.7882301
 },
 {
   "STT": 1019,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hoàng Văn Nhuận",
   "address": "Số 10, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1641758,
   "Latitude": 105.8440749
 },
 {
   "STT": 1020,
   "Name": "Phòng khám Chuyên khoa Phụ Sản-Khhgđ Trần Thị Thanh Thủy",
   "address": "Tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.17064,
   "Latitude": 105.8424
 },
 {
   "STT": 1021,
   "Name": "Phòng khám Chuyên khoa Phụ Sản -Khhgđ Trần Minh An",
   "address": "Tổ 8, phường Phú La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9602408,
   "Latitude": 105.7654355
 },
 {
   "STT": 1022,
   "Name": "Phòng khám Đa Khoa Bình Minh",
   "address": "Số 101 - 103 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025061,
   "Latitude": 105.8415395
 },
 {
   "STT": 1023,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Xuân Mai",
   "address": "Số 11A phố Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0166531,
   "Latitude": 105.8495593
 },
 {
   "STT": 1024,
   "Name": "Phòng khám Chuyên khoa Mắt Pgs-Ts Nguyễn Chí Dũng",
   "address": "Số 445 phố Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 1025,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Thị Thu Hiền",
   "address": "Căn hộ 106 - nhà B - Khu tập thể Địa chất công trình, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.989171,
   "Latitude": 105.7835348
 },
 {
   "STT": 1026,
   "Name": "Nha Khoa Nga",
   "address": "Tầng 1 số 78 phố Thái Hà mới (số cũ: 78E, tổ 29), phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0108448,
   "Latitude": 105.822425
 },
 {
   "STT": 1027,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trường Thủy",
   "address": "90 ngõ Chợ Khâm Thiên, phường Trung Phụng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.016964,
   "Latitude": 105.83847
 },
 {
   "STT": 1028,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Minh Phương",
   "address": "Số 82 Nghi Tàm, phường Yên Phụ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.052022,
   "Latitude": 105.839066
 },
 {
   "STT": 1029,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nha Khoa Quốc Tế Số 5 Trực Thuộc Công Ty Tnhh Nha Khoa Quốc Tế Nguyễn Khoa",
   "address": "Số 5 phố Nguyễn Du, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0182949,
   "Latitude": 105.851421
 },
 {
   "STT": 1030,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tập thể Z157, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0421075,
   "Latitude": 105.7607001
 },
 {
   "STT": 1031,
   "Name": "Cơ Sở Dịch Vụ  Kính Thuốc Lê Thị Minh Vân",
   "address": "Số 101A, nhà C5, tập thể Giảng Võ, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0060745,
   "Latitude": 105.8264154
 },
 {
   "STT": 1032,
   "Name": "Phòng khám Đa Khoa Đức Minh",
   "address": "Số 32 phố Phùng Hưng, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0301329,
   "Latitude": 105.8449727
 },
 {
   "STT": 1033,
   "Name": "Nha Khoa Bảo Việt Trực Thuộc Công Ty Tnhh Chăm Sóc Sức Khỏe Bảo Việt",
   "address": "Số 117, phố Trần Đăng Ninh, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0377099,
   "Latitude": 105.7935638
 },
 {
   "STT": 1034,
   "Name": "Nha Khoa Trâu Quỳ",
   "address": "Số 52, đường Ngô Xuân Quảng, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0177861,
   "Latitude": 105.936946
 },
 {
   "STT": 1035,
   "Name": "Phòng khám Chuyên khoa Mắt Bùi Thị Thu",
   "address": "Thôn Chợ, xã Bình Minh, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8891038,
   "Latitude": 105.7631835
 },
 {
   "STT": 1036,
   "Name": "Phòng khám Chuyên khoa Nội Số 1 Kim Chung",
   "address": "Thôn Hậu Dưỡng, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1227594,
   "Latitude": 105.7706677
 },
 {
   "STT": 1037,
   "Name": "Phòng khám Đa Khoa Hồng Hà Trực Thuộc Công Ty Tnhh Phương Đông Hồng",
   "address": "Số 244 Lê Thanh Nghị, tổ 7, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0019395,
   "Latitude": 105.8424078
 },
 {
   "STT": 1038,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Đức Chí",
   "address": "Thôn Nhật Tiến, xã Trường Yên, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.91782,
   "Latitude": 105.6423
 },
 {
   "STT": 1039,
   "Name": "Nha Khoa Phương Nam",
   "address": "Số 147 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0215369,
   "Latitude": 105.8090314
 },
 {
   "STT": 1040,
   "Name": "Nha Khoa Hân Đào",
   "address": "1M6B thị trấn 6 Bắc Linh Đàm, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9737064,
   "Latitude": 105.8521343
 },
 {
   "STT": 1041,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Biodent Hà Nội",
   "address": "Số 88 phố Tô Vĩnh Diện, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9998518,
   "Latitude": 105.8197921
 },
 {
   "STT": 1042,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Vũ Trường Phong",
   "address": "28/35 khu A, tổ 17B, ngõ 109, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9981236,
   "Latitude": 105.8375499
 },
 {
   "STT": 1043,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Văn Hòa",
   "address": "Số 115 phố Hoàng Văn Thái, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9947815,
   "Latitude": 105.824671
 },
 {
   "STT": 1044,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Hữu Duy",
   "address": "P104 (sử dụng tầng 1), nhà E8, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0220463,
   "Latitude": 105.8124494
 },
 {
   "STT": 1045,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Bác sĩ : Ngô Thu Lan",
   "address": "Số 19 phố Nhà Chung, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.028349,
   "Latitude": 105.8500509
 },
 {
   "STT": 1046,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Thế Đô",
   "address": "Cụm 1 Thôn Quỳnh Đô, xã Vĩnh Quỳnh, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.938595,
   "Latitude": 105.8395922
 },
 {
   "STT": 1047,
   "Name": "Phòng khám Chuyên khoa Nhi Lê Thị Hồng Minh",
   "address": "Thôn Chợ, xã Bình Minh, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8891038,
   "Latitude": 105.7631835
 },
 {
   "STT": 1048,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Trần Tiến Phong",
   "address": "Số 12C, tổ 28, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1624741,
   "Latitude": 105.8521164
 },
 {
   "STT": 1049,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 20C, ngõ 135 tổ 13, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.0372448,
   "Latitude": 105.8744723
 },
 {
   "STT": 1050,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đỗ Nguyên Nhung",
   "address": "Số 16, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1562117,
   "Latitude": 105.8493705
 },
 {
   "STT": 1051,
   "Name": "Nha Khoa Quốc Tế",
   "address": "Số nhà 371 đường Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0629379,
   "Latitude": 105.8951136
 },
 {
   "STT": 1052,
   "Name": "Nha Khoa Việt Ngọc",
   "address": "Số 25, đường 2, xã Phù Lỗ, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.4045522,
   "Latitude": 105.3140504
 },
 {
   "STT": 1053,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Kim Đức Bình",
   "address": "Ki ốt 8, chợ Mai Động, phường Mai Động, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9922067,
   "Latitude": 105.8633397
 },
 {
   "STT": 1054,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Newcare",
   "address": "Số 79 phố Phủ Doãn, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0311543,
   "Latitude": 105.8491764
 },
 {
   "STT": 1055,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 12, tổ 40, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1056,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Đoàn Thị Kim Liên",
   "address": "Tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.17064,
   "Latitude": 105.8424
 },
 {
   "STT": 1057,
   "Name": "Phòng khám Sản Phụ Khoa Siêu Âm Tâm Đức",
   "address": "Số nhà 21C ngõ 1 Lệ Mật, tổ 8, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0533486,
   "Latitude": 105.9009639
 },
 {
   "STT": 1058,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Nội Tiết Thanh Bình 1",
   "address": "Số 52, lô A1, khu đô thị mới Đại Kim, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9877167,
   "Latitude": 105.8315992
 },
 {
   "STT": 1059,
   "Name": "Phòng khám Chuyên khoa Nhi Nguyễn Thị Nết",
   "address": "Đội 4, Thôn Lâm Hộ, xã Thanh Lâm, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.2207398,
   "Latitude": 105.7409852
 },
 {
   "STT": 1060,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 74 phố Tân Thụy, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0435466,
   "Latitude": 105.8982188
 },
 {
   "STT": 1061,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Chợ Đồng Vàng, Hoàng Long, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7545905,
   "Latitude": 105.8322752
 },
 {
   "STT": 1062,
   "Name": "Phòng khám Chuyên khoa Mắt Phạm Ngọc Quý - 1",
   "address": "Số 3, tổ 31, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1063,
   "Name": "Phòng khám Chuyên khoa Sản Phụ Kim Ngưu 3",
   "address": "Số 01, ngõ 370, đường Cầu Giấy, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0359706,
   "Latitude": 105.7916605
 },
 {
   "STT": 1064,
   "Name": "Phòng khám Chuyên khoa Mắt Trực Thuộc Công Ty Tnhh Phát Triển",
   "address": "Tầng 1, Đơn nguyên B, nhà N3A, khu đô thị Trung Hòa - Nhân Chính, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0046545,
   "Latitude": 105.8027037
 },
 {
   "STT": 1065,
   "Name": "Phòng khám Đa Khoa Nguyễn Thị Soạn",
   "address": "Số 122, khu quốc lộ 3, Phù Lỗ, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2065111,
   "Latitude": 105.8488145
 },
 {
   "STT": 1066,
   "Name": "Phòng khám Đa Khoa Phúc Thái Trực Thuộc Công Ty Cổ Phần Công Nghiệp Thương Mại Và Dịch Vụ Y Tế Phúc Thái",
   "address": "Xóm 7, xã Ninh Hiệp, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0762299,
   "Latitude": 105.9494248
 },
 {
   "STT": 1067,
   "Name": "Phòng khám Sản Phụ Khoa - Khhgđ 24/46 Lê Văn Thiêm",
   "address": "Số 24 khu tập thể Thủy sản, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0030564,
   "Latitude": 105.8026455
 },
 {
   "STT": 1068,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Lưu Thị Hồng",
   "address": "Số 11 phố Thiền Quang, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0195662,
   "Latitude": 105.8424139
 },
 {
   "STT": 1069,
   "Name": "Phòng khám Chuyên khoa Nội Bạch Mai",
   "address": "Số 58 (tầng 1 + gác lửng), phố Đào Tấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0325939,
   "Latitude": 105.8087418
 },
 {
   "STT": 1070,
   "Name": "Phòng khám Chuyên khoa Nội K15",
   "address": "Số 8C Vạn Phúc, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0332389,
   "Latitude": 105.8179646
 },
 {
   "STT": 1071,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Nha Khoa Thiên Phúc",
   "address": "Số 115, đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1072,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Bác sĩ Nguyễn Hùng Sơn",
   "address": "Tầng 1 mặt sau tòa nhà 17T8 khu đô thị Trung Hòa- Nhân Chính, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0096221,
   "Latitude": 105.8007972
 },
 {
   "STT": 1073,
   "Name": "Phòng Xét Nghiệm Y Khoa Hà Nội",
   "address": "Số 180, ngõ xã Đàn 2, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0130526,
   "Latitude": 105.8317816
 },
 {
   "STT": 1074,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Lê Hồng Công",
   "address": "Số 52/553, đường Giải Phóng, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9860789,
   "Latitude": 105.8414778
 },
 {
   "STT": 1075,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Bùi Bạch Dương",
   "address": "Số 4, phố Quán Thánh, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0424038,
   "Latitude": 105.8421737
 },
 {
   "STT": 1076,
   "Name": "Nha Khoa Tây Hồ",
   "address": "Số 94 Âu Cơ, phường Tứ Liên, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.062066,
   "Latitude": 105.832544
 },
 {
   "STT": 1077,
   "Name": "Phòng khám Đa Khoa Dr.Binh Teleclinic Trực Thuộc Công Ty Tnhh Giải Pháp E2E",
   "address": "Tầng 1 đến tầng 4, số 11-13-15 phố Trần Xuân Soạn, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0169506,
   "Latitude": 105.8540824
 },
 {
   "STT": 1078,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Thái Hà",
   "address": "Số 490 phố xã Đàn, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0171379,
   "Latitude": 105.8314356
 },
 {
   "STT": 1079,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Đỗ Xuân Vinh",
   "address": "Số 41, ngõ 1142, đường La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0282852,
   "Latitude": 105.8066815
 },
 {
   "STT": 1080,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Số 6, ngách 85, ngõ 12, phố Đào Tấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0340247,
   "Latitude": 105.8095127
 },
 {
   "STT": 1081,
   "Name": "Phòng khám Chuyên khoa Nhi Abcd",
   "address": "Nhà 2, ngõ 160, tổ 40 Hào Nam, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0252002,
   "Latitude": 105.8273843
 },
 {
   "STT": 1082,
   "Name": "Nha Khoa Kim Cương",
   "address": "Số 23 phố Hàm Tử Quan, phường Phúc Tân, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0318528,
   "Latitude": 105.8578286
 },
 {
   "STT": 1083,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Trực Thuộc Công Ty Cổ Phần Dược Thiết Bị Y Tế Biphartek",
   "address": "Số 61E, đường Đê La Thành, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0155086,
   "Latitude": 105.8335127
 },
 {
   "STT": 1084,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": ", xã Hoàng Văn Thụ, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8595873,
   "Latitude": 105.6353878
 },
 {
   "STT": 1085,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đỗ Nguyên Nhung",
   "address": "Số 16, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1562117,
   "Latitude": 105.8493705
 },
 {
   "STT": 1086,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Vũ Mạnh Cường",
   "address": "Số 76 Sài Đồng, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0351147,
   "Latitude": 105.9106483
 },
 {
   "STT": 1087,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Điền",
   "address": "Phòng 101 nhà A1, thị trấn Thanh Xuân Bắc, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9930672,
   "Latitude": 105.7976362
 },
 {
   "STT": 1088,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 04 phố Thanh Yên, phường Phúc Tân, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0190488,
   "Latitude": 105.8582171
 },
 {
   "STT": 1089,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đức Sơn",
   "address": "Số 110/35 Cát Linh, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0291475,
   "Latitude": 105.828273
 },
 {
   "STT": 1090,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Răng Miệng Hồ Gia",
   "address": "Số 65, Thôn Đình Thôn, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0197327,
   "Latitude": 105.7775795
 },
 {
   "STT": 1091,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Xuân Hiệp",
   "address": "Số 470 đường Trần Khát Chân, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0083416,
   "Latitude": 105.8528868
 },
 {
   "STT": 1092,
   "Name": "Phòng khám Nha Khoa - Đức - Hiếu",
   "address": "Số nhà 372, đường Hồ Tùng Mậu, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0405798,
   "Latitude": 105.7649142
 },
 {
   "STT": 1093,
   "Name": "Phòng khám Chuyên khoa Sản Phụ Khoa 66 Trương Định",
   "address": "Số 66 đường Trương Định, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9937815,
   "Latitude": 105.8466875
 },
 {
   "STT": 1094,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Phòng 101 - B10, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0430757,
   "Latitude": 105.7952231
 },
 {
   "STT": 1095,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Tân Phong",
   "address": "Số 121 Yên Phụ, phường Yên Phụ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0533997,
   "Latitude": 105.8369574
 },
 {
   "STT": 1096,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Phòng 08 dãy H11 tập thể ĐHSP, tổ 22, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0339527,
   "Latitude": 105.7850022
 },
 {
   "STT": 1097,
   "Name": "Cơ Sở Dịch Vụ Y Tế Bạch Mai",
   "address": "Số 58 (tầng 2), phố Đào Tấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0328771,
   "Latitude": 105.8077431
 },
 {
   "STT": 1098,
   "Name": "Phòng khám Chuyên khoa Xquang",
   "address": "Số 39B Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9712372,
   "Latitude": 105.7836435
 },
 {
   "STT": 1099,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Vạn Phúc",
   "address": "Số 4 Khối Đoàn Kết, phường Vạn Phúc, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9608838,
   "Latitude": 105.9061031
 },
 {
   "STT": 1100,
   "Name": "Nha Khoa Kim Đồng",
   "address": "Số 79 ngõ 24 phố Kim Đồng, phường Thịnh Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.984336,
   "Latitude": 105.843603
 },
 {
   "STT": 1101,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Số 78, tổ 3, khu Chiến Thắng, Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.893522,
   "Latitude": 105.571545
 },
 {
   "STT": 1102,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Du Lịch Và Dược Phẩm Sơn Lâm",
   "address": "Số 101, đường Chiến Thắng, phường Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9811053,
   "Latitude": 105.7945874
 },
 {
   "STT": 1103,
   "Name": "Phòng khám Chuyên khoa Xquang 103 Phương Linh",
   "address": "Tầng 1 số nhà 5/159, phố Tây Sơn, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0840235,
   "Latitude": 105.6712668
 },
 {
   "STT": 1104,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh - Siêu Âm",
   "address": "Số 37 phố Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0622249,
   "Latitude": 105.8966481
 },
 {
   "STT": 1105,
   "Name": "Phòng khám Chuyên khoa Siêu Âm Chẩn Đoán Tâm Loan",
   "address": "Số nhà 317, phố Ga, thị trấn Thường Tín, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8713505,
   "Latitude": 105.8642509
 },
 {
   "STT": 1106,
   "Name": "Phòng khám Chuyên khoa Nội Đức Nhân",
   "address": "Số 274 phố Ngọc Trì tổ 11, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0208555,
   "Latitude": 105.9111901
 },
 {
   "STT": 1107,
   "Name": "Cơ Sở Dịch Vụ Cấp Cứu, Hỗ Trợ Vận Chuyển Người Bệnh Trong Nước Trực Thuộc Công Ty Tnhh Vận Chuyển Người Bệnh Bắc Việt",
   "address": "Số 48, phố Tăng Bạt Hổ, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0147724,
   "Latitude": 105.8588628
 },
 {
   "STT": 1108,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Toàn",
   "address": "Số 310 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995837,
   "Latitude": 105.861181
 },
 {
   "STT": 1109,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Tuyết Nhung",
   "address": "Tiểu khu đường, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7825722,
   "Latitude": 105.9156477
 },
 {
   "STT": 1110,
   "Name": "Nha Khoa Tín Nghĩa",
   "address": "Số 218, đường Cổ Bi, xóm 2, Thôn Cam, xã Cổ Bi, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0288599,
   "Latitude": 105.9450188
 },
 {
   "STT": 1111,
   "Name": "Phòng khám Sản Phụ Khoa",
   "address": "Căn hộ số N4A3, nhà số 4, Dự án khu nhà ở để bán, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0345864,
   "Latitude": 105.7629924
 },
 {
   "STT": 1112,
   "Name": "Nha Khoa Kiêu Kỵ",
   "address": "Thôn Trung Dương, xã Kiêu Kỵ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9829872,
   "Latitude": 105.9604474
 },
 {
   "STT": 1113,
   "Name": "Nha Khoa - Văn Tiến",
   "address": "Xóm Tự, Thôn Phù Đổng, xã Phù Đổng, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0597645,
   "Latitude": 105.9660728
 },
 {
   "STT": 1114,
   "Name": "Phòng khám Nha Khoa Thảo Nguyên",
   "address": "Thôn Xuân Dục, xã Yên Thường, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0932767,
   "Latitude": 105.9047355
 },
 {
   "STT": 1115,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 341 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0455361,
   "Latitude": 105.8690714
 },
 {
   "STT": 1116,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 26 ngách 135/48, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0448412,
   "Latitude": 105.8686635
 },
 {
   "STT": 1117,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm 103 Phương Linh",
   "address": "Tầng 2, số nhà 5/159, phố Tây Sơn, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 1118,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Ngọc Trọng",
   "address": "Thôn Phú Mỹ, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0280371,
   "Latitude": 105.7752199
 },
 {
   "STT": 1119,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Duy Thơm",
   "address": "Khu tập thể Học viện Kỹ thuật quân sự, số nhà 45, đường Lê Văn Hiến, xã Đông Ngạc, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0472562,
   "Latitude": 105.7862592
 },
 {
   "STT": 1120,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Quang Huy",
   "address": "204 Trương Định, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9904629,
   "Latitude": 105.847697
 },
 {
   "STT": 1121,
   "Name": "Phòng khám Chuyên khoa Nhi Bác sĩ Lê Thị Việt Hoa",
   "address": "Số 45 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0478334,
   "Latitude": 105.8766922
 },
 {
   "STT": 1122,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 26 đường Phùng Hưng, TDP 2, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 1123,
   "Name": "Phòng khám Chuyên khoa Nội Đông Á",
   "address": "Lô số 4, khu chia lô biệt thự nhà vườn, xã Tiền Phong, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1605321,
   "Latitude": 105.7644596
 },
 {
   "STT": 1124,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Trực Thuộc Công Ty Cổ Phần Tản Đà",
   "address": ", xã Tản Lĩnh, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.124523,
   "Latitude": 105.3907014
 },
 {
   "STT": 1125,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm Chí Hạnh",
   "address": "Số nhà 14, tổ 40, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1126,
   "Name": "Phòng khám Đa Khoa Minh Đức",
   "address": "Số 7 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1146851,
   "Latitude": 105.4955408
 },
 {
   "STT": 1127,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Nam Hà",
   "address": "Số 123 Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0022144,
   "Latitude": 105.8415378
 },
 {
   "STT": 1128,
   "Name": "Phòng khám Nha Khoa Hạnh Phúc",
   "address": "Số 6, phố Nguyễn Trường Tộ, phường Nguyễn Trung Trực, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0420907,
   "Latitude": 105.8469163
 },
 {
   "STT": 1129,
   "Name": "Phòng khám Chuyên khoa Nội: Nội Soi Tiêu Hóa 77E Hai Bà Trưng - Bác sĩ Tống Văn Lược",
   "address": "Số 77E Hai Bà Trưng, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0271474,
   "Latitude": 105.8422974
 },
 {
   "STT": 1130,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Vũ Quang Lượng",
   "address": "Số 135 đường Cổ Linh, khu tái định cư Long Biên, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.026675,
   "Latitude": 105.8936
 },
 {
   "STT": 1131,
   "Name": "Nha Khoa Lê Nguyên",
   "address": "Số 213B, đường Ngô Xuân Quảng, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0157873,
   "Latitude": 105.9365092
 },
 {
   "STT": 1132,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Việt -Úc",
   "address": "Số 38 ngõ 9 ngách 30 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.996499,
   "Latitude": 105.853042
 },
 {
   "STT": 1133,
   "Name": "Phòng khám Đa Khoa ",
   "address": "Số 77E phố Hai Bà Trưng, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.027142,
   "Latitude": 105.8424816
 },
 {
   "STT": 1134,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Phòng 101-B10, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0430757,
   "Latitude": 105.7952231
 },
 {
   "STT": 1135,
   "Name": "Phòng khám Đa Khoa Bác sĩ Nguyễn Tiến Tuấn",
   "address": "Số 9 khu tập thể Công ty xây lắp 386, xã Đình Xuyên, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0843731,
   "Latitude": 105.9313291
 },
 {
   "STT": 1136,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Bác sĩ Nguyễn Lê Hùng",
   "address": "Số 6A phố Trần Hưng Đạo, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.020069,
   "Latitude": 105.8551071
 },
 {
   "STT": 1137,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Bệnh Viện Hữu Nghị Quốc Tế Hà Nội",
   "address": "Số 456-458 Trần Khát Chân, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0089409,
   "Latitude": 105.8541463
 },
 {
   "STT": 1138,
   "Name": "Nha Khoa Răng Sinh",
   "address": "Số 242Q phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9950765,
   "Latitude": 105.855632
 },
 {
   "STT": 1139,
   "Name": "Nha Khoa Sài Gòn ",
   "address": "Số 79 phố Quan Nhân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0034035,
   "Latitude": 105.8110102
 },
 {
   "STT": 1140,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hải Tuyết",
   "address": "Số 17 phố Hoàng Cầu (cũ 102), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0197428,
   "Latitude": 105.825237
 },
 {
   "STT": 1141,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Phòng 110 nhà G3 tập thể Trung Tự, phường Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.010362,
   "Latitude": 105.833555
 },
 {
   "STT": 1142,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Kim Dung - 57",
   "address": "P112-C6A tập thể Quỳnh Mai, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0009366,
   "Latitude": 105.8591528
 },
 {
   "STT": 1143,
   "Name": "Cơ Sở Dịch Vụ Răng Giả Minh Tân",
   "address": "Số 15A ngõ 508 đường Láng, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0105957,
   "Latitude": 105.8121796
 },
 {
   "STT": 1144,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Hiếu Lê",
   "address": "Số nhà 72 ngõ 49 Huỳnh Thúc Kháng, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0161737,
   "Latitude": 105.809242
 },
 {
   "STT": 1145,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Thận - Tiết Niệu",
   "address": "Số 52, ngõ 553 đường Giải Phóng, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9860789,
   "Latitude": 105.8414778
 },
 {
   "STT": 1146,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Văn Trì, xã Minh Khai, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0540643,
   "Latitude": 105.7413764
 },
 {
   "STT": 1147,
   "Name": "Phòng khám Chuyên khoa Nội Phạm Thị Ngọc Mai",
   "address": "Số 5 ngõ 9 phố Quỳnh Lôi, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025489,
   "Latitude": 105.859697
 },
 {
   "STT": 1148,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Cổ Phần Thương Mại Và Dịch Vụ Thẩm Mỹ Viện Anh Hải Lâm",
   "address": "Số 47 phố Hòa Mã, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.015133,
   "Latitude": 105.851774
 },
 {
   "STT": 1149,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Hồng Sim",
   "address": "Tầng 1 nhà số 4, phố Chùa Thông, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1162867,
   "Latitude": 105.4949277
 },
 {
   "STT": 1150,
   "Name": "Phòng khám Chuyên khoa Nội - Bình Minh 36",
   "address": "158A Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9679103,
   "Latitude": 105.7875291
 },
 {
   "STT": 1151,
   "Name": "Phòng khám Chuyên khoa Ngoại -Bình Minh 36",
   "address": "Số 156 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 1152,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Huyết Học - Nhân Thành",
   "address": "Số 200B phố Xốm, phường Phú Lãm, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9405812,
   "Latitude": 105.7569217
 },
 {
   "STT": 1153,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Hóa Sinh - Bình Minh 36",
   "address": "Số 156B đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9679737,
   "Latitude": 105.787547
 },
 {
   "STT": 1154,
   "Name": "Phòng khám Chuyên khoa Ngoại - Nhân Thành",
   "address": "Số 200A phố Xốm, phường Phú Lãm, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9405812,
   "Latitude": 105.7569217
 },
 {
   "STT": 1155,
   "Name": "Phòng khám Chuyên khoa Ngoại Nguyễn Văn Thành",
   "address": "Số 2 TDP9, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9654323,
   "Latitude": 105.7894045
 },
 {
   "STT": 1156,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 226 phố Lê Lợi, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7357925,
   "Latitude": 105.7710608
 },
 {
   "STT": 1157,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Bùi Tiến Trung",
   "address": ", xã Hòa Thạch, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.9473282,
   "Latitude": 105.5650239
 },
 {
   "STT": 1158,
   "Name": "Phòng khám Chuyên khoa Nội Bác sĩ Dương Quỳnh Hương",
   "address": "Số 33A phố Thợ Nhuộm, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0265712,
   "Latitude": 105.8450516
 },
 {
   "STT": 1159,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ánh",
   "address": "Số 60 (tầng 2) phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 1160,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Lê Thị Thanh - 45",
   "address": "Tầng 2, số 95 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1161,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Cổ Phần Bệnh Viện Hữu Nghị Quốc Tế Hà Nội",
   "address": "Số 456-458 Trần Khát Chân, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0089409,
   "Latitude": 105.8541463
 },
 {
   "STT": 1162,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Nguyễn Xuân Trinh",
   "address": "Tổ 4, xóm Bến, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9493584,
   "Latitude": 105.8444419
 },
 {
   "STT": 1163,
   "Name": "Phòng khám Đa Khoa Hòa Bình",
   "address": "Số 30 phố Hoàng Văn Thái, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9965037,
   "Latitude": 105.8261259
 },
 {
   "STT": 1164,
   "Name": "Phòng khám Nha Khoa Quốc Tế Tâm An - Công Ty Tnhh Sức Sống Tâm An",
   "address": "Số 19 Nguyễn Trường Tộ, phường Nguyễn Trung Trực, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0419959,
   "Latitude": 105.8466566
 },
 {
   "STT": 1165,
   "Name": "Nha Khoa Châu Á Thái Bình Dương",
   "address": "Tầng 1, số 297, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0285645,
   "Latitude": 105.7995097
 },
 {
   "STT": 1166,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Khám Chữa Bệnh Và Tư Vấn Sức Khỏe Ngọc Khánh",
   "address": "Số 140 phố Chùa Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0230198,
   "Latitude": 105.8012521
 },
 {
   "STT": 1167,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Ánh Sáng",
   "address": "Phòng 103, nhà lô A2, phố Nguyễn Khánh Toàn, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0380035,
   "Latitude": 105.8008454
 },
 {
   "STT": 1168,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nam An",
   "address": "Số nhà 32 ngõ Thái Thịnh I, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104071,
   "Latitude": 105.8174239
 },
 {
   "STT": 1169,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Thanh Hồng - 56",
   "address": "Số 38 ngõ 132 đê Tô Hoàng, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.006781,
   "Latitude": 105.84901
 },
 {
   "STT": 1170,
   "Name": "Phòng khám Chuyên khoa Mắt Phạm Thị Kim Thanh",
   "address": "Số 69 tổ 2 Giáp Nhất, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.008314,
   "Latitude": 105.813262
 },
 {
   "STT": 1171,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà B26, lô 6, khu đô thị Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.8722854,
   "Latitude": 105.533896
 },
 {
   "STT": 1172,
   "Name": "Nha Khoa Quốc Tế Việt Đức",
   "address": "Số 02 phố Phủ Doãn, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0297384,
   "Latitude": 105.848079
 },
 {
   "STT": 1173,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 82 phố Ngô Thì Nhậm, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.01816,
   "Latitude": 105.852596
 },
 {
   "STT": 1174,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Tập Đoàn Y Dược Phương Anh",
   "address": "Số nhà 520 đường Nguyễn Trãi, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 1175,
   "Name": "Phòng khám Royal City",
   "address": "72 A Nguyễn Trãi, , Thanh Xuân, Hà Nội",
   "Longtitude": 21.0006925,
   "Latitude": 105.8160726
 },
 {
   "STT": 1176,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 4 ngõ 36 Hoàng Ngọc Phách (cũ: số 5 - B19 Nam Thành Công), phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0161595,
   "Latitude": 105.8114112
 },
 {
   "STT": 1177,
   "Name": "Nha Khoa Phố Sủi",
   "address": "Thôn Phú Thụy, xã Phú Thị, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0276351,
   "Latitude": 105.9729255
 },
 {
   "STT": 1178,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Nguyễn Thị Thu Hà",
   "address": "Số 7, ngách 45/7, ngõ 45, phố Hoa Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0576338,
   "Latitude": 105.8951094
 },
 {
   "STT": 1179,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Nguyễn Hòa",
   "address": "Số 45B phố Trường Lâm, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.06229,
   "Latitude": 105.8981502
 },
 {
   "STT": 1180,
   "Name": "Phòng khám Chuyên khoa Nhi 46",
   "address": "Số 52 đường Long Biên I, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0445564,
   "Latitude": 105.8677232
 },
 {
   "STT": 1181,
   "Name": "Nha Khoa Lê Dung",
   "address": "Số 965 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0725962,
   "Latitude": 105.90455
 },
 {
   "STT": 1182,
   "Name": "Phòng khám Chuyên khoa Nội - Siêu Âm Tiêu Hóa",
   "address": "Số 283 phố Bồ Đề, tổ 17, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.0380895,
   "Latitude": 105.8700008
 },
 {
   "STT": 1183,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 14 hẻm 135/48/40 ngõ 154 phố Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0451762,
   "Latitude": 105.8709056
 },
 {
   "STT": 1184,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Tytnhh Thương Mại Và Truyền Thông Nhất Nam",
   "address": "Số 1A Cầu Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0129845,
   "Latitude": 105.8201332
 },
 {
   "STT": 1185,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Số 2 ngách 6/15 phố Đặng Văn Ngữ, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.012407,
   "Latitude": 105.833535
 },
 {
   "STT": 1186,
   "Name": "Phòng khám Chuyên khoa Phụ Sản-Khhgđ Phía Nam",
   "address": "Ngã Tư, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9839201,
   "Latitude": 105.8412337
 },
 {
   "STT": 1187,
   "Name": "Phòng khám Chuyên khoa Phụ Sản-Khhgđ Ánh Sáng",
   "address": "Phòng 202, nhà lô A2, phố Nguyễn Khánh Toàn, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0380035,
   "Latitude": 105.8008454
 },
 {
   "STT": 1188,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Nhà số 2, khu tập thể X275, tổ 16, phường Lĩnh Nam, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9770005,
   "Latitude": 105.896559
 },
 {
   "STT": 1189,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Phòng khám Gia Đình Hà Nội",
   "address": "Số 298I Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.031104,
   "Latitude": 105.818295
 },
 {
   "STT": 1190,
   "Name": "Nha Khoa Quốc Tế Việt Đức - Cơ Sở 1",
   "address": "Số 84A phố Hai Bà Trưng, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0269884,
   "Latitude": 105.8443344
 },
 {
   "STT": 1191,
   "Name": "Phòng khám Siêu Âm Sản Phụ Khoa Tâm An",
   "address": "Số 79, đường bờ sông Tô Lịch, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0367702,
   "Latitude": 105.8011445
 },
 {
   "STT": 1192,
   "Name": "Nha Khoa Việt Nhật - 2",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 1193,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Thôn Văn Trì, xã Minh Khai, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0540643,
   "Latitude": 105.7413764
 },
 {
   "STT": 1194,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "P101 B, nhà C5, tập thể Giảng Võ, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0277174,
   "Latitude": 105.8230671
 },
 {
   "STT": 1195,
   "Name": "Phòng khám Nội Khoa Liên Phương",
   "address": "Số 8 hẻm 29/39/14 phố Khương Hạ, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.992275,
   "Latitude": 105.815869
 },
 {
   "STT": 1196,
   "Name": "Phòng khám Nha Khoa Medita Trực Thuộc Công Ty Cổ Phần Chăm Sóc Sức Khỏe Medita",
   "address": "Số 376 phố xã Đàn, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0163445,
   "Latitude": 105.83142
 },
 {
   "STT": 1197,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 45 đường Khuất Duy Tiến, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.995839,
   "Latitude": 105.7995509
 },
 {
   "STT": 1198,
   "Name": "Nha Khoa Linh Anh",
   "address": "Số nhà 117C nhà D1, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.023572,
   "Latitude": 105.8135502
 },
 {
   "STT": 1199,
   "Name": "Nha Khoa Việt Nhật",
   "address": "Số 32 ngõ 155 đường Trường Chinh, phường Phương Liệt, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9985014,
   "Latitude": 105.8364423
 },
 {
   "STT": 1200,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thanh Hương",
   "address": "Thôn Lưu Phái, xã Ngũ Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9282023,
   "Latitude": 105.8613233
 },
 {
   "STT": 1201,
   "Name": "Nha Khoa Ngọc Dung",
   "address": "Số nhà 20 tập thể Bệnh viện Nông Nghiệp, xã Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9210189,
   "Latitude": 105.8542059
 },
 {
   "STT": 1202,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Bác sĩ Khanh",
   "address": "Ô 87 khu A khu đô thi mới Đại Kim, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9765428,
   "Latitude": 105.8351431
 },
 {
   "STT": 1203,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ ",
   "address": "Số 28, lô 6, khu đô thị Trung Yên, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 20.9846465,
   "Latitude": 105.7938756
 },
 {
   "STT": 1204,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Tầng 2, số nhà 129, đường bờ sông Tô Lịch, tổ 2, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0367702,
   "Latitude": 105.8011445
 },
 {
   "STT": 1205,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nhâm Tuấn Anh",
   "address": "Số 38 ngõ 3 phố Cù Chính Lan, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0348064,
   "Latitude": 105.8163337
 },
 {
   "STT": 1206,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 04, ngõ 52, phố Quan Nhân, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0080777,
   "Latitude": 105.8116791
 },
 {
   "STT": 1207,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Cổ Phần Công Nghệ Vip Lab Việt Nam",
   "address": "Số 35 Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.009828,
   "Latitude": 105.81108
 },
 {
   "STT": 1208,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Đầu Tư Phú Cường Hà Nội",
   "address": "Số 615 Nguyễn Văn Cừ, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0529673,
   "Latitude": 105.8861947
 },
 {
   "STT": 1209,
   "Name": "Phòng khám Đa Khoa Thanh Xuân",
   "address": "Số 216 đường Nguyễn Trãi, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 1210,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cỏ Phần Bệnh Viện Đa Khoa Việt Anh",
   "address": "Số 31 đường Vũ Phạm Hàm, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0192688,
   "Latitude": 105.80003
 },
 {
   "STT": 1211,
   "Name": "Phòng khám Đa Khoa Hồng Ngọc 2 Trực Thuộc Công Ty Tnhh Y Tế Hồng Ngọc",
   "address": "Tầng 10, tòa nhà 70 tầng, tòa nhà Keangnam Hanoi Landmark Tower, khu E6, khu đô thị Cầu Giấy, Mễ Trì, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0168415,
   "Latitude": 105.7837524
 },
 {
   "STT": 1212,
   "Name": "Phòng khám Chuyên khoa Nội Soi Tiêu Hóa",
   "address": "P2-C2 tập thể Viện 108 số 1 phố Trần Thánh Tông, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0183344,
   "Latitude": 105.8614591
 },
 {
   "STT": 1213,
   "Name": "Nha Khoa S-A-K-U-R-A",
   "address": "Số 124, phố Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.018517,
   "Latitude": 105.7928367
 },
 {
   "STT": 1214,
   "Name": "Nha Khoa F-A-M-I-L-Y",
   "address": "Tầng 1, số 3, đường Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0106727,
   "Latitude": 105.8094871
 },
 {
   "STT": 1215,
   "Name": "Nha Khoa Tố Nhiên",
   "address": "Tầng 1, nhà số 261, phố Trần Quốc Hoàn, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0416805,
   "Latitude": 105.7855295
 },
 {
   "STT": 1216,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 30 ngõ 349 phố Minh Khai, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9974651,
   "Latitude": 105.8654102
 },
 {
   "STT": 1217,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 15 phố Nguyễn Bỉnh Khiêm, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151954,
   "Latitude": 105.8483727
 },
 {
   "STT": 1218,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 47, đường Nguyễn Phong Sắc, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0406301,
   "Latitude": 105.7904009
 },
 {
   "STT": 1219,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Thôn Lộc, xã Xuân Đỉnh, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0681741,
   "Latitude": 105.790872
 },
 {
   "STT": 1220,
   "Name": "Nha Khoa Khánh Anh",
   "address": "Số 485 Âu Cơ, tổ 18 cụm 2, phường Nhật Tân, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.079389,
   "Latitude": 105.819017
 },
 {
   "STT": 1221,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Tnhh Kính Mắt Việt Nam",
   "address": "Số 138B Giảng Võ, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0287964,
   "Latitude": 105.8256764
 },
 {
   "STT": 1222,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "16-A8, tập thể đại học Ngoại ngữ, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0412775,
   "Latitude": 105.7822509
 },
 {
   "STT": 1223,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 24 ngõ 19 phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002848,
   "Latitude": 105.8621928
 },
 {
   "STT": 1224,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Tầng 2, Số 144 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 1225,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 13 Vạn Bảo, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0352903,
   "Latitude": 105.8177942
 },
 {
   "STT": 1226,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 10 phố Đỗ Hành, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0097957,
   "Latitude": 105.8506996
 },
 {
   "STT": 1227,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc",
   "address": "Số 144 phố Bà Triệu (tầng 1), phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 1228,
   "Name": "Nha Khoa Hà My",
   "address": "Số 105, đường Nguyễn Đức Cảnh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.987068,
   "Latitude": 105.8516523
 },
 {
   "STT": 1229,
   "Name": "Phòng khám Siêu Âm Màu 4D",
   "address": "Số 42 phố Ngô Thì Nhậm, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0035913,
   "Latitude": 105.863893
 },
 {
   "STT": 1230,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số nhà 6, ngõ 154, đường Trần Duy Hưng, tổ 12, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0107492,
   "Latitude": 105.799529
 },
 {
   "STT": 1231,
   "Name": "Phòng khám Chuyên khoa Phụ Sản ",
   "address": "Số 87 phố Nguyễn Du, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0206666,
   "Latitude": 105.8418263
 },
 {
   "STT": 1232,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Bác sĩ Thắng",
   "address": "Số 25 phố Đoàn Nhữ Hài, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0215516,
   "Latitude": 105.8460023
 },
 {
   "STT": 1233,
   "Name": "Phòng khám Mắt Ánh Sáng",
   "address": "Số 36 phố Thi Sách, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0160057,
   "Latitude": 105.8540962
 },
 {
   "STT": 1234,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế 7A Quang Vinh",
   "address": "Số 185 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9701684,
   "Latitude": 105.784545
 },
 {
   "STT": 1235,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Việt - Hàn",
   "address": "P102, nhà V3-2, số 635C Kim Mã, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0292278,
   "Latitude": 105.8078631
 },
 {
   "STT": 1236,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Đông Á",
   "address": "Số 212 (tầng 1+2+3+4), phố Kim Mã, phường Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0314597,
   "Latitude": 105.8226568
 },
 {
   "STT": 1237,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nga Châu",
   "address": "Số 66B phố Nguyễn Chí Thanh (cũ: số 1 phố Pháo Đài Láng), phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.019858,
   "Latitude": 105.807897
 },
 {
   "STT": 1238,
   "Name": "Nha Khoa Hoàng Oanh",
   "address": "Số 93 ngõ Thái Thịnh 1, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0095213,
   "Latitude": 105.8164167
 },
 {
   "STT": 1239,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Xóm 2, Thôn Hoàng, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0576943,
   "Latitude": 105.7791327
 },
 {
   "STT": 1240,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế Quốc Tế Việt Mỹ",
   "address": "Phố Sủi, xã Phú Thị, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0175485,
   "Latitude": 105.9648468
 },
 {
   "STT": 1241,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 165 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1242,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Hồng Vân",
   "address": "68 dốc Bệnh viện, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7254524,
   "Latitude": 105.7736337
 },
 {
   "STT": 1243,
   "Name": "Phòng khám Chuyên khoa Nội Hồng Vân",
   "address": "68 dốc Bệnh viện, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7254524,
   "Latitude": 105.7736337
 },
 {
   "STT": 1244,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Khám Chữa Bệnh Sơn Trang",
   "address": "Khu dịch vụ, đường 21B, Thôn Tảo Dương, xã Hồng Dương, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.7939085,
   "Latitude": 105.7774559
 },
 {
   "STT": 1245,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Một Thành Viên Phòng khám , Chữa Bệnh Chuyên khoa Nội Số 7 Duy Thành",
   "address": "Thôn Yên Bệ, xã Kim Chung, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0471992,
   "Latitude": 105.705993
 },
 {
   "STT": 1246,
   "Name": "Phòng khám Chuyên khoa Nội Số 10",
   "address": "Số 197 Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9679354,
   "Latitude": 105.7860294
 },
 {
   "STT": 1247,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số A16-thị trấn 8 khu đô thị Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9741262,
   "Latitude": 105.7894045
 },
 {
   "STT": 1248,
   "Name": "Phòng khám Chuyên khoa Nhi - Mai Thanh",
   "address": "C4 thị trấn 1, đô thị Văn Quán, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9741262,
   "Latitude": 105.7894045
 },
 {
   "STT": 1249,
   "Name": "Nha Khoa Mỹ Bác sĩ Bùi Ngọc Hương",
   "address": "Ki ốt 9-B10 tập thể Kim Liên, phố Phạm Ngọc Thạch, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0070652,
   "Latitude": 105.8348712
 },
 {
   "STT": 1250,
   "Name": "Nha Khoa Trung Đức",
   "address": "247 Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.968067,
   "Latitude": 105.7874035
 },
 {
   "STT": 1251,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Đình Bảo",
   "address": "Số 03 Ngô Thì Nhậm, phường Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9673095,
   "Latitude": 105.7704093
 },
 {
   "STT": 1252,
   "Name": "Phòng khám Nha Khoa Bạch Mai",
   "address": "Thôn 1, xã Quảng Bị, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8684407,
   "Latitude": 105.7035791
 },
 {
   "STT": 1253,
   "Name": "Phòng khám Chuyên khoa Mắt Thuận Phát",
   "address": "Số 142 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 1254,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng An Sinh",
   "address": "Thôn Nam Bình, xã Hòa Bình, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.878276,
   "Latitude": 105.8378366
 },
 {
   "STT": 1255,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Số 23 ngách 10/2 phố Tôn Thất Tùng, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 20.9983065,
   "Latitude": 105.8537639
 },
 {
   "STT": 1256,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Minh Loan",
   "address": ", xã Hòa Bình, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.878276,
   "Latitude": 105.8378366
 },
 {
   "STT": 1257,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 25 Lô 3 Hồ Đầm Xoài, viện Y học cổ truyền dân tộc quân đội, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 20.9728757,
   "Latitude": 105.823155
 },
 {
   "STT": 1258,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 38 tập thể sư đoàn 361, tổ 44B, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0218044,
   "Latitude": 105.790872
 },
 {
   "STT": 1259,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số nhà 20, ngõ 24, phố Phan Văn Trường, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0390913,
   "Latitude": 105.7860958
 },
 {
   "STT": 1260,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Phố Nỷ, xã Trung Giã, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.3136608,
   "Latitude": 105.8679295
 },
 {
   "STT": 1261,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 61, tổ 52, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.17032,
   "Latitude": 105.8489
 },
 {
   "STT": 1262,
   "Name": "Phòng khám Chuyên khoa Nội 16A Trực Thuộc Công Ty Tnhh Một Thành Viên 16A",
   "address": "Số 203 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9688505,
   "Latitude": 105.7862033
 },
 {
   "STT": 1263,
   "Name": "Phòng khám Chuyên khoa Nội Bác sĩ Hoa",
   "address": "Cụm 1, Thôn Quỳnh Đô, xã Vĩnh Quỳnh, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.938595,
   "Latitude": 105.8395922
 },
 {
   "STT": 1264,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Phương Lan",
   "address": "Số 23, tổ 55, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1265,
   "Name": "Phòng khám Chuyên khoa Nội Lương Văn Thành",
   "address": "Xóm 5, xã Kim Chung, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0575925,
   "Latitude": 105.7263819
 },
 {
   "STT": 1266,
   "Name": "Phòng khám Chuyên khoa Tâm Thần",
   "address": ", xã Hòa Bình, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.878276,
   "Latitude": 105.8378366
 },
 {
   "STT": 1267,
   "Name": "Phòng khám Đa Khoa 589 Hoàng Hoa Thám Trực Thuộc Công Ty Cổ Phần Phát Triển Khoa Học Kỹ Thuật Y Sinh",
   "address": "Số 589, phố Hoàng Hoa Thám, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.046725,
   "Latitude": 105.809648
 },
 {
   "STT": 1268,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Bác sĩ Nguyễn Ngọc Thập",
   "address": "Số 38 phố Hàng Mã, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.036892,
   "Latitude": 105.8476857
 },
 {
   "STT": 1269,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Tư Vấn Và Dịch Vụ Y Tế Bình An",
   "address": "183A đường Âu Cơ, phường Quảng An, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0647869,
   "Latitude": 105.8288606
 },
 {
   "STT": 1270,
   "Name": "Nha Khoa Nguyễn Trần",
   "address": "Ki ốt 21, Học viện Quốc Phòng, phố Phùng Chí Kiên, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0458975,
   "Latitude": 105.8018682
 },
 {
   "STT": 1271,
   "Name": "Phòng khám Chuyên khoa Nhi 123",
   "address": "Số 55 phố 8/3, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9985164,
   "Latitude": 105.8602965
 },
 {
   "STT": 1272,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 94, ngõ 168 đường Kim Giang, tổ 31, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9792224,
   "Latitude": 105.8158003
 },
 {
   "STT": 1273,
   "Name": "Nha Khoa P/S",
   "address": "Số 3 ngõ 78 đường Giải Phóng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021881,
   "Latitude": 105.8412069
 },
 {
   "STT": 1274,
   "Name": "Phòng khám Đa Khoa Quốc Tế Kỳ Hưng",
   "address": "Số 27, phố Trần Thái Tông, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.031806,
   "Latitude": 105.7884853
 },
 {
   "STT": 1275,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Nguyễn Thị Lân",
   "address": "Số 225, tổ 13, phố Trạm, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0260022,
   "Latitude": 105.8939799
 },
 {
   "STT": 1276,
   "Name": "Phòng khám Chuyên khoa Nội Vũ Thúy Cầm",
   "address": "Số 486 Trương Định, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.984026,
   "Latitude": 105.846054
 },
 {
   "STT": 1277,
   "Name": "Phòng khám Phía Nam - Chuyên khoa Ngoại",
   "address": "Số 936 đường Trương Định, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9776304,
   "Latitude": 105.8421832
 },
 {
   "STT": 1278,
   "Name": "Phòng khám Siêu Âm Sản Phụ Khoa - Bác sĩ Trần Quốc Nhân",
   "address": "P103 - D3 tập thể Bộ Nông nghiệp, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0012748,
   "Latitude": 105.8393044
 },
 {
   "STT": 1279,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Trọng Trúc",
   "address": "Số 414, tổ 13, phường Hoàng Văn Thụ, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9848364,
   "Latitude": 105.8598553
 },
 {
   "STT": 1280,
   "Name": "Phòng khám Chuyên khoa Mắt Nguyễn Văn Toan",
   "address": "Số 89 đường Thạch Bàn, tổ 4, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0231982,
   "Latitude": 105.906053
 },
 {
   "STT": 1281,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Anh Đào (Skr)",
   "address": "Số 19B phố Hàng Chuối, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0166093,
   "Latitude": 105.8575429
 },
 {
   "STT": 1282,
   "Name": "Phòng khám Chuyên khoa Phụ Sản 170-C4",
   "address": "Số 170C4 khu đô thị Đại Kim, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9789973,
   "Latitude": 105.8134334
 },
 {
   "STT": 1283,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà Văn Chiến",
   "address": "Số 188 Định Công Thượng, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9846817,
   "Latitude": 105.8225279
 },
 {
   "STT": 1284,
   "Name": "Phòng khám Đa Khoa Truực Thuộc Công Ty Cổ Phần Gia Hòa Phát",
   "address": "225 Hoàng Tăng Bí, xã Đông Ngạc, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.083802,
   "Latitude": 105.7796168
 },
 {
   "STT": 1285,
   "Name": "Phòng khám Đa Khoa",
   "address": "Số 38A Trần Phú, phường Điện Biên, Ba Đình, Hà Nội",
   "Longtitude": 21.030744,
   "Latitude": 105.841101
 },
 {
   "STT": 1286,
   "Name": "Phòng khám Chuyên khoa Nhi Nguyễn Thị Vinh",
   "address": "Số 42 phố Vũ Trọng Phụng, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9998099,
   "Latitude": 105.8079757
 },
 {
   "STT": 1287,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 21 tập thể Cảnh sát hình sự, tổ 59, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0218044,
   "Latitude": 105.790872
 },
 {
   "STT": 1288,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Đình Liêu",
   "address": "Số 181 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0010237,
   "Latitude": 105.8416146
 },
 {
   "STT": 1289,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 30 phố Phan Đình Giót, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9909561,
   "Latitude": 105.8403848
 },
 {
   "STT": 1290,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 02, ngõ 111 đường Nguyễn Phong Sắc, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0368285,
   "Latitude": 105.7897645
 },
 {
   "STT": 1291,
   "Name": "Phòng khám Chyên Khoa Chẩn Đoán Hình Ảnh Xquang Hồng Vân",
   "address": "68 dốc Bệnh viện, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7254524,
   "Latitude": 105.7736337
 },
 {
   "STT": 1292,
   "Name": "Phòng khám Chuyên khoa Siêu Âm Sản Phụ Khoa",
   "address": "Khu nhà văn hóa cũ, phố huyện, thị trấn Quốc Oai, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.9950369,
   "Latitude": 105.6441852
 },
 {
   "STT": 1293,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 25A phố Thi Sách, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016904,
   "Latitude": 105.854116
 },
 {
   "STT": 1294,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phúc Hưng",
   "address": "P101B nhà A tập thể nhà máy rượu dốc Thọ Lão, phường Đồng Nhân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0110549,
   "Latitude": 105.8572163
 },
 {
   "STT": 1295,
   "Name": "Phòng khám Nha Khoa Hoàn Mỹ",
   "address": "14A1 phố Nguyễn Quý Đức, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9934546,
   "Latitude": 105.7969188
 },
 {
   "STT": 1296,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nhân Chính",
   "address": "Số 108 phố Vũ Trọng Phụng, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9998099,
   "Latitude": 105.8079757
 },
 {
   "STT": 1297,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 237D phố Bạch Mai, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 1298,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Nha Khoa Mỹ Trực Thuộc Công Ty Cổ Phần Y Tế - Khám Chữa Bệnh Việt Nam",
   "address": "Số 11, phố Cửa Nam, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0283353,
   "Latitude": 105.8431086
 },
 {
   "STT": 1299,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Số 90 ngõ 10 phố 8-3, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.999007,
   "Latitude": 105.861417
 },
 {
   "STT": 1300,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đào Thúy Nga",
   "address": "Số 9 ngách 61/2 phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0019778,
   "Latitude": 105.8666694
 },
 {
   "STT": 1301,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Hoàng Đình Ngọc",
   "address": "Số 31 B1 tập thể Tin học bãi than Vọng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0171104,
   "Latitude": 105.8131718
 },
 {
   "STT": 1302,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 53 (tầng 1), phố Nguyễn Công Hoan, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0272768,
   "Latitude": 105.8152894
 },
 {
   "STT": 1303,
   "Name": "Phòng khám Đa Khoa 125 Thái Thịnh Trực Thuộc Công Ty Cổ Phần Bệnh Viện Thái Thịnh",
   "address": "Số 125, phố Thái Thịnh, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0116453,
   "Latitude": 105.8172078
 },
 {
   "STT": 1304,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 78 đường Quang Trung, phường Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.961431,
   "Latitude": 105.763987
 },
 {
   "STT": 1305,
   "Name": "Phòng khám Chuyên khoa Nhi Bích Diệp",
   "address": "Thôn Thượng Thụy, xã Đức Thượng, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0776081,
   "Latitude": 105.6911121
 },
 {
   "STT": 1306,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 78 Quang Trung, phường Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9700477,
   "Latitude": 105.7745771
 },
 {
   "STT": 1307,
   "Name": "Phòng khám Chuyên khoa Nhi Bác sĩ: Nguyễn Trung Trinh",
   "address": "Số 70 phố Thợ Nhuộm, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0246713,
   "Latitude": 105.8464358
 },
 {
   "STT": 1308,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Tập thể cầu 7, xã Thụy Phương, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.07319,
   "Latitude": 105.7853701
 },
 {
   "STT": 1309,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ ",
   "address": "Xóm, , huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.112473,
   "Latitude": 105.687842
 },
 {
   "STT": 1310,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "121 Phùng Hưng, Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9699762,
   "Latitude": 105.7847029
 },
 {
   "STT": 1311,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "319 Quang Trung, phường Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9657818,
   "Latitude": 105.7696608
 },
 {
   "STT": 1312,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Khu A tập thể chăn nuôi Trung Ương, Ngọc Hồi, Thanh Trì, Hà Nội",
   "Longtitude": 20.9202213,
   "Latitude": 105.853641
 },
 {
   "STT": 1313,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Khu nhà ở chất lượng cao, thị trấn Phùng, Đan Phượng, Hà Nội",
   "Longtitude": 21.0872799,
   "Latitude": 105.6532977
 },
 {
   "STT": 1314,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh",
   "address": "145 Thanh Bình, Mộ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9786494,
   "Latitude": 105.7801196
 },
 {
   "STT": 1315,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hp",
   "address": "Thôn Phú Mỹ, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0280371,
   "Latitude": 105.7752199
 },
 {
   "STT": 1316,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Mỹ Đình",
   "address": "Số 08, nhà A1, tập thể Công ty xây lắp điện I, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.984641,
   "Latitude": 105.795526
 },
 {
   "STT": 1317,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Công Nghiệp Vip Lab Việt Nam",
   "address": "Số 35 Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.009828,
   "Latitude": 105.81108
 },
 {
   "STT": 1318,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Siêu Âm Tân Việt",
   "address": "Số 60 phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 1319,
   "Name": "Phòng khám Chuyên khoa Mắt Phạm Văn Tần",
   "address": "Số 29 ngách 162/29 phố Lê Trọng Tấn, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9939419,
   "Latitude": 105.8314746
 },
 {
   "STT": 1320,
   "Name": "Phòng khám Chuyên khoa Nhi Số 38 Nguyễn Đổng Chi",
   "address": "Số 38, tổ 18 (số cũ tổ 40), đường Nguyễn Đổng Chi, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.03845,
   "Latitude": 105.764406
 },
 {
   "STT": 1321,
   "Name": "Phòng khám Chuyên khoa Nội - Phạm Văn Đếm",
   "address": "Xóm 1, Thôn Phú Đô, xã Mễ Trì, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0120655,
   "Latitude": 105.7632509
 },
 {
   "STT": 1322,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 44, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0350848,
   "Latitude": 105.7787168
 },
 {
   "STT": 1323,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Phan Thị Thu Nga",
   "address": "Số 6 Q4 Trương Định, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.986444,
   "Latitude": 105.846109
 },
 {
   "STT": 1324,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Minh Xuyên ",
   "address": "Số 43 phố Cửa Nam, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0278696,
   "Latitude": 105.8426047
 },
 {
   "STT": 1325,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Bác sĩ Nguyễn Thị Lệ Thu",
   "address": "Số 15 ngõ 106 phố Chùa Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0482983,
   "Latitude": 105.7949535
 },
 {
   "STT": 1326,
   "Name": "Phòng khám Chuyên khoa Phụ Sản An Bình",
   "address": "Tầng 1,2, số nhà 185, đường Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.01253,
   "Latitude": 105.8073272
 },
 {
   "STT": 1327,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Siêu Âm Thái Bình",
   "address": "Tầng 03, số 185, đường Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.012766,
   "Latitude": 105.806931
 },
 {
   "STT": 1328,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Tập thể đoàn văn công Tổng cục Hậu cần, xã Xuân Đỉnh, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0726621,
   "Latitude": 105.7985866
 },
 {
   "STT": 1329,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "P105A-B2, tập thể Thành Công, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.021683,
   "Latitude": 105.8150866
 },
 {
   "STT": 1330,
   "Name": "Phòng khám Chuyên khoa Nội Phạm Thị Quyến",
   "address": "Số 158, tổ 9, đường K3 (Số 02 Khu tập thể Quân đội A36 Cục Quân Lương), thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0420039,
   "Latitude": 105.7620892
 },
 {
   "STT": 1331,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 113 Nguyễn Lương Bằng, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0149839,
   "Latitude": 105.827893
 },
 {
   "STT": 1332,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Nhà 11 ngõ 38 dốc Thọ Lão, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0110862,
   "Latitude": 105.8569772
 },
 {
   "STT": 1333,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Đỗ Đức Khẩm",
   "address": "Xóm Tràng Chợ, xã Dương Liễu, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0581353,
   "Latitude": 105.6729486
 },
 {
   "STT": 1334,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Mai Thế Sỹ",
   "address": "Thôn Chợ, xã Bình Minh, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8891038,
   "Latitude": 105.7631835
 },
 {
   "STT": 1335,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Nha Khoa 68",
   "address": "A6 thị trấn 10, khu đô thị Văn Quán, phường Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9776467,
   "Latitude": 105.7875118
 },
 {
   "STT": 1336,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Thanh Hà",
   "address": "203 Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9685717,
   "Latitude": 105.7864905
 },
 {
   "STT": 1337,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 113 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0042414,
   "Latitude": 105.8410567
 },
 {
   "STT": 1338,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nha Khoa Lạc Hồng",
   "address": "Số 7, M2, ngõ 30, phố Nguyễn Thị Định, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0123683,
   "Latitude": 105.803813
 },
 {
   "STT": 1339,
   "Name": "Nha Khoa Víp Nguyên Hồng",
   "address": "Số 54 phố Nguyên Hồng, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0165389,
   "Latitude": 105.8107787
 },
 {
   "STT": 1340,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Nguyệt",
   "address": "Tổ dân phố 10 Trinh Lương, Phú Lương, Hà Đông, Hà Nội",
   "Longtitude": 20.939376,
   "Latitude": 105.7576575
 },
 {
   "STT": 1341,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Thị Tuyết Nhung",
   "address": "Tiểu khu đường, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7825722,
   "Latitude": 105.9156477
 },
 {
   "STT": 1342,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Duy Hiểu",
   "address": "Thôn Phương Cù, xã Thắng Lợi, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8264654,
   "Latitude": 105.8794285
 },
 {
   "STT": 1343,
   "Name": "Phòng khám Chuyên khoa Nội Nguyễn Mạnh Hào",
   "address": "Thôn Bình Xá, xã Bình Phú, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0208685,
   "Latitude": 105.6092729
 },
 {
   "STT": 1344,
   "Name": "Phòng khám Chuyên khoa Nội Đào Ngọc Bảo",
   "address": "BT4, vị trí 3, khu đô thị Xa La, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.962248,
   "Latitude": 105.794057
 },
 {
   "STT": 1345,
   "Name": "Phòng khám Chuyên khoa Nhi ",
   "address": "Số 92, phố Đốc Ngữ, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0411699,
   "Latitude": 105.8134245
 },
 {
   "STT": 1346,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "P107-A3 ngõ 72 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.018043,
   "Latitude": 105.8067774
 },
 {
   "STT": 1347,
   "Name": "Phòng khám Chuyên khoa Nhi An Khang",
   "address": ", xã Nguyễn Trãi, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8476414,
   "Latitude": 105.8554513
 },
 {
   "STT": 1348,
   "Name": "Phòng khám Chuyên khoa Nhi Hoàng Thị Mai Khuyên",
   "address": "Số 12 tổ 10, phường Phú Lương, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9389446,
   "Latitude": 105.7580577
 },
 {
   "STT": 1349,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Võ Thanh Quang",
   "address": "Số 27, ngõ 198 Lê Trọng Tấn, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9933302,
   "Latitude": 105.8305146
 },
 {
   "STT": 1350,
   "Name": "Phòng khám Đa Khoa Việt Đức",
   "address": "Thôn Chúc Lý, xã Ngọc Hòa, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9202554,
   "Latitude": 105.6958162
 },
 {
   "STT": 1351,
   "Name": "Phòng khám Đa Khoa Đặng Thị Huơng",
   "address": "Phố Mới, thị trấn Quốc Oai, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.9927752,
   "Latitude": 105.6401432
 },
 {
   "STT": 1352,
   "Name": "Phòng khám Đa Khoa Sông Hồng",
   "address": "Số 38 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.971288,
   "Latitude": 105.7834882
 },
 {
   "STT": 1353,
   "Name": "Phòng khám Chuyên khoa Mắt Khải An",
   "address": "Tầng 3+4 nhà A1-A2 tập thể Bộ Công an, tổ 67 Hoàng Cầu, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.018075,
   "Latitude": 105.826095
 },
 {
   "STT": 1354,
   "Name": "Phòng khám Chuyên khoa Nội Tim Mạch Khải An",
   "address": "Tầng 2+3 nhà A1-A2 tập thể Bộ Công an, tổ 67 Hoàng Cầu, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.018075,
   "Latitude": 105.826095
 },
 {
   "STT": 1355,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Nguyễn Đức Thái - 155B",
   "address": "Số 155B, phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349255,
   "Latitude": 105.8252758
 },
 {
   "STT": 1356,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyễn Trọng Nghiệp",
   "address": "Phòng 104 khu thị trấn Học viện Quân y 103, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9685511,
   "Latitude": 105.78732
 },
 {
   "STT": 1357,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 9, ngõ 10 Lê Lợi, phường Nguyễn Trãi, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9715331,
   "Latitude": 105.7773337
 },
 {
   "STT": 1358,
   "Name": "Phòng khám Chuyên khoa Phụ Sản ",
   "address": "Tiểu khu đường, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7825722,
   "Latitude": 105.9156477
 },
 {
   "STT": 1359,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Tùng Thảo",
   "address": "Xóm Đồng, xã Sơn Đồng, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0439563,
   "Latitude": 105.7021123
 },
 {
   "STT": 1360,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Lê Thị Thúy",
   "address": "Thôn Kỳ Dương, xã Chương Dương, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8479214,
   "Latitude": 105.900921
 },
 {
   "STT": 1361,
   "Name": "Phòng khám Chuyên khoa Sản Phụ Khoa Xa La",
   "address": "P602-CT2B, đô thị Xa La, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9620921,
   "Latitude": 105.7949898
 },
 {
   "STT": 1362,
   "Name": "Phòng khám Sản Phụ Khoa Thái An",
   "address": "Số 209 đường Lĩnh Nam, phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.984883,
   "Latitude": 105.8721328
 },
 {
   "STT": 1363,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Lê Thị Hải",
   "address": "Tế Tiêu, thị trấn Đại Nghĩa, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6865777,
   "Latitude": 105.7453621
 },
 {
   "STT": 1364,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Thôn Ngọc Giả, xã Ngọc Hòa, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.924149,
   "Latitude": 105.7000094
 },
 {
   "STT": 1365,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Xóm Minh Khai, xã La Phù, huyện Hoài Đức, Hà Nội",
   "Longtitude": 20.9832706,
   "Latitude": 105.7310801
 },
 {
   "STT": 1366,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Thôn Phượng Mỹ, xã Mỹ Hưng, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.9021599,
   "Latitude": 105.7996769
 },
 {
   "STT": 1367,
   "Name": "Phòng khám Chuyên khoa Nhi - 57 Việt Nhật",
   "address": "Số 1 ngõ 75 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0026814,
   "Latitude": 105.8420706
 },
 {
   "STT": 1368,
   "Name": "Phòng khám Chuyên khoa Nhi - Nhi Tâm",
   "address": "Số 18 ngõ Giếng, phố Đại La (số mới: 126 phố Trần Đại Nghĩa), phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.018243,
   "Latitude": 105.8280409
 },
 {
   "STT": 1369,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 85, phố Trung Kính, tổ 2, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0143337,
   "Latitude": 105.8001661
 },
 {
   "STT": 1370,
   "Name": "Phòng khám Đa Khoa Sakura Trực Thuộc Công Ty Tnhh Liên Kết Y Tế Nhật Việt",
   "address": "Số 49 ngõ 612 Lạc Long Quân, tổ 11 cụm 2, phường Nhật Tân, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0765324,
   "Latitude": 105.8147917
 },
 {
   "STT": 1371,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 03, phố Tô Hiệu, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.04201,
   "Latitude": 105.7907904
 },
 {
   "STT": 1372,
   "Name": "Phòng khám Đa Khoa 98 Hàng Buồm Trực Thuộc Công Ty Cổ Phần Dược Phẩm Thiết Bị Y Tế Hà Nội",
   "address": "98 phố Hàng Buồm, phường Hàng Buồm, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.035555,
   "Latitude": 105.8505314
 },
 {
   "STT": 1373,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Cổ Phần Phòng khám Đa Khoa 161",
   "address": "Số 161, đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1374,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Phòng khám Đa Khoa 161",
   "address": "Số 161, đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1375,
   "Name": "Phòng khám Chuyên khoa Nhi Thuận Thiên",
   "address": "Số 16thị trấn 2 Khu đô thị Hà Đô, số 183 phố Hoàng Văn Thái, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9959592,
   "Latitude": 105.8233558
 },
 {
   "STT": 1376,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: X Quang Trần Văn Long 59",
   "address": "Số 18 phố Tăng Bạt Hổ, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0140065,
   "Latitude": 105.8591348
 },
 {
   "STT": 1377,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 35 ngõ 20 đường Trương Định, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9963098,
   "Latitude": 105.8490693
 },
 {
   "STT": 1378,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 1, Nguyễn An Ninh, tổ 4, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9892555,
   "Latitude": 105.8458991
 },
 {
   "STT": 1379,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội Tiêu Hóa - 57 Việt Nhật",
   "address": "Số 1 ngõ 75 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0026814,
   "Latitude": 105.8420706
 },
 {
   "STT": 1380,
   "Name": "Nha Khoa Bảo Tín",
   "address": "Số 24, đường Trung Văn, tập thể Công ty cổ phần đá quý và vàng Hà Nội, xã Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.989171,
   "Latitude": 105.7835348
 },
 {
   "STT": 1381,
   "Name": "Nha Khoa Minh Thư",
   "address": "Thôn Đông, xã Xuân Đỉnh, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0706048,
   "Latitude": 105.7879972
 },
 {
   "STT": 1382,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Thôn Đìa, xã Nam Hồng, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1630658,
   "Latitude": 105.7835348
 },
 {
   "STT": 1383,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 1384,
   "Name": "Phòng khám Nha Khoa Thành Tín",
   "address": "Số 158, Thôn Kiều Mai, xã Phú Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0438749,
   "Latitude": 105.7490542
 },
 {
   "STT": 1385,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 22, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1386,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Ki ốt số 1 chợ Vàng, Thôn Vàng, xã Cổ Bi, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0345612,
   "Latitude": 105.9384099
 },
 {
   "STT": 1387,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 46, ngõ 394, đường Mỹ Đình, Thôn Đình Thôn, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.021536,
   "Latitude": 105.7749121
 },
 {
   "STT": 1388,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 20, ngõ 56/2/5, đường Lê Quang Đạo, Khu X3, Thôn Phú Đô, xã Mễ Trì, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0110448,
   "Latitude": 105.7717843
 },
 {
   "STT": 1389,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Thôn Tó, xã Tây Mỗ, , Hà Nội",
   "Longtitude": 20.9957265,
   "Latitude": 105.7456016
 },
 {
   "STT": 1390,
   "Name": "Phòng khám Chuyên khoa Nội Đức Trí",
   "address": "Xóm 2, Thôn Thượng, xã Đông Dư, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9831564,
   "Latitude": 105.916438
 },
 {
   "STT": 1391,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 14B, tổ 7, thị trấn Cầu Diễn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0403832,
   "Latitude": 105.7629924
 },
 {
   "STT": 1392,
   "Name": "Cơ Sở Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Khu Hòa Sơn, thị trấn Chúc Sơn, Chương Mỹ, Hà Nội",
   "Longtitude": 20.91966,
   "Latitude": 105.7003
 },
 {
   "STT": 1393,
   "Name": "Phòng khám Chuyên khoa Phụ Sản 16A Quang Vinh",
   "address": "Thôn Chúc Lý, xã Ngọc Hòa, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9202554,
   "Latitude": 105.6958162
 },
 {
   "STT": 1394,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 207A phố Huế, phường Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0127659,
   "Latitude": 105.8519926
 },
 {
   "STT": 1395,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế Bắc Đuống",
   "address": "Số 16B đường Đặng Phúc Thông, xã Yên Thường, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0985652,
   "Latitude": 105.9305422
 },
 {
   "STT": 1396,
   "Name": "Phòng khám Đa Khoa Bắc Thăng Long Trực Thuộc Chi Nhánh Công Ty Cổ Phần Đại An Sinh ",
   "address": "Tầng 1, nhà C3, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.129996,
   "Latitude": 105.7745057
 },
 {
   "STT": 1397,
   "Name": "Phòng khám Sản Phụ Khoa Thanh Thanh",
   "address": "Số 9 ngõ 272 đường Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.009253,
   "Latitude": 105.8567869
 },
 {
   "STT": 1398,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Minh Hương",
   "address": "Ô số 11, lô B, Khu tái định cư Đồng Me, xã Mễ Trì, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0114816,
   "Latitude": 105.7767894
 },
 {
   "STT": 1399,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 174 phố Bạch Mai, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 1400,
   "Name": "Phòng khám Chuyên khoa Tiết Niệu - Nam Học",
   "address": "Số 13 ngõ 124 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.994868,
   "Latitude": 105.8524237
 },
 {
   "STT": 1401,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 169 phố Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.013283,
   "Latitude": 105.8498919
 },
 {
   "STT": 1402,
   "Name": "Phòng khám Siêu Âm Sản Phụ Khoa",
   "address": "Số nhà 115 Phố Tía, xã Tô Hiệu, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.804516,
   "Latitude": 105.883937
 },
 {
   "STT": 1403,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Khối 10, phường Vạn Phúc, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9786876,
   "Latitude": 105.771796
 },
 {
   "STT": 1404,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Việt Tệp",
   "address": "21/15 Trần Phú, phường Mộ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9827231,
   "Latitude": 105.7908028
 },
 {
   "STT": 1405,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 119 phố Hoàng Ngân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.007781,
   "Latitude": 105.8090399
 },
 {
   "STT": 1406,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Duy Hưng",
   "address": "Phòng 111 nhà H4 tập thể Thanh Xuân Nam, phường Thanh Xuân Nam, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0209473,
   "Latitude": 105.8156828
 },
 {
   "STT": 1407,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Thôn Đống Xung, xã Thắng Lợi, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8248668,
   "Latitude": 105.8818766
 },
 {
   "STT": 1408,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: X Quang  ",
   "address": "Số 159 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1409,
   "Name": "Phòng  Khám Chuyên khoa Chẩn Đoán Hình Ảnh: X Quang - Thụy Ứng",
   "address": "Tầng 1, số 196, phố Tây Sơn, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0840235,
   "Latitude": 105.6712668
 },
 {
   "STT": 1410,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: X Quang  ",
   "address": "Số 33 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1140882,
   "Latitude": 105.4955339
 },
 {
   "STT": 1411,
   "Name": "Phòng khám Chuyên khoa Nội Khang Dân",
   "address": "Số 159 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1412,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tầng 2 số nhà 33 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1140882,
   "Latitude": 105.4955339
 },
 {
   "STT": 1413,
   "Name": "Phòng khám Chyên Khoa Nội",
   "address": "Số 19, hẻm 462/35/1, đường Bưởi, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0360768,
   "Latitude": 105.8074889
 },
 {
   "STT": 1414,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Dũng Trí",
   "address": "Số 204 đường Khương Đình, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9932642,
   "Latitude": 105.8138351
 },
 {
   "STT": 1415,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ -  Sao Mai",
   "address": "Số 235B3, đường Lạc Long Quân, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0500407,
   "Latitude": 105.8070408
 },
 {
   "STT": 1416,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm",
   "address": "Cửa hàng số 01, địa chỉ số 25A phố Hạ Đình, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9882578,
   "Latitude": 105.8085515
 },
 {
   "STT": 1417,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 8/20 ngõ 100 đường Nguyễn Xiển (I1, tổ 21), phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9893322,
   "Latitude": 105.8041331
 },
 {
   "STT": 1418,
   "Name": "Phòng khám Đa Khoa Amdic",
   "address": "Số 111 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1419,
   "Name": "Phòng khám Đa Khoa 68A",
   "address": "111 Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9702108,
   "Latitude": 105.7844835
 },
 {
   "STT": 1420,
   "Name": "",
   "address": ", , Hà Nội",
   "Longtitude": 21.0277644,
   "Latitude": 105.8341598
 },
 {
   "STT": 1421,
   "Name": "Phòng khám Đa Khoa Tư Nhân Vietlife - Mri Trực Thuộc Công Ty Cổ Phần Cẩm Hà",
   "address": "Số 14 Trần Bình Trọng, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0206367,
   "Latitude": 105.8441078
 },
 {
   "STT": 1422,
   "Name": "Phòng khám Chuyên khoa Ngoại",
   "address": "61 phố Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1424567,
   "Latitude": 105.5020139
 },
 {
   "STT": 1423,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Minh Anh",
   "address": "Số 122 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 1424,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Duy Linh",
   "address": "Số 5 ngách 203/41 ngõ 203 phố Kim Ngưu, phường Thanh Lương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0043966,
   "Latitude": 105.8611706
 },
 {
   "STT": 1425,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 74 khu Chợ, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9344418,
   "Latitude": 105.8462288
 },
 {
   "STT": 1426,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 142 phố Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0153912,
   "Latitude": 105.8514797
 },
 {
   "STT": 1427,
   "Name": "Nha Khoa O.Z",
   "address": "Số 11 ngõ 38 phố Phương Mai, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0060808,
   "Latitude": 105.8366855
 },
 {
   "STT": 1428,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Tập thể Bệnh viện 19-8 Bộ Công an, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0332928,
   "Latitude": 105.7779325
 },
 {
   "STT": 1429,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 13 phố Bùi Ngọc Dương, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020019,
   "Latitude": 105.8543588
 },
 {
   "STT": 1430,
   "Name": "Nha Khoa Châu Sơn",
   "address": "Số 111/135 ngõ 34 phố Vĩnh Tuy, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9967183,
   "Latitude": 105.8742668
 },
 {
   "STT": 1431,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Trường Giang - 74",
   "address": "Số 193B phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995456,
   "Latitude": 105.859058
 },
 {
   "STT": 1432,
   "Name": "Nha Khoa Thanh Hằng",
   "address": "Số 1100 đường Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.025348,
   "Latitude": 105.798166
 },
 {
   "STT": 1433,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 46, phố Quan Nhân, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0046868,
   "Latitude": 105.8113547
 },
 {
   "STT": 1434,
   "Name": "Nha Khoa Đức",
   "address": "Số 32 đường Lê Văn Lương (kéo dài), phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0064529,
   "Latitude": 105.8064435
 },
 {
   "STT": 1435,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 03 ngõ 79A phố Lý Nam Đế, phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0318859,
   "Latitude": 105.8443155
 },
 {
   "STT": 1436,
   "Name": "Nha Khoa Hoàng Long",
   "address": "Phần diện tích 30m2, số 14, phố Vũ Phạm Hàm, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0168765,
   "Latitude": 105.7982138
 },
 {
   "STT": 1437,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Nhà F ngõ 115 Nguyễn Khuyến, phường Văn Miếu, quận Đống Đa, Hà Nội",
   "Longtitude": 21.02851,
   "Latitude": 105.837467
 },
 {
   "STT": 1438,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 202 B7, tổ 28, phố Đức Giang, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0667415,
   "Latitude": 105.8836638
 },
 {
   "STT": 1439,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Tim Mạch Tạ Mạnh Cường - 65",
   "address": "Số 68 ngách 27 ngõ 41 phố Vọng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9977724,
   "Latitude": 105.8431078
 },
 {
   "STT": 1440,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 49, ngõ 116, tổ 7, phường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0588582,
   "Latitude": 105.8583873
 },
 {
   "STT": 1441,
   "Name": "Phòng khám Chuyên khoa Nhi Hoa Lan",
   "address": "Số 24, ngõ 87, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0219437,
   "Latitude": 105.7978113
 },
 {
   "STT": 1442,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 104 phố Nguyễn Du, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.020552,
   "Latitude": 105.842033
 },
 {
   "STT": 1443,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Ki ốt số 2 + 3 tòa nhà B14 Kim Liên, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0083873,
   "Latitude": 105.8343435
 },
 {
   "STT": 1444,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số nhà 18B4B, khu tập thể Nghĩa Tân, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0434009,
   "Latitude": 105.7923394
 },
 {
   "STT": 1445,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Tâm Anh",
   "address": "Ki ốt số 4, ngõ 291/40, đường Lạc Long Quân, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0539916,
   "Latitude": 105.8088567
 },
 {
   "STT": 1446,
   "Name": "Phòng khám Chuyên khoa Siêu Âm Sản Phụ Khoa",
   "address": "1B-I20 B Thành Công, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0211674,
   "Latitude": 105.8128851
 },
 {
   "STT": 1447,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc - Gia Hưng",
   "address": "Số 445 phố Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 1448,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 4, ngách 93/4, ngõ 93, đường Hoàng Quốc Việt, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.046057,
   "Latitude": 105.801261
 },
 {
   "STT": 1449,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 11 phố Hai Bà Trưng, phường Tràng Tiền, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.025053,
   "Latitude": 105.8503485
 },
 {
   "STT": 1450,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 15 ngõ 103 phố 8/3, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0003174,
   "Latitude": 105.8588087
 },
 {
   "STT": 1451,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 54 phố Thọ Lão, phường Đống Mác, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0112199,
   "Latitude": 105.8575272
 },
 {
   "STT": 1452,
   "Name": "Phòng khám Chuyên khoa Da Liễu - Thái Thịnh",
   "address": "Số 211 - V2 (59A Thái Thịnh 2), phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.010146,
   "Latitude": 105.817391
 },
 {
   "STT": 1453,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm",
   "address": "99 phố Vồi, thị trấn Thường Tín, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8715715,
   "Latitude": 105.8627913
 },
 {
   "STT": 1454,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Cụm 7, Yên Duyên, phường Yên Sở, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9735895,
   "Latitude": 105.8674671
 },
 {
   "STT": 1455,
   "Name": "Nha Khoa Thúy Nga",
   "address": "Tổ 28 khu tái định cư X2B, phường Yên Sở, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9620007,
   "Latitude": 105.8745575
 },
 {
   "STT": 1456,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 34D đường Xuân La, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0691031,
   "Latitude": 105.8108604
 },
 {
   "STT": 1457,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 186A, phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349405,
   "Latitude": 105.8244294
 },
 {
   "STT": 1458,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Xóm 7, xã Yên Mỹ, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.947025,
   "Latitude": 105.8735569
 },
 {
   "STT": 1459,
   "Name": "Phòng khám Chuyên khoa Tâm Thần",
   "address": "(Tầng 2) Số 6 ngõ 289 đường Giáp Bát, tổ 21B, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9837328,
   "Latitude": 105.8439125
 },
 {
   "STT": 1460,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm - Ultracare",
   "address": "Số 6 đường Nguyễn Hữu Thọ, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9703699,
   "Latitude": 105.8404958
 },
 {
   "STT": 1461,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm Trực Thuộc Công Ty Tnhh Đầu Từ Công Nghệ Và Dịch Vụ Y Tế Thịnh An",
   "address": "Số 88, dốc Phụ Sản, đường Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026825,
   "Latitude": 105.809323
 },
 {
   "STT": 1462,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 32 phố Vạn Bảo, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.034455,
   "Latitude": 105.817441
 },
 {
   "STT": 1463,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Trực Thuộc Công Ty Tnhh Đầu Từ Công Nghệ Và Dịch Vụ Y Tế Thịnh An",
   "address": "Số 88, dốc Phụ Sản, đường Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026825,
   "Latitude": 105.809323
 },
 {
   "STT": 1464,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Tnhh Đầu Tư Công Nghệ Và Dịch Vụ Y Tế Thịnh An",
   "address": "Số 88, dốc Phụ Sản, đường Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026825,
   "Latitude": 105.809323
 },
 {
   "STT": 1465,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 27, ngõ 81 Láng Hạ, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0194874,
   "Latitude": 105.8108711
 },
 {
   "STT": 1466,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 408 Thụy Khuê, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.046246,
   "Latitude": 105.812901
 },
 {
   "STT": 1467,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "A11, tập thể Học viện Hành chính Quốc gia, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0449231,
   "Latitude": 105.7948314
 },
 {
   "STT": 1468,
   "Name": "Nha Khoa Hòa Phương",
   "address": "Số 210K (tầng 1), phố Đội Cấn, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0349255,
   "Latitude": 105.8252758
 },
 {
   "STT": 1469,
   "Name": "Nha Khoa Phúc Hậu",
   "address": "Số 3 ngách 218/125 ngõ 218 phố Chợ Khâm Thiên, phường Trung Phụng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0089559,
   "Latitude": 105.8234402
 },
 {
   "STT": 1470,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Thôn Yên Phú, xã Liên Ninh, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9072074,
   "Latitude": 105.8529797
 },
 {
   "STT": 1471,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 11 lô A tập thể Đại học Khoa học tự nhiên, ngõ 443 đường Nguyễn Trãi, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9959344,
   "Latitude": 105.8079725
 },
 {
   "STT": 1472,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Vietclinic Việt Nam",
   "address": "Số 21 đường Nguyễn Công Hoan, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.027133,
   "Latitude": 105.81491
 },
 {
   "STT": 1473,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Ts8",
   "address": "Số 2, ngõ 36, phố Trung Hòa, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0148083,
   "Latitude": 105.8023656
 },
 {
   "STT": 1474,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 154 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 1475,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 07 thị trấn Sở Lương thực, phường Vạn Phúc, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9786876,
   "Latitude": 105.771796
 },
 {
   "STT": 1476,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nguyễn Văn Nam - Thủy",
   "address": "Thôn Phan Xá, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1409106,
   "Latitude": 105.8477646
 },
 {
   "STT": 1477,
   "Name": "Phòng khám Chuyên khoa Nội Ân Trạch",
   "address": "Số 22 phố Hàng Than, phường Nguyễn Trung Trực, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0414943,
   "Latitude": 105.8470793
 },
 {
   "STT": 1478,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 164 phố Hoàng Văn Thái, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9965037,
   "Latitude": 105.8261259
 },
 {
   "STT": 1479,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Hòa Bình",
   "address": "Số 17, ngõ 190 đường Hoàng Mai, phường Hoàng Văn Thụ, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9857744,
   "Latitude": 105.8619151
 },
 {
   "STT": 1480,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Nhật Linh",
   "address": "Tầng 2 nhà số 375 đường Giải Phóng, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9914079,
   "Latitude": 105.8410822
 },
 {
   "STT": 1481,
   "Name": "Phòng khám Chuyên khoa Nhi Pgs.Ts Nguyễn Thị Yến",
   "address": "Số 40C, ngõ 219, đường Nguyễn Ngọc Vũ, tổ 26, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.013743,
   "Latitude": 105.8057239
 },
 {
   "STT": 1482,
   "Name": "Phòng khám Nha Khoa Sài Gòn Hà Nội",
   "address": "Số 88 đường Lê Trọng Tấn, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9939419,
   "Latitude": 105.8314746
 },
 {
   "STT": 1483,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Lê Việt",
   "address": "Số 28 phố Hàng Vôi, phường Lý Thái Tổ, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0298759,
   "Latitude": 105.8572115
 },
 {
   "STT": 1484,
   "Name": "Phòng khám Chuyên khoa Nội Thần Kinh",
   "address": "Số A5, lô 12 phố Trần Nguyên Đán, khu đô thị Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9867402,
   "Latitude": 105.832536
 },
 {
   "STT": 1485,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 2 ngõ 144 An Dương Vương, phường Phú Thượng, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0897755,
   "Latitude": 105.7954645
 },
 {
   "STT": 1486,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Đội 2, Tả Thanh Oai, Thanh Trì, Hà Nội",
   "Longtitude": 20.945579,
   "Latitude": 105.8073252
 },
 {
   "STT": 1487,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Đội 1, Tả Thanh Oai, Thanh Trì, Hà Nội",
   "Longtitude": 20.9501775,
   "Latitude": 105.8977152
 },
 {
   "STT": 1488,
   "Name": "Phòng khám Chuyên khoa Nội - 101B - C5",
   "address": "P101B - C5 tập thể Giảng Võ (mặt phố Trần Huy Liệu), phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0278908,
   "Latitude": 105.8217151
 },
 {
   "STT": 1489,
   "Name": "Phòng khám Chuyên khoa Nhi Bác sĩ Hảo",
   "address": "Số nhà 29, ngõ 337, đường Cầu Giấy, tổ 37, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0355706,
   "Latitude": 105.7915485
 },
 {
   "STT": 1490,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 7-A18 Khu tập thể Nghĩa Tân, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0434009,
   "Latitude": 105.7923394
 },
 {
   "STT": 1491,
   "Name": "Phòng khám Chuyên khoa Nhi Ngọc Hải",
   "address": "Số 20 tổ 7 khu Ga, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9493584,
   "Latitude": 105.8444419
 },
 {
   "STT": 1492,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Xóm Tràng, Thôn Nhân Mỹ, xã Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.023289,
   "Latitude": 105.7744719
 },
 {
   "STT": 1493,
   "Name": "Nha Khoa Đại Mỗ",
   "address": "Thôn Chợ, xã Đại Mỗ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9984373,
   "Latitude": 105.7519272
 },
 {
   "STT": 1494,
   "Name": "Nha Khoa Trần Cung",
   "address": "Số 188, phố Trần Cung, Thôn Hoàng 4, xã Cổ Nhuế, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0583202,
   "Latitude": 105.7835931
 },
 {
   "STT": 1495,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Hóa Sinh - 15 Nguyễn Viết Xuân",
   "address": "Tầng 2 số 15 phố Nguyễn Viết Xuân, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.998894,
   "Latitude": 105.82758
 },
 {
   "STT": 1496,
   "Name": "Phòng khám Chuyên khoa Da Liễu Xuân Hùng",
   "address": "Số 37B Hoàng Diệu, phường Quang Trung, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1371613,
   "Latitude": 105.503597
 },
 {
   "STT": 1497,
   "Name": "Phòng khám Chuyên khoa Nội Tiền Phong",
   "address": "Khu 15, đội 17, xóm Hồ, Thôn Yên Nhân, xã Tiền Phong, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1441214,
   "Latitude": 105.7577211
 },
 {
   "STT": 1498,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Phố Bình Đà, Thôn Chợ, xã Bình Minh, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.88956,
   "Latitude": 105.7626
 },
 {
   "STT": 1499,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 113 phố Trần Đăng Ninh, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7305099,
   "Latitude": 105.7744709
 },
 {
   "STT": 1500,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Đức Điềm",
   "address": "Tiểu khu Thao Chính, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.74565,
   "Latitude": 105.911
 },
 {
   "STT": 1501,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "07 Công ty công nghệ phẩm, Đa Sỹ, Kiến Hưng, Hà Đông, Hà Nội",
   "Longtitude": 20.9614947,
   "Latitude": 105.785197
 },
 {
   "STT": 1502,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Khu đường Quốc lộ 2, xã Phù Lỗ, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 10.664292,
   "Latitude": 106.569372
 },
 {
   "STT": 1503,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Hồng Thái",
   "address": "Tổ 5, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1623217,
   "Latitude": 105.8487225
 },
 {
   "STT": 1504,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Thôn Hà Xá, xã Đại Hưng, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6652414,
   "Latitude": 105.7615252
 },
 {
   "STT": 1505,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm",
   "address": "Thôn Hưng Đạo, thị trấn Tây Đằng, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2067429,
   "Latitude": 105.4273093
 },
 {
   "STT": 1506,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh - Bình Minh 36",
   "address": "158B Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9679103,
   "Latitude": 105.7875291
 },
 {
   "STT": 1507,
   "Name": "Phòng khám Dđa Khoa Trực Thuộc Công Ty Tnhh Khám Chữa Bệnh Hà Nội Hight Quality",
   "address": "Thôn Vôi Đá, xã Trần Phú, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8310708,
   "Latitude": 105.6498975
 },
 {
   "STT": 1508,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Hạnh Phúc",
   "address": "Số 75 đường Phùng Hưng, phường Phúc La, Hà Đông, Hà Nội",
   "Longtitude": 20.9707869,
   "Latitude": 105.7839203
 },
 {
   "STT": 1509,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình - 55",
   "address": "Số 201 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9688505,
   "Latitude": 105.7862033
 },
 {
   "STT": 1510,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Nguyễn Thị Thanh Thu",
   "address": "Số 615 Chùa Thông, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1299202,
   "Latitude": 105.5069258
 },
 {
   "STT": 1511,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 58B phố Vạn Kiếp, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0187737,
   "Latitude": 105.8631417
 },
 {
   "STT": 1512,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Thái Yên Dương",
   "address": "Số 174 đường Láng, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.006492,
   "Latitude": 105.8161609
 },
 {
   "STT": 1513,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Khu đường Quốc lộ 2, xã Phù Lỗ, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 10.664292,
   "Latitude": 106.569372
 },
 {
   "STT": 1514,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Đồi Chè, phường Trung Sơn Trầm, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1032279,
   "Latitude": 105.4969964
 },
 {
   "STT": 1515,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Vạn Phúc",
   "address": "Số 21 phố Vạn Phúc, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0332108,
   "Latitude": 105.8194328
 },
 {
   "STT": 1516,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Mika",
   "address": "Thôn Chúc Lý, xã Ngọc Hòa, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9202554,
   "Latitude": 105.6958162
 },
 {
   "STT": 1517,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Số 40, ngõ 99 phố Đức Giang, tổ 17, phường Thượng Thanh, quận Long Biên, Hà Nội",
   "Longtitude": 21.0666584,
   "Latitude": 105.8833742
 },
 {
   "STT": 1518,
   "Name": "Phòng khám Nha Khoa Pro",
   "address": "Số 506 Quang Trung, phường La Khê, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9608895,
   "Latitude": 105.7629099
 },
 {
   "STT": 1519,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "A44-thị trấn 17 khu đô thị Văn Quán - Yên Phúc, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.970333,
   "Latitude": 105.7880497
 },
 {
   "STT": 1520,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Mạnh Hà",
   "address": "Tầng 2 nhà số 4, Chùa Thông, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1174311,
   "Latitude": 105.49694
 },
 {
   "STT": 1521,
   "Name": "Phòng khám Chuyên khoa Nội Duy Đạt",
   "address": "Số 5-M20 tập thể Mai Hương, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.000444,
   "Latitude": 105.8516943
 },
 {
   "STT": 1522,
   "Name": "Phòng khám Đa Khoa Tâm Thành",
   "address": "Số 161 phố Đặng Tiến Đông, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013307,
   "Latitude": 105.822675
 },
 {
   "STT": 1523,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Nagomi Trực Thuộc Công Ty Tnhh Nha Khoa Nagomi",
   "address": "Phòng 221, 222, 223, Trung tâm thương mại Indochina Plaza Hà Nội, số 239, đường Xuân Thủy, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.036056,
   "Latitude": 105.782275
 },
 {
   "STT": 1524,
   "Name": "Nha Khoa Minh Tâm 2",
   "address": "Số 45 phố Thi Sách, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0156972,
   "Latitude": 105.8543384
 },
 {
   "STT": 1525,
   "Name": "Phòng khám Chuyên khoa Nhi Tây Hồ",
   "address": "Số 12C ngõ 218 Lạc Long Quân, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0538486,
   "Latitude": 105.8107035
 },
 {
   "STT": 1526,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Xương, Khớp, Cơ, Thần Kinh, Cột Sống Hoa Kỳ Prochiro Trực Thuộc Công Ty Tnhh Thương Mại Hà Minh Anh",
   "address": "Căn hộ số 01+02, nhà A1 thị trấn Lắp máy ĐN&XD, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0039519,
   "Latitude": 105.8421025
 },
 {
   "STT": 1527,
   "Name": "Phòng khám Chuyên khoa Da Liễu Trực Thuộc Công Ty Tnhh Lê Long",
   "address": "42 Ngô Thì Nhậm, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0174592,
   "Latitude": 105.8528152
 },
 {
   "STT": 1528,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 175 đường Doãn Kế Thiện, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0414805,
   "Latitude": 105.7772575
 },
 {
   "STT": 1529,
   "Name": "Phòng khám Chuyên khoa Da Liễu Saigon Smile",
   "address": "Văn phòng 7 tòa nhà 17T9 khu đô thị Trung Hòa - Nhân Chính, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0076142,
   "Latitude": 105.8036935
 },
 {
   "STT": 1530,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Đường 419, xã Bình Phú, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0208269,
   "Latitude": 105.6085569
 },
 {
   "STT": 1531,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Tnhh Y Tế Sinh Đường",
   "address": "Số 40, phố Đốc Ngữ, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0411699,
   "Latitude": 105.8134245
 },
 {
   "STT": 1532,
   "Name": "Phòng khám Chuyên khoa Mắt Hà An",
   "address": "Số nhà 10 phố Chùa Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.022966,
   "Latitude": 105.8009235
 },
 {
   "STT": 1533,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 19 Phù Đổng Thiên Vương, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016095,
   "Latitude": 105.853722
 },
 {
   "STT": 1534,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp ",
   "address": "Xóm Điếm, Hữu Hòa, Thanh Trì, Hà Nội",
   "Longtitude": 20.95068,
   "Latitude": 105.808
 },
 {
   "STT": 1535,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Quỳnh Lan",
   "address": "P1-C6 phố Lương Định Của, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.004013,
   "Latitude": 105.835841
 },
 {
   "STT": 1536,
   "Name": "Nha Khoa Minh Anh",
   "address": "Số 106, tổ dân phố Lộc, phường Xuân Đỉnh, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0678286,
   "Latitude": 105.7886365
 },
 {
   "STT": 1537,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Thanh Ấm, thị trấn Vân Đình, Ứng Hòa, Hà Nội",
   "Longtitude": 20.7249952,
   "Latitude": 105.7724041
 },
 {
   "STT": 1538,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Quỳnh Lan",
   "address": "Số 138 phố Tây Sơn, phường Quang Trung, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013141,
   "Latitude": 105.826775
 },
 {
   "STT": 1539,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Quỳnh Lan",
   "address": "Số 138 phố Tây Sơn, phường Quang Trung, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013141,
   "Latitude": 105.826775
 },
 {
   "STT": 1540,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Long Nghĩa",
   "address": "Số 19A ngõ 72 phố Tôn Thất Tùng, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0016721,
   "Latitude": 105.828934
 },
 {
   "STT": 1541,
   "Name": "Nha Khoa Phương I",
   "address": "Số 12 đường Văn Cao, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.041879,
   "Latitude": 105.816158
 },
 {
   "STT": 1542,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 93, đường Hồ Tùng Mậu, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0365054,
   "Latitude": 105.777382
 },
 {
   "STT": 1543,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Phúc Gia",
   "address": "Số 150 Lô A4 Khu đô thị mới Đại Kim - Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9877167,
   "Latitude": 105.8315992
 },
 {
   "STT": 1544,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Ngọc Lan",
   "address": "Số 1 ngách 84/39 ngõ 84 phố Ngọc Khánh, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0256269,
   "Latitude": 105.8143806
 },
 {
   "STT": 1545,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Tiêu Hóa",
   "address": "Số 3, ngõ 329, đường Cầu Giấy, tổ 39, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.035232,
   "Latitude": 105.7927393
 },
 {
   "STT": 1546,
   "Name": "Nha Khoa Hoàn Mỹ",
   "address": "Số 283A phố Khương Trung, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.994637,
   "Latitude": 105.817116
 },
 {
   "STT": 1547,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Tara",
   "address": "Số 24, phố Trung Hòa, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0162018,
   "Latitude": 105.8021109
 },
 {
   "STT": 1548,
   "Name": "Phòng khám Chuyên khoa Da Liễu Trực Thuộc Công Ty Cổ Phần Quốc Tế Y Tế Tam Sơn",
   "address": "Tầng 1, Khách sạn ATS, 33B Phạm Ngũ Lão, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0218888,
   "Latitude": 105.8594181
 },
 {
   "STT": 1549,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Vũ Đức",
   "address": "Số 13 nhà 5A phố Hàng Chuối, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0166093,
   "Latitude": 105.8575429
 },
 {
   "STT": 1550,
   "Name": "Phòng khám Chuyên khoa Da Liễu Hữu Nghị",
   "address": "Số 91 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1551,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Tnhh Pamas Spa Và Phòng khám ",
   "address": "(Tầng 3) Số 37, phố Thợ Nhuộm, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0265712,
   "Latitude": 105.8450516
 },
 {
   "STT": 1552,
   "Name": "Nha Khoa S-A-K-U-R-A.V.N",
   "address": "Số 12, ngõ 604 Trường Chinh, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0014408,
   "Latitude": 105.8279446
 },
 {
   "STT": 1553,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Đầu Tư Thương Mại Toàn Khánh",
   "address": "Số 104, đường Lạc Long Quân, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0557222,
   "Latitude": 105.8088857
 },
 {
   "STT": 1554,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 377 phố Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 1555,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 286 Khâm Thiên, phường Thổ Quan, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0194026,
   "Latitude": 105.8338369
 },
 {
   "STT": 1556,
   "Name": "Nha Khoa Đức Toàn",
   "address": "Số 273 đường Thạch Bàn, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0231982,
   "Latitude": 105.906053
 },
 {
   "STT": 1557,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 1 phố Bùi Huy Bích, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9674038,
   "Latitude": 105.8452522
 },
 {
   "STT": 1558,
   "Name": "Nha Khoa Vũ Anh",
   "address": "Số 43, ngõ huyện, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0293143,
   "Latitude": 105.8480903
 },
 {
   "STT": 1559,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 29 phố Thanh Nhàn, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0008348,
   "Latitude": 105.8606695
 },
 {
   "STT": 1560,
   "Name": "Nha Khoa Minh Châu",
   "address": "Tầng 1 nhà P1 khu đô thị Việt Hưng, phường Giang Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0627864,
   "Latitude": 105.9083056
 },
 {
   "STT": 1561,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 36A",
   "address": "Số 88 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9686803,
   "Latitude": 105.7855714
 },
 {
   "STT": 1562,
   "Name": "Nha Khoa Phú Quý",
   "address": "Số 37 Lương Khánh Thiện, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.985839,
   "Latitude": 105.851071
 },
 {
   "STT": 1563,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Thôn Tân Độ, xã Hồng Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7881315,
   "Latitude": 105.8240762
 },
 {
   "STT": 1564,
   "Name": "Phòng khám Chuyên khoa Nội Văn Chương",
   "address": "Số 16, xóm Nông Cụ, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7337728,
   "Latitude": 105.7704551
 },
 {
   "STT": 1565,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Bình An",
   "address": "Số 7/97, đường Nguyễn Đổng Chi, phường Cầu Diễn, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0388214,
   "Latitude": 105.7645938
 },
 {
   "STT": 1566,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Nhà số 30 ngách 1 ngõ 36 phố Trần Điền, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9886207,
   "Latitude": 105.8293837
 },
 {
   "STT": 1567,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "129 H1 đường Nguyễn Trãi, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 1568,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế  - Khám Chữa Bệnh Việt Nam (Hoạt Động Theo Mô Hình Xã Hội Hóa Tại Trung Tâm Y Tế Dự Phòng Hà Nội)",
   "address": "Số 70 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0186952,
   "Latitude": 105.8066597
 },
 {
   "STT": 1569,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Tiểu khu Phú Gia, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7825722,
   "Latitude": 105.9156477
 },
 {
   "STT": 1570,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 23 ngõ 654 đường Lạc Long Quân, tổ 1 cụm 2, phường Nhật Tân, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0652024,
   "Latitude": 105.8101498
 },
 {
   "STT": 1571,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Dịch Vụ Y Tế Thiên Bình",
   "address": "Số 26, tổ 16A, phố Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9831371,
   "Latitude": 105.8319653
 },
 {
   "STT": 1572,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 229 đường Tam Trinh, phường Mai Động, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.989694,
   "Latitude": 105.863298
 },
 {
   "STT": 1573,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 91 Xuân La, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.063271,
   "Latitude": 105.807004
 },
 {
   "STT": 1574,
   "Name": "Phòng khám Chuyên khoa Nội 16A Quang Vinh",
   "address": "Thôn Chúc Lý, xã Ngọc Hòa, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9202554,
   "Latitude": 105.6958162
 },
 {
   "STT": 1575,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Khớp",
   "address": "Số 41/74 ngõ Thịnh Hào 1, phường Hàng Bột, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0239698,
   "Latitude": 105.8306752
 },
 {
   "STT": 1576,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Y Khoa Bạch Mai",
   "address": "Số 188 phố Tây Sơn, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0857071,
   "Latitude": 105.6601168
 },
 {
   "STT": 1577,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "93 phố Nguyễn Hữu Huân, phường Lý Thái Tổ, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0316112,
   "Latitude": 105.8546642
 },
 {
   "STT": 1578,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 189, đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9690806,
   "Latitude": 105.7860641
 },
 {
   "STT": 1579,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 15, ngõ 18, tổ 9, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 21.0178584,
   "Latitude": 105.8111302
 },
 {
   "STT": 1580,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Nhà A2 Đền Lừ II, phường Hoàng Văn Thụ, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9839285,
   "Latitude": 105.8583738
 },
 {
   "STT": 1581,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 54 tầng 2 phố Lê Thanh Nghị, phường Bách Khoa, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.003452,
   "Latitude": 105.849162
 },
 {
   "STT": 1582,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "17M5, Bắc Linh Đàm, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9711704,
   "Latitude": 105.8282959
 },
 {
   "STT": 1583,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Phòng 110 - C9, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0450408,
   "Latitude": 105.7921796
 },
 {
   "STT": 1584,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 110-C9, phố Tô Hiệu, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0416888,
   "Latitude": 105.7914228
 },
 {
   "STT": 1585,
   "Name": "Phòng khám Chuyên khoa Nhi Thảo Nguyên",
   "address": "Thôn Thượng, xã Thanh Liệt, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9682472,
   "Latitude": 105.8225861
 },
 {
   "STT": 1586,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh",
   "address": "(Tầng 5) Số 99 phố Phủ Doãn, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0311543,
   "Latitude": 105.8491764
 },
 {
   "STT": 1587,
   "Name": "Nha Khoa Châu Á Thái Bình Dương",
   "address": "Số 104 phố Thanh Nhàn, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0027377,
   "Latitude": 105.8574772
 },
 {
   "STT": 1588,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 8 ngách 51/5 phố Lãng Yên, phường Thanh Lương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0092216,
   "Latitude": 105.8666563
 },
 {
   "STT": 1589,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Trực Thuộc Công Ty Cổ Phần Dược Thiết Bị Y Tế Biphartek",
   "address": "Số 61E, đường Đê La Thành, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0155086,
   "Latitude": 105.8335127
 },
 {
   "STT": 1590,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Linh Chi",
   "address": "Số 50 phố Chùa Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0230627,
   "Latitude": 105.803082
 },
 {
   "STT": 1591,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 16 phố Cù Chính Lan, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9997091,
   "Latitude": 105.8233505
 },
 {
   "STT": 1592,
   "Name": "Nha Khoa Thu Hà",
   "address": "Số 7 ngõ 9 phố Lương Định Của, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.007656,
   "Latitude": 105.83443
 },
 {
   "STT": 1593,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Tâm Đức",
   "address": "Số 962 đường Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0221863,
   "Latitude": 105.8007005
 },
 {
   "STT": 1594,
   "Name": "Phòng khám Chuyên khoa Nội Bảo Minh",
   "address": "C11 ngõ 25 Vũ Ngọc Phan, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014919,
   "Latitude": 105.8094747
 },
 {
   "STT": 1595,
   "Name": "Nha Khoa Việt Chiến",
   "address": "Số 634 đường La Thành, phường Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0238997,
   "Latitude": 105.8174235
 },
 {
   "STT": 1596,
   "Name": "Phòng khám Chuyên khoa Nội Đại Nghĩa",
   "address": "Số 29, phố Đại Đồng, thị trấn Đại Nghĩa, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6866081,
   "Latitude": 105.7415645
 },
 {
   "STT": 1597,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Đinh Xuyên, Hòa Nam, Ứng Hòa, Hà Nội",
   "Longtitude": 20.6807948,
   "Latitude": 105.7578572
 },
 {
   "STT": 1598,
   "Name": "Phòng khám Chuyên khoa Phụ Sản An Thương",
   "address": "Đội 29, Tân Trại, Phú Cường, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2104798,
   "Latitude": 105.7956828
 },
 {
   "STT": 1599,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Thôn 3, xã Yên Sở, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0419692,
   "Latitude": 105.6754035
 },
 {
   "STT": 1600,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 23 khu tập thể huyện đội, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9493584,
   "Latitude": 105.8451285
 },
 {
   "STT": 1601,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng An",
   "address": "Số 6, thị trấn Trạm Trôi, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.068162,
   "Latitude": 105.7098055
 },
 {
   "STT": 1602,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 2/14 Chùa Thông, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1237251,
   "Latitude": 105.5010009
 },
 {
   "STT": 1603,
   "Name": "Phòng khám Đa Khoa Quân Hằng",
   "address": "Số 50, tổ 3, khu Xuân Hà, thị trấn Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9014044,
   "Latitude": 105.5798009
 },
 {
   "STT": 1604,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Sồ 2 phố Phượng Trì, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0823361,
   "Latitude": 105.6740919
 },
 {
   "STT": 1605,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc",
   "address": "Số7 tổ 2 Khối 1A, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1606,
   "Name": "Nha Khoa Việt Xô",
   "address": "285 đường Lâm Du, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.0337638,
   "Latitude": 105.8756337
 },
 {
   "STT": 1607,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Thành Vinh",
   "address": "Số 5, ngõ 381 đường Nguyễn Văn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.026999,
   "Latitude": 105.797579
 },
 {
   "STT": 1608,
   "Name": "Phòng khám Dũng Nhi",
   "address": "Thôn Hạ, xã Mễ Trì, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0077194,
   "Latitude": 105.7790888
 },
 {
   "STT": 1609,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Số 37/95 phố Vũ Xuân Thiều, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0349208,
   "Latitude": 105.919251
 },
 {
   "STT": 1610,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Tại nhà ông (Nguyễn Thiện Căn) Thôn Thượng Phúc, xã Thượng Phúc, huyện Tả Thanh Oai, Hà Nội",
   "Longtitude": 20.929913,
   "Latitude": 105.7976556
 },
 {
   "STT": 1611,
   "Name": "Phòng khám Chyên Khoa Nội",
   "address": " Thôn Yên Mỹ, xã Dương Quang, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0049289,
   "Latitude": 105.983208
 },
 {
   "STT": 1612,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Minh Trí",
   "address": "Phố Sủi, Thôn Phú Thụy, xã Phú Thị, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0162381,
   "Latitude": 105.9672384
 },
 {
   "STT": 1613,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 15, tổ 23 đường Cổ Linh, phường Long Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0280599,
   "Latitude": 105.8926853
 },
 {
   "STT": 1614,
   "Name": "Phòng khám Nội Khoa Bác sĩ Đàm Thị Giá",
   "address": "Xóm 4, Thôn Đoàn Kết, xã Cổ Đông, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.04679,
   "Latitude": 105.505885
 },
 {
   "STT": 1615,
   "Name": " Nha Khoa Thăng Long",
   "address": "Thôn Phú Nghĩa, xã Phú Kim, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.062464,
   "Latitude": 105.5778405
 },
 {
   "STT": 1616,
   "Name": "Nha Khoa Nụ Cười",
   "address": "Số 83 đường Thạch Bàn, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0270164,
   "Latitude": 105.9105484
 },
 {
   "STT": 1617,
   "Name": "Nha Khoa Hà Dương",
   "address": "Ngõ Hòa Bình, tổ 10, phường Cự Khối, quận Long Biên, Hà Nội",
   "Longtitude": 21.0123666,
   "Latitude": 105.9059286
 },
 {
   "STT": 1618,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc",
   "address": "Số nhà 41A, tổ 4, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1619,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng ",
   "address": "Chợ Mơ, xã Vạn Thắng, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2484348,
   "Latitude": 105.3955142
 },
 {
   "STT": 1620,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: X Quang",
   "address": "Tiểu khu Mỹ Lâm, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.733944,
   "Latitude": 105.9130779
 },
 {
   "STT": 1621,
   "Name": "Phòng Xét Nghiệm Thuốc Việt",
   "address": "Số 93 Láng Hạ, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.015379,
   "Latitude": 105.814104
 },
 {
   "STT": 1622,
   "Name": "Phòng Xét Nghiệm Quốc Tế Gentis Trực Thuộc Công Ty Cổ Phần Dịch Vụ Phân Tích Di Truyến",
   "address": "Số 15 - 17 phố Nguyễn Chí Thanh, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0294783,
   "Latitude": 105.8129042
 },
 {
   "STT": 1623,
   "Name": "Phòng Xét Nghiệm Dr. Lab Trực Thuộc Công Ty Cổ Phần Y Học Dr. Labo",
   "address": "Tầng 3, số 91 Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0099992,
   "Latitude": 105.8108463
 },
 {
   "STT": 1624,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt  Trực Thuộc Công Ty Cổ Phần Quốc Tế Nha Khoa Việt Pháp",
   "address": "Số 6 phố Thái Hà , phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.012597,
   "Latitude": 105.8201087
 },
 {
   "STT": 1625,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc",
   "address": "Số 56 phố Tràng Tiền, phường Tràng Tiền, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0243921,
   "Latitude": 105.8570065
 },
 {
   "STT": 1626,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Căn hộ 5, nhà A1, thị trấn Công ty lắp điện số I, số 128, đường Trần Bình, phường Mỹ Đình 2, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0312411,
   "Latitude": 105.7774192
 },
 {
   "STT": 1627,
   "Name": "Nha Khoa Bảo An",
   "address": "Số 97, phố Phan Văn Trường, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0400484,
   "Latitude": 105.7860086
 },
 {
   "STT": 1628,
   "Name": "",
   "address": ", , Hà Nội",
   "Longtitude": 21.0277644,
   "Latitude": 105.8341598
 },
 {
   "STT": 1629,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Tầng 1, số 30, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 1630,
   "Name": "Phòng khám Chuyên khoa Siêu Âm An Dương",
   "address": "Số 11 ngách 32/15 phố An Dương, phường Yên Phụ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0518953,
   "Latitude": 105.8414651
 },
 {
   "STT": 1631,
   "Name": "Phòng khám Tai Mũi Họng Đức Duy",
   "address": "Số 6, phố Nguyễn Trung Trực, phường Nguyễn Trung Trực, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0423197,
   "Latitude": 105.8479634
 },
 {
   "STT": 1632,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Nhà E14, lô NO5, Khu đô thị mới Dịch Vọng, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0279801,
   "Latitude": 105.7941962
 },
 {
   "STT": 1633,
   "Name": "Nha Khoa Thu Lan",
   "address": "Số 104 - I17 Thái Hà mới, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014897,
   "Latitude": 105.816864
 },
 {
   "STT": 1634,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Nắng Mới Trực Thuộc Công Ty Tnhh Hỗ Trợ Sáng Kiến Phát Triển Cộng Đồng.",
   "address": "Số 240, Mai Anh Tuấn, phường Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0183206,
   "Latitude": 105.8182579
 },
 {
   "STT": 1635,
   "Name": "Phòng Xét Nghiệm Thăng Long",
   "address": "Số 121 Phủ Doãn, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0273589,
   "Latitude": 105.8478642
 },
 {
   "STT": 1636,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Cổ Phần Viện Mắt Quốc Tế Việt - Nga",
   "address": "Số 1-2, C2 làng quốc tế Thăng Long, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0385796,
   "Latitude": 105.7938069
 },
 {
   "STT": 1637,
   "Name": "Nha Khoa 96",
   "address": "Số 46 đường Tứ Hiệp, thị trấn Văn Điển, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9470426,
   "Latitude": 105.8488908
 },
 {
   "STT": 1638,
   "Name": "Nha Khoa Thẩm Mỹ Tuyết Lộc",
   "address": "Số 178 Khâm Thiên, phường Thổ Quan, quận Đống Đa, Hà Nội",
   "Longtitude": 21.019303,
   "Latitude": 105.834185
 },
 {
   "STT": 1639,
   "Name": "Phòng khám Sản Phụ Khoa C4 Thanh Xuân Bắc",
   "address": "Phòng 107 nhà C4 Tập thể Thanh Xuân Bắc, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.992434,
   "Latitude": 105.801811
 },
 {
   "STT": 1640,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình.",
   "address": "Số 2, tầng 1, phố Lê Đức Thọ, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0361644,
   "Latitude": 105.7704543
 },
 {
   "STT": 1641,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Vi Sinh",
   "address": "Số 35, phố Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0110793,
   "Latitude": 105.8046712
 },
 {
   "STT": 1642,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trí Tâm",
   "address": "Tầng 3 số 73 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1643,
   "Name": "Phòng khám Chuyên khoa Ngoại",
   "address": "Số 62 ngõ Trung Tiền, phường Khâm Thiên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0200794,
   "Latitude": 105.838109
 },
 {
   "STT": 1644,
   "Name": "Phòng khám Chuyên khoa Mắt O-M-A-T",
   "address": "Số A17, khu Tràng Hào, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0101531,
   "Latitude": 105.7988345
 },
 {
   "STT": 1645,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Phòng 101,C3, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.042146,
   "Latitude": 105.793086
 },
 {
   "STT": 1646,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Đức Minh",
   "address": "Số 23, ngõ 76, phố Mai Dịch, tổ 23, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0407926,
   "Latitude": 105.7758575
 },
 {
   "STT": 1647,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 17, ngách 15, ngõ 117, đường Trần Cung, phường Cổ Nhuế 1, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0528931,
   "Latitude": 105.7886185
 },
 {
   "STT": 1648,
   "Name": "Phòng khám Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Y Học Công Nghệ Cao Vệ Nữ",
   "address": "Số 430 xã Đàn, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0158014,
   "Latitude": 105.8328126
 },
 {
   "STT": 1649,
   "Name": "Phòng Tư Vấn Khám Bệnh, Chữa Bệnh Qua Điện Thoại Trực Thuộc Công Ty Tnhh Truyền Thông Ucare Việt",
   "address": "Số 27, ngõ 77/5 Bùi Xương Trạch, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.991806,
   "Latitude": 105.8172723
 },
 {
   "STT": 1650,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Khu 7 phố Yên, xã Tiền Phong, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1539824,
   "Latitude": 105.7571236
 },
 {
   "STT": 1651,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phạm Tuấn Anh 83",
   "address": "Số nhà 14, tổ 27, thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1652,
   "Name": "Phòng khám Chuyên khoa Nội Thiện Tâm",
   "address": "Phố Thạch Lỗi, xã Thanh Xuân, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.22099,
   "Latitude": 105.7708
 },
 {
   "STT": 1653,
   "Name": "Phòng khám Chuyên khoa Nội 108 Tổ 27",
   "address": "Thôn Kính Nỗ, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1613528,
   "Latitude": 105.8593965
 },
 {
   "STT": 1654,
   "Name": "Phòng khám Chuyên khoa Nội Hồng Thái",
   "address": "Vị trí 15, liền kề 2, khu đô thị Đại Thanh, xã Tả Thanh Oai, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9558682,
   "Latitude": 105.8062809
 },
 {
   "STT": 1655,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Đường 3, xã Mai Đình, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.19924,
   "Latitude": 105.8309
 },
 {
   "STT": 1656,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm 108",
   "address": "Thôn Kính Nỗ, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1613528,
   "Latitude": 105.8593965
 },
 {
   "STT": 1657,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 122, Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9682925,
   "Latitude": 105.7866454
 },
 {
   "STT": 1658,
   "Name": "Phòng khám Chuyên khoa Nhi Thanh Mai",
   "address": "Diện tích 12m2, tầng 01, tòa nhà CT1 - VIMECO, phố Nguyễn Chánh, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0087081,
   "Latitude": 105.7945381
 },
 {
   "STT": 1659,
   "Name": "Phòng khám Đa Khoa Quốc Tế Việt - Nga Trực Thuộc Công Ty Cổ Phần Công Nghệ Và Y Tế Việt Nga",
   "address": "Số 36 Tuệ Tĩnh, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0150892,
   "Latitude": 105.8500326
 },
 {
   "STT": 1660,
   "Name": "Phòng khám Đa Khoa Nhân Ái Hà Nội - Trực Thuộc Công Ty Tnhh Xuất Nhập Khẩu Thiết Bị Y Tế Minh Bang Việt Nam",
   "address": "Số 709 Giải Phóng, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9882499,
   "Latitude": 105.8414536
 },
 {
   "STT": 1661,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "72 Lương Khánh Thiện, tổ 18, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.984216,
   "Latitude": 105.8511815
 },
 {
   "STT": 1662,
   "Name": "Phòng khám Đa Khoa Khoa 162",
   "address": "Số 162 đường Lĩnh Nam, phường Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9869476,
   "Latitude": 105.8682975
 },
 {
   "STT": 1663,
   "Name": "Phòng khám Đa Khoa Y Cao Trực Thuộc Công Ty Cổ Phần Dịch Vụ Chăm Sóc Phát Triển Cộng Đồng Abc",
   "address": "Khu 8, xã Tiền Phong, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1605321,
   "Latitude": 105.7644596
 },
 {
   "STT": 1664,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 31, N3, tổ 103, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0200535,
   "Latitude": 105.8246266
 },
 {
   "STT": 1665,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty  Tnhh Nha Khoa Biotis",
   "address": "GBLK C3, GBLK C4 tháp The Manor Hà Nội, Mỹ Đình, Mễ Trì, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0141712,
   "Latitude": 105.7758312
 },
 {
   "STT": 1666,
   "Name": "Phòng khám Chuyên khoa Ngoại Kiên Thành",
   "address": "Số 8 ngõ 253 Thụy Khuê, phường Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.043769,
   "Latitude": 105.815989
 },
 {
   "STT": 1667,
   "Name": "Nha Khoa Hà Nội",
   "address": "Số 1/64 Đặng Văn Ngữ, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013324,
   "Latitude": 105.8335291
 },
 {
   "STT": 1668,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Tnhh Thẩm Mỹ Viện Bác sĩ Văn",
   "address": "Số 9A Đặng Trần Côn, phường Quốc Tử Giám, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0272222,
   "Latitude": 105.8330098
 },
 {
   "STT": 1669,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 268 ngõ Quỳnh, phố Thanh Nhàn, phường Quỳnh Lôi, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.999236,
   "Latitude": 105.8573569
 },
 {
   "STT": 1670,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ ",
   "address": "Số 3T1 dãy nhà 2, tập thế K95 Tôn Thất Thiệp, (ngõ 34A Trần Phú), phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.031876,
   "Latitude": 105.843231
 },
 {
   "STT": 1671,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "P107 - A1 ngõ 29 phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0028514,
   "Latitude": 105.8658014
 },
 {
   "STT": 1672,
   "Name": "Nha Khoa Mạnh Thắng",
   "address": "Số 24A, M1, ngõ 3,đường Tô Hiệu, phường Nguyễn Trãi, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9685829,
   "Latitude": 105.779849
 },
 {
   "STT": 1673,
   "Name": "Nha Khoa Duyên Anh",
   "address": "Số 75 ngõ 184 đê Trần Khát Chân, phường Thanh Lương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0053916,
   "Latitude": 105.8674255
 },
 {
   "STT": 1674,
   "Name": " Nha Khoa Phúc Lợi",
   "address": "Số 93, tổ 9, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0430694,
   "Latitude": 105.9261333
 },
 {
   "STT": 1675,
   "Name": "Phòng khám Chuyên khoa Nhi Thanh Hương",
   "address": "BT4 - VT3, khu đô thị Xa La, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.962248,
   "Latitude": 105.794057
 },
 {
   "STT": 1676,
   "Name": "Nha Khoa Hà Nội",
   "address": "Số 188, đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 1677,
   "Name": "Nha Khoa Khánh Vân",
   "address": "Số 260 đường Xuân Đỉnh, phường Xuân Đỉnh, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0736026,
   "Latitude": 105.7892321
 },
 {
   "STT": 1678,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Thanh Bình",
   "address": "125 Thanh Bình, phường Mộ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9782499,
   "Latitude": 105.7801368
 },
 {
   "STT": 1679,
   "Name": "Phòng khám Chuyên khoa Nội Đức Hùng",
   "address": "Số 188 phố Lê Lợi, thị trấn Vân Đình, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.9689277,
   "Latitude": 105.7795472
 },
 {
   "STT": 1680,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 21, phố Long Biên 2, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0432252,
   "Latitude": 105.8697191
 },
 {
   "STT": 1681,
   "Name": "Nha Khoa Mạc Cẩm Thúy",
   "address": "Số nhà 92, đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9704752,
   "Latitude": 105.7840405
 },
 {
   "STT": 1682,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Doanh Nghiệp Tư Nhân Trung Tâm Khám Bệnh Đa Khoa Nhân Dân",
   "address": "Số 73 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 1683,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 21 khu biệt thự NO2B đô thị mới Sài Đồng, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.04038,
   "Latitude": 105.9085889
 },
 {
   "STT": 1684,
   "Name": "Thẩm Mỹ Viện Kang Nam",
   "address": "Số 38 phố Nguyễn Du, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0149823,
   "Latitude": 105.8474451
 },
 {
   "STT": 1685,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Thôn Thanh Lũng, xã Tiên Phong, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1835489,
   "Latitude": 105.4192549
 },
 {
   "STT": 1686,
   "Name": "Phòng khám Nhi Bác sĩ Thắng ",
   "address": "Số nhà 23, ngõ 98, đường Xuân Thủy, phường Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0210606,
   "Latitude": 105.7978846
 },
 {
   "STT": 1687,
   "Name": "Phòng khám Chuyên khoa Nhi Lương Thị San",
   "address": "Số nhà 29, nhà D6, tập thể Thanh Xuân Bắc, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9926462,
   "Latitude": 105.8004107
 },
 {
   "STT": 1688,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Hoàng Quốc Việt",
   "address": "Phòng 12 - A10, khu tập thể Bắc Nghĩa Tân, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0432165,
   "Latitude": 105.7934009
 },
 {
   "STT": 1689,
   "Name": "Phòng khám Chuyên khoa Nội Thanh Bình",
   "address": "Số 50B, phố Phúc Xá, phường Phúc Xá, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0478999,
   "Latitude": 105.8468498
 },
 {
   "STT": 1690,
   "Name": "Nha Khoa Trần Gia",
   "address": "Số nhà 35 ngõ 117 đường Khương Đình, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.995429,
   "Latitude": 105.814348
 },
 {
   "STT": 1691,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số nhà 79, tổ 9, phường Phú Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.047768,
   "Latitude": 105.7615252
 },
 {
   "STT": 1692,
   "Name": "Phòng khám Răng Hàm Mặt ",
   "address": "Số 283, phố Đội Cấn, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0360729,
   "Latitude": 105.8169223
 },
 {
   "STT": 1693,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Phần diện tích 75m2, kiôt tầng 1, nhà NO7 - B3, đơn nguyên 2, khu đô thị mới Dịch Vọng, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.034476,
   "Latitude": 105.766349
 },
 {
   "STT": 1694,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Nhà 2, ngách 13, ngõ 133, phố Tân Ấp, phường Phúc Xá, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0143321,
   "Latitude": 105.8144036
 },
 {
   "STT": 1695,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 37E ngõ 144 phố Quan Nhân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0042013,
   "Latitude": 105.8098519
 },
 {
   "STT": 1696,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Khám Chữa Bệnh Y Học Hà Nội",
   "address": "Số 956 Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0274205,
   "Latitude": 105.8093053
 },
 {
   "STT": 1697,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Công Nghiệp Thương Mại Và Dịch Vụ Y Tế Phúc Thái",
   "address": "Số 2A, ngõ 266, phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025083,
   "Latitude": 105.841925
 },
 {
   "STT": 1698,
   "Name": "Phòng khám Chuyên khoa Nội Tim Mạch An Bình",
   "address": "Số 14 ngõ 4 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.006042,
   "Latitude": 105.8389977
 },
 {
   "STT": 1699,
   "Name": "Phòng khám Chuyên khoa Nội Hải Ly",
   "address": "Số 34 Ngõ Giếng, phố Đông Các, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0186954,
   "Latitude": 105.8271825
 },
 {
   "STT": 1700,
   "Name": "Phòng khám Chuyên khoa Mắt Khải Nam Trực Thuộc Công Ty Tnhh Khải Nam",
   "address": "Số 138 Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0175899,
   "Latitude": 105.849254
 },
 {
   "STT": 1701,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Dịch Vụ Y Tế Trí Tâm",
   "address": "Số 244, phố Lê Thanh Nghị, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020826,
   "Latitude": 105.8459377
 },
 {
   "STT": 1702,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ Trực Thuộc Công Ty Cổ Phần Đầu Tư Thương Mại Toàn Khánh",
   "address": "Số 933C đường La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0269875,
   "Latitude": 105.8095799
 },
 {
   "STT": 1703,
   "Name": "Nha Khoa Thẩm Mỹ Hàn Lâm",
   "address": "Số 17 ngõ 131 phố Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0124282,
   "Latitude": 105.8186982
 },
 {
   "STT": 1704,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 628 đường Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0583534,
   "Latitude": 105.8906658
 },
 {
   "STT": 1705,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - Hà Răng",
   "address": "Số 514, đường Hà Huy Tập, thị trấn Yên Viên, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0858175,
   "Latitude": 105.9177869
 },
 {
   "STT": 1706,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ - Siêu Âm Sản Phụ Khoa",
   "address": "Số 119 ngõ 66 đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0505892,
   "Latitude": 105.8675355
 },
 {
   "STT": 1707,
   "Name": "Phòng khám Chuyên khoa Nhi Hanh Hương",
   "address": "P102 H4, khu đô thị Việt Hưng, phường Giang Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.0621919,
   "Latitude": 105.9115654
 },
 {
   "STT": 1708,
   "Name": "Phòng khám Chuyên khoa Nhi Thảo Hương",
   "address": "Tầng 1 số 16 ngách 26 ngõ Thái Thịnh 2, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104507,
   "Latitude": 105.8174745
 },
 {
   "STT": 1709,
   "Name": "Phòng khám Chuyên khoa Ngoại Mỹ Việt",
   "address": "Số 620 Hoàng Hoa Thám, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0467597,
   "Latitude": 105.8098222
 },
 {
   "STT": 1710,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Bác sĩ Hoàng Thị Yến",
   "address": "Tầng 1 số 16 ngách 26 ngõ Thái Thịnh 2, phường Thịnh Quang, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104507,
   "Latitude": 105.8174745
 },
 {
   "STT": 1711,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Số 01 ngõ 345 đường Thạch Bàn, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0231982,
   "Latitude": 105.906053
 },
 {
   "STT": 1712,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Đặng Thân",
   "address": "P102 số 54 ngõ 295 phố Bạch Mai, phường Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0021753,
   "Latitude": 105.8510337
 },
 {
   "STT": 1713,
   "Name": "Nha Khoa Á Châu",
   "address": "Số 638 đường Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0657541,
   "Latitude": 105.8981395
 },
 {
   "STT": 1714,
   "Name": "Nha Khoa Thuận An",
   "address": "Số 17 ngách 151A/29 ngõ 165 Thái Hà, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0142257,
   "Latitude": 105.8162491
 },
 {
   "STT": 1715,
   "Name": "Phòng khám Chuyên khoa Nội Đoàn Giang Long",
   "address": "Số 16 Thanh Vị, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1160791,
   "Latitude": 105.4949454
 },
 {
   "STT": 1716,
   "Name": "Phòng khám Chuyên khoa Nội Thần Kinh Hoàng Văn Thuận",
   "address": "A32-thị trấn 13, đô thị Văn Quán, phường Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9742349,
   "Latitude": 105.7872951
 },
 {
   "STT": 1717,
   "Name": "Phòng khám Chuyên khoa Mắt Trần Thị Chu Quý",
   "address": "G13, khu đấu giá Ngô Thị Nhậm, tổ 2, phường Hà Cầu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9686968,
   "Latitude": 105.7688325
 },
 {
   "STT": 1718,
   "Name": "Nha Khoa Bát Tràng",
   "address": "Số 17, đường Đa Tốn, Thôn Thuận Tốn, xã Đa Tốn, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.986163,
   "Latitude": 105.930381
 },
 {
   "STT": 1719,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trần Đức Cường",
   "address": "Số nhà 200, đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9677131,
   "Latitude": 105.7881166
 },
 {
   "STT": 1720,
   "Name": "Nha Khoa Việt Mỹ",
   "address": "Số 61 đường Trường Chinh, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.997901,
   "Latitude": 105.840186
 },
 {
   "STT": 1721,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Đỗ Đức Mầm",
   "address": "Số 19 K5 (Tổ 40), thị trấn Đông Anh, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 1722,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ - Siêu Âm Sản Phụ Khoa",
   "address": "Số 7, ngõ 87, phố Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 1723,
   "Name": "Phòng khám Chuyên khoa Nội 93 Đường Giải Phóng",
   "address": "Số 93 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1724,
   "Name": "Nha Khoa Ngọc Linh",
   "address": "Số 16 ngách 214/35 đường Nguyễn Xiển, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9872454,
   "Latitude": 105.8059255
 },
 {
   "STT": 1725,
   "Name": "Nha Khoa Thẩm Mỹ Thủ Đô",
   "address": "Số 8 phố Vạn Kiếp, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0186912,
   "Latitude": 105.8623427
 },
 {
   "STT": 1726,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 94 Âu Cơ, phường Tứ Liên, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.062066,
   "Latitude": 105.832544
 },
 {
   "STT": 1727,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng ",
   "address": "Ki ốt số 3, nhà H2 khu đô thị Việt Hưng, phường Giang Biên, quận Long Biên, Hà Nội",
   "Longtitude": 21.068151,
   "Latitude": 105.9085351
 },
 {
   "STT": 1728,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Số 21A, tổ 3, khu Tân Bình, thị trấn Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.896141,
   "Latitude": 105.574274
 },
 {
   "STT": 1729,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Cụm 8, thị trấn Phúc Thọ, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1032387,
   "Latitude": 105.5441093
 },
 {
   "STT": 1730,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Nam Hà",
   "address": "Số 123 Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0022144,
   "Latitude": 105.8415378
 },
 {
   "STT": 1731,
   "Name": "Phòng khám Chuyên khoa Thần Kinh Cột Sống Xương Khớp Trực Thuộc Công Ty Cổ Phần Đầu Tư Kim Sa",
   "address": "Số 12 Lê Quý Đôn, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.013315,
   "Latitude": 105.861777
 },
 {
   "STT": 1732,
   "Name": "Nha Khoa Minh Hải",
   "address": "Số 49, Dương Quảng Hàm, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0344389,
   "Latitude": 105.7991472
 },
 {
   "STT": 1733,
   "Name": "Nha Khoa Quốc Tế Âu Mỹ",
   "address": "Số 109 (tầng 1), phố Đốc Ngữ, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0411832,
   "Latitude": 105.813469
 },
 {
   "STT": 1734,
   "Name": "Nha Khoa Lê Hoa",
   "address": "Số 139 ngõ 46 đường Nguyễn Trãi, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 1735,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 54 phố Vĩnh Hồ, phường Ngã Tư Sở, quận Đống Đa, Hà Nội",
   "Longtitude": 21.006723,
   "Latitude": 105.820659
 },
 {
   "STT": 1736,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Y Khoa Bạch Mai",
   "address": "Tiểu khu Thao Chính, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.74565,
   "Latitude": 105.911
 },
 {
   "STT": 1737,
   "Name": "Phòng khám Chuyên khoa Nhi Hải Hồng",
   "address": "18/B khu tập thể Tổng cục II, xã Xuân Đỉnh, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0656733,
   "Latitude": 105.7619816
 },
 {
   "STT": 1738,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 14 phố 8/3, phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.998779,
   "Latitude": 105.860737
 },
 {
   "STT": 1739,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Chăm Sóc Sức Khỏe Hoàng Thành",
   "address": "Số 10, đường Lê Quý Đôn, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0133707,
   "Latitude": 105.8621013
 },
 {
   "STT": 1740,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 74 (tầng 1+2) phố Đào Tấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0325939,
   "Latitude": 105.8087418
 },
 {
   "STT": 1741,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 1, ngách 629/25, phố Kim Mã, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0297473,
   "Latitude": 105.8103271
 },
 {
   "STT": 1742,
   "Name": "Phòng khám Da Liễu Yên Phụ",
   "address": "Số 14 (tầng 4), đường Yên Phụ, phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0472211,
   "Latitude": 105.8434063
 },
 {
   "STT": 1743,
   "Name": "Phòng khám Sản Phụ Khoa 128",
   "address": "Số 128 Thái Thịnh, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.012296,
   "Latitude": 105.817908
 },
 {
   "STT": 1744,
   "Name": "Phòng khám Chuyên khoa Nhi Việt Hưng",
   "address": "Số 01, ngách 637- 16 đường Trương Định, tổ 13, phường Thịnh Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.2506222,
   "Latitude": 105.9779443
 },
 {
   "STT": 1745,
   "Name": "Thẩm Mỹ Viện Hàn Việt",
   "address": "Số 8A phố Hàm Long, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0189841,
   "Latitude": 105.8536485
 },
 {
   "STT": 1746,
   "Name": "Phòng Siêu Âm Sản Phụ Khoa Hoàng Quốc Việt",
   "address": "Phần diện tích 25m2, phòng 12 - A10, khu tập thể Bắc Nghĩa Tân, phường Nghĩa Tân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0440932,
   "Latitude": 105.7922019
 },
 {
   "STT": 1747,
   "Name": "Cơ Sở Dịch Vụ Tiêm (Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Medlatec Hà Đông Trực Thuộc Công Ty Tnhh Công Nghệ Và Xét Nghiệm Y Học",
   "address": "Số 28 đường Phùng Hưng, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9676061,
   "Latitude": 105.7879355
 },
 {
   "STT": 1748,
   "Name": "Phòng khám Chuyên khoa Nhi ",
   "address": "Thôn 1, xã Đông Mỹ, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9209247,
   "Latitude": 105.8655509
 },
 {
   "STT": 1749,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 63, phố Châu Long, phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0446123,
   "Latitude": 105.8423376
 },
 {
   "STT": 1750,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: X Quang",
   "address": "Thôn Xuân Sơn, xã Trung Giã, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.3030059,
   "Latitude": 105.8657274
 },
 {
   "STT": 1751,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Số nhà 71 đường 3 Miếu Thờ, xã Tiên Dược, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2428157,
   "Latitude": 105.843708
 },
 {
   "STT": 1752,
   "Name": "Phòng khám Đa Khoa",
   "address": "Thôn Đông Ngàn, xã Đông Hội, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.0711235,
   "Latitude": 105.8664614
 },
 {
   "STT": 1753,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Số 25 ngách 10/21 phố Tôn Thất Tùng, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 20.9983065,
   "Latitude": 105.8537639
 },
 {
   "STT": 1754,
   "Name": "Phòng khám Chuyên khoa Ngoại Kim Liên",
   "address": "Số 5 ngách 5 phố Hoàng Tích Trí, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.029124,
   "Latitude": 105.808374
 },
 {
   "STT": 1755,
   "Name": "Phòng khám Chuyên khoa Chấn Thương Chỉnh Hình",
   "address": "Số 3 ngõ 64 Đặng Văn Ngữ (cũ: số 40B tổ 45C), phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.013324,
   "Latitude": 105.8335291
 },
 {
   "STT": 1756,
   "Name": "Nha Khoa Thanh Tâm",
   "address": "Thôn Nhân Hiền, xã Hiền Giang, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8741441,
   "Latitude": 105.8202234
 },
 {
   "STT": 1757,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Tầng 1, số 75, phố Nguyễn Thị Định, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0098998,
   "Latitude": 105.8047815
 },
 {
   "STT": 1758,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình ",
   "address": "Số 24, tổ 3, phường Phúc Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0445176,
   "Latitude": 105.7518169
 },
 {
   "STT": 1759,
   "Name": "Thẩm Mỹ Viện Bác sĩ Hà Thanh",
   "address": "Số 243 Giảng Võ, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0245919,
   "Latitude": 105.8212439
 },
 {
   "STT": 1760,
   "Name": "Phòng khám Đa Khoa Yên Hòa Trực Thuộc Trung Tâm Y Tế quận Cầu Giấy",
   "address": "Đường Dương Đình Nghệ, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0215526,
   "Latitude": 105.7878867
 },
 {
   "STT": 1761,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Cổ Phần Quốc Tế Y Tế Tam Sơn",
   "address": "Tầng I, Khách sạn ATS, 33B Phạm Ngũ Lão, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0218888,
   "Latitude": 105.8594181
 },
 {
   "STT": 1762,
   "Name": "Phòng khám Đa Khoa Phú Cường Trực Thuộc Công Ty Tnhh Đầu Tư Phú Cường Hà Nội",
   "address": "Số 615 Nguyễn Văn Cừ, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0529673,
   "Latitude": 105.8861947
 },
 {
   "STT": 1763,
   "Name": "Bệnh Viện Đa Khoa Huyện Phúc Thọ",
   "address": "Thị trấn Phúc Thọ, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1029672,
   "Latitude": 105.5445064
 },
 {
   "STT": 1764,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 86B, ngõ 43, phố Trung Kính, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0130095,
   "Latitude": 105.7999183
 },
 {
   "STT": 1765,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Nguyễn Thị Thanh Mai - Nk",
   "address": "83D đường Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.020142,
   "Latitude": 105.827878
 },
 {
   "STT": 1766,
   "Name": "Phòng khám Sản Phụ Khoa 128",
   "address": "Số 128 Dốc Phụ Sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.026823,
   "Latitude": 105.8082685
 },
 {
   "STT": 1767,
   "Name": "Phòng khám Sản Phụ Khoa Hải Lâm Trực Thuộc Công Ty Cổ Phần  Dịch Vụ Y Tế Health Care Việt Nam",
   "address": "Số 38- M2, khu đô thị Yên Hòa, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0220587,
   "Latitude": 105.7877078
 },
 {
   "STT": 1768,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "716 Nguyễn Văn Cừ, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0540502,
   "Latitude": 105.8873422
 },
 {
   "STT": 1769,
   "Name": "Phòng khám Tư Nhân Bác sĩ Chuyên khoa ii Lê Thị Tiến Vinh",
   "address": "Số 10 ngõ 96 Đê La Thành (cũ: số 1 tổ 46A), phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014101,
   "Latitude": 105.83547
 },
 {
   "STT": 1770,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "716 Nguyễn Văn Cừ, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0540502,
   "Latitude": 105.8873422
 },
 {
   "STT": 1771,
   "Name": "Phòng khám Chuyên khoa Ung Bướu",
   "address": "G2 khu đấu giá quyền sử dụng đất Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.965788,
   "Latitude": 105.794266
 },
 {
   "STT": 1772,
   "Name": "Y Tế Hapu - Phòng khám Nhi",
   "address": "Tầng 1 mặt sau tòa nhà 21T2 Hapulico, số 1 Nguyễn Huy Tưởng, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0010052,
   "Latitude": 105.8067453
 },
 {
   "STT": 1773,
   "Name": "Nha Khoa Giảng Võ",
   "address": "Số 193 Giảng Võ, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.025429,
   "Latitude": 105.822231
 },
 {
   "STT": 1774,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Hương Giang",
   "address": "Số 149 phố Láng Hạ, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0170062,
   "Latitude": 105.8150862
 },
 {
   "STT": 1775,
   "Name": "Nha Khoa Thẩm Mỹ Hùng Hải",
   "address": "Số 21 đường Nguyễn Khoái, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0135171,
   "Latitude": 105.8632131
 },
 {
   "STT": 1776,
   "Name": "Nha Khoa Sơn",
   "address": "Số 152 phố Kim Ngưu, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9963687,
   "Latitude": 105.8429555
 },
 {
   "STT": 1777,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh : Siêu Âm",
   "address": "Số nhà 54, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 1778,
   "Name": "Phòng khám Chuyên khoa Việt Anh",
   "address": "Số 393 đường Giải Phóng, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9946367,
   "Latitude": 105.8409095
 },
 {
   "STT": 1779,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 37A5 phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0371448,
   "Latitude": 105.8454154
 },
 {
   "STT": 1780,
   "Name": "Phòng khám Chuyên khoa Da Liễu Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Dịch Vụ Trường Giang  ",
   "address": "Số 169 Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0125534,
   "Latitude": 105.8499695
 },
 {
   "STT": 1781,
   "Name": "Nha Khoa Sơn Anh",
   "address": "Khu 1ha, thị trấn Trạm Trôi, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.071136,
   "Latitude": 105.705818
 },
 {
   "STT": 1782,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh : Siêu Âm",
   "address": "Số 389 Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1528175,
   "Latitude": 105.5052557
 },
 {
   "STT": 1783,
   "Name": "Phòng khám Chuyên khoa Nội Hòa Thuận",
   "address": "Số 49 Tùng Thiện, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1137113,
   "Latitude": 105.4955033
 },
 {
   "STT": 1784,
   "Name": "Phòng khám Đa Khoa Quân Dân Y Hà Nội",
   "address": "Xứ đồng Mài Lại, phường Quang Trung, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1347408,
   "Latitude": 105.5071413
 },
 {
   "STT": 1785,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Thu Hồng",
   "address": "Số 1 ngách 2 ngõ 628 Hoàng Hoa Thám, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0470359,
   "Latitude": 105.8092081
 },
 {
   "STT": 1786,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Tnhh Pamas Spa Và Phòng khám ",
   "address": "(Tầng 3) Số 37, phố Thợ Nhuộm, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0265712,
   "Latitude": 105.8450516
 },
 {
   "STT": 1787,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Thịnh Lợi",
   "address": "Số 416 đường Ngọc Lâm, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0499119,
   "Latitude": 105.8808414
 },
 {
   "STT": 1788,
   "Name": "Phòng khám Sản Phụ Khoa, Kế Hoạch Hóa Gia Đình 166",
   "address": "Số nhà 34 D5, tổ dân phố 5, khu giãn dânyên Phúc, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.970333,
   "Latitude": 105.7880497
 },
 {
   "STT": 1789,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ - Bác sĩ Thọ 108",
   "address": "Số 32 phố đường Thành( tầng 5), phường Cửa Đông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0337914,
   "Latitude": 105.8471712
 },
 {
   "STT": 1790,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 155 Tô Hiệu",
   "address": "Tầng 1, số nhà 155, phố Tô Hiệu, phường Nghĩa Đô, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.04201,
   "Latitude": 105.7907904
 },
 {
   "STT": 1791,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Hỗ Trợ Phát Triển Nha Khoa Beam",
   "address": "Số 124, phố xã Đàn, phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0112887,
   "Latitude": 105.8370696
 },
 {
   "STT": 1792,
   "Name": "Phòng khám Nha Khoa Sài Gòn H.N - Công Ty Tnhh Đầu Tư Và Phát Triển Nha Khoa Sài Gòn H.N",
   "address": "Số 208 Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0215387,
   "Latitude": 105.7913168
 },
 {
   "STT": 1793,
   "Name": "Nha Khoa Mỹ An",
   "address": "Số 66 phố Đại La, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.99648,
   "Latitude": 105.847994
 },
 {
   "STT": 1794,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Phòng A105, Tập thể nhà máy chế tạo biến thế, tổ 17, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9625316,
   "Latitude": 105.8399127
 },
 {
   "STT": 1795,
   "Name": "Phòng khám Sản Phụ Khoa Số 7",
   "address": "Số 931 (tầng 1), đường La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0237111,
   "Latitude": 105.8178428
 },
 {
   "STT": 1796,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Hoa Hồng",
   "address": "Số 28 tổ 45, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0006046,
   "Latitude": 105.8158204
 },
 {
   "STT": 1797,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Chi Nhánh Công Ty Cổ Phần Thẩm Mỹ Quốc Tế Dencos",
   "address": "135- 137 Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0139481,
   "Latitude": 105.8500612
 },
 {
   "STT": 1798,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Dược Phúc Thành",
   "address": "Số 522 đường Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0657541,
   "Latitude": 105.8981395
 },
 {
   "STT": 1799,
   "Name": "Phòng khám Đa Khoa Tư Nhân Việt Pháp 107",
   "address": "Số 107 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1800,
   "Name": "Cơ Sở Dịch Vụ Chăm Sóc Sức Khỏe Tại Nhà",
   "address": "Phòng 1317 chung cư B15 Đô thị Đại Kim, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9775627,
   "Latitude": 105.8224709
 },
 {
   "STT": 1801,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ An Phát",
   "address": "Tầng 1+2+3 số 189 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1802,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "58 Hoàng Như Tiếp, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.0437521,
   "Latitude": 105.875625
 },
 {
   "STT": 1803,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 107",
   "address": "Số 107 phố Tôn Đức Thắng, phường Hàng Bột, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0240845,
   "Latitude": 105.8326591
 },
 {
   "STT": 1804,
   "Name": "Nha Khoa Minh Tâm 3",
   "address": "Số 442 phố Bạch Đằng, phường Chương Dương, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0211662,
   "Latitude": 105.8628394
 },
 {
   "STT": 1805,
   "Name": "Nha Khoa Thẩm Mỹ 108",
   "address": "273 đường Nguyễn Văn Linh, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0395434,
   "Latitude": 105.898051
 },
 {
   "STT": 1806,
   "Name": "Nha Khoa Minh Tâm ",
   "address": "Số 51, phố Đỗ Quang, tổ 40, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0111175,
   "Latitude": 105.8019279
 },
 {
   "STT": 1807,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Thôn Vật Phụ, xã Vật Lại, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2026641,
   "Latitude": 105.4071259
 },
 {
   "STT": 1808,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - 50",
   "address": "Số 376 phố Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.009253,
   "Latitude": 105.8567869
 },
 {
   "STT": 1809,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội Dị Ứng, Miễn Dịch Lâm Sàng - Tâm Phúc",
   "address": "262 đường Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9871382,
   "Latitude": 105.8423441
 },
 {
   "STT": 1810,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 4 ngách 814/3 ngõ 814 đường Láng, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0186206,
   "Latitude": 105.8028528
 },
 {
   "STT": 1811,
   "Name": "Nha Khoa  Hoàn Mỹ",
   "address": "Số 39, phố Quang Trung, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.019928,
   "Latitude": 105.8479553
 },
 {
   "STT": 1812,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Thôn Nguyên Hanh, xã Văn Tự, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8055441,
   "Latitude": 105.8838579
 },
 {
   "STT": 1813,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 6, ngõ 22, đường Tân Xuân, phường Xuân Đỉnh, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0783421,
   "Latitude": 105.7867303
 },
 {
   "STT": 1814,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội Dị Ứng, Miễn Dịch Lâm Sàng - Tâm Phúc",
   "address": "262 đường Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9871382,
   "Latitude": 105.8423441
 },
 {
   "STT": 1815,
   "Name": "Phòng khám Chuyên khoa Nội Trần Đức",
   "address": "Tổ dân phố Thượng, phường Tây Tựu, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0798157,
   "Latitude": 105.7272321
 },
 {
   "STT": 1816,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Phương Viên ",
   "address": "Số 313, đường Hoàng Mai, tổ 45, phường Hoàng Văn Thụ, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.982471,
   "Latitude": 105.861447
 },
 {
   "STT": 1817,
   "Name": "Nha Khoa Minh Sang 108",
   "address": "Số 123, phố Đỗ Đức Dục, tổ dân phố Hạ, phường Mễ Trì, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0055267,
   "Latitude": 105.7791327
 },
 {
   "STT": 1818,
   "Name": "Nha Khoa Trần Cung 1",
   "address": "Số 39, phố Trần Cung, tổ dân phố Thôn Hoàng 4, phường Cổ Nhuế 1, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0583202,
   "Latitude": 105.7835931
 },
 {
   "STT": 1819,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 20, Nguyễn Khuyến, phường Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9779303,
   "Latitude": 105.7874084
 },
 {
   "STT": 1820,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 56 tòa nhà VP5 Bán đảo Linh Đàm, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9632051,
   "Latitude": 105.8315932
 },
 {
   "STT": 1821,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "A8 Tập thể số 8, phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0371448,
   "Latitude": 105.8454154
 },
 {
   "STT": 1822,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 236 đường Khương Đình, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9923873,
   "Latitude": 105.8134541
 },
 {
   "STT": 1823,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 26G ngõ 7, tổ 8A phố Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0129876,
   "Latitude": 105.8200121
 },
 {
   "STT": 1824,
   "Name": "Phòng khám Chuyên khoa Nhi Đông Dương",
   "address": "Số 2 nhà N6, Tập thể quân đội T17, Bộ tư lệnh Công Binh, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0396911,
   "Latitude": 105.8105001
 },
 {
   "STT": 1825,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Minh Ngọc",
   "address": "Thôn 4, xã Ninh Hiệp, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0762299,
   "Latitude": 105.9494248
 },
 {
   "STT": 1826,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Thôn Phú Thụy, xã Phú Thị, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0276351,
   "Latitude": 105.9729255
 },
 {
   "STT": 1827,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "P112 - B1 tập thể Nam Đồng, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0126594,
   "Latitude": 105.8324425
 },
 {
   "STT": 1828,
   "Name": " Phòng khám Nha Khoa Khánh Phương",
   "address": "Số 62 đường Yên Thường, Thôn Xuân Dục, xã Yên Thường, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0904124,
   "Latitude": 105.9052426
 },
 {
   "STT": 1829,
   "Name": "Phòng Xét Nghiệm Thuốc Việt",
   "address": "Số 93 Láng Hạ, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.015379,
   "Latitude": 105.814104
 },
 {
   "STT": 1830,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Tổ 2, cụm dân cư Bằng A, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9621208,
   "Latitude": 105.8205904
 },
 {
   "STT": 1831,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Số 128 Phùng Khắc Khoan, phường Quang Trung, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1330355,
   "Latitude": 105.5105417
 },
 {
   "STT": 1832,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả",
   "address": "Số 12 phố Phùng Hưng, phường Ngô Quyền, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1420039,
   "Latitude": 105.5032741
 },
 {
   "STT": 1833,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Tổ dân phố 4, phường La Khê, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9724114,
   "Latitude": 105.7615252
 },
 {
   "STT": 1834,
   "Name": "Phòng khám Chuyên khoa Mắt Hoàng Phượng",
   "address": "Thôn Cổ Chế, xã Phúc Tiến, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7269929,
   "Latitude": 105.9215215
 },
 {
   "STT": 1835,
   "Name": "Phòng khám Chuyên khoa Nội A16 Tân Triều",
   "address": "Số 14 khu tập thể cai nghiện, cụm II, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9941274,
   "Latitude": 105.7988754
 },
 {
   "STT": 1836,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Y Tế Hà Đông",
   "address": "Số 232 đường 70, Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9996534,
   "Latitude": 105.7513326
 },
 {
   "STT": 1837,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Siêu Âm Trực Thuộc Công Ty Tnhh Y Tế Hà Đông",
   "address": "Số 232 đường 70, Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9996534,
   "Latitude": 105.7513326
 },
 {
   "STT": 1838,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Cổ Phần Khám Chữa Bệnh Trẻ Em Nhi Cao",
   "address": "(Tầng 3) Số 32 Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0208337,
   "Latitude": 105.8004791
 },
 {
   "STT": 1839,
   "Name": "Phòng khám Nhi Cao Trực Thuộc Công Ty Cổ Phần Khám Chữa Bệnh Trẻ Em Nhi Cao",
   "address": "(Tầng 2) Số 32 Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0208337,
   "Latitude": 105.8004791
 },
 {
   "STT": 1840,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Liên Doanh Khách Sạn Thống Nhất Metropole",
   "address": "15 Ngô Quyền, phường Tràng Tiền, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.02559,
   "Latitude": 105.8564147
 },
 {
   "STT": 1841,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Khu tập thể bệnh viện thị trấn TW, xã Hòa Bình, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.869719,
   "Latitude": 105.8438506
 },
 {
   "STT": 1842,
   "Name": "Nha Khoa Thúy Anh",
   "address": "Số 27, ngõ 461, đường Nguyễn Văn Linh, tổ 15, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.03485,
   "Latitude": 105.9070436
 },
 {
   "STT": 1843,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Sài Gòn",
   "address": "P4 - 162 Tôn Đức Thắng, phường Hàng Bột, quận Đống Đa, Hà Nội",
   "Longtitude": 21.024376,
   "Latitude": 105.832655
 },
 {
   "STT": 1844,
   "Name": "Phòng khám Đa Khoa Elise Trực Thuộc Công Ty Cổ Phần Y Tế Phú Hưng",
   "address": "Khu đất 3A dự án KTKĐ 62, đường Trường Chinh, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.997838,
   "Latitude": 105.840966
 },
 {
   "STT": 1845,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Tổ dân phố Hoàng Hanh, phường Dương Nội, quận Hà Đông, Hà Nội",
   "Longtitude": 20.979202,
   "Latitude": 105.747162
 },
 {
   "STT": 1846,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Thôn Xuân Sơn, xã Trung Giã, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.3030059,
   "Latitude": 105.8657274
 },
 {
   "STT": 1847,
   "Name": "Nha Khoa Đa Phúc",
   "address": "Thôn Vệ Linh, xã Phù Linh, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2716042,
   "Latitude": 105.8532494
 },
 {
   "STT": 1848,
   "Name": "Nha Khoa Bình Minh",
   "address": "Số 54 Nguyễn Phúc Lai, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0203887,
   "Latitude": 105.8202159
 },
 {
   "STT": 1849,
   "Name": "Nha Khoa Châu Á Thái Bình Dương",
   "address": "Tầng 1 số 106, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0232671,
   "Latitude": 105.7946587
 },
 {
   "STT": 1850,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 310, phố Đội Cấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0378608,
   "Latitude": 105.8106352
 },
 {
   "STT": 1851,
   "Name": "Nha Khoa Thái Hưng",
   "address": "Số 120A, đường Nguyễn An Ninh, phường Tương Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9892555,
   "Latitude": 105.8458991
 },
 {
   "STT": 1852,
   "Name": "Nha Khoa Minh Khanh",
   "address": "Số 205 Tựu Liệt, xã Tam Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.953048,
   "Latitude": 105.8354051
 },
 {
   "STT": 1853,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 76 phố Thợ Nhuộm, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0246713,
   "Latitude": 105.8464358
 },
 {
   "STT": 1854,
   "Name": "Nha Khoa Khánh Linh",
   "address": "Khu 5, phố Tô Hiệu, Hà Trì, phường Hà Cầu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9666271,
   "Latitude": 105.7773263
 },
 {
   "STT": 1855,
   "Name": "Phòng khám Bác sĩ Gia Đình Việt Úc Trực Thuộc Công Ty Tnhh Dịch Vụ Y Tế Và Chăm Sóc  Tại Nhà Việt Úc",
   "address": "Lô B6, tầng 1 (tòa B), Khu N03, KĐT Đông Nam Trần Duy Hưng (Mandarin Garden), đường Hoàng Minh Giám, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0048114,
   "Latitude": 105.7987278
 },
 {
   "STT": 1856,
   "Name": "Phòng khám Chuyên khoa Tâm Thần",
   "address": "Khu tập thể bệnh viện tâm thần TW, xã Hòa Bình, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8686566,
   "Latitude": 105.8425534
 },
 {
   "STT": 1857,
   "Name": "Phòng khám Chuyên khoa Nội A11",
   "address": "Số nhà 22, ngách 3, ngõ 58, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0346389,
   "Latitude": 105.7785927
 },
 {
   "STT": 1858,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Dịch Vụ Trường Giang",
   "address": "Số 169 Bùi Thị Xuân (tầng 9), phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0125534,
   "Latitude": 105.8499695
 },
 {
   "STT": 1859,
   "Name": "Phòng khám Sản Phụ Khoa An Hòa",
   "address": "Số 89C, dốc Bệnh viện Phụ sản, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0268832,
   "Latitude": 105.8091706
 },
 {
   "STT": 1860,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Số 1 ",
   "address": "Số 85b (tầng 1) đường La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0237111,
   "Latitude": 105.8178428
 },
 {
   "STT": 1861,
   "Name": "Phòng khám Chuyên khoa Phụ Sản  ",
   "address": "1/2 P102, tòa nhà Văn phòng làm việc và nhà ở - số 143 phố Đốc Ngữ, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.038813,
   "Latitude": 105.812859
 },
 {
   "STT": 1862,
   "Name": "Phòng khám Sản Phụ Khoa  ",
   "address": "Số 32 Định Công Thượng, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9820079,
   "Latitude": 105.8204757
 },
 {
   "STT": 1863,
   "Name": "Phòng khám Bác sĩ Gia Đình",
   "address": "Số 36, ngách 99/133, tổ 31, phường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0588582,
   "Latitude": 105.8583873
 },
 {
   "STT": 1864,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 82, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0358609,
   "Latitude": 105.7785371
 },
 {
   "STT": 1865,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng 198",
   "address": "Số nhà 82, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 1866,
   "Name": "Phòng khám Nội Xương Khớp Hồng Hoa",
   "address": "Số 50, đường Xuân Đỉnh, tổ dân phố Cáo Đỉnh, phường Xuân Đỉnh, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0728488,
   "Latitude": 105.7912388
 },
 {
   "STT": 1867,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm Trực Thuộc Công Ty Cổ Phần Khám Chữa Bệnh Trẻ Em Nhi Cao",
   "address": "(Tầng 3) Số 32 Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0208337,
   "Latitude": 105.8004791
 },
 {
   "STT": 1868,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm ",
   "address": "Thôn Yên, xã Thạch Xá, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0311743,
   "Latitude": 105.594449
 },
 {
   "STT": 1869,
   "Name": "Nha Khoa Minh Tỉnh",
   "address": "Số 81, phố Vân Trì, xã Vân Nội, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1513657,
   "Latitude": 105.8169083
 },
 {
   "STT": 1870,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Căn hộ A2 - Nhà 32, phố Sơn Tây, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0329591,
   "Latitude": 105.8313638
 },
 {
   "STT": 1871,
   "Name": "Nha Khoa Khánh Linh",
   "address": "Khu 5, phố Tô Hiệu, Hà Trì, phường Hà Cầu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9666271,
   "Latitude": 105.7773263
 },
 {
   "STT": 1872,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Thân Thiện",
   "address": "Số 47B, ngõ 2 Đại Lộ Thăng Long, tổ dân phố Thượng, phường Mễ Trì, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0032356,
   "Latitude": 105.7785866
 },
 {
   "STT": 1873,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Phòng 1102, CT5, Đơn Nguyên II, Khu đô thị mới Mỹ Đình II, phường Mỹ Đình 2, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0312482,
   "Latitude": 105.7694511
 },
 {
   "STT": 1874,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Trực Thuộc Công Ty Cổ Phần Chăm Sóc Sức Khỏe Việt",
   "address": "Số 32 phố Đại Từ, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9716181,
   "Latitude": 105.8353485
 },
 {
   "STT": 1875,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Tnhh Tư Vấn Và Đầu Tư Y Tế Quốc Tế",
   "address": "Số 126 Bùi Thị Xuân, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0130655,
   "Latitude": 105.8498259
 },
 {
   "STT": 1876,
   "Name": "Phòng khám Sản Phụ Khoa Chúc An",
   "address": "Lô E4, Bồ Hỏa, phường Hà Cầu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9636429,
   "Latitude": 105.7776653
 },
 {
   "STT": 1877,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm Trực Thuộc Công Ty Tnhh Y Tế Hà Đông",
   "address": "Số 232 đường 70, Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9996534,
   "Latitude": 105.7513326
 },
 {
   "STT": 1878,
   "Name": "Phòng khám Đa Khoa Dr.Binh Teleclinic - Chi Nhánh Công Ty Tnhh Giải Pháp E2E",
   "address": "Tầng 1 đến tầng 4, số 11-13-15 phố Trần Xuân Soạn, phường Ngô Thì Nhậm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0169506,
   "Latitude": 105.8540824
 },
 {
   "STT": 1879,
   "Name": "Phòng khám Đa Khoa Hải Anh",
   "address": "Khu chợ Tó, xã Uy Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1431669,
   "Latitude": 105.8554513
 },
 {
   "STT": 1880,
   "Name": "Phòng khám Chuyên khoa Phụ Sản An Sinh",
   "address": "Số 65 (tầng 1) phố Đốc Ngữ, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.032168,
   "Latitude": 105.812427
 },
 {
   "STT": 1881,
   "Name": "Phòng khám Chuyên khoa Nội Hiền Nhiên",
   "address": "Tòa nhà No 10A khu đô thị Sài Đồng, phường Phúc Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0394802,
   "Latitude": 105.9111499
 },
 {
   "STT": 1882,
   "Name": "Phòng khám Ngoại Khoa Hoàng Tuấn",
   "address": "Nhà số 5 (tầng 1, tầng 2), khu tập thể Trung tâm Nhiệt đới Việt Nga, phường Cổ Nhuế 1, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0444957,
   "Latitude": 105.7986087
 },
 {
   "STT": 1883,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Y Khoa Thủ Đô",
   "address": "Lô 07 - 3A, cụm thị trấn CN Hai Bà Trưng, phường Hoàng Văn Thụ, quận Hoàng Mai, Hà Nội",
   "Longtitude": 21.0251735,
   "Latitude": 105.8496706
 },
 {
   "STT": 1884,
   "Name": "Phòng khám Chuyên khoa Sản Phụ Khoa Việt Hoa",
   "address": "Cửa hàng số 01, số 25A phố Hạ Đình, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9895016,
   "Latitude": 105.8079602
 },
 {
   "STT": 1885,
   "Name": "Phòng khám Chuyên khoa Điều Dưỡng, Phục Hồi Chức Năng Và Vật Lý Trị Liệu - Công Ty Tnhh Phát Triển Sức Khỏe Bền Vững Viethealth",
   "address": "Số 16, lô 13B đường Trung Yên 11, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.015765,
   "Latitude": 105.8001355
 },
 {
   "STT": 1886,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm",
   "address": "128A Nguyễn Sơn, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0471594,
   "Latitude": 105.8775462
 },
 {
   "STT": 1887,
   "Name": "Phòng khám Đa Khoa Y Cao Thiện Đức",
   "address": "Tổ 1, Thôn Miếu Thờ, xã Tiên Dược, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2443458,
   "Latitude": 105.8502999
 },
 {
   "STT": 1888,
   "Name": "Phòng khám Chuyên khoa Dinh Dưỡng - Tiết Chế Trực Thuộc Công Ty Tnhh Mednufood Care Hà Nội",
   "address": "Số 15, ngõ 30, phố Phan Đình Giót, phường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0111187,
   "Latitude": 105.8663728
 },
 {
   "STT": 1889,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Vệt Hàn",
   "address": "Tầng 1-2, tòa nhà 21T1, số 1 Nguyễn Huy Tưởng, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0004109,
   "Latitude": 105.8071962
 },
 {
   "STT": 1890,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Trung Anh",
   "address": "Tổ 8, khu Tân Bình, thị trấn Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8964524,
   "Latitude": 105.5818066
 },
 {
   "STT": 1891,
   "Name": "Nha Khoa Bảo Minh",
   "address": "Số 163, phố Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0155762,
   "Latitude": 105.7944906
 },
 {
   "STT": 1892,
   "Name": "Phòng khám Nha Khoa Gia Đình",
   "address": "Số 08, phố Trần Thái Tông, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.031806,
   "Latitude": 105.7884853
 },
 {
   "STT": 1893,
   "Name": "Cơ Sở Dịch Vụ Làm Răng Giả Phong Vinh",
   "address": "Số 32, đường Quang Lãm, tổ dân phố 4, phường Phú Lãm, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9426712,
   "Latitude": 105.7541892
 },
 {
   "STT": 1894,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Đội 8, xã Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9182821,
   "Latitude": 105.843708
 },
 {
   "STT": 1895,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Thôn Cổ Điển, xã Hải Bối, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1095432,
   "Latitude": 105.7901382
 },
 {
   "STT": 1896,
   "Name": "Cơ Sở Dịch Vụ Y Tế Kết Liên",
   "address": "Tiểu khu Phú Gia, thị trấn Phú Minh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7825722,
   "Latitude": 105.9156477
 },
 {
   "STT": 1897,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 14 phố Chùa Thông, phường Sơn Lộc, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1257544,
   "Latitude": 105.4995571
 },
 {
   "STT": 1898,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Cổ Phần Tập Đoàn Y Dược Phương Anh",
   "address": "Vị trí 19, Ki ốt nhà CT5, lô M, Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9691213,
   "Latitude": 105.795275
 },
 {
   "STT": 1899,
   "Name": "Phòng khám Chuyên khoa Nhi Thúy Nga",
   "address": "Số 13A phố Đinh Tiên Hoàng, phường Ngô Quyền, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1429693,
   "Latitude": 105.5032888
 },
 {
   "STT": 1900,
   "Name": "Phòng khám Chuyên khoa Nhi ",
   "address": "Cụm 1, xã Phụng Thượng, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.0933272,
   "Latitude": 105.5855438
 },
 {
   "STT": 1901,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 158, đường Chiến Thắng, phường Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9796427,
   "Latitude": 105.7958776
 },
 {
   "STT": 1902,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Nguyên Khoa",
   "address": "Ki ốt số 44, tầng 1 tòa nhà chung cư VP5 - Khu dịch vụ tổng hợp và nhà ở Hồ Linh Đàm, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9632051,
   "Latitude": 105.8315932
 },
 {
   "STT": 1903,
   "Name": "Phòng khám Chuyên khoa Tâm Thần",
   "address": ", xã Hòa Bình, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.878276,
   "Latitude": 105.8378366
 },
 {
   "STT": 1904,
   "Name": "Phòng khám Chuyên khoa Da Liễu Thành Công",
   "address": "Số 14 Vũ Thạnh ( số cũ: A3 khu Hào Nam), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0263112,
   "Latitude": 105.8269951
 },
 {
   "STT": 1905,
   "Name": "Phòng khám Chuyên khoa Da Liễu Bích Nguyệt",
   "address": "Số 57, phố Nguyễn Công Hoan, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0271686,
   "Latitude": 105.813489
 },
 {
   "STT": 1906,
   "Name": "Phòng khám Chuyên khoa Mắt Đỗ Duy Hòa",
   "address": "Ô A1 lô LK20 khu nhà ở Phú Thịnh, phường Phú Thịnh, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1537625,
   "Latitude": 105.4976181
 },
 {
   "STT": 1907,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Thôn Đại Đồng, xã Đại Mạch, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.124049,
   "Latitude": 105.7558315
 },
 {
   "STT": 1908,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số nhà 341, đường Thạch Bàn, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0231982,
   "Latitude": 105.906053
 },
 {
   "STT": 1909,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Siêu Âm Sản Phụ Khoa",
   "address": "Số 9 ngõ 198 xã Đàn (cũ: số 2A2 Tập thể điện lực tổ 1B), phường Phương Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0118591,
   "Latitude": 105.8381362
 },
 {
   "STT": 1910,
   "Name": "Phòng khám Sản Phụ Khoa Bảo Sinh",
   "address": "Số nhà 128 phố Thạch Bàn, tổ 3, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0231982,
   "Latitude": 105.906053
 },
 {
   "STT": 1911,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Xóm 9, Thôn 6, xã Song Phương, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0138269,
   "Latitude": 105.6911121
 },
 {
   "STT": 1912,
   "Name": "Nha Khoa Số 4 Tôn Đức Thắng",
   "address": "Số 4 Tôn Đức Thắng, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.029995,
   "Latitude": 105.83569
 },
 {
   "STT": 1913,
   "Name": "Phòng khám Đa Khoa 103",
   "address": "Tổ 3, thị trấn Quang Minh, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1915793,
   "Latitude": 105.7732633
 },
 {
   "STT": 1914,
   "Name": " Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 182 phố Nguyễn Sơn, phường Bồ Đề, quận Long Biên, Hà Nội",
   "Longtitude": 21.045722,
   "Latitude": 105.880347
 },
 {
   "STT": 1915,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Medi Long Biên    ",
   "address": "Số 32, phố Việt Hưng, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0551345,
   "Latitude": 105.9014098
 },
 {
   "STT": 1916,
   "Name": "Phòng khám Chuyên khoa Da Liễu Trực Thuộc Công Ty Tnhh Y Học Công Nghệ Cao Vệ Nữ",
   "address": "430 xã Đàn, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0158014,
   "Latitude": 105.8328126
 },
 {
   "STT": 1917,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Việt Nhật",
   "address": "Thôn Đoài, xã Kim Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1371732,
   "Latitude": 105.7967419
 },
 {
   "STT": 1918,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Chăm Sóc Sức Khỏe Cộng Đồng Thiên Lý",
   "address": "Số 639 đường Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 1919,
   "Name": "Phòng khám Chuyên khoa Nội Ánh Tuyết",
   "address": "Tổ 1, Miếu Thờ, xã Tiên Dược, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2443458,
   "Latitude": 105.8502999
 },
 {
   "STT": 1920,
   "Name": "Nha Khoa Việt Sing",
   "address": "Số 247 phố Trần Đại Nghĩa, phường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995172,
   "Latitude": 105.8453928
 },
 {
   "STT": 1921,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà Tín",
   "address": "Số 99 phố Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0664009,
   "Latitude": 105.8828458
 },
 {
   "STT": 1922,
   "Name": "Nha Khoa Trịnh",
   "address": "Số 521 đường Thụy Khuê, tổ 25 cụm 5, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.047891,
   "Latitude": 105.810102
 },
 {
   "STT": 1923,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng Đức Quân",
   "address": "Số 41, ngõ 62, phố Trần Bình, tập thể Học viện Biên phòng, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0322758,
   "Latitude": 105.7774913
 },
 {
   "STT": 1924,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 85 phố Định Công Thượng, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9809888,
   "Latitude": 105.8230378
 },
 {
   "STT": 1925,
   "Name": "Phòng khám Tư Nhân Chuyên khoa Mắt",
   "address": "Số 113 Trường Lâm, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0625253,
   "Latitude": 105.8993947
 },
 {
   "STT": 1926,
   "Name": "Nha Khoa Thiên An",
   "address": "Số 134 đường Nguyễn Trãi, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 1927,
   "Name": "Phòng khám Sản Phụ Khoa Và Siêu Âm ",
   "address": "Số 81+83 phố Lò Đúc, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0143594,
   "Latitude": 105.8572806
 },
 {
   "STT": 1928,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng ",
   "address": "Số 649 H5 Tân Mai, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9833913,
   "Latitude": 105.8514398
 },
 {
   "STT": 1929,
   "Name": "Phòng khám Chuyên khoa Nội Thanh Tùng",
   "address": "Thôn Quang Trung, xã Dương Quang, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0049289,
   "Latitude": 105.983208
 },
 {
   "STT": 1930,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Phòng khám Quốc Tế Tokyo",
   "address": "Tầng 10 - tòa nhà Hanoi Tourist, số 18 phố Lý Thường Kiệt, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0188815,
   "Latitude": 105.8539688
 },
 {
   "STT": 1931,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm, X Quang",
   "address": "Phú Nhi 3, phường Phú Thịnh, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.151162,
   "Latitude": 105.4927464
 },
 {
   "STT": 1932,
   "Name": "Phòng khám Chuyên khoa Nội Medi Long Biên",
   "address": "Số 32, phố Việt Hưng, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0551345,
   "Latitude": 105.9014098
 },
 {
   "STT": 1933,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 226 phố Định Công Hạ, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9808944,
   "Latitude": 105.8326992
 },
 {
   "STT": 1934,
   "Name": "Phòng khám Nha Khoa An Phú",
   "address": "Xóm 8, xã Ninh Hiệp, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0781908,
   "Latitude": 105.9409153
 },
 {
   "STT": 1935,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 93 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 1936,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm",
   "address": "936 Trương Định, phường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.977155,
   "Latitude": 105.841717
 },
 {
   "STT": 1937,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Trực Thuộc Công Ty Cổ Phần Chăm Sóc Sức Khỏe Cộng Đồng Thiên Lý",
   "address": "Số 639 đường Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 1938,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc",
   "address": "Số 46C phố Ngọc Hà, phường Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.03548,
   "Latitude": 105.831317
 },
 {
   "STT": 1939,
   "Name": "Phòng khám Đa Khoa Y Cao Hà Nội",
   "address": ", xã Hà Hồi, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8628013,
   "Latitude": 105.8789403
 },
 {
   "STT": 1940,
   "Name": "Phòng khám Đa Khoa Y Cao Hà Nội",
   "address": " Thôn Nội Hợp, xã Nam Phong, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7540878,
   "Latitude": 105.9259271
 },
 {
   "STT": 1941,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - 1958",
   "address": "Số 1 phố Lê Đại Hành, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0088968,
   "Latitude": 105.8507623
 },
 {
   "STT": 1942,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt - 61",
   "address": "Số 30 phố Đại Cồ Việt, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0100881,
   "Latitude": 105.8496585
 },
 {
   "STT": 1943,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 97, phố Nguyễn Thị Định, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0091593,
   "Latitude": 105.8047739
 },
 {
   "STT": 1944,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 4, Lô B1 khu đô thị Đại Kim, phường Đại Kim, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9759056,
   "Latitude": 105.8357778
 },
 {
   "STT": 1945,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 187 phố Đặng Tiến Đông, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0145071,
   "Latitude": 105.8213786
 },
 {
   "STT": 1946,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 126, phố Tân Mai, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.983624,
   "Latitude": 105.849442
 },
 {
   "STT": 1947,
   "Name": "Nha Khoa Hoàn Mỹ",
   "address": "Số 62 Hoàng Văn Thái, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9963419,
   "Latitude": 105.827333
 },
 {
   "STT": 1948,
   "Name": "Nha Khoa Việt Úc",
   "address": "Số 9/630 Trường Chinh, phường Ngã Tư Sở, quận Đống Đa, Hà Nội",
   "Longtitude": 21.00275,
   "Latitude": 105.821299
 },
 {
   "STT": 1949,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Thu Hà",
   "address": "Số 134 phố Bà Triệu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0184469,
   "Latitude": 105.8492925
 },
 {
   "STT": 1950,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Chi Nhánh Công Ty Tnhh Đỉnh Vàng",
   "address": "Phòng E106, Golden Westlake, số 151 Thụy Khuê, phường Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0421911,
   "Latitude": 105.8214638
 },
 {
   "STT": 1951,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Cổ Phần Dược Phẩm Bạch Mã Vạn Xuân",
   "address": "Số 127, phố Nguyễn Khoái, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0135171,
   "Latitude": 105.8632131
 },
 {
   "STT": 1952,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Nhà B4 khu dự án, ngõ 87 đường Tam Trinh, phường Mai Động, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.995187,
   "Latitude": 105.863311
 },
 {
   "STT": 1953,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Nha Khoa Hà Nội",
   "address": "Số 167 đường bờ sông Quan Hoa, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0353351,
   "Latitude": 105.8048415
 },
 {
   "STT": 1954,
   "Name": "Nha Khoa Phú Gia",
   "address": "24 phố Phú Gia, phường Phú Thượng, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0838905,
   "Latitude": 105.8084823
 },
 {
   "STT": 1955,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 46, phố Quán Sứ, phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 1956,
   "Name": "Phòng khám Chuyên khoa Ngoại Hòa Bình",
   "address": "Số 29 ngõ 18 phố Nguyễn Đình Chiểu (số 23 ngõ 14 Vân Hồ 1), phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.014529,
   "Latitude": 105.8470025
 },
 {
   "STT": 1957,
   "Name": "Thẩm Mỹ Viện S.L.I.N.E",
   "address": "Số 273 phố Tôn Đức Thắng, phường Hàng Bột, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0203371,
   "Latitude": 105.8310594
 },
 {
   "STT": 1958,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Nhà C1, phố Trung Kính, tổ 43, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0130095,
   "Latitude": 105.7999183
 },
 {
   "STT": 1959,
   "Name": "Phòng khám Chuyên khoa Nhi Việt Chi",
   "address": "Số 6 ngõ 10A đường Nguyễn Thị Định, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.006775,
   "Latitude": 105.8060035
 },
 {
   "STT": 1960,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 33 ngõ 231 Chùa Bộc, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0084059,
   "Latitude": 105.8260003
 },
 {
   "STT": 1961,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Số 49A ngõ Thái Hà, phố Thái Hà, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0149357,
   "Latitude": 105.8154645
 },
 {
   "STT": 1962,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Thái Hàn",
   "address": "Số 48A phố Tăng Bạt Hổ, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0140065,
   "Latitude": 105.8591348
 },
 {
   "STT": 1963,
   "Name": "Phòng khám Sản Phụ Khoa Tây Hồ",
   "address": "Số 438 đường Âu Cơ, phường Nhật Tân, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.078145,
   "Latitude": 105.820297
 },
 {
   "STT": 1964,
   "Name": "Phòng khám Sản Phụ Khoa Xuân Hương",
   "address": "Số 115+117 ngõ 283 đường Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0090402,
   "Latitude": 105.8592917
 },
 {
   "STT": 1965,
   "Name": "Phòng khám Sản Phụ Khoa Toàn Khánh",
   "address": "Số 933(tầng 1) đường La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0237111,
   "Latitude": 105.8178428
 },
 {
   "STT": 1966,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Khhgđ - Siêu Âm Sản Phụ Khoa",
   "address": "Số 18, tổ 11, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0509914,
   "Latitude": 105.8857844
 },
 {
   "STT": 1967,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 6 ngõ 10A phố Nguyễn Thị Định, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.006775,
   "Latitude": 105.8060035
 },
 {
   "STT": 1968,
   "Name": "Nha Khoa Hk",
   "address": "Số 8 ngõ 180 Thái Thịnh, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0130661,
   "Latitude": 105.8162772
 },
 {
   "STT": 1969,
   "Name": "Nha Khoa Như Khang",
   "address": "Số 51(tầng 1), ngách 6/9, phố Vĩnh Phúc, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0444487,
   "Latitude": 105.8102781
 },
 {
   "STT": 1970,
   "Name": "Phòng khám Chuyên khoa Da Liễu 107",
   "address": "Số 107- E8 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0026692,
   "Latitude": 105.8356464
 },
 {
   "STT": 1971,
   "Name": "Phòng khám Chuyên khoa Da Liễu Nam Việt",
   "address": "P105 - E8 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.003318,
   "Latitude": 105.8364789
 },
 {
   "STT": 1972,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "CL2- Lô A, khu đô thị Nam La Khê/368B, phố Quang Trung, phường La Khê, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9746348,
   "Latitude": 105.7583488
 },
 {
   "STT": 1973,
   "Name": "Phòng khám Đa Khoa K - Hướng Dương Trực Thuộc Công Ty Tnhh Công Nghệ Y Tế Toàn Cầu",
   "address": "Số 9 ngõ 83 đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0219433,
   "Latitude": 105.7978115
 },
 {
   "STT": 1974,
   "Name": "Phòng khám Chuyên khoa Dinh Dưỡng Cộng Đồng Trực Thuộc Công Ty Tnhh Viện Dinh Dưỡng Cộng Đồng",
   "address": "Ô đất số 10, lô 1B, phố Vũ Phạm Hàm, KĐT mới Trung Yên, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.015326,
   "Latitude": 105.79746
 },
 {
   "STT": 1975,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 3T1 dãy 2, tập thể K95 Tôn Thất Thiệp (số mới: Nhà N3, Ngách 34A/2 phố Trần Phú), phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0316018,
   "Latitude": 105.8431121
 },
 {
   "STT": 1976,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Tầng 1, dự án tổ hợp văn phòng và nhà ở cao tầng CT3, lô đất CT3, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.016174,
   "Latitude": 105.7825403
 },
 {
   "STT": 1977,
   "Name": "Phòng khám Chuyên khoa Nội Thần Kinh",
   "address": "Số 5, nhà A1, khu X2, phố Trung Kính, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0039519,
   "Latitude": 105.8421025
 },
 {
   "STT": 1978,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 19 phố Mai Hắc Đế, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016191,
   "Latitude": 105.8509289
 },
 {
   "STT": 1979,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Vạn Vạn Phúc",
   "address": "Số 16, Lô A, Vạn Phúc, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.033212,
   "Latitude": 105.8169693
 },
 {
   "STT": 1980,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Tầng 1, dự án tổ hợp văn phòng và nhà ở cao tầng CT3, tòa nhà C'Land Handico, đường Lê Đức Thọ, phường Mỹ Đình 2, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0253886,
   "Latitude": 105.772714
 },
 {
   "STT": 1981,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Thanh Ngọc",
   "address": "Số 26 phố Phan Phù Tiên, phường Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0283429,
   "Latitude": 105.8338303
 },
 {
   "STT": 1982,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "B8, ngõ 20 Ngô Quyền, phường Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9755625,
   "Latitude": 105.7758739
 },
 {
   "STT": 1983,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 43, ngõ 420, đường Hà Huy Tập, thị trấn Yên Viên, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0858175,
   "Latitude": 105.9177869
 },
 {
   "STT": 1984,
   "Name": "Phòng khám Chuyên khoa Tâm Thần",
   "address": "Số 37 Trần Phú, thị trấn Thường Tín, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8677926,
   "Latitude": 105.8617189
 },
 {
   "STT": 1985,
   "Name": "Phòng khám Chuyên khoa Nội Nhân Mã",
   "address": "Số 33, phố Lê Hồng Phong, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.034178,
   "Latitude": 105.833505
 },
 {
   "STT": 1986,
   "Name": "Phòng khám Chuyên khoa Nội - Vân Lan",
   "address": "Số 79 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 1987,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Đoàn Thùy",
   "address": "Thôn Cổ Điển, xã Hải Bối, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1095432,
   "Latitude": 105.7901382
 },
 {
   "STT": 1988,
   "Name": "Phòng khám Chuyên khoa Nội Thần Kinh Bác sĩ Thanh Bình",
   "address": "Số 6 ngách 1, ngõ 46 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0012748,
   "Latitude": 105.8393044
 },
 {
   "STT": 1989,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 4, ngõ 53 đường Nguyễn Ngọc Vũ, tổ 32, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.01253,
   "Latitude": 105.8073272
 },
 {
   "STT": 1990,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Ki ốt số 9, tầng 1 khu chung cư Green House khu đô thị Việt Hưng, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.057669,
   "Latitude": 105.906358
 },
 {
   "STT": 1991,
   "Name": "Phòng khám Sản Phụ Khoa An Bình",
   "address": "Số 5 dãy B6 tập thể khí tượng Thủy văn - 40/62 phố Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0214823,
   "Latitude": 105.8075314
 },
 {
   "STT": 1992,
   "Name": "Nha Khoa Bạch Mai Ii",
   "address": "Số 13 (1/2 tầng 1 và toàn bộ tầng 2), phố Nguyễn Công Hoan, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.027214,
   "Latitude": 105.814882
 },
 {
   "STT": 1993,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Medi Long Biên",
   "address": "Số 32, phố Việt Hưng, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0551345,
   "Latitude": 105.9014098
 },
 {
   "STT": 1994,
   "Name": "Thẩm Mỹ Y Khoa Bác sĩ Hải Lê",
   "address": "Số 36 phố Đỗ Quang, tổ 41, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0111175,
   "Latitude": 105.8019279
 },
 {
   "STT": 1995,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Hóa Sinh Trực Thuộc Công Ty Tnhh Y Khoa Bạch Mai",
   "address": "Tiểu khu Thao Chính, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.74565,
   "Latitude": 105.911
 },
 {
   "STT": 1996,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 402 đường Hồ Tùng Mậu, tổ 11, phường Phú Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0367229,
   "Latitude": 105.7788626
 },
 {
   "STT": 1997,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 135 đường Cầu Diễn, phường Phúc Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0455583,
   "Latitude": 105.7505117
 },
 {
   "STT": 1998,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Cụm 8, thị trấn Phúc Thọ, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1032387,
   "Latitude": 105.5441093
 },
 {
   "STT": 1999,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 19, đường 2, xã Phù Lỗ, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.4047837,
   "Latitude": 105.3137341
 },
 {
   "STT": 2000,
   "Name": "Phòng khám Sản Phụ Khoa Hà Nội",
   "address": "Số 61, phố Quan Hoa, tổ 3, phường Quan Hoa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0367702,
   "Latitude": 105.8011445
 },
 {
   "STT": 2001,
   "Name": "Phòng khám Sản Phụ Khoa Mẹ Và Bé",
   "address": "Căn hộ 01 - Nhà E tập thể quân đội số 8 phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0371448,
   "Latitude": 105.8454154
 },
 {
   "STT": 2002,
   "Name": "Phòng khám Chuyên Sản Phụ Khoa - Kế Hoạch Hóa Gia Đình",
   "address": "Số 79 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 2003,
   "Name": "Nha Khoa Quốc Tế Hà Nội",
   "address": "Số 82, phố Tân Mai, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.983656,
   "Latitude": 105.848416
 },
 {
   "STT": 2004,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 22 đường Giải Phóng, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021881,
   "Latitude": 105.8412069
 },
 {
   "STT": 2005,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Hà Thành",
   "address": "Số 22 ngõ 174 phố Kim Ngưu, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0043966,
   "Latitude": 105.8611706
 },
 {
   "STT": 2006,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm",
   "address": "Số 12, ngách 29/72 phố Khương Hạ, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.997399,
   "Latitude": 105.818164
 },
 {
   "STT": 2007,
   "Name": "Phòng khám Chuyên khoa Nội Hùng Cường 108",
   "address": "Số 824 đường Bạch Đằng, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0104672,
   "Latitude": 105.8676042
 },
 {
   "STT": 2008,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Xóm Minh Đức, Thôn Đa Phúc, xã Sài Sơn, huyện Quốc Oai, Hà Nội",
   "Longtitude": 21.0325431,
   "Latitude": 105.6584412
 },
 {
   "STT": 2009,
   "Name": "Phòng khám Chuyên khoa Da Liễu Hoàng Tuấn",
   "address": "Nhà số 5 (tầng 3), khu tập thể Trung tâm Nhiệt đới Việt Nga, phường Cổ Nhuế 1, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0444957,
   "Latitude": 105.7986087
 },
 {
   "STT": 2010,
   "Name": "Phòng khám Đa Khoa Thái Hà Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Thiên Tâm",
   "address": "Số 11 Thái Hà, phường Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0104807,
   "Latitude": 105.822221
 },
 {
   "STT": 2011,
   "Name": "Phòng khám Chuyên khoa Nội An Bình",
   "address": "Số 181A đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0017135,
   "Latitude": 105.8417026
 },
 {
   "STT": 2012,
   "Name": "Phòng khám Chuyên khoa Tâm Thần Ngọc Minh",
   "address": "Số 3 ngách 5 ngõ 259 phố Vọng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995376,
   "Latitude": 105.842856
 },
 {
   "STT": 2013,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 26 dãy B1 khu Đầm Trấu, phường Bạch Đằng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0118555,
   "Latitude": 105.8651409
 },
 {
   "STT": 2014,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng My Anh",
   "address": "Số 362C phố xã Đàn (cũ: số 97 Đê La Thành), phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.017077,
   "Latitude": 105.831588
 },
 {
   "STT": 2015,
   "Name": "Phòng khám Sản Phụ Khoa Phương Mai",
   "address": "Số 102 phố Nguyễn Du, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.020536,
   "Latitude": 105.842096
 },
 {
   "STT": 2016,
   "Name": "Phòng khám Chuyên khoa Mắt Trực Thuộc Công Ty Tnhh Khải Nam",
   "address": "Số 477 Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0495151,
   "Latitude": 105.8767613
 },
 {
   "STT": 2017,
   "Name": "Phòng khám Nha Khoa Quốc Tế Hà Nội Seoul Trực Thuộc Công Ty Tnhh Nha Khoa Thiên Phúc",
   "address": "Số 95 Vũ Ngọc Phan, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0149709,
   "Latitude": 105.8091129
 },
 {
   "STT": 2018,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Hoàng Tuấn",
   "address": "Tầng 1 và tầng 2, nhà số 5, khu tập thể Nhiệt đới Việt Nga, phường Cổ Nhuế 1, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0444957,
   "Latitude": 105.7986087
 },
 {
   "STT": 2019,
   "Name": "Phòng khám Chuyên khoa Nội Minh Khuê",
   "address": "Số 8, Lương Ngọc Quyến, phường Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9846503,
   "Latitude": 105.7944568
 },
 {
   "STT": 2020,
   "Name": "Phòng khám Sản Phụ Khoa",
   "address": "M33 Ngô Thì Nhậm, phường Hà Cầu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9690802,
   "Latitude": 105.7689016
 },
 {
   "STT": 2021,
   "Name": "Phòng khám Chuyên khoa Nội Soi Tiêu Hóa Trên Tân Triều",
   "address": "Kiốt số 1C Km số 2 đường 70, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 21.0059089,
   "Latitude": 105.7487073
 },
 {
   "STT": 2022,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Tân Triều",
   "address": "Kiốt 3B Km số 2 đường 70, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 21.0059089,
   "Latitude": 105.7487073
 },
 {
   "STT": 2023,
   "Name": "Phòng khám Chuyên khoa Ung Bướu Tân Triều",
   "address": "Kiốt số 1B, Km số 2 đường 70, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 21.0059089,
   "Latitude": 105.7487073
 },
 {
   "STT": 2024,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Tân Triều",
   "address": "Kiốt 4B và 4C Km số 2 đường 70, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.963282,
   "Latitude": 105.79719
 },
 {
   "STT": 2025,
   "Name": "Nha Khoa Đẹp",
   "address": "Nhà số 21-LK6A, khu đô thị Mỗ Lao, phường Mộ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9866676,
   "Latitude": 105.7835446
 },
 {
   "STT": 2026,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà Thành",
   "address": "Số 13, Tô Hiệu, phường Nguyễn Trãi, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9702718,
   "Latitude": 105.7820268
 },
 {
   "STT": 2027,
   "Name": "Phòng khám Răng Hàm Mặt Hà Nội 1",
   "address": "Số 410 phố xã Đàn, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.015892,
   "Latitude": 105.8328384
 },
 {
   "STT": 2028,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 35, ngõ 2, đường Nguyễn Chánh, tổ 38, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0088494,
   "Latitude": 105.7947647
 },
 {
   "STT": 2029,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 44, phố Quan Nhân, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0046868,
   "Latitude": 105.8113547
 },
 {
   "STT": 2030,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 69D phố Lạc Trung, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0029556,
   "Latitude": 105.8677139
 },
 {
   "STT": 2031,
   "Name": "Phòng khám Chuyên khoa Nội Tân Triều",
   "address": "Kiốt số 4A Km số 2 đường 70, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 21.0059089,
   "Latitude": 105.7487073
 },
 {
   "STT": 2032,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Cụm 13, xã Tân Lập, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0811921,
   "Latitude": 105.7145801
 },
 {
   "STT": 2033,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm ",
   "address": "Cụm 13, xã Tân Lập, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0811921,
   "Latitude": 105.7145801
 },
 {
   "STT": 2034,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Đức Việt",
   "address": "Cụm 13, xã Tân Lập, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0811921,
   "Latitude": 105.7145801
 },
 {
   "STT": 2035,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Trực Thuộc Công Ty Tnhh Nha Khoa Aquacare Việt Nam",
   "address": "Số 173C Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0125129,
   "Latitude": 105.8072069
 },
 {
   "STT": 2036,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Việt Nhật 1",
   "address": "Tổ dân phố Trung Văn, phường Trung Văn, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 20.9896224,
   "Latitude": 105.7852105
 },
 {
   "STT": 2037,
   "Name": "Phòng khám Đa Khoa An Khang Trực Thuộc Công Ty Tnhh Y Học Hồng Phúc",
   "address": "Số 96 Đông Các, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.01965,
   "Latitude": 105.826926
 },
 {
   "STT": 2038,
   "Name": "Cơ Sở Dịch Vụ Cấp Cứu, Hỗ Trợ Vận Chuyển Người Bệnh Trực Thuộc Chi Nhánh Công Ty Tnhh International Sos Việt Nam Tại Hà Nội",
   "address": "Số 51 Xuân Diệu, phường Quảng An, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0637088,
   "Latitude": 105.8274747
 },
 {
   "STT": 2039,
   "Name": "Phòng khám Đa Khoa - Chi Nhánh Công Ty Tnhh Bệnh Viện Hồng Ngọc",
   "address": "Tầng 3 Khối B Trung tâm thương mại Savico Megamall, 7 - 9 Nguyễn Văn Linh, phường Gia Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0509185,
   "Latitude": 105.8941774
 },
 {
   "STT": 2040,
   "Name": "Phòng khám Chuyên khoa Mắt Bác sĩ Huy",
   "address": "Số 664 B, đường Hà Huy Tập, thị trấn Yên Viên, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0858175,
   "Latitude": 105.9177869
 },
 {
   "STT": 2041,
   "Name": "Phòng khám Sản Phụ Khoa Nhật Việt",
   "address": "Số 635 phố Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 2042,
   "Name": "Phòng khám Chuyên khoa Nhi Tô Hiệu",
   "address": "Lô A14, đường Tô Hiệu, phường Hà Cầu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9666271,
   "Latitude": 105.7773263
 },
 {
   "STT": 2043,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 43 phố Lê Ngọc Hân, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016018,
   "Latitude": 105.8554039
 },
 {
   "STT": 2044,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ - Bb Thanh Mai",
   "address": "Tầng 6 số 74 phố Tô Hiến Thành, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.01346,
   "Latitude": 105.8504259
 },
 {
   "STT": 2045,
   "Name": "Phòng khám Chuyên khoa Nội ",
   "address": "Thôn Xuân Sơn, xã Trung Giã, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.3030059,
   "Latitude": 105.8657274
 },
 {
   "STT": 2046,
   "Name": "Phòng khám Chuyên khoa Nội Kim Anh",
   "address": "Số 4, Thạch Lỗi, xã Thanh Xuân, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.221435,
   "Latitude": 105.77108
 },
 {
   "STT": 2047,
   "Name": "Nha Khoa Hà Nội",
   "address": "Số nhà 38, Nguyễn Thái Học, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0866623,
   "Latitude": 105.6636554
 },
 {
   "STT": 2048,
   "Name": "Phòng khám Chuyên khoa Nhi Họa Mi",
   "address": "Số 3A ngõ 172 phố Vũ Hữu, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9995063,
   "Latitude": 105.7957649
 },
 {
   "STT": 2049,
   "Name": "Phòng khám Chuyên khoa Nội Đồng Trí Đức ",
   "address": "Số 1B tổ 29, phường Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0030438,
   "Latitude": 105.8282959
 },
 {
   "STT": 2050,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm",
   "address": "Số 44 phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 2051,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Labo Th Việt Nam",
   "address": "Số nhà 6A - B3 tập thể Khương Thượng, phường Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0074254,
   "Latitude": 105.8198106
 },
 {
   "STT": 2052,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": " Phần diện tích 70m2, lô T1- 10, tầng 1, tòa nhà 34T, khu đô thị mới Trung Hòa - Nhân Chính, đường Hoàng Đạo Thúy, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0086549,
   "Latitude": 105.8008038
 },
 {
   "STT": 2053,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt 30 Hoàng Đạo Thành",
   "address": "Số 30 ngõ 58A phố Hoàng Đạo Thành, phường Kim Giang, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9830938,
   "Latitude": 105.8129692
 },
 {
   "STT": 2054,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Y Tế Hoàng Ngân",
   "address": "Xóm Chợ, Thôn Bắc, xã Kim Nỗ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1329655,
   "Latitude": 105.7952744
 },
 {
   "STT": 2055,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Số 9N1 ngõ 53 Hoàng Cầu, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0167097,
   "Latitude": 105.824842
 },
 {
   "STT": 2056,
   "Name": "Phòng khám Chuyên khoa Trị Liệu Thần Kinh Cột Sống Hoa Kỳ - Chi Nhánh Tại Hà Nội",
   "address": "Số 44 phố Nguyễn Du, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0189342,
   "Latitude": 105.8488081
 },
 {
   "STT": 2057,
   "Name": "Nha Khoa Nghĩa Vân",
   "address": "Số 16 ngõ 49 phố Huỳnh Thúc Kháng, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0170105,
   "Latitude": 105.8095644
 },
 {
   "STT": 2058,
   "Name": "Nha Khoa Phương Nam",
   "address": "Số 325 đường Nguyễn Trãi, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 2059,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Một Thành Viên Nước Sạch Hà Nội",
   "address": "Số 44 đường Yên Phụ, phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0446859,
   "Latitude": 105.8453878
 },
 {
   "STT": 2060,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Ô 17 lô 7 khu Đồng Dưới, Thôn Phúc Am, xã Duyên Thái, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8986756,
   "Latitude": 105.8671954
 },
 {
   "STT": 2061,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 86, tổ 6, khu Tân Bình, thị trấn Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.896141,
   "Latitude": 105.574274
 },
 {
   "STT": 2062,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh Siêu Âm Trực Thuộc Công Ty Tnhh Y Tế Hà Đông",
   "address": "Số 232 đường 70, Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9996534,
   "Latitude": 105.7513326
 },
 {
   "STT": 2063,
   "Name": "Phòng khám Chuyên khoa Nhi Tiến Hưng",
   "address": "Xóm 4, Thôn Cam, xã Cổ Bi, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9625894,
   "Latitude": 105.9072044
 },
 {
   "STT": 2064,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 169 Vũ Xuân Thiều, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0333774,
   "Latitude": 105.9171134
 },
 {
   "STT": 2065,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà Đương",
   "address": "Số 5, tổ 8, phường Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0146936,
   "Latitude": 105.9072409
 },
 {
   "STT": 2066,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Thôn Lai Xá, xã Kim Chung, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0575925,
   "Latitude": 105.7263819
 },
 {
   "STT": 2067,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt ",
   "address": "Số 72, phố Việt Hưng, phường Việt Hưng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0561222,
   "Latitude": 105.9008539
 },
 {
   "STT": 2068,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Huyền Trang",
   "address": "Số 86C, ngõ 381, đường Nguyễn Khang, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0282387,
   "Latitude": 105.7954126
 },
 {
   "STT": 2069,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình",
   "address": "Số 854 Ba La, phường Phú La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9560054,
   "Latitude": 105.7573962
 },
 {
   "STT": 2070,
   "Name": "Phòng khám Sản Phụ Khoa Hoa Xa",
   "address": "Số nhà 17, ngõ 12, phố Đỗ Quang, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0111175,
   "Latitude": 105.8019279
 },
 {
   "STT": 2071,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Số 1A",
   "address": "Số 83B (tầng 1) Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0237111,
   "Latitude": 105.8178428
 },
 {
   "STT": 2072,
   "Name": "Phòng khám Sản Phụ Khoa  Thanh Lương",
   "address": "Số 2 ngách 325/69 phố Kim Ngưu, phường Thanh Lương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0044533,
   "Latitude": 105.8637957
 },
 {
   "STT": 2073,
   "Name": "Nha Khoa Vương Thành",
   "address": "NO7-LK4, khu Văn Phú, phường Phú La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9586775,
   "Latitude": 105.7688614
 },
 {
   "STT": 2074,
   "Name": "Nha Khoa Dũng Trí",
   "address": "Số 47 phố Khương Trung, tổ 6, phường Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.000543,
   "Latitude": 105.8187925
 },
 {
   "STT": 2075,
   "Name": "Phòng khám Chuyên khoa Nội Hoàng Hạnh",
   "address": "Số 35 ngõ 445 Lạc Long Quân, tổ 4 cụm 1, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0573795,
   "Latitude": 105.8064268
 },
 {
   "STT": 2076,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số nhà 78, phố Trần Bình, tập thể Bệnh viện 198, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0332928,
   "Latitude": 105.7779325
 },
 {
   "STT": 2077,
   "Name": "Nha Khoa Minh Anh",
   "address": "Số 106 tổ dân phố Lộc, phường Xuân Đỉnh, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0678286,
   "Latitude": 105.7886365
 },
 {
   "STT": 2078,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình ",
   "address": "Số 29, phố Quán Thánh, phường Quán Thánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0424038,
   "Latitude": 105.8421737
 },
 {
   "STT": 2079,
   "Name": "Phòng khám Chuyên khoa Phụ Sản ",
   "address": "Số nhà 54, phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0338687,
   "Latitude": 105.7788272
 },
 {
   "STT": 2080,
   "Name": "Nha Khoa Thiên Phúc",
   "address": "Số 370, tổ 14, đường Cầu Giấy, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.032547,
   "Latitude": 105.797439
 },
 {
   "STT": 2081,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Phòng 103 nhà E8 tập thể Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0220463,
   "Latitude": 105.8124494
 },
 {
   "STT": 2082,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Chăm Sóc Sức Khỏe Cộng Đồng Thiên Lý",
   "address": "Số 639 đường Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 2083,
   "Name": "Phòng khám Đa Khoa Thiên Phúc Trực Thuộc Công Ty Cổ Phần Thương Mại Và Dịch Vụ Đồng Hưng",
   "address": "Ngã Tư, phố Phú Hà, phường Phú Thịnh, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.148153,
   "Latitude": 105.500476
 },
 {
   "STT": 2084,
   "Name": "Phòng khám Da Liễu Thu Hiền",
   "address": "P108 - A3 thị trấn VP Chính Phủ, ngõ 4/26 Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0062784,
   "Latitude": 105.8387183
 },
 {
   "STT": 2085,
   "Name": "Nha Khoa Nụ Cười",
   "address": "Số 1 Nghi Tàm, phường Yên Phụ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.050871,
   "Latitude": 105.839978
 },
 {
   "STT": 2086,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Minh Thu",
   "address": "Tầng 2 số 193C phố Bà Triệu, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0130326,
   "Latitude": 105.8491033
 },
 {
   "STT": 2087,
   "Name": "Phòng khám Chuyên khoa Nhi Bích Vân",
   "address": "Diện tích 40m2/142m2 tầng 1 nhà DDN1 (Lô No.01),(Theo HĐ số 56/HĐTN-QL&PTNHN), khu nhà ở công nhân, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.133051,
   "Latitude": 105.7791327
 },
 {
   "STT": 2088,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Vi Sinh",
   "address": "Tầng 1, nhà DDN1 (theo HĐ số 56/HĐTN-QL&PTNHN), khu nhà ở công nhân, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.133051,
   "Latitude": 105.7791327
 },
 {
   "STT": 2089,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 52, ngõ 230, phố Định Công Thượng, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9831729,
   "Latitude": 105.8232641
 },
 {
   "STT": 2090,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Công Nghệ Cao Healthcare Việt Nam Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Healthcare Việt Nam",
   "address": "Số 38M2 khu đô thị Yên Hòa, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0220587,
   "Latitude": 105.7877078
 },
 {
   "STT": 2091,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Ô 20, LK26, khu đô thị mới Văn Phú, phường Phú La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9598391,
   "Latitude": 105.7643176
 },
 {
   "STT": 2092,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Hải Ly",
   "address": "Số 34 Ngõ Giếng, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0186954,
   "Latitude": 105.8271825
 },
 {
   "STT": 2093,
   "Name": "Phòng khám Chuyên khoa Nội - 88",
   "address": "Tổ 16, ngõ 193, phường Phú Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.051992,
   "Latitude": 105.7587186
 },
 {
   "STT": 2094,
   "Name": "Phòng khám Chuyên khoa Nội Kết Châu",
   "address": "Số 21, hẻm 35/69/95 phố Khương Hạ, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.999509,
   "Latitude": 105.8189
 },
 {
   "STT": 2095,
   "Name": "Nha Khoa Nụ Cười Hà Nội",
   "address": "Số 248 đường Nguyễn Trãi, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 2096,
   "Name": "Phòng khám Chuyên khoa Siêu Âm Sản Phụ Khoa",
   "address": "Phòng 107 nhà C4 Tập thể Thanh Xuân Bắc, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.992434,
   "Latitude": 105.801811
 },
 {
   "STT": 2097,
   "Name": "Phòng khám Chuyên khoa Da Liễu Hd",
   "address": "Số 88 đường Đê Tô Hoàng, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0072039,
   "Latitude": 105.8495134
 },
 {
   "STT": 2098,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số nhà 132 đường Cổ Nhuế, phường Cổ Nhuế 2, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.080561,
   "Latitude": 105.7779796
 },
 {
   "STT": 2099,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Anh Huy",
   "address": "Số 82, tổ 5, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0437208,
   "Latitude": 105.9259271
 },
 {
   "STT": 2100,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Công Nghệ Cao Healthcare Việt Nam Trực Thuộc Công Ty Cổ Phần Dịch Vụ Y Tế Healthcare Việt Nam",
   "address": "Số 38M2 khu đô thị Yên Hòa, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0220587,
   "Latitude": 105.7877078
 },
 {
   "STT": 2101,
   "Name": "Cơ Sở Dịch Vụ Kính Thuốc Trực Thuộc Công Ty Cổ Phần Kính Mắt Việt Tiến",
   "address": "Số 50 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0250118,
   "Latitude": 105.8110428
 },
 {
   "STT": 2102,
   "Name": "Phòng khám Chuyên khoa Da Liễu Hd",
   "address": "Số 88 đường Đê Tô Hoàng, phường Cầu Dền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0072039,
   "Latitude": 105.8495134
 },
 {
   "STT": 2103,
   "Name": "Nha Khoa Anh Tiên",
   "address": "Kiot số 3, nhà A2, CT2, Tòa nhà Rainbow Linh Đàm, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9638996,
   "Latitude": 105.824299
 },
 {
   "STT": 2104,
   "Name": "Nha Khoa Hiếu Quyền",
   "address": "Số 20A phố Hàn Thuyên, phường Phạm Đình Hổ, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0187831,
   "Latitude": 105.8578941
 },
 {
   "STT": 2105,
   "Name": "Nha Khoa Vạn Xuân",
   "address": "Số 105 ngõ chợ Khâm Thiên, phường Trung Phụng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.016205,
   "Latitude": 105.8380449
 },
 {
   "STT": 2106,
   "Name": "Nha Khoa Linh Hằng",
   "address": "Số 44D Triều Khúc, phường Thanh Xuân Nam, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.985797,
   "Latitude": 105.798315
 },
 {
   "STT": 2107,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Tân Việt",
   "address": "Số 54 phố Quán Sứ (tầng 2), phường Hàng Bông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0275592,
   "Latitude": 105.846
 },
 {
   "STT": 2108,
   "Name": "Phòng khám Chuyên khoa Ngoại Nguyên Tâm",
   "address": "Số 62 phố Nguyễn Phúc Lai, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0210248,
   "Latitude": 105.8218703
 },
 {
   "STT": 2109,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Bệnh Viện Tim Hà Nội (Cơ Sở 2)",
   "address": "Ngõ 603, đường Lạc Long Quân, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0645778,
   "Latitude": 105.8095837
 },
 {
   "STT": 2110,
   "Name": "Phòng khám Siêu Âm Sản Phụ Khoa",
   "address": "Phòng 109 nhà C2 tập thể Thanh Xuân Bắc, phường Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.992434,
   "Latitude": 105.801811
 },
 {
   "STT": 2111,
   "Name": "Phòng khám Sản Khoa Thuận An",
   "address": "Số 35 phố Tràng Thi, phường Hàng Trống, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0311543,
   "Latitude": 105.8491764
 },
 {
   "STT": 2112,
   "Name": "Nha Khoa Tâm Đức",
   "address": "Số 116, phố Sài Đồng, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.033183,
   "Latitude": 105.907417
 },
 {
   "STT": 2113,
   "Name": "Nha Khoa Đức Hòa",
   "address": "Số 44 phố Đức Giang, tổ 22B, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 2114,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 8 ngõ 366, đường Ngọc Lâm, phường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0473441,
   "Latitude": 105.875035
 },
 {
   "STT": 2115,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 103 phố Vũ Xuân Thiều, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0380238,
   "Latitude": 105.9220836
 },
 {
   "STT": 2116,
   "Name": "Nha Khoa Số 10",
   "address": "Khu 7, thị trấn Trạm Trôi, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.071136,
   "Latitude": 105.705818
 },
 {
   "STT": 2117,
   "Name": "Nha Khoa Hoa Bằng",
   "address": "Số 46 phố Hoa Bằng, phường Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0249595,
   "Latitude": 105.795396
 },
 {
   "STT": 2118,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 101 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0043616,
   "Latitude": 105.8399623
 },
 {
   "STT": 2119,
   "Name": "Nha Khoa Toàn Khánh",
   "address": "Số 71 đường Xuân La, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.053459,
   "Latitude": 105.840214
 },
 {
   "STT": 2120,
   "Name": "Nha Khoa Đại Dương",
   "address": "Căn hộ số A6, khu nhà ở bán, phường Cổ Nhuế 1, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0502095,
   "Latitude": 105.7783275
 },
 {
   "STT": 2121,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số nhà 159, tổ 7, thửa số 19 - Khu phát triển đô thị 4A, phường La Khê, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9737221,
   "Latitude": 105.7629924
 },
 {
   "STT": 2122,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 10 phố Cự Lộc, phường Thượng Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0008467,
   "Latitude": 105.8138979
 },
 {
   "STT": 2123,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Dược 198",
   "address": "Số 147 phố Kim Bài, thị trấn Kim Bài, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8517273,
   "Latitude": 105.7659269
 },
 {
   "STT": 2124,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "Đội 9, xã Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.921933,
   "Latitude": 105.8510475
 },
 {
   "STT": 2125,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng An Bình",
   "address": "Số 236 đường Nguyễn Trãi, phường Trung Văn, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 2126,
   "Name": "Phòng khám Ccb Phía Nam Sản Phụ Khoa",
   "address": "Số 2, ngách 325, ngõ 192 phố Lê Trọng Tấn, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9879172,
   "Latitude": 105.8258583
 },
 {
   "STT": 2127,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 12 ngách 9 ngõ 175 phố Định Công, phường Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9827283,
   "Latitude": 105.8368379
 },
 {
   "STT": 2128,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Tân Triều",
   "address": "Kiot 3B km số 2 đường 70, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 21.0059089,
   "Latitude": 105.7487073
 },
 {
   "STT": 2129,
   "Name": "Phòng khám Chuyên khoa Ngoại Kim Liên",
   "address": "Số nhà 30 ngõ 9 ngách 39 phố Lương Đình Của, phường Kim Liên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0039659,
   "Latitude": 105.835933
 },
 {
   "STT": 2130,
   "Name": "Phòng khám Phục Hồi Chức Năng Trực Thuộc Công Ty Cổ Phần Tư Vấn Đào Tạo Phát Triển Y Tế Và Nguồn Nhân Lực Trị Liệu Thiên Nhiên",
   "address": "Số 27, ngõ 61 đường Trần Duy Hưng, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0135581,
   "Latitude": 105.8034099
 },
 {
   "STT": 2131,
   "Name": "Phòng khám Chuyên khoa Mắt Thành An",
   "address": "Số 19 Nguyễn Bỉnh Khiêm, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0166472,
   "Latitude": 105.8485659
 },
 {
   "STT": 2132,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Bông Sen",
   "address": "Kiot số 36 tòa nhà HH 4A, ô đất HH4 lô CC6, khu dịch vụ tổng hợp và nhà ở Hồ Linh Đàm, phường Hoàng Liệt, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9682526,
   "Latitude": 105.8323323
 },
 {
   "STT": 2133,
   "Name": "Nha Khoa Việt Nga",
   "address": "Số 30 ngõ 95 phố Vũ Xuân Thiều, phường Phúc Lợi, quận Long Biên, Hà Nội",
   "Longtitude": 21.0380238,
   "Latitude": 105.9220836
 },
 {
   "STT": 2134,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 189 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0010237,
   "Latitude": 105.8416146
 },
 {
   "STT": 2135,
   "Name": "Phòng khám Chuyên khoa Nội Đông Á",
   "address": "Số nhà 30B, ngõ 34 đường Xuân La, tổ 33 cụm 5, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0673807,
   "Latitude": 105.8059687
 },
 {
   "STT": 2136,
   "Name": "Phòng khám Phụ Khoa Song Sinh",
   "address": "Đội 1, Thôn Lạc Thị, xã Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.930183,
   "Latitude": 105.835567
 },
 {
   "STT": 2137,
   "Name": "Phòng khám Chuyên khoa Phụ Sản",
   "address": "Số 20, ngõ 36, phố Đào Tấn, phường Cống Vị, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0331959,
   "Latitude": 105.808579
 },
 {
   "STT": 2138,
   "Name": "Phòng khám Chuyên khoa -Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Tnhh Thẩm Mỹ Tiên Sơn",
   "address": "Số 12A3, phố Lý Nam Đế, phường Hàng Mã, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0371448,
   "Latitude": 105.8454154
 },
 {
   "STT": 2139,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Lô 38 - A3 chung cư Thăng Long Garden số 250 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9939105,
   "Latitude": 105.8571178
 },
 {
   "STT": 2140,
   "Name": "Thẩm Mỹ Viện Li Li A",
   "address": "Số 270+272 phố Bà Triệu (lối đi: phố Tô Hiến Thành), phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0133684,
   "Latitude": 105.8491251
 },
 {
   "STT": 2141,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Tnhh Thẩm Mỹ Minh Khánh",
   "address": "Số 9 Nguyễn Khắc Nhu, phường Trúc Bạch, quận Ba Đình, Hà Nội",
   "Longtitude": 21.04418,
   "Latitude": 105.84522
 },
 {
   "STT": 2142,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Thành Công",
   "address": "Số 6 phố Trần Hưng Đạo, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0194881,
   "Latitude": 105.85756
 },
 {
   "STT": 2143,
   "Name": "Nha Khoa Tuệ Tâm",
   "address": "Số 22 ngõ 128C ngõ Đại La, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9976193,
   "Latitude": 105.8433318
 },
 {
   "STT": 2144,
   "Name": "Nha Khoa Thủ Đô",
   "address": "Số 4 đường Lê Văn Hiến, phường Đức Thắng, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.074457,
   "Latitude": 105.773483
 },
 {
   "STT": 2145,
   "Name": "Cơ Sở Dịch Vụ Cấp Cứu, Hỗ Trợ Vận Chuyển Người Bệnh Trong Nước Trực Thuộc Công Ty Tnhh Vận Chuyển Người Bệnh Thủ Đô.",
   "address": "Số 5, ngõ 74, phố Kim Ngưu, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0043966,
   "Latitude": 105.8611706
 },
 {
   "STT": 2146,
   "Name": "Nha Khoa Quang Anh",
   "address": "Số 482 Hoàng Hoa Thám, phường Bưởi, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0454972,
   "Latitude": 105.8118772
 },
 {
   "STT": 2147,
   "Name": "Phòng khám Chuyên khoa Nội An Việt",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 2148,
   "Name": "Nha Khoa Quang Trung",
   "address": "Xóm 3, xã Đông Dư, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9840163,
   "Latitude": 105.9164655
 },
 {
   "STT": 2149,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Y Cao Hà Nội",
   "address": "Cụm 8, thị trấn Phúc Thọ, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1032387,
   "Latitude": 105.5441093
 },
 {
   "STT": 2150,
   "Name": "Phòng khám Siêu Âm 4D",
   "address": "Khu tập thể bách hóa Trâu Quỳ, số 2 đường Ngô Xuân Quảng, thị trấn Trâu Quỳ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0179888,
   "Latitude": 105.937002
 },
 {
   "STT": 2151,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Cụm 8, thị trấn Phúc Thọ, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1032387,
   "Latitude": 105.5441093
 },
 {
   "STT": 2152,
   "Name": "Phòng khám Đa Khoa Đông Đô Trực Thuộc Công Ty Cổ Phần Y Tế Thăng Long",
   "address": "Số 59 -61 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0037782,
   "Latitude": 105.8376342
 },
 {
   "STT": 2153,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Y Dược Tân Triều",
   "address": "Lô E1, E2, E3 khu đấu thầu quyền sử dụng đất, Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9670992,
   "Latitude": 105.796058
 },
 {
   "STT": 2154,
   "Name": "Phòng khám Đa Khoa Hoàng Long Hà Nội Trực Thuộc Công Ty Cổ Phần Y Tế Hoàng Long",
   "address": "Tầng 10, tòa tháp VCCI, số 9 phố Đào Duy Anh, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0086933,
   "Latitude": 105.8380773
 },
 {
   "STT": 2155,
   "Name": "Nha Khoa Duy Hậu",
   "address": "Thôn Ổ Thôn, xã Thọ Lộc, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1217278,
   "Latitude": 105.5349503
 },
 {
   "STT": 2156,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Bách Nhung",
   "address": "Xóm 7, xã Ninh Hiệp, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0762299,
   "Latitude": 105.9494248
 },
 {
   "STT": 2157,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Phụ sảnoriasis And Skin Việt Nam",
   "address": "Số 114A Mai Hắc Đế, phường Lê Đại Hành, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.011381,
   "Latitude": 105.8506346
 },
 {
   "STT": 2158,
   "Name": "Phòng khám Chuyên khoa Nội An Phát - Số 189 Giải Phóng",
   "address": "Số 189 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0010237,
   "Latitude": 105.8416146
 },
 {
   "STT": 2159,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Y Tế Hoàng Long",
   "address": "Tầng 10, tòa tháp VCCI, số 9 phố Đào Duy Anh, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0086933,
   "Latitude": 105.8380773
 },
 {
   "STT": 2160,
   "Name": "Phòng khám Chuyên khoa Mắt Trực Thuộc Công Ty Cổ Phần Tân Hùng Phú",
   "address": "Số nhà 18, đường Cầu Giấy, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0290202,
   "Latitude": 105.8030631
 },
 {
   "STT": 2161,
   "Name": "Nha Khoa  Á - Âu ",
   "address": "Thôn Chúc Lý, xã Ngọc Hòa, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9202554,
   "Latitude": 105.6958162
 },
 {
   "STT": 2162,
   "Name": "Phòng khám Chuyên khoa Nội Tim Mạch An Phước Trực Thuộc Công Ty Tnhh Dịch Vụ Y Tế An Đạt",
   "address": "Tầng 1, tòa nhà GP INVEST, số 170 đường La Thành, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0210391,
   "Latitude": 105.8281626
 },
 {
   "STT": 2163,
   "Name": "Nha Khoa M Y S M I L E",
   "address": "Số 14, ngõ 171 đường Nguyễn Ngọc Vũ, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0116228,
   "Latitude": 105.8066817
 },
 {
   "STT": 2164,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp",
   "address": "Số 34, khu Yên Sơn, thị trấn Chúc Sơn, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.9151641,
   "Latitude": 105.7008731
 },
 {
   "STT": 2165,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh : Siêu Âm An Việt 1",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 2166,
   "Name": "Nha Khoa Quốc Tế J.E.T.D.E.N.T.I.S",
   "address": "Số 175 Tôn Đức Thắng, phường Hàng Bột, quận Đống Đa, Hà Nội",
   "Longtitude": 21.022406,
   "Latitude": 105.831811
 },
 {
   "STT": 2167,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình Phương Mai",
   "address": "Số 183 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0009121,
   "Latitude": 105.8415042
 },
 {
   "STT": 2168,
   "Name": "Nha Khoa Quốc Tế Phương Đông",
   "address": "98B Trần Phú, phường Mộ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9795614,
   "Latitude": 105.7868639
 },
 {
   "STT": 2169,
   "Name": "Nha Khoa Ngôi Sao Thăng Long",
   "address": "Số nhà 64 phố Vồi, thị trấn Thường Tín, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8715715,
   "Latitude": 105.8627913
 },
 {
   "STT": 2170,
   "Name": "Phòng khám Chuyên khoa Thuộc Hệ Nội: Cơ Xương Khớp Bảo Ngọc ",
   "address": "Số 73 hẻm 72/73/30 phố Quan Nhân, phường Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0057638,
   "Latitude": 105.8118503
 },
 {
   "STT": 2171,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Dịch Vụ Y Tế An Thịnh",
   "address": "Số 58 phố Trần Bình, phường Mai Dịch, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0349608,
   "Latitude": 105.7760097
 },
 {
   "STT": 2172,
   "Name": "Phòng khám Đa Khoa Quốc Tế Thanh Chân Trực Thuộc Công Ty Cổ Phần Y Tế Thanh Chân",
   "address": "Số 6 đường Nguyễn Thị Thập, khu đô thị Trung Hòa - Nhân Chính, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0101531,
   "Latitude": 105.7988345
 },
 {
   "STT": 2173,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh : Siêu Âm",
   "address": "Số nhà 75, phố Sài Đồng, phường Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.0315251,
   "Latitude": 105.9110344
 },
 {
   "STT": 2174,
   "Name": "Nha Khoa Anh Đức",
   "address": "Số 33, ngõ 376 đường Bưởi, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0406761,
   "Latitude": 105.8070174
 },
 {
   "STT": 2175,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 135 ngõ xã Đàn 2, phường Nam Đồng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0147642,
   "Latitude": 105.831386
 },
 {
   "STT": 2176,
   "Name": "Phòng khám Nha Khoa Smile Care Trực Thuộc Công Ty Cổ Phần Chăm Sóc Nụ Cười Đẹp",
   "address": "Số 30 Nguyên Hồng, phường Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0167913,
   "Latitude": 105.8109322
 },
 {
   "STT": 2177,
   "Name": "Nha Khoa S.Mi.Le",
   "address": "Số 25 phố Thanh Nhàn (Cửa sau số 7 ngõ 9 phố Quỳnh Lôi), phường Quỳnh Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025489,
   "Latitude": 105.859697
 },
 {
   "STT": 2178,
   "Name": "Phòng khám Siêu Âm Sản Phụ Khoa Hiền Lương",
   "address": "Số 30, ngõ 376 phố Vĩnh Hưng, phường Thanh Trì, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9929188,
   "Latitude": 105.879674
 },
 {
   "STT": 2179,
   "Name": "Phòng khám Siêu Âm Sản Phụ Khoa ",
   "address": "Số 650 H5, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.983687,
   "Latitude": 105.848156
 },
 {
   "STT": 2180,
   "Name": "Phòng khám Chuyên khoa Phụ Sản - Kế Hoạch Hóa Gia Đình An Phát - 189 Giải Phóng",
   "address": " Tầng 1+2+3 số 189 đường Giải Phóng, phường Đồng Tâm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9980934,
   "Latitude": 105.8411504
 },
 {
   "STT": 2181,
   "Name": "Phòng khám Sản Phụ Khoa Ngọc Linh",
   "address": "Thôn Bầu, xã Kim Chung, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1293583,
   "Latitude": 105.7798664
 },
 {
   "STT": 2182,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "P104 - E8 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.003318,
   "Latitude": 105.8364789
 },
 {
   "STT": 2183,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm Long Biên",
   "address": "Số 486 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0707563,
   "Latitude": 105.9050599
 },
 {
   "STT": 2184,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 8, ngách 44, ngõ 260, phố Tân Mai, phường Tân Mai, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9836985,
   "Latitude": 105.8508339
 },
 {
   "STT": 2185,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ quận E.E.N",
   "address": "Số 19B phố Mai Hắc Đế, phường Bùi Thị Xuân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016191,
   "Latitude": 105.8509289
 },
 {
   "STT": 2186,
   "Name": "Phòng khám Chuyên khoa Mắt",
   "address": "B04 tập thể Viện công nghệ Laser - tổ 9 (cũ: tổ 87), phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0200535,
   "Latitude": 105.8246266
 },
 {
   "STT": 2187,
   "Name": "Phòng khám Đa Khoa Đức Trí Trực Thuộc Công Ty Cổ Phần Y Dược Đức Trí",
   "address": "Phòng 102 tầng 1, tầng 2, tầng 3, tầng 4, tầng 5, nhà số 06, phố Nhổn, phường Minh Khai, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0543853,
   "Latitude": 105.7328295
 },
 {
   "STT": 2188,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 109, tổ 9, phường Cự Khối, quận Long Biên, Hà Nội",
   "Longtitude": 21.0114103,
   "Latitude": 105.9046153
 },
 {
   "STT": 2189,
   "Name": "Phòng khám Chuyên khoa Nhi",
   "address": "Số 1 tập thể Viện điều tra quy hoạch rừng, xã Vĩnh Quỳnh, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9299901,
   "Latitude": 105.8462768
 },
 {
   "STT": 2190,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm",
   "address": "Tầng 2, số 196 phố Tây Sơn, thị trấn Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0840235,
   "Latitude": 105.6712668
 },
 {
   "STT": 2191,
   "Name": "Phòng khám Đa Khoa Y Cao Thiện Đức Trực Thuộc Công Ty Tnhh Y Dược Thiện Đức",
   "address": "Tổ 1 Thôn Miếu Thờ, xã Tiên Dược, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2443458,
   "Latitude": 105.8502999
 },
 {
   "STT": 2192,
   "Name": "Phòng khám Đa Khoa Âu Việt Trực Thuộc Công Ty Cổ Phần Bệnh Viện Âu Việt",
   "address": "Số 15A Trần Khánh Dư, phường Phan Chu Trinh, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0227458,
   "Latitude": 105.8607696
 },
 {
   "STT": 2193,
   "Name": "Phòng khám Chuyên khoa Chẩn Đoán Hình Ảnh: Siêu Âm Y Cao",
   "address": "Số 209 đường Đa Tốn, xã Đa Tốn, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.986163,
   "Latitude": 105.930381
 },
 {
   "STT": 2194,
   "Name": "Phòng khám Đa Khoa Hồng Hải",
   "address": "Quốc lộ 3, Phù Mã, Phù Linh, Sóc Sơn, Hà Nội",
   "Longtitude": 21.2682797,
   "Latitude": 105.8541568
 },
 {
   "STT": 2195,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Thành Tâm",
   "address": "Số 106 Bùi Xương Trạch, phường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9899744,
   "Latitude": 105.8175916
 },
 {
   "STT": 2196,
   "Name": "Phòng khám Đa Khoa Đức Minh 2 Trực Thuộc Công Ty Cổ Phần Y Học Đức Minh",
   "address": "Số L13, khu đấu giá đất Tân Triều, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.965788,
   "Latitude": 105.794266
 },
 {
   "STT": 2197,
   "Name": "Phòng khám Đa Khoa 216 Nguyễn Trãi Trực Thuộc Trung Tâm Sức Khỏe Nghề Nghiệp - Viện Nghiên Cứu Khoa Học Kỹ Thuật Bảo Hộ Lao Động",
   "address": "Số 216 Nguyễn Trãi, phường Trung Văn, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 20.9879824,
   "Latitude": 105.7979395
 },
 {
   "STT": 2198,
   "Name": "Nha Khoa Việt Tiệp",
   "address": "Tiểu khu Mỹ Lâm, thị trấn Phú Xuyên, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.733944,
   "Latitude": 105.9130779
 },
 {
   "STT": 2199,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Thủy Tuấn",
   "address": "Số 2/12 phố Lê Lợi, phường Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1425846,
   "Latitude": 105.5054087
 },
 {
   "STT": 2200,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ Trực Thuộc Công Ty Cổ Phần Kinh Doanh Dịch Vụ Lpl Việt Nam",
   "address": "Số nhà 32 phố Huế, phường Hàng Bài, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.018833,
   "Latitude": 105.851732
 },
 {
   "STT": 2201,
   "Name": "Phòng khám Chuyên khoa Da Liễu",
   "address": "Số 25B ngõ 236 Lê Trọng Tấn, phường Khương Mai, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9913466,
   "Latitude": 105.8329822
 },
 {
   "STT": 2202,
   "Name": "Phòng khám Tai Mũi Họng Ngọc Lân",
   "address": "Tầng 1, số 212 phố Hoàng Ngân, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0100401,
   "Latitude": 105.8042861
 },
 {
   "STT": 2203,
   "Name": "Phòng khám Chyên Khoa Mắt",
   "address": "Thôn Mai Trai, xã Vạn Thắng, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.2491722,
   "Latitude": 105.3955686
 },
 {
   "STT": 2204,
   "Name": "Phòng khám Da Liễu Minh Tâm",
   "address": "Tầng 1 số 65 ngõ 2 phố Phương Mai, phường Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0048002,
   "Latitude": 105.8401338
 },
 {
   "STT": 2205,
   "Name": "Phòng khám Chuyên khoa Xét Nghiệm An Nhi",
   "address": "Số 49, ngõ 54 Nguyễn Chí Thanh, phường Láng Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0238903,
   "Latitude": 105.8101971
 },
 {
   "STT": 2206,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Family Dental",
   "address": "Số 59, phố Trần Quốc Hoàn, phường Dịch Vọng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0416805,
   "Latitude": 105.7855295
 },
 {
   "STT": 2207,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng Hoàng Huy",
   "address": "Số nhà 210, phố Hoàng Ngân, phường Trung Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.010189,
   "Latitude": 105.804459
 },
 {
   "STT": 2208,
   "Name": "Phòng khám Đa Khoa ",
   "address": "Tổ 3, thị trấn Quang Minh, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1915793,
   "Latitude": 105.7732633
 },
 {
   "STT": 2209,
   "Name": "Nha Khoa Bảo Ngân",
   "address": "Số 166+168 đường Khương Đình, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9941431,
   "Latitude": 105.8141121
 },
 {
   "STT": 2210,
   "Name": "Phòng khám Chuyên khoa Nội",
   "address": "Số 7+8, nhà N4, tập thể Cống Vị, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.035935,
   "Latitude": 105.8099499
 },
 {
   "STT": 2211,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt",
   "address": "Số 252 đường Nguyễn Trãi, phường Thanh Xuân Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 2212,
   "Name": "Nha Khoa Hoàng Tùng",
   "address": "Số 517 phố Kim Ngưu, phường Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0001263,
   "Latitude": 105.8620263
 },
 {
   "STT": 2213,
   "Name": "Phòng khám Chuyên khoa Nội Skylight",
   "address": "Tầng 1- CT1 chung cư Skylight ngõ Hòa Bình 6,số 125 phố Minh Khai, phường Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9971656,
   "Latitude": 105.8547534
 },
 {
   "STT": 2214,
   "Name": "Cơ Sở Dịch Vụ Tiêm ( Chích), Thay Băng, Đếm Mạch, Đo Nhiệt Độ, Đo Huyết Áp Medlatec Mỹ Đình Trực Thuộc Công Ty Tnhh Công Nghệ Và Xét Nghiệm Y Học",
   "address": "TDP số 6 Thôn Phú Mỹ Đình 2, phường Mỹ Đình 2, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0279571,
   "Latitude": 105.771796
 },
 {
   "STT": 2215,
   "Name": "Phòng khám Chuyên khoa Nội Trực Thuộc Công Ty Tnhh Một Thành Viên Kims Clinic And Health Care",
   "address": "Tầng 3, Golden Palace, phường Mễ Trì, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0114251,
   "Latitude": 105.775357
 },
 {
   "STT": 2216,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Cổ Phần Đầu Tư Và Xây Dựng Xuân Mai",
   "address": "Thôn Xuân Trung, xã Thủy Xuân Tiên, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8800638,
   "Latitude": 105.5687206
 },
 {
   "STT": 2217,
   "Name": "Phòng khám Chuyên khoa Nhi Vũ Sơn",
   "address": "Số nhà 90, phố Tía, xã Tô Hiệu, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8056811,
   "Latitude": 105.8839744
 },
 {
   "STT": 2218,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Phương Lan",
   "address": "Ki ốt 08+07 ao cổ cò, khu 600 Yên Phúc, phường Phúc La, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9703841,
   "Latitude": 105.7868365
 },
 {
   "STT": 2219,
   "Name": "Phòng khám Chuyên khoa Tai Mũi Họng",
   "address": "Tổ 4 tập thể trường Hữu Nghị 80, phường Phú Thịnh, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1410055,
   "Latitude": 105.4962651
 },
 {
   "STT": 2220,
   "Name": "Nha Khoa Gia Đình",
   "address": "Số 178 Ngô Gia Tự, phường Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0612852,
   "Latitude": 105.8942368
 },
 {
   "STT": 2221,
   "Name": "Nha Khoa Đức Thiện",
   "address": "Thôn Yên Ngưu, xã Tam Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9510097,
   "Latitude": 105.8413357
 },
 {
   "STT": 2222,
   "Name": "Phòng khám Chuyên khoa Răng Hàm Mặt Hà San",
   "address": "Tầng 2 nhà số 302 đường Tựu Liệt, xã Tam Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9538379,
   "Latitude": 105.8373692
 },
 {
   "STT": 2223,
   "Name": "Phòng khám Chuyên khoa Nhi Bé Khỏe Bé Vui",
   "address": "Xóm Tràng, xã Thanh Liệt, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9661062,
   "Latitude": 105.8108613
 },
 {
   "STT": 2224,
   "Name": "Phòng khám Nội Tim Mạch Tâm An",
   "address": "Tầng 2 LK6 A5 Đô thị Mỗ Lao, phường Mộ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9870449,
   "Latitude": 105.784677
 },
 {
   "STT": 2225,
   "Name": "Phòng khám Chuyên khoa Nhi - Bác sĩ Nho",
   "address": "Số 42 phố Quang Trung, phường Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.01963,
   "Latitude": 105.847652
 },
 {
   "STT": 2226,
   "Name": "Nha Khoa Vũ Kế",
   "address": "Số 21 phố Lê Duẩn, phường Cửa Nam, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0247546,
   "Latitude": 105.8414851
 },
 {
   "STT": 2227,
   "Name": "Nha Khoa Việt Khoa",
   "address": "9A Nguyễn Đình Chiểu, phường Nguyễn Du, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016561,
   "Latitude": 105.847665
 },
 {
   "STT": 2228,
   "Name": "Nha Khoa Thiện Mỹ",
   "address": "Số 05 ngõ 44 tổ 38 phố Hào Nam, phường Ô Chợ Dừa, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0242366,
   "Latitude": 105.8268752
 },
 {
   "STT": 2229,
   "Name": "Nha Khoa Nụ Cười",
   "address": "Số 71 Nguyễn Hoàng Tôn, phường Xuân La, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0705125,
   "Latitude": 105.8084798
 },
 {
   "STT": 2230,
   "Name": "Phòng khám Tư Vấn Sức Khỏe Sinh Sản Đức Minh",
   "address": "Ki ốt 11 nhà CT5A Thôn Yên Xá, xã Tân Triều, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9645101,
   "Latitude": 105.7946904
 },
 {
   "STT": 2231,
   "Name": "Phòng khám Chuyên khoa Da Liễu Minh Trang",
   "address": "Số 32, ngõ 125 phố Vĩnh Phúc, phường Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0427268,
   "Latitude": 105.8101234
 },
 {
   "STT": 2232,
   "Name": "Phòng khám Chuyên khoa Phục Hồi Chức Năng",
   "address": "Số nhà 5, ngách 38 khu 28B, đường Điện Biên Phủ, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 20.9975023,
   "Latitude": 105.8846456
 },
 {
   "STT": 2233,
   "Name": "Phòng khám Đa Khoa Trực Thuộc Công Ty Tnhh Khám Chữa Bệnh Y Học Hà Nội",
   "address": "Số 956 Đê La Thành, phường Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0274205,
   "Latitude": 105.8093053
 },
 {
   "STT": 2234,
   "Name": "Phòng khám Sản Phụ Khoa Âu Cơ",
   "address": "Số 284 đường Khương Đình, phường Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9910929,
   "Latitude": 105.8132131
 },
 {
   "STT": 2235,
   "Name": "Phòng khám Chuyên khoa Phẫu Thuật Thẩm Mỹ",
   "address": "Số 19 Điện Biên Phủ, phường Điện Biên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0301634,
   "Latitude": 105.8412883
 },
 {
   "STT": 2236,
   "Name": "Phòng khám Từ Thiện Chùa Linh Sơn - Thanh Nhàn",
   "address": "Số 1 ngõ 331 đường Trần Khát Chân, phường Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0081909,
   "Latitude": 105.8571635
 },
 {
   "STT": 2237,
   "Name": "Phòng khám Chuyên khoa Phụ Sản Vạn Phúc",
   "address": "Số 21 phố Vạn Phúc, phường Liễu Giai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0332108,
   "Latitude": 105.8194328
 },
 {
   "STT": 2238,
   "Name": "Phòng Chẩn Trị Y Học Dân Tộc Bảo Long",
   "address": "Số 51 đường Trần Duy Hưng, Cầu Giấy, Hà Nội",
   "Longtitude": 21.0132303,
   "Latitude": 105.8022949
 },
 {
   "STT": 2239,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Quang Nhợi",
   "address": "Nhà A15 Lô 15 khu đô thị Định Công, Hoàng Mai, Hà Nội",
   "Longtitude": 20.9833374,
   "Latitude": 105.8318795
 },
 {
   "STT": 2240,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vũ Thị Lan Anh",
   "address": "93 Lê Hồng Phong, Hà Đông, Hà Nội",
   "Longtitude": 20.9664061,
   "Latitude": 105.7743324
 },
 {
   "STT": 2241,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Biên",
   "address": "Số 6 Trần Phú, Hà Đông, Hà Nội",
   "Longtitude": 20.9838134,
   "Latitude": 105.7920675
 },
 {
   "STT": 2242,
   "Name": "Cơ Sở Gia Truyền Nguyễn Cao Oánh",
   "address": "Số 142 Quán Thánh, Ba Đình, Hà Nội",
   "Longtitude": 21.0423985,
   "Latitude": 105.8420381
 },
 {
   "STT": 2243,
   "Name": "Phòng khám Chữa Bệnh Bằng Y Học Cổ Truyền",
   "address": "Số 10 lô A khu Văn phòng Chính phủ, phố Vạn Phúc, Ba Đình, Hà Nội",
   "Longtitude": 21.0240716,
   "Latitude": 105.7875929
 },
 {
   "STT": 2244,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 9 ngõ 281 phố Chợ Khâm Thiên, Đống Đa, Hà Nội",
   "Longtitude": 20.9889233,
   "Latitude": 105.847339
 },
 {
   "STT": 2245,
   "Name": "Tràng Thái Đường",
   "address": "590 đường Thụy Khuê, Tây Hồ, Hà Nội",
   "Longtitude": 21.048543,
   "Latitude": 105.807665
 },
 {
   "STT": 2246,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thọ Xuân Đường",
   "address": "Số 7 tập thể Thủy Sản, Thanh Xuân, Hà Nội",
   "Longtitude": 21.0031906,
   "Latitude": 105.8030632
 },
 {
   "STT": 2247,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đào Đức Tuấn",
   "address": "Số 176 phố Quan Nhân, Thanh Xuân, Hà Nội",
   "Longtitude": 21.0034035,
   "Latitude": 105.8110102
 },
 {
   "STT": 2248,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Thị Kim Thoa",
   "address": "Số 5 ngõ 20 Trần Phú, tổ 11, Hà Đông, Hà Nội",
   "Longtitude": 20.955835,
   "Latitude": 105.7563658
 },
 {
   "STT": 2249,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Gia Trí",
   "address": "Số 89 phố Tuệ Tĩnh, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.016357,
   "Latitude": 105.8517957
 },
 {
   "STT": 2250,
   "Name": "Phòng Chẩn Trị Y Học Dân Tộc Bác sĩ Võ Văn Trành",
   "address": "Số 17 ngách 128/10 phố Hoàng Văn Thái, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9947815,
   "Latitude": 105.824671
 },
 {
   "STT": 2251,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Dân Tộc Đại Đức Đường",
   "address": "Số 10 ngõ 56 phố Trần Quang Diệu, , Đống Đa, Hà Nội",
   "Longtitude": 21.0155411,
   "Latitude": 105.8233599
 },
 {
   "STT": 2252,
   "Name": "Cơ Sở Gia Truyền Phạm Văn Khiêm",
   "address": "Số 11 Phố Nguyễn Công Trứ, Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.01427,
   "Latitude": 105.856731
 },
 {
   "STT": 2253,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vũ Văn Sử",
   "address": "Số 92 Đại Yên, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0359795,
   "Latitude": 105.8251653
 },
 {
   "STT": 2254,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Huy Mão",
   "address": "18 ngõ 189/6 Giảng Võ, Đống Đa, Hà Nội",
   "Longtitude": 21.0178931,
   "Latitude": 105.8110489
 },
 {
   "STT": 2255,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Xuân Hùng",
   "address": "Số 3 Ngõ 1 Thọ Lão, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0129421,
   "Latitude": 105.855835
 },
 {
   "STT": 2256,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tại nhà, Thôn Tô Khê, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0237762,
   "Latitude": 105.9686829
 },
 {
   "STT": 2257,
   "Name": "Phòng khám Đông Y Bình Minh",
   "address": "Số nhà 8A, ngõ 16, phố Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0334969,
   "Latitude": 105.7878377
 },
 {
   "STT": 2258,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phòng 120-C4 tập thể Kim Liên, Đống Đa, Hà Nội",
   "Longtitude": 21.0085396,
   "Latitude": 105.8352679
 },
 {
   "STT": 2259,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Việt Nga",
   "address": "Số 104 Đê La Thành, quận Đống Đa, Hà Nội",
   "Longtitude": 21.015555,
   "Latitude": 105.833555
 },
 {
   "STT": 2260,
   "Name": "Y Học Cổ Truyền Thái Hà",
   "address": "Số 16 ngách 343/18 Ngõ 343 đường Trần Khát Chân, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0090543,
   "Latitude": 105.8586182
 },
 {
   "STT": 2261,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Y Dược Bảo Long",
   "address": "Số nhà 38, ngõ 335, đường Nguyễn Trãi, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 2262,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số nhà 64, ngõ 81, đường Lạc Long Quân, tổ 21, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0362368,
   "Latitude": 105.7905825
 },
 {
   "STT": 2263,
   "Name": "Cơ Sở Gia Truyền Nắn Bó Gẫy Xương",
   "address": "Số 34 phố Thợ Nhuộm, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.026523,
   "Latitude": 105.844957
 },
 {
   "STT": 2264,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 29 phố Lê Thánh Tông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0218081,
   "Latitude": 105.857853
 },
 {
   "STT": 2265,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 103A đường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9976916,
   "Latitude": 105.8452896
 },
 {
   "STT": 2266,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 454 phố Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.998486,
   "Latitude": 105.8502479
 },
 {
   "STT": 2267,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Cường",
   "address": "Số 15/11 ngõ 167 đường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0020143,
   "Latitude": 105.8560147
 },
 {
   "STT": 2268,
   "Name": "Nhà Thuốc Đông Y Kim Bảng",
   "address": "Số 27 phố Nhân Hòa, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0042545,
   "Latitude": 105.8264289
 },
 {
   "STT": 2269,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Hồ Thị Thiệp",
   "address": "Số 440 phố Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 2270,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Danh Tuyến",
   "address": "Đội 5 xã Dương Liễu, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0577177,
   "Latitude": 105.6755457
 },
 {
   "STT": 2271,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Thị Hải Vân",
   "address": "Số 6 phố Yên Bái II, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0095422,
   "Latitude": 105.8522415
 },
 {
   "STT": 2272,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trịnh Bá Nham",
   "address": "Khu 6, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0684819,
   "Latitude": 105.7108662
 },
 {
   "STT": 2273,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Đỗ Khắc Hòa",
   "address": "Phú Sơn, Ba Vì, Hà Nội",
   "Longtitude": 21.2095519,
   "Latitude": 105.3658125
 },
 {
   "STT": 2274,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Công Hoan",
   "address": "Tập thể Viện Y học cổ truyền quân đội, tổ 11,, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9728757,
   "Latitude": 105.823155
 },
 {
   "STT": 2275,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiên Thảo Vạn Thọ Đường",
   "address": "Số 35/150 Yên Phụ, Tây Hồ, Hà Nội",
   "Longtitude": 21.056405,
   "Latitude": 105.835182
 },
 {
   "STT": 2276,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hạnh Lâm Đường",
   "address": "Số nhà 206 đường Trần Duy Hưng, tổ 10, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0107492,
   "Latitude": 105.799529
 },
 {
   "STT": 2277,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vũ Thế Nam",
   "address": "Số 37A Xa La,  Hà Đông, Hà Nội",
   "Longtitude": 20.9645685,
   "Latitude": 105.7896006
 },
 {
   "STT": 2278,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Nhâm",
   "address": "Số 08 Bế Văn Đàn,  Hà Đông, Hà Nội",
   "Longtitude": 20.9700048,
   "Latitude": 105.7734897
 },
 {
   "STT": 2279,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trịnh Thị Thảo",
   "address": "Số 48 Trần Hưng Đạo,  Hà Đông, Hà Nội",
   "Longtitude": 20.9701382,
   "Latitude": 105.7786132
 },
 {
   "STT": 2280,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Văn Đế",
   "address": "Thôn Du Nội, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.0867424,
   "Latitude": 105.8926678
 },
 {
   "STT": 2281,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Xuân Tiến",
   "address": "Phòng 111, E5, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0041844,
   "Latitude": 105.8055785
 },
 {
   "STT": 2282,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Hoàng Xuân Huy",
   "address": "Số 347 Thanh Bình,  Hà Đông, Hà Nội",
   "Longtitude": 20.9831724,
   "Latitude": 105.7782097
 },
 {
   "STT": 2283,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  ",
   "address": "Số 97 ( Cửa sau 01 Đinh Liệt), phố Hàng Bạc, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0337029,
   "Latitude": 105.8520024
 },
 {
   "STT": 2284,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  ",
   "address": "P106-L2 tập thể 93 Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.015379,
   "Latitude": 105.814104
 },
 {
   "STT": 2285,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Toàn Mỹ",
   "address": "Số 1A ngách 22 ngõ 530 Thụy Khuê, Tây Hồ, Hà Nội",
   "Longtitude": 21.049067,
   "Latitude": 105.8087176
 },
 {
   "STT": 2286,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Nguyễn Đăng Bảo",
   "address": "Số 5, ngõ 544, đường La Thành, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0240306,
   "Latitude": 105.8157015
 },
 {
   "STT": 2287,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Ngọc Cừ",
   "address": "Thôn Nguyên Khê, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1780811,
   "Latitude": 105.8312314
 },
 {
   "STT": 2288,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Hưng Đường - Công Ty Tnhh Đông Dược Phúc Hưng",
   "address": "Km 3 Ql21B, , huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.9513193,
   "Latitude": 105.7528794
 },
 {
   "STT": 2289,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Thị Hồng",
   "address": "Số 17 Nguyễn Viết Xuân,  Hà Đông, Hà Nội",
   "Longtitude": 20.9700323,
   "Latitude": 105.7739717
 },
 {
   "STT": 2290,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hồng Hưng",
   "address": "Số 96 phố Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0353362,
   "Latitude": 105.8238927
 },
 {
   "STT": 2291,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Đức Hùng",
   "address": ", huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8055441,
   "Latitude": 105.8838579
 },
 {
   "STT": 2292,
   "Name": "Phòng khám Y Học Cổ Truyền Nguyễn Văn Vinh",
   "address": "Số 24 phố Trần Xuân Soạn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0168656,
   "Latitude": 105.8535492
 },
 {
   "STT": 2293,
   "Name": " Phòng Chẩn Trị Y Học Cổ Truyền Nguyên Sinh",
   "address": "P409 - C6A phố Quỳnh Lôi, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.001682,
   "Latitude": 105.859724
 },
 {
   "STT": 2294,
   "Name": "Phòng khám Bệnh Gia Truyền Phong Tê Thấp Đặng Quốc Tuấn",
   "address": "Số 217A đường Trường Chinh, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0014569,
   "Latitude": 105.826257
 },
 {
   "STT": 2295,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Thị Kim Thoa",
   "address": "Số 56, phố Hoàng Văn Thái, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9947815,
   "Latitude": 105.824671
 },
 {
   "STT": 2296,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Hiền Lương",
   "address": "Số 32, phố Vũ Hữu, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9989308,
   "Latitude": 105.7954338
 },
 {
   "STT": 2297,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Hoàng Khoa",
   "address": "Số 18 đường Chiến Thắng,  Hà Đông, Hà Nội",
   "Longtitude": 20.9797044,
   "Latitude": 105.7957259
 },
 {
   "STT": 2298,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thanh Giang",
   "address": "Số nhà 5 ngõ 68 phố Ngụy Như Kon Tum, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0332653,
   "Latitude": 105.8022285
 },
 {
   "STT": 2299,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Tất Dũng",
   "address": "Số 6 ngách 102/20 phố Hoàng Đạo Thành, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9830938,
   "Latitude": 105.8129692
 },
 {
   "STT": 2300,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lương Y Phạm Đức Hiệp",
   "address": "Ngọc Trì, quận Long Biên, Hà Nội",
   "Longtitude": 21.0208921,
   "Latitude": 105.9112897
 },
 {
   "STT": 2301,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Trọng Nơi",
   "address": "Số 4, ngõ 166, phố Kim Mã, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0321744,
   "Latitude": 105.825457
 },
 {
   "STT": 2302,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 2 ngách 62, ngõ Lương Sử C, Đống Đa, Hà Nội",
   "Longtitude": 21.0258009,
   "Latitude": 105.8377286
 },
 {
   "STT": 2303,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Kim Dung",
   "address": "Số 501 phố Kim Ngưu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025637,
   "Latitude": 105.8617727
 },
 {
   "STT": 2304,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Thị Thanh Bình - 32",
   "address": "Số 32 ngách 6/6 phố Đội Nhân, quận Ba Đình, Hà Nội",
   "Longtitude": 21.041958,
   "Latitude": 105.810829
 },
 {
   "STT": 2305,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiện Hạnh",
   "address": "Số 25E ngõ 31 đường Xuân Diệu tổ 18 cụm 3, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0811211,
   "Latitude": 105.8180306
 },
 {
   "STT": 2306,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 8 ngách 91/17 Nguyễn Chí Thanh, Đống Đa, Hà Nội",
   "Longtitude": 21.019898,
   "Latitude": 105.810945
 },
 {
   "STT": 2307,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bùi Văn Khôi",
   "address": "Số 37 phố Phan Đình Phùng, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0402857,
   "Latitude": 105.8436772
 },
 {
   "STT": 2308,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Mộng Tuân",
   "address": "Số 177 đường Giải Phóng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.001069,
   "Latitude": 105.841469
 },
 {
   "STT": 2309,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thanh Đường 1",
   "address": "Số 52 phố Phùng Hưng, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0387398,
   "Latitude": 105.8466397
 },
 {
   "STT": 2310,
   "Name": "Phòng Trẩn Trị Y Học Cổ Truyền Bảo Thanh Đường",
   "address": "Số 102 phố Trần Hưng Đạo, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0229634,
   "Latitude": 105.8473777
 },
 {
   "STT": 2311,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Toàn Mỹ",
   "address": "Số 56 phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0354233,
   "Latitude": 105.8486502
 },
 {
   "STT": 2312,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Dũng",
   "address": "Tổ 5 Quang Lãm,  Hà Đông, Hà Nội",
   "Longtitude": 20.9450461,
   "Latitude": 105.7576108
 },
 {
   "STT": 2313,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Sâm",
   "address": "Tổ 3, khu Xuân Hà, Chương Mỹ, Hà Nội",
   "Longtitude": 20.898453,
   "Latitude": 105.5778586
 },
 {
   "STT": 2314,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Sơn",
   "address": "Số 14A Khu Hòa Sơn, Chương Mỹ, Hà Nội",
   "Longtitude": 20.91966,
   "Latitude": 105.7003
 },
 {
   "STT": 2315,
   "Name": "Phòng khám Đức An Đường",
   "address": "Số 6/169A, đường Xuân Thủy, tổ 54, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0365018,
   "Latitude": 105.7855108
 },
 {
   "STT": 2316,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Mậu Khang",
   "address": "Số 12 Nguyễn Công Trứ, Hà Đông, Hà Nội",
   "Longtitude": 20.9714849,
   "Latitude": 105.7848227
 },
 {
   "STT": 2317,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Mai Y Đường",
   "address": "Số 238 phố Tây Sơn, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0823212,
   "Latitude": 105.6746368
 },
 {
   "STT": 2318,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Ngô Như Cộng",
   "address": ", huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8055441,
   "Latitude": 105.8838579
 },
 {
   "STT": 2319,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Xuân Luân",
   "address": "Thôn Kim Hoàng, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.040578,
   "Latitude": 105.7277431
 },
 {
   "STT": 2320,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Ngọc Đồng",
   "address": "Số nhà 41 phố Phan Đình Phùng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0850982,
   "Latitude": 105.6682618
 },
 {
   "STT": 2321,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Minh Đức",
   "address": "Khu 6, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0684819,
   "Latitude": 105.7108662
 },
 {
   "STT": 2322,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thơm",
   "address": "Số 02 đường Nguyễn Trãi, Hà Đông, Hà Nội",
   "Longtitude": 20.9715697,
   "Latitude": 105.7799658
 },
 {
   "STT": 2323,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lầu Cẩm Tình",
   "address": "Số 28A đường Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9712716,
   "Latitude": 105.7758927
 },
 {
   "STT": 2324,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Đức Huy",
   "address": "Thôn Hoa Chử, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.084855,
   "Latitude": 105.6690963
 },
 {
   "STT": 2325,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hà Dương Vận",
   "address": "Số 48, Khối 3A, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1245303,
   "Latitude": 105.8271398
 },
 {
   "STT": 2326,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Phi Hùng",
   "address": "Số 733 A đường Quang Trung, Hà Đông, Hà Nội",
   "Longtitude": 20.9600149,
   "Latitude": 105.7620891
 },
 {
   "STT": 2327,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 14, tổ 1, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0432863,
   "Latitude": 105.7858518
 },
 {
   "STT": 2328,
   "Name": "Phòng Trẩn Trị Y Học Cổ Truyền Phạm Kim Tuyến",
   "address": "Tiểu Khu Mỹ Lâm, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.733944,
   "Latitude": 105.9130779
 },
 {
   "STT": 2329,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Nhật Hòa",
   "address": ", huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8055441,
   "Latitude": 105.8838579
 },
 {
   "STT": 2330,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Đức Sinh Đường",
   "address": "Căn hộ 306- nhà CT3 - Dự án khu nhà ở để bán, , huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0345864,
   "Latitude": 105.7629924
 },
 {
   "STT": 2331,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Y Duyên",
   "address": "Số 44, dường 800A, tổ 32, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0404099,
   "Latitude": 105.8015303
 },
 {
   "STT": 2332,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Mậu Minh",
   "address": "P806 nhà N3A khu đô thị Trung Hòa - Nhân Chính, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0046545,
   "Latitude": 105.8027037
 },
 {
   "STT": 2333,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Hiệu Thuốc Đông Y Phú Mỹ",
   "address": "Số 56 phố Thuốc Bắc, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0354108,
   "Latitude": 105.848233
 },
 {
   "STT": 2334,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "15 Ngõ 89 Thái Hà, Đống Đa, Hà Nội",
   "Longtitude": 21.0151855,
   "Latitude": 105.8086376
 },
 {
   "STT": 2335,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đỗ Đức Hệ",
   "address": "Số 29, đường Ngô Xuân Quảng, tổ dân phố Chính Trung, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0157873,
   "Latitude": 105.9365092
 },
 {
   "STT": 2336,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Ngân",
   "address": "Ngã Tư xã Vân Canh, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0335796,
   "Latitude": 105.7377527
 },
 {
   "STT": 2337,
   "Name": "Phòng khám Đông Y Đại Đức Đường",
   "address": "Số nhà 12, ngõ 612 đường Trường Chinh, Đống Đa, Hà Nội",
   "Longtitude": 21.0026699,
   "Latitude": 105.821622
 },
 {
   "STT": 2338,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tâm Dược Đường",
   "address": "Số 103 tổ 9 phố Tư Đình, quận Long Biên, Hà Nội",
   "Longtitude": 21.0281836,
   "Latitude": 105.88235
 },
 {
   "STT": 2339,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Quang Luân",
   "address": "Số 97, khu Bình Sơn, Chương Mỹ, Hà Nội",
   "Longtitude": 20.918747,
   "Latitude": 105.7026122
 },
 {
   "STT": 2340,
   "Name": " Phòng Chẩn Trị Y Học Cổ Truyền Vũ Hữu Đức",
   "address": "Lô 11 đường 430, Hà Đông, Hà Nội",
   "Longtitude": 20.978441,
   "Latitude": 105.773795
 },
 {
   "STT": 2341,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vũ Hồng Khương",
   "address": "Số 15, tổ 11A, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9959819,
   "Latitude": 105.8097244
 },
 {
   "STT": 2342,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Nam",
   "address": "Thôn Thường Lệ, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1850027,
   "Latitude": 105.7204476
 },
 {
   "STT": 2343,
   "Name": "Nhà Thuốc Gia Truyền Khương Lâm 1",
   "address": "Số 342 Tây Sơn, quận Đống Đa, Hà Nội",
   "Longtitude": 21.00494,
   "Latitude": 105.822154
 },
 {
   "STT": 2344,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Tuấn",
   "address": "Thôn Tuy Lộc, Phúc Thọ, Hà Nội",
   "Longtitude": 21.0988074,
   "Latitude": 105.5269218
 },
 {
   "STT": 2345,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Bình",
   "address": "Số 14, ngách 25/8, phố Cự Lộc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0008467,
   "Latitude": 105.8138979
 },
 {
   "STT": 2346,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Anh Khuyên",
   "address": ", Chương Mỹ, Hà Nội",
   "Longtitude": 20.8746466,
   "Latitude": 105.6712465
 },
 {
   "STT": 2347,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tống Quang Long",
   "address": ", Chương Mỹ, Hà Nội",
   "Longtitude": 20.8746466,
   "Latitude": 105.6712465
 },
 {
   "STT": 2348,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Đình Voi",
   "address": "Tổ 3, Hà Đông, Hà Nội",
   "Longtitude": 20.9608314,
   "Latitude": 105.7864163
 },
 {
   "STT": 2349,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 121 phố Vĩnh Hồ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0078817,
   "Latitude": 105.8197191
 },
 {
   "STT": 2350,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Đức Thắng",
   "address": "Tập thể 28B (Phòng 7, ngách 28/38) Điện Biên Phủ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0325614,
   "Latitude": 105.8394679
 },
 {
   "STT": 2351,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Thanh",
   "address": "Số 83 Bế Văn Đàn, Hà Đông, Hà Nội",
   "Longtitude": 20.9705072,
   "Latitude": 105.7743355
 },
 {
   "STT": 2352,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Duy Hưng",
   "address": "Số 2, ngõ 44/1, phố Đỗ Quang, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0104271,
   "Latitude": 105.8022447
 },
 {
   "STT": 2353,
   "Name": "Phòng Chẩn Trị Y học cổ truyền Hiệu Xuân Thảo",
   "address": "Số 162 phố Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.034479,
   "Latitude": 105.825634
 },
 {
   "STT": 2354,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Vỹ Sửu",
   "address": "Số 96 Nhuệ Giang, Hà Đông, Hà Nội",
   "Longtitude": 20.9711454,
   "Latitude": 105.7815331
 },
 {
   "STT": 2355,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đào Ngọc Dương",
   "address": "Thôn Hà Lâm 3, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1713853,
   "Latitude": 105.8936224
 },
 {
   "STT": 2356,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Khải",
   "address": "Số 26, tổ 27, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1394195,
   "Latitude": 105.8545155
 },
 {
   "STT": 2357,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 4 Bế Văn Đàn, Hà Đông, Hà Nội",
   "Longtitude": 20.9713619,
   "Latitude": 105.7752961
 },
 {
   "STT": 2358,
   "Name": "Phòng khám Bệnh Bằng Thuốc Nam- Chi Nhánh Hợp Tác Xã Dân Tộc Chùa Bộc",
   "address": "Số 10 phố Lương Văn Can, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0324138,
   "Latitude": 105.8525554
 },
 {
   "STT": 2359,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vĩnh Trụ Đường",
   "address": "Số 256 phố Nguyễn Khoái, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0123804,
   "Latitude": 105.8635765
 },
 {
   "STT": 2360,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bùi Lục Hà",
   "address": "Số 17 phố Nguyễn Ngọc Nại, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9988373,
   "Latitude": 105.8257652
 },
 {
   "STT": 2361,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Vì Cộng Đồng Kim Thiên",
   "address": "Tầng 2, nhà N03, phố Trần Quý Kiên, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0376474,
   "Latitude": 105.7925168
 },
 {
   "STT": 2362,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Linh Đàm Đường",
   "address": "Số 13 BT2 Bán đảo Linh Đàm, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9653572,
   "Latitude": 105.8325856
 },
 {
   "STT": 2363,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Văn Thành",
   "address": "Số 335 đường Ngọc Hồi, khu Chợ, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9476774,
   "Latitude": 105.8443628
 },
 {
   "STT": 2364,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Y Học Cổ Truyền Lê Thái Tôn",
   "address": "Số 65, ngõ 35 Cát Linh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0282707,
   "Latitude": 105.8282557
 },
 {
   "STT": 2365,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 17 ngách 195 ngõ xã Đàn 2, , quận Đống Đa, Hà Nội",
   "Longtitude": 21.0130253,
   "Latitude": 105.8321966
 },
 {
   "STT": 2366,
   "Name": "Hùng Thịnh  Y Học Cổ Truyền",
   "address": "Số 278 phố Phúc Tân, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0353887,
   "Latitude": 105.8555889
 },
 {
   "STT": 2367,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lã Ngọc Tân",
   "address": "Số 27, ngõ 274 đường Trương Định, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9893984,
   "Latitude": 105.8466697
 },
 {
   "STT": 2368,
   "Name": " Bảo Nhân Đường Cơ Sở Gia  Truyền 3 Bài Thuốc: Cam, Thuốc Ho, Thuốc Sâu Răng - Phạm Xuân Toại",
   "address": "Số 32 phố Hàng Bạc, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0329432,
   "Latitude": 105.8528824
 },
 {
   "STT": 2369,
   "Name": "Phòng khám Đông Y Châm Cứu Ngọc Duyên",
   "address": "Số 17 đường Tựu Liệt, Quốc Bảo, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9545972,
   "Latitude": 105.8399583
 },
 {
   "STT": 2370,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 4 ngõ 6 đường Tây Hồ, tổ 28, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0661089,
   "Latitude": 105.8258692
 },
 {
   "STT": 2371,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Xuân Linh Đường",
   "address": "Số 11B ngõ 4 phố Quang Trung, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1370068,
   "Latitude": 105.5031827
 },
 {
   "STT": 2372,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tạ Minh Khang",
   "address": "Thôn Trung Kỳ, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0343834,
   "Latitude": 105.683504
 },
 {
   "STT": 2373,
   "Name": "Phòng Chẩn Trị Đông Y Nhà Thuốc Khương Lâm - Hưng",
   "address": "Số 283 Tây Sơn, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0046545,
   "Latitude": 105.822308
 },
 {
   "STT": 2374,
   "Name": "Phòng Chẩn Trị Y  Học Cổ Truyền Công Ty Cổ Phần Dược Trung Ương Mediplantex",
   "address": "Số 358 đường Giải Phóng, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.987008,
   "Latitude": 105.8407939
 },
 {
   "STT": 2375,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tạ Thị Doãn",
   "address": "Thôn Hậu Ái, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0343294,
   "Latitude": 105.7204934
 },
 {
   "STT": 2376,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Tiến Dũng",
   "address": "Thôn Minh Hiệp 2, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0623358,
   "Latitude": 105.6724132
 },
 {
   "STT": 2377,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Giang",
   "address": ", huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.084855,
   "Latitude": 105.6690963
 },
 {
   "STT": 2378,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Đức",
   "address": "Thuần Nghệ, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1363476,
   "Latitude": 105.5096566
 },
 {
   "STT": 2379,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Trung Dũng",
   "address": "Số 12A thị trấn Công an Đa Sỹ, Hà Đông, Hà Nội",
   "Longtitude": 20.9577076,
   "Latitude": 105.7827747
 },
 {
   "STT": 2380,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đỗ Anh Huy",
   "address": "Số 5 Nguyễn Viết Xuân, Hà Đông, Hà Nội",
   "Longtitude": 20.9699197,
   "Latitude": 105.7740921
 },
 {
   "STT": 2381,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 18 ngõ 172 đường Âu Cơ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.064732,
   "Latitude": 105.830662
 },
 {
   "STT": 2382,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 55 phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.035387,
   "Latitude": 105.848744
 },
 {
   "STT": 2383,
   "Name": "Đông Y Lang Tòng",
   "address": "Tiền Huân, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1347139,
   "Latitude": 105.5151997
 },
 {
   "STT": 2384,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Nhật Tý",
   "address": "Số 8A, ngõ 138, đường Giáp Bát, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.987913,
   "Latitude": 105.842631
 },
 {
   "STT": 2385,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 7 phố Thể Giao, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0137687,
   "Latitude": 105.8477997
 },
 {
   "STT": 2386,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Thuân",
   "address": "Số nhà 103, tổ 23, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9836984,
   "Latitude": 105.8636257
 },
 {
   "STT": 2387,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bùi Quang Bảy",
   "address": "Số 29 ngõ 47 Nam Dư, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9864235,
   "Latitude": 105.8892714
 },
 {
   "STT": 2388,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Quốc  Trinh",
   "address": "Kiốt số 2 trường dạy nghề số 17, khu đô thị Định Công, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9869676,
   "Latitude": 105.8326939
 },
 {
   "STT": 2389,
   "Name": "Cơ Sở Thuốc Đông Y Gia Truyền",
   "address": "Số 26 phố Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0353362,
   "Latitude": 105.8238927
 },
 {
   "STT": 2390,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hải Châu",
   "address": "Xóm 5, Thôn Hoàng 3, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0506076,
   "Latitude": 105.7909724
 },
 {
   "STT": 2391,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Thanh 106",
   "address": "Phòng 106, nhà E6, tập thể Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.021683,
   "Latitude": 105.8150866
 },
 {
   "STT": 2392,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thái Đường",
   "address": "Số 2 Đốc Ngữ, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1435406,
   "Latitude": 105.50592
 },
 {
   "STT": 2393,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trường Xuân Đường",
   "address": "Số 26 Phùng Hưng, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1423921,
   "Latitude": 105.5030042
 },
 {
   "STT": 2394,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Văn Tư",
   "address": "Thuần Nghệ, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1363476,
   "Latitude": 105.5096566
 },
 {
   "STT": 2395,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiện Uy",
   "address": "Số 17 Trần Phú,  Hà Đông, Hà Nội",
   "Longtitude": 20.9827231,
   "Latitude": 105.7908028
 },
 {
   "STT": 2396,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Xuân Hòa",
   "address": "16 Bùi Bằng Đoàn, Hà Đông, Hà Nội",
   "Longtitude": 20.9709148,
   "Latitude": 105.7812652
 },
 {
   "STT": 2397,
   "Name": "Phòng khám Chữa Bệnh Đông Y",
   "address": "Số 25 C Phan Đình Giót, Hà Đông, Hà Nội",
   "Longtitude": 20.9660547,
   "Latitude": 105.7657955
 },
 {
   "STT": 2398,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 236 đường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0448412,
   "Latitude": 105.8686635
 },
 {
   "STT": 2399,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tầng 1, số 137 phố Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0448412,
   "Latitude": 105.8686635
 },
 {
   "STT": 2400,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 94 phố Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.033183,
   "Latitude": 105.907417
 },
 {
   "STT": 2401,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Chúc Thọ",
   "address": "Số 4 phố Trường Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0599722,
   "Latitude": 105.8935787
 },
 {
   "STT": 2402,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 94 phố Sài Đồng, quận Long Biên, Hà Nội",
   "Longtitude": 21.033183,
   "Latitude": 105.907417
 },
 {
   "STT": 2403,
   "Name": "Phòng Chẩn Trị Y Học Dân Tộc  Hồng Hưng ",
   "address": "Số 38 phố Hàng Cân, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.034369,
   "Latitude": 105.84978
 },
 {
   "STT": 2404,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nội - Lộc",
   "address": "Số 69 phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.035409,
   "Latitude": 105.8483523
 },
 {
   "STT": 2405,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Ông Lang Cường",
   "address": "Số 225 tổ 12 Yên Lãng, Đống Đa, Hà Nội",
   "Longtitude": 21.0136607,
   "Latitude": 105.8183633
 },
 {
   "STT": 2406,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Tiến Cường - 64",
   "address": "Số 28 ngõ 176 đường Trương Định, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9913508,
   "Latitude": 105.8479052
 },
 {
   "STT": 2407,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khương Lâm Sinh",
   "address": "Số 454 đường Lê Duẩn, Đống Đa, Hà Nội",
   "Longtitude": 21.007832,
   "Latitude": 105.841279
 },
 {
   "STT": 2408,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thiên Quyến",
   "address": "Số 8, đường Yên Phụ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.051316,
   "Latitude": 105.841279
 },
 {
   "STT": 2409,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Dương Tuấn Dũng",
   "address": "Số 2 phố Thể Giao, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0137687,
   "Latitude": 105.8477997
 },
 {
   "STT": 2410,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Nguyên Hồng",
   "address": "Số 46, ngách 29, ngõ 191, đường Lạc Long Quân, tổ 10, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0514843,
   "Latitude": 105.8058753
 },
 {
   "STT": 2411,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 175 khu giãn dân, , Hà Đông, Hà Nội",
   "Longtitude": 20.955835,
   "Latitude": 105.7563658
 },
 {
   "STT": 2412,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Hoàng 4, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0582781,
   "Latitude": 105.7837165
 },
 {
   "STT": 2413,
   "Name": "Cơ Sở Đông Y Gia Truyền Long Lâm",
   "address": "Số 64, xóm 8, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.046723,
   "Latitude": 105.7484312
 },
 {
   "STT": 2414,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Đức Triển",
   "address": "Phòng 104, nhà E8, tập thể Thanh Xuân Bắc, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0220463,
   "Latitude": 105.8124494
 },
 {
   "STT": 2415,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Chợ Phú Túc, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7649218,
   "Latitude": 105.8107066
 },
 {
   "STT": 2416,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Sinh Sinh Đường",
   "address": "Số 94 đường Láng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0043201,
   "Latitude": 105.8185379
 },
 {
   "STT": 2417,
   "Name": "Chẩn Trị Y Học Cổ Truyền Sinh Sinh Đường",
   "address": "Số 86C phố Tuệ Tĩnh, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151208,
   "Latitude": 105.8495714
 },
 {
   "STT": 2418,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tư Nhân Hồng Phúc Đường",
   "address": "Số 12 ngách 29/10, phố Đội Nhân, quận Ba Đình, Hà Nội",
   "Longtitude": 21.040997,
   "Latitude": 105.8113602
 },
 {
   "STT": 2419,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tư Nhân Phúc Minh Đường",
   "address": "Số 66 phố Đốc Ngữ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0418,
   "Latitude": 105.814
 },
 {
   "STT": 2420,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Y Dược Tinh Hoa Ld Hàn Việt",
   "address": "Số 442 đường Bưởi, quận Ba Đình, Hà Nội",
   "Longtitude": 21.042215,
   "Latitude": 105.806314
 },
 {
   "STT": 2421,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền An Tĩnh",
   "address": "Số 12 phố Nguyễn Thượng Hiền, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0188411,
   "Latitude": 105.842216
 },
 {
   "STT": 2422,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 263 phố chợ Khâm Thiên, Đống Đa, Hà Nội",
   "Longtitude": 21.013787,
   "Latitude": 105.836536
 },
 {
   "STT": 2423,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tư Nhân Kinh Bắc",
   "address": "Số 263 phố chợ Khâm Thiên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0156404,
   "Latitude": 105.8376558
 },
 {
   "STT": 2424,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Bình",
   "address": "Số 5, ngõ 27, phố Vạn Bảo, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0341986,
   "Latitude": 105.8172682
 },
 {
   "STT": 2425,
   "Name": " Đinh Thái Hoàn Chuyên khoa Gia Truyền Chữa Trị Bệnh Gan",
   "address": "Số 01 phố Tràng Thi, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0267656,
   "Latitude": 105.8482933
 },
 {
   "STT": 2426,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 15 ngõ 9 Lương Đình Của, quận Đống Đa, Hà Nội",
   "Longtitude": 21.007656,
   "Latitude": 105.83443
 },
 {
   "STT": 2427,
   "Name": "Khám Chữa Bệnh Y Học Cổ Truyền  Vĩnh Hà",
   "address": "Số 55B phố Hàng Cót, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0369361,
   "Latitude": 105.847208
 },
 {
   "STT": 2428,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 12 ngõ 318 Đê La Thành, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0220426,
   "Latitude": 105.8241932
 },
 {
   "STT": 2429,
   "Name": "Gia Truyền Chữa Viêm Xoang, Viêm Mũi - Lương Y: Đỗ Thị Xuyến",
   "address": "Số 19 ngõ Trung Yên, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0334921,
   "Latitude": 105.8522688
 },
 {
   "STT": 2430,
   "Name": "Minh Đạo Y Gia Đường",
   "address": "Số 343 đường La Thành, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0228708,
   "Latitude": 105.8215844
 },
 {
   "STT": 2431,
   "Name": "Đinh Thái Hoàn Chuyên khoa Gia Truyền Chữa Trị Bệnh Gan",
   "address": "Số 01 phố Tràng Thi, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0267656,
   "Latitude": 105.8482933
 },
 {
   "STT": 2432,
   "Name": "Phòng khám Chữa Bệnh Y Học Cổ Truyền An Sinh",
   "address": "Số 90 ngõ 64 đường Kim Giang, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9817227,
   "Latitude": 105.8130759
 },
 {
   "STT": 2433,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Hòa Đường",
   "address": "Số 95 Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.034713,
   "Latitude": 105.827679
 },
 {
   "STT": 2434,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Bá Ngọc",
   "address": "Số 9, ngõ 97, đường Hoàng Hoa Thám, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0392899,
   "Latitude": 105.8158021
 },
 {
   "STT": 2435,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lương Y: Trần Văn Quảng",
   "address": "40 (trên gác) phố Bát Đàn, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.03371,
   "Latitude": 105.8469731
 },
 {
   "STT": 2436,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Tống Trần Luân",
   "address": "P106-C6 tập thể Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0090668,
   "Latitude": 105.8336803
 },
 {
   "STT": 2437,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Danh Phương",
   "address": "Số 595 phố Kim Ngưu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025637,
   "Latitude": 105.8617727
 },
 {
   "STT": 2438,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Cao Văn Sơn",
   "address": "Thôn An Mỹ, Chương Mỹ, Hà Nội",
   "Longtitude": 20.8381214,
   "Latitude": 105.7071451
 },
 {
   "STT": 2439,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Trung Việt",
   "address": "Số 31 ngõ 28B phố Hạ Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0028918,
   "Latitude": 105.8355282
 },
 {
   "STT": 2440,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Đại",
   "address": "Thôn Đoàn Kết, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.04679,
   "Latitude": 105.505885
 },
 {
   "STT": 2441,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Đắc Lương",
   "address": "Khu 5, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.064972,
   "Latitude": 105.704941
 },
 {
   "STT": 2442,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Định An Đường",
   "address": "Số 2A ngõ 1 ngách 1/4 phố Bùi Xương Trạch, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9887717,
   "Latitude": 105.8180319
 },
 {
   "STT": 2443,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Sơn",
   "address": "Số 6, ngách 132/64, phố Khương Trung, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.993637,
   "Latitude": 105.815247
 },
 {
   "STT": 2444,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thu Đạt",
   "address": "Số 112 phố Định Công, Thanh Xuân, Hà Nội",
   "Longtitude": 20.9834471,
   "Latitude": 105.8216982
 },
 {
   "STT": 2445,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Văn Miền",
   "address": "Cụm 8, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.127381,
   "Latitude": 105.6480087
 },
 {
   "STT": 2446,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Hữu Lễ",
   "address": "Thôn Nhật Tiến, Chương Mỹ, Hà Nội",
   "Longtitude": 20.91782,
   "Latitude": 105.6423
 },
 {
   "STT": 2447,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đỗ Sỹ Điệt",
   "address": "Thôn Phú Hữu I, Chương Mỹ, Hà Nội",
   "Longtitude": 20.9359513,
   "Latitude": 105.6559158
 },
 {
   "STT": 2448,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Hữu Vinh",
   "address": "Thôn 4, , huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0522745,
   "Latitude": 105.6024471
 },
 {
   "STT": 2449,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khuất Duy Thân",
   "address": "Khu Chợ, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0841388,
   "Latitude": 105.5661464
 },
 {
   "STT": 2450,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 19 ngách 3/24 ngõ 3 Thái Hà, quận Đống Đa, Hà Nội",
   "Longtitude": 21.009093,
   "Latitude": 105.8226208
 },
 {
   "STT": 2451,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bảo Long Trực Thuộc Công Ty Tnhh Đông Nam Dược Bảo Long",
   "address": "Số nhà 95 đường Hoàng Quốc Việt, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0461118,
   "Latitude": 105.8014372
 },
 {
   "STT": 2452,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đức Sinh Bình",
   "address": "Số nhà 1, ngách 81/24/40, đường Lạc Long Quân, tổ 18, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.3044629,
   "Latitude": 105.4012011
 },
 {
   "STT": 2453,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "137 phố Pháo Đài Láng, Đống Đa, Hà Nội",
   "Longtitude": 21.019333,
   "Latitude": 105.805588
 },
 {
   "STT": 2454,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Phương",
   "address": "Phòng 404, nhà C1, số 34A, phố Trần Phú, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0317267,
   "Latitude": 105.8432202
 },
 {
   "STT": 2455,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đức Hưng",
   "address": "Số 70 Hàng Than, quận Ba Đình, Hà Nội",
   "Longtitude": 21.040387,
   "Latitude": 105.847573
 },
 {
   "STT": 2456,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tuấn Ngọc",
   "address": "Xóm 3, Thôn Hạ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.111355,
   "Latitude": 105.7904836
 },
 {
   "STT": 2457,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Ngọc Lâm",
   "address": "Đội 2, xóm Kho, Thôn Phú Mỹ, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0308351,
   "Latitude": 105.7739716
 },
 {
   "STT": 2458,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Linh",
   "address": "Xóm 7, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9831411,
   "Latitude": 105.9156599
 },
 {
   "STT": 2459,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Thu",
   "address": "Tại nhà, Thôn Trung Dương, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9812664,
   "Latitude": 105.9519017
 },
 {
   "STT": 2460,
   "Name": "Cơ Sở Gia Truyền Phong Thấp, Bổ Huyết Vinh Cân -  Phúc Thái",
   "address": "Số 58 phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0352123,
   "Latitude": 105.8484798
 },
 {
   "STT": 2461,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Ngọc Dũng",
   "address": "Gác 3, nhà 162 Phố Huế, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0149415,
   "Latitude": 105.8515827
 },
 {
   "STT": 2462,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Đình Thuyên",
   "address": "Số 4 N7 (24A) tập thể quân y 108, phố Vĩnh Tuy, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9951234,
   "Latitude": 105.8754535
 },
 {
   "STT": 2463,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Thị Thuần",
   "address": "Tập thể bao bì xuất khẩu, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.994941,
   "Latitude": 105.8906859
 },
 {
   "STT": 2464,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đào Xương",
   "address": "Số 75, tổ 12, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9836984,
   "Latitude": 105.8636257
 },
 {
   "STT": 2465,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Y Thiên Đức",
   "address": "Số 166, đường Lĩnh Nam, tổ 18, khu 5, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.988309,
   "Latitude": 105.866468
 },
 {
   "STT": 2466,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lương Y Nguyễn Đăng Tĩnh",
   "address": "Xóm 6, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9611634,
   "Latitude": 105.9031664
 },
 {
   "STT": 2467,
   "Name": "Phòng Chẩn Trị Y  Học Cổ Truyền Vĩnh An Đường Trực Thuộc Chi Nhánh Công Ty Tnhh Quang Thắng",
   "address": "298 Nguyễn Trãi, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.996676,
   "Latitude": 105.810917
 },
 {
   "STT": 2468,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Yến - 70",
   "address": "P106 - A6, số 257 phố Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0034159,
   "Latitude": 105.8513053
 },
 {
   "STT": 2469,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Đức Hồng",
   "address": "Tổ 10, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9902111,
   "Latitude": 105.8452833
 },
 {
   "STT": 2470,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Chu Xuân Trường",
   "address": "Số 1, ngõ 1 đường Lĩnh Nam, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9819356,
   "Latitude": 105.8797073
 },
 {
   "STT": 2471,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đỗ Văn Cấn",
   "address": "Số nhà 46, tổ 18, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.046723,
   "Latitude": 105.7484312
 },
 {
   "STT": 2472,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khương Lâm",
   "address": "Số 342 phố Tây Sơn, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0128373,
   "Latitude": 105.8268249
 },
 {
   "STT": 2473,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Bình",
   "address": "Số 64 đường Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.970591,
   "Latitude": 105.7751086
 },
 {
   "STT": 2474,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hà Đình Tấn",
   "address": "Số 3, ngách 102/32, phố Hoàng Đạo Thành, , quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9842682,
   "Latitude": 105.8107343
 },
 {
   "STT": 2475,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "B12 - BT6 khu đô thị Văn Quán, Yên Phúc, quận Hà Đông, Hà Nội",
   "Longtitude": 20.970333,
   "Latitude": 105.7880497
 },
 {
   "STT": 2476,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiên Bảo",
   "address": "Số 46 Bà Triệu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9684244,
   "Latitude": 105.7816903
 },
 {
   "STT": 2477,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thuốc Cam Gia Truyền Hiệu Cá Vàng",
   "address": "Số 17, phố Yên Hòa, tổ 32, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.029457,
   "Latitude": 105.784013
 },
 {
   "STT": 2478,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tế Mỹ Đường",
   "address": "Nhà số 9, ngõ 18, tổ 25, phố Yên Hòa, , quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0219136,
   "Latitude": 105.7978258
 },
 {
   "STT": 2479,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiện Tâm Đường",
   "address": "Phòng 107- C2 tập thể Vĩnh Hồ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0050226,
   "Latitude": 105.8202186
 },
 {
   "STT": 2480,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 372 Khâm Thiên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.019412,
   "Latitude": 105.831706
 },
 {
   "STT": 2481,
   "Name": "Phòng Chẩn Trị Đông Y",
   "address": "Số 36/2 ngõ 1 An Dương Vương - Tổ 47B cụm 7, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.089282,
   "Latitude": 105.808685
 },
 {
   "STT": 2482,
   "Name": "Thuốc Cam Hươu Sao Vàng Ngọc Kim",
   "address": "Số 9 phố Hòe Nhai, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0423197,
   "Latitude": 105.8479634
 },
 {
   "STT": 2483,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiên Ân",
   "address": "Căn hộ 114-G3B tập thể Quân đội Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0231845,
   "Latitude": 105.8141743
 },
 {
   "STT": 2484,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Đức Hồng",
   "address": "Tổ 10, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9902111,
   "Latitude": 105.8452833
 },
 {
   "STT": 2485,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 166 phố Chùa Láng (cũ: số 151 tổ 10), quận Đống Đa, Hà Nội",
   "Longtitude": 21.0235829,
   "Latitude": 105.8052281
 },
 {
   "STT": 2486,
   "Name": "Phòng khám Đông Y An Thái Trực Thuộc Công Ty Tnhh Y Dược Thiên Hương",
   "address": "Số 58 phố Sơn Tây, quận Ba Đình, Hà Nội",
   "Longtitude": 21.032987,
   "Latitude": 105.831247
 },
 {
   "STT": 2487,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 45 phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0353424,
   "Latitude": 105.853301
 },
 {
   "STT": 2488,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Y Phương",
   "address": "Số nhà 3, khu biệt thự IV, Pháp Vân, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9537375,
   "Latitude": 105.8498226
 },
 {
   "STT": 2489,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Huy Tiến - Khám Chữa Bệnh Ngoài Da",
   "address": "15 phố Hàng Cót, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0381363,
   "Latitude": 105.8469534
 },
 {
   "STT": 2490,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Toàn",
   "address": "Số nhà 69, phố Nguyễn Thái Học, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0292679,
   "Latitude": 105.840052
 },
 {
   "STT": 2491,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Toàn",
   "address": "Số nhà 69, phố Nguyễn Thái Học, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0292679,
   "Latitude": 105.840052
 },
 {
   "STT": 2492,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Hải Chừng",
   "address": "Số 417, phố Ga, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.7714771,
   "Latitude": 105.901304
 },
 {
   "STT": 2493,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Xuân An",
   "address": "Thôn Kim Hoàng, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.040578,
   "Latitude": 105.7277431
 },
 {
   "STT": 2494,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bùi Cao Síu",
   "address": "Thôn Lũng Kênh, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.057309,
   "Latitude": 105.703494
 },
 {
   "STT": 2495,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vương Thị Phương Nghi",
   "address": "Thôn 8, huyện Thạch Thất, Hà Nội",
   "Longtitude": 21.0228544,
   "Latitude": 105.5161285
 },
 {
   "STT": 2496,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 62 dốc Bệnh Viện, Thôn Thanh Ấm, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 21.0277644,
   "Latitude": 105.8341598
 },
 {
   "STT": 2497,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lâm Thị Nhàn",
   "address": "Khu tái định cư QL32, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0470659,
   "Latitude": 105.7632082
 },
 {
   "STT": 2498,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Ngọc Lan",
   "address": ", huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.084855,
   "Latitude": 105.6690963
 },
 {
   "STT": 2499,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khương Lâm Hải",
   "address": "Số 9 ngõ 630 đường Trường Chinh, quận Đống Đa, Hà Nội",
   "Longtitude": 21.00275,
   "Latitude": 105.821299
 },
 {
   "STT": 2500,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Mạnh Hùng",
   "address": "Số 24 Phan Đình Phùng, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0407149,
   "Latitude": 105.8432575
 },
 {
   "STT": 2501,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nông Đình Bang",
   "address": "Số 90, phố Nguyễn An Ninh, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9902111,
   "Latitude": 105.8452833
 },
 {
   "STT": 2502,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 14 Nguyễn Như Đổ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.026545,
   "Latitude": 105.839357
 },
 {
   "STT": 2503,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đức Thọ",
   "address": "Thôn Thọ Am, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9180785,
   "Latitude": 105.8616885
 },
 {
   "STT": 2504,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền An Thọ Đường",
   "address": "Số 61, ngõ 26 Tư Đình, quận Long Biên, Hà Nội",
   "Longtitude": 21.0277867,
   "Latitude": 105.882344
 },
 {
   "STT": 2505,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Hoài Văn",
   "address": "Tổ dân phố số 06, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1753431,
   "Latitude": 105.7308045
 },
 {
   "STT": 2506,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Dược Đại An",
   "address": "Số 455 đường Giải Phóng, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9940248,
   "Latitude": 105.8412901
 },
 {
   "STT": 2507,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bác sĩ Vũ Xuân Bình",
   "address": "Số 82 phố Thợ Nhuộm, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0230265,
   "Latitude": 105.8475182
 },
 {
   "STT": 2508,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đường Lâm Viện",
   "address": "Tầng 2 số nhà 12 phố Trịnh Hoài Đức, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0208131,
   "Latitude": 105.8321259
 },
 {
   "STT": 2509,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Sinh Đường",
   "address": "Số 2, A28, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0455756,
   "Latitude": 105.793761
 },
 {
   "STT": 2510,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Thị Ngọc Diệp",
   "address": "Số 11 ngõ Thanh Miến, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0292816,
   "Latitude": 105.8378005
 },
 {
   "STT": 2511,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tiến Đạt",
   "address": "Số 13A phố Châu Long, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0445519,
   "Latitude": 105.8417677
 },
 {
   "STT": 2512,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Võ Thị Hồng Phương",
   "address": "Số 102 phố Triệu Việt Vương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0156639,
   "Latitude": 105.85025
 },
 {
   "STT": 2513,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Khám Chữa Bệnh Gia Truyền Bà Lang Thịnh",
   "address": "Số 28 (cũ:9) phố Phan Phù Tiên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0283429,
   "Latitude": 105.8338303
 },
 {
   "STT": 2514,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tiên Hương",
   "address": "P108B - B1 (cũ: P32B - B1) tập thể Khương Thượng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0171104,
   "Latitude": 105.8131718
 },
 {
   "STT": 2515,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhân Thọ Đường 2",
   "address": "Thôn Phương Nhị, , huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9089545,
   "Latitude": 105.8604857
 },
 {
   "STT": 2516,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Y Dược Tinh Hoa",
   "address": "A24 - thị trấn 18 Khu đô thị Văn Quán, Yên Phúc, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9709163,
   "Latitude": 105.790719
 },
 {
   "STT": 2517,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Xuân Hậu",
   "address": "Số 16 Bùi Bằng Đoàn, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9709148,
   "Latitude": 105.7812652
 },
 {
   "STT": 2518,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đào Ngọc Dương",
   "address": "Số 9 tổ 31, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1245303,
   "Latitude": 105.8271398
 },
 {
   "STT": 2519,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đào Thị Hiến",
   "address": "Thôn Trung Văn, huyện Từ Liêm, Hà Nội",
   "Longtitude": 20.9896503,
   "Latitude": 105.7855378
 },
 {
   "STT": 2520,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Thụy Tường",
   "address": "Số nhà 14, ngõ 131, phố Phượng Trì, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.089302,
   "Latitude": 105.6612314
 },
 {
   "STT": 2521,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Thị Lan Anh",
   "address": "Xóm Thống Nhất, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0580649,
   "Latitude": 105.6684401
 },
 {
   "STT": 2522,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đinh Công Bằng",
   "address": "Thôn Sơn Hà, huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1208466,
   "Latitude": 105.3833808
 },
 {
   "STT": 2523,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2524,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Văn Bình",
   "address": "Số 88, tổ 10, khu Xuân Mai, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.896292,
   "Latitude": 105.5787167
 },
 {
   "STT": 2525,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lương Y Vũ Quốc Trung",
   "address": "Số nhà 14A ngõ 538 đường Láng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.012911,
   "Latitude": 105.810945
 },
 {
   "STT": 2526,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 13 phố Đỗ Hạnh, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0196452,
   "Latitude": 105.8419096
 },
 {
   "STT": 2527,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Tuất",
   "address": "P6, số 33 Nguyễn Bỉnh Khiêm, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151954,
   "Latitude": 105.8483727
 },
 {
   "STT": 2528,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Minh Quang",
   "address": "Số 10D phố Lý Nam Đế, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0348557,
   "Latitude": 105.8449486
 },
 {
   "STT": 2529,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đào Tiên",
   "address": "Số 908 đường Láng, tổ 11, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0212593,
   "Latitude": 105.8013595
 },
 {
   "STT": 2530,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khương Sơn",
   "address": "Số 15 đường Láng (cũ: số 27 ngõ 35 đường Láng), quận Đống Đa, Hà Nội",
   "Longtitude": 21.0119001,
   "Latitude": 105.8093579
 },
 {
   "STT": 2531,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Thị Thanh Hương",
   "address": "Số 80, ngõ Núi Trúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0301317,
   "Latitude": 105.8254187
 },
 {
   "STT": 2532,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhân Hòa",
   "address": "Số 32, ngõ 44 phố Nguyễn Đình Chiểu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.014529,
   "Latitude": 105.8470025
 },
 {
   "STT": 2533,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bác sĩ Hồ Xuân Ba",
   "address": "Số 70 phố Trần Quốc Toản, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": null,
   "Latitude": null
 },
 {
   "STT": 2534,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hành Thiện",
   "address": "Số 63B phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0352123,
   "Latitude": 105.8484798
 },
 {
   "STT": 2535,
   "Name": "Nhà Thuốc Đông Y Gia Truyền Lam Kiều",
   "address": "Số 178C phố Nguyễn Lương Bằng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0161877,
   "Latitude": 105.8286036
 },
 {
   "STT": 2536,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phạm Quốc Toán",
   "address": "Số 21 Dốc Tam Đa, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0428004,
   "Latitude": 105.8187512
 },
 {
   "STT": 2537,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Quảng Lợi",
   "address": "Số 18 phố Đồng Xuân, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.038787,
   "Latitude": 105.8494992
 },
 {
   "STT": 2538,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Y Học Cổ Truyền Chùa Bộc",
   "address": "Số 16 phố Chùa Bộc, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0076731,
   "Latitude": 105.8279595
 },
 {
   "STT": 2539,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Huy Kiền",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2540,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đinh Ngọc Lê",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2541,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Thị Phương",
   "address": "Số 20 Trần Nhật Duật, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9730327,
   "Latitude": 105.7744496
 },
 {
   "STT": 2542,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khương Viên",
   "address": "Số 341 phố Tây Sơn, quận Đống Đa, Hà Nội",
   "Longtitude": 21.003896,
   "Latitude": 105.8217766
 },
 {
   "STT": 2543,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khương Viên",
   "address": "Số 341 phố Tây Sơn, quận Đống Đa, Hà Nội",
   "Longtitude": 21.003896,
   "Latitude": 105.8217766
 },
 {
   "STT": 2544,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Phú Cường",
   "address": "Số 08 Trần Hưng Đạo, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9707341,
   "Latitude": 105.7795844
 },
 {
   "STT": 2545,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 9/A2 tổ 8, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9836984,
   "Latitude": 105.8636257
 },
 {
   "STT": 2546,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn khám",
   "address": "Số 1/120, ngõ 72, đường Nguyễn Trãi, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0020214,
   "Latitude": 105.8170799
 },
 {
   "STT": 2547,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bảo Hòa - Gia Truyền Thuốc Dạ Dày",
   "address": "Số 15, phố Bát Đàn, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.03371,
   "Latitude": 105.8469731
 },
 {
   "STT": 2548,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Y Gia",
   "address": "Số 35 ngõ Nguyễn Công Trứ (số cũ 7), quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0124503,
   "Latitude": 105.8536989
 },
 {
   "STT": 2549,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Y Tô",
   "address": "Số 17 phố Trần Xuân Soạn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0168688,
   "Latitude": 105.8547903
 },
 {
   "STT": 2550,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Y Tô",
   "address": "Số 17 phố Trần Xuân Soạn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0168688,
   "Latitude": 105.8547903
 },
 {
   "STT": 2551,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Htcac",
   "address": "Số 45 phố Phạm Hồng Thái, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0437994,
   "Latitude": 105.8446234
 },
 {
   "STT": 2552,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nghĩa Hưng Long",
   "address": "Số nhà 160, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.9924634,
   "Latitude": 105.6404254
 },
 {
   "STT": 2553,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thúy Hòa",
   "address": "Số 02 Tản Đà, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9689932,
   "Latitude": 105.7778933
 },
 {
   "STT": 2554,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Quý Nhân Đường",
   "address": "Số 40 Nhuệ Giang, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9722145,
   "Latitude": 105.7808411
 },
 {
   "STT": 2555,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Ô 18, LK11 đô thị Mỗ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9870449,
   "Latitude": 105.784677
 },
 {
   "STT": 2556,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền 138 Giảng Võ - Hà Nội",
   "address": "Số 138, phố Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0290986,
   "Latitude": 105.8262639
 },
 {
   "STT": 2557,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bác sĩ Đỗ Gia Thịnh",
   "address": "Số 83 phố Thuốc Bắc, quận, Hà Nội",
   "Longtitude": 21.0344365,
   "Latitude": 105.8482458
 },
 {
   "STT": 2558,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Cao Thành Năm",
   "address": "Đội 1, Thôn Tranh Khúc, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.924974,
   "Latitude": 105.8888109
 },
 {
   "STT": 2559,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Tuyển",
   "address": " Gia Trung, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1926095,
   "Latitude": 105.7776653
 },
 {
   "STT": 2560,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lâm Tùng",
   "address": "Số 616, đường Hà Huy Tập, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.0861118,
   "Latitude": 105.9179985
 },
 {
   "STT": 2561,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phùng Đức Đỗ",
   "address": "Chợ Vồi, huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8656817,
   "Latitude": 105.8676672
 },
 {
   "STT": 2562,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhân Tâm",
   "address": "P110 C2, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9959819,
   "Latitude": 105.8097244
 },
 {
   "STT": 2563,
   "Name": "Phòng khám Đông Y Sơn Thế Đức",
   "address": "Ki-ốt số 3, tầng 1, tòa nhà CT1 trung tầng, khu đô thị Mỹ Đình, Mễ Trì, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0165182,
   "Latitude": 105.7795285
 },
 {
   "STT": 2564,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thị Thắng",
   "address": "Ngã Tư, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0705662,
   "Latitude": 105.7064743
 },
 {
   "STT": 2565,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Cấn Xuân Quý",
   "address": "Số 122 Lê Lợi, thị xã Sơn Tây, Hà Nội",
   "Longtitude": 21.1447911,
   "Latitude": 105.5055992
 },
 {
   "STT": 2566,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đinh Thị Huệ",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2567,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lý Văn Nguyên",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2568,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Văn Trọng",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2569,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Công Hạnh",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2570,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Loan",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2571,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Hội",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2572,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 40 ngõ 12 Trung Phụng, tổ 45B, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0151077,
   "Latitude": 105.8385705
 },
 {
   "STT": 2573,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phương Đông",
   "address": "Số 03, ngõ huyện, , quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.029692,
   "Latitude": 105.848999
 },
 {
   "STT": 2574,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Lê Danh Thoa",
   "address": "Thôn Hậu Ái, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.0343294,
   "Latitude": 105.7204934
 },
 {
   "STT": 2575,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Hồng Siêm",
   "address": "339 Quang Trung, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9655608,
   "Latitude": 105.7694037
 },
 {
   "STT": 2576,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Phương Đường",
   "address": "Số 18, LK13 đô thị Văn Phú, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9598391,
   "Latitude": 105.7643176
 },
 {
   "STT": 2577,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Trọng ",
   "address": "Thôn Đình Vỹ, huyện Gia Lâm, Hà Nội",
   "Longtitude": 21.1107186,
   "Latitude": 105.9295984
 },
 {
   "STT": 2578,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phòng 109 - G1 Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0095739,
   "Latitude": 105.8351123
 },
 {
   "STT": 2579,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Đông Y An Việt",
   "address": "Số 202 phố Lê Trọng Tấn, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9938395,
   "Latitude": 105.8318009
 },
 {
   "STT": 2580,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 714D Lạc Long Quân, tổ 16 cụm 2, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0803791,
   "Latitude": 105.8175857
 },
 {
   "STT": 2581,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Y Học Cổ Truyền Taytang",
   "address": "Số 45-thị trấn 4 khu đô thị Mỹ Đình, huyện Từ Liêm, Hà Nội",
   "Longtitude": 21.0312482,
   "Latitude": 105.7694511
 },
 {
   "STT": 2582,
   "Name": "Nhà Thuốc Gia Truyền Cầu Bây",
   "address": "Số 337 đường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0477968,
   "Latitude": 105.8735902
 },
 {
   "STT": 2583,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Lương",
   "address": "Số 192 phố Đức Giang, quận Long Biên, Hà Nội",
   "Longtitude": 21.0660138,
   "Latitude": 105.881559
 },
 {
   "STT": 2584,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "P9-C28, tập thể Kim Giang, ngõ 82, phố Hoàng Đạo Thành, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9837135,
   "Latitude": 105.8128877
 },
 {
   "STT": 2585,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đỗ Duy Thực                                                                                                                                                                                                                                                                                                                                                                              ",
   "address": "Xóm Thượng Khê, Thôn Ngọc Than, huyện Quốc Oai, Hà Nội",
   "Longtitude": 20.9643107,
   "Latitude": 105.6122966
 },
 {
   "STT": 2586,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Sơn Đường",
   "address": "Số 33 ngõ An Trạch 2, quận Đống Đa, Hà Nội",
   "Longtitude": 21.028457,
   "Latitude": 105.8316098
 },
 {
   "STT": 2587,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Số 1",
   "address": "Số 3, ngõ 25, phố Nguyễn Chí Thanh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0229461,
   "Latitude": 105.8098437
 },
 {
   "STT": 2588,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 24 đường Giải Phóng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0021881,
   "Latitude": 105.8412069
 },
 {
   "STT": 2589,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số nhà 19, ngõ 61, phố Nguyễn Thái Học, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.029144,
   "Latitude": 105.840503
 },
 {
   "STT": 2590,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Tổ 6, huyện Sóc Sơn, Hà Nội",
   "Longtitude": 21.2538916,
   "Latitude": 105.8491233
 },
 {
   "STT": 2591,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Thành Y Quán",
   "address": "Số 11 ngõ Trạm, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.030803,
   "Latitude": 105.845848
 },
 {
   "STT": 2592,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trọng Nghĩa Đường",
   "address": "Số 3 ngách 4/22 phố Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0012748,
   "Latitude": 105.8393044
 },
 {
   "STT": 2593,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 586 Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.048519,
   "Latitude": 105.80779
 },
 {
   "STT": 2594,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Ngọc Liên Đường",
   "address": "Số 43 ngõ 77 tổ 14 Cụm 2, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0624668,
   "Latitude": 105.8059701
 },
 {
   "STT": 2595,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tiến Hoa",
   "address": "Tiểu khu Phú Thịnh, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7826366,
   "Latitude": 105.9143337
 },
 {
   "STT": 2596,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 237D phố Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 2597,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tiểu khu Thao Chính, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.74565,
   "Latitude": 105.911
 },
 {
   "STT": 2598,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Lô 80 Khu giãn dân Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9765453,
   "Latitude": 105.7896797
 },
 {
   "STT": 2599,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 20 ngõ Trại Giăng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9938424,
   "Latitude": 105.8480043
 },
 {
   "STT": 2600,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 9, ngách 19/60, phố Mai Động, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9906233,
   "Latitude": 105.8614635
 },
 {
   "STT": 2601,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Bảo Gia",
   "address": "Số 170 đường Phương Liệt, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9842138,
   "Latitude": 105.8437816
 },
 {
   "STT": 2602,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 20, ngõ 167 đường Giải Phóng, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0028719,
   "Latitude": 105.8521449
 },
 {
   "STT": 2603,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Lộc Cao",
   "address": "Số 174 phố Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.99833,
   "Latitude": 105.8552922
 },
 {
   "STT": 2604,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Khán Lân Đường",
   "address": "Số 296 phố Vĩnh Hưng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9892354,
   "Latitude": 105.8745359
 },
 {
   "STT": 2605,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 1A, B11, tập thể Công ty Xây dựng số 1, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9954183,
   "Latitude": 105.7992904
 },
 {
   "STT": 2606,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Hưng Đường",
   "address": "Số nhà 111 A1 phố Nguyễn Quý Đức, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9930093,
   "Latitude": 105.7969384
 },
 {
   "STT": 2607,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Xóm Chợ, huyện Đông Anh, Hà Nội",
   "Longtitude": 21.1180116,
   "Latitude": 105.8705556
 },
 {
   "STT": 2608,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Tây",
   "address": "Số 10 đường 7 Tập thể F361, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0338618,
   "Latitude": 105.8495876
 },
 {
   "STT": 2609,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Phương",
   "address": "Số 36 ngõ 188 phố Vương Thừa Vũ, tổ 70, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9967918,
   "Latitude": 105.8217638
 },
 {
   "STT": 2610,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Hạnh Đàn, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0811921,
   "Latitude": 105.7145801
 },
 {
   "STT": 2611,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 60 phố Hàng Chuối, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0076776,
   "Latitude": 105.8683845
 },
 {
   "STT": 2612,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 127 phố Triệu Việt Vương, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0170436,
   "Latitude": 105.8504523
 },
 {
   "STT": 2613,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền An Dược",
   "address": "Số 115 đường Hoàng Ngân, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0078696,
   "Latitude": 105.8090847
 },
 {
   "STT": 2614,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tổ dân phố 3, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9724114,
   "Latitude": 105.7615252
 },
 {
   "STT": 2615,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Đại Việt Diên Đức",
   "address": "Số nhà 136F Trấn Vũ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0451885,
   "Latitude": 105.8410084
 },
 {
   "STT": 2616,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Hưng",
   "address": "Số 86 phố Tuệ Tĩnh, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151325,
   "Latitude": 105.8492108
 },
 {
   "STT": 2617,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tuệ Tâm Đường",
   "address": "Phòng 121, nhà A, tập thề Nghĩa Đô, tổ 31, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0438759,
   "Latitude": 105.802612
 },
 {
   "STT": 2618,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 76, ngách 475/20, đường Nguyễn Trãi, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9876154,
   "Latitude": 105.8004034
 },
 {
   "STT": 2619,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số nhà 19 ngõ 216 Đê La Thành, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0053575,
   "Latitude": 105.833574
 },
 {
   "STT": 2620,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tuệ Tâm Đường",
   "address": "Số 2 ngách 102/22 phố Tô Vĩnh Diện, (92A tổ 12), quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.99897,
   "Latitude": 105.820357
 },
 {
   "STT": 2621,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tâm Phúc",
   "address": "Số 19 ngách 260/11 đường Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.040478,
   "Latitude": 105.82771
 },
 {
   "STT": 2622,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hương Phúc Đường",
   "address": "Số 20 ngách 143/269 Nguyễn Chính, tổ 40, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9779152,
   "Latitude": 105.8483729
 },
 {
   "STT": 2623,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Đông Nam Y Dược Việt Hoa",
   "address": "Số 8 Trần Phú, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9839421,
   "Latitude": 105.7918218
 },
 {
   "STT": 2624,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tam Thế",
   "address": "Số 398 đường Ngọc Thụy, quận Long Biên, Hà Nội",
   "Longtitude": 21.0503311,
   "Latitude": 105.8667636
 },
 {
   "STT": 2625,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - Số 213",
   "address": "Số 109, nhà A5, tập thể Giảng Võ, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0298307,
   "Latitude": 105.820361
 },
 {
   "STT": 2626,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Mạnh Khang",
   "address": "Số 25 ngõ A6 thị trấn Đại học Hà Nội, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0151855,
   "Latitude": 105.8086376
 },
 {
   "STT": 2627,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 26-B1 khu Đầm Trấu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0118555,
   "Latitude": 105.8651409
 },
 {
   "STT": 2628,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Xóm Mới, Thôn Tự Khoát, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9256234,
   "Latitude": 105.8561733
 },
 {
   "STT": 2629,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Nhà C8, tổ 54A, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0362368,
   "Latitude": 105.7905825
 },
 {
   "STT": 2630,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nam Sơn",
   "address": "Số 86B phố Tuệ Tĩnh, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0151208,
   "Latitude": 105.8495714
 },
 {
   "STT": 2631,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Diệu Phương Đường",
   "address": "Phòng 121, tầng 1, nhà 127, đường Nguyễn Phong Sắc, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0386819,
   "Latitude": 105.790194
 },
 {
   "STT": 2632,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Nhà số 9, hẻm 6, ngách 55, ngõ 381, đường Nguyễn Khang, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0280469,
   "Latitude": 105.7957381
 },
 {
   "STT": 2633,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Đồng Tiến, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.7930365,
   "Latitude": 105.8435941
 },
 {
   "STT": 2634,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Xóm Đồng Tranh, Thôn Quảng Nguyên, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7755005,
   "Latitude": 105.7780506
 },
 {
   "STT": 2635,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 28 Thôn Thanh Ấm, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.7249952,
   "Latitude": 105.7724041
 },
 {
   "STT": 2636,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Y Học Cổ Truyền Việt Hung",
   "address": "Số 37 Lý Nam Đế, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.036281,
   "Latitude": 105.845341
 },
 {
   "STT": 2637,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Khu dân cư Phú Diễn, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0508448,
   "Latitude": 105.7502747
 },
 {
   "STT": 2638,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phường 103 N9 - Số 15C phố Trần Khánh Dư, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.021225,
   "Latitude": 105.8614831
 },
 {
   "STT": 2639,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Thiện Đường",
   "address": "Số 26 ngõ 51/2 phố Lãng Yên, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0092216,
   "Latitude": 105.8666563
 },
 {
   "STT": 2640,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Minh Đức",
   "address": "Số 276 phố Lê Trọng Tấn, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9939419,
   "Latitude": 105.8314746
 },
 {
   "STT": 2641,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tâm Đức Đường",
   "address": "Số 18/15/134 phố Lê Trọng Tấn, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9939419,
   "Latitude": 105.8314746
 },
 {
   "STT": 2642,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 33A ngõ 117 đường Khương Đình, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.995429,
   "Latitude": 105.814348
 },
 {
   "STT": 2643,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phúc Hưng Đường Trực Thuộc Công Ty Tnhh Đông Dược Phúc Hưng",
   "address": "Cửa hàng tầng 1, nhà C1 khu dự án Làng Quốc tế Thăng Long, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.038457,
   "Latitude": 105.7929245
 },
 {
   "STT": 2644,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Văn Sơn",
   "address": "Cụm 3, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1241396,
   "Latitude": 105.5939066
 },
 {
   "STT": 2645,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "4, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6174102,
   "Latitude": 105.7893761
 },
 {
   "STT": 2646,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Cụm 1, huyện Phúc Thọ, Hà Nội",
   "Longtitude": 21.1241396,
   "Latitude": 105.5939066
 },
 {
   "STT": 2647,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Cụm 8, Phúc Thọ, Hà Nội",
   "Longtitude": 21.1032387,
   "Latitude": 105.5441093
 },
 {
   "STT": 2648,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Thành Huy",
   "address": "Số 137 đường Đại Nghĩa, Thôn Tế Tiêu, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6829352,
   "Latitude": 105.7445111
 },
 {
   "STT": 2649,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Cụm 6, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.083795,
   "Latitude": 105.7140464
 },
 {
   "STT": 2650,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": ", huyện Ba Vì, Hà Nội",
   "Longtitude": 21.1992298,
   "Latitude": 105.4232116
 },
 {
   "STT": 2651,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tế Phương Đường",
   "address": "Số nhà 12, ngõ 64 phố Thạch Bàn, quận Long Biên, Hà Nội",
   "Longtitude": 21.0216695,
   "Latitude": 105.9141793
 },
 {
   "STT": 2652,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 55, tổ 27, quận Long Biên, Hà Nội",
   "Longtitude": 21.0548635,
   "Latitude": 105.8884966
 },
 {
   "STT": 2653,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tuân Sinh Đường",
   "address": "Số 67, phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0346894,
   "Latitude": 105.8481876
 },
 {
   "STT": 2654,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Phương Y Quán",
   "address": "Số 41 ngõ 378 Lê Duẩn, quận Đống Đa, Hà Nội",
   "Longtitude": 21.009781,
   "Latitude": 105.841227
 },
 {
   "STT": 2655,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": " Số 397 đường Thụy Khuê, , quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0458285,
   "Latitude": 105.8132794
 },
 {
   "STT": 2656,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Diệu Phương Đường",
   "address": "Số 27A, tổ 10A, ngõ 122 đường Láng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.009306,
   "Latitude": 105.8162
 },
 {
   "STT": 2657,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 6 ngõ 126 phố Hoàng Văn Thái, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0246732,
   "Latitude": 105.827562
 },
 {
   "STT": 2658,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 2 ngách 68 ngõ Lương Sử C, quận Đống Đa, Hà Nội",
   "Longtitude": 21.025096,
   "Latitude": 105.837173
 },
 {
   "STT": 2659,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tập thể xí nghiệp X81 ngõ Thịnh Hào 1, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0241543,
   "Latitude": 105.8302796
 },
 {
   "STT": 2660,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Y Học Cổ Truyền Taytang",
   "address": "Số 17, ngõ 18, đường Huỳnh Thúc Kháng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0191046,
   "Latitude": 105.8105103
 },
 {
   "STT": 2661,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Y Dược Phong Phú",
   "address": "Số 5 đường Tứ Hiệp, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9470426,
   "Latitude": 105.8488908
 },
 {
   "STT": 2662,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Y Dược Phong Phú",
   "address": "Quỳnh Đô, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9378094,
   "Latitude": 105.8363687
 },
 {
   "STT": 2663,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "P402- K18, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0134733,
   "Latitude": 105.848009
 },
 {
   "STT": 2664,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 5 ngõ 82 phố Phạm Ngọc Thạch, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0238716,
   "Latitude": 105.8062378
 },
 {
   "STT": 2665,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": ", huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8746466,
   "Latitude": 105.6712465
 },
 {
   "STT": 2666,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Nam Dương, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.69028,
   "Latitude": 105.7571236
 },
 {
   "STT": 2667,
   "Name": "Phần Chẩn Trị Y Học Cổ Truyền Đức Đạo",
   "address": "Số 72, tổ 9, quận Hà Đông, Hà Nội",
   "Longtitude": 20.955835,
   "Latitude": 105.7563658
 },
 {
   "STT": 2668,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiện Tâm Trực Thuộc Công Ty Cổ Phần Công Nghệ Y Dược Đức An",
   "address": "Thôn Chu Xá, huyện Gia Lâm, Hà Nội",
   "Longtitude": 20.9446806,
   "Latitude": 105.9040407
 },
 {
   "STT": 2669,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Tương Chúc, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9207281,
   "Latitude": 105.8654457
 },
 {
   "STT": 2670,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hương Phúc Đường",
   "address": "Số nhà 306, đường Xuân Đỉnh, tổ dân phố Trung, , quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0739704,
   "Latitude": 105.790872
 },
 {
   "STT": 2671,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 95 Láng Hạ, quận Đống Đa, Hà Nội",
   "Longtitude": 21.014739,
   "Latitude": 105.813971
 },
 {
   "STT": 2672,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 49B ngõ 33 phố Nguyễn An Ninh, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.991418,
   "Latitude": 105.8444462
 },
 {
   "STT": 2673,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Moonday",
   "address": "Số 25 ngõ 282 phố Nguyễn Huy Tưởng, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.995629,
   "Latitude": 105.802381
 },
 {
   "STT": 2674,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Trung Tâm Thừa Kế, Ứng Dụng Đông Y ",
   "address": "Số 1 phố Bà Triệu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9721371,
   "Latitude": 105.7786756
 },
 {
   "STT": 2675,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Văn Tuấn",
   "address": "Số 5 ngõ 82 phố Phạm Ngọc Thạch, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0238716,
   "Latitude": 105.8062378
 },
 {
   "STT": 2676,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phòng A0812, tòa nhà Hồ Gươm Plaza, khu đô thị Mỗ Lao, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9830386,
   "Latitude": 105.7834697
 },
 {
   "STT": 2677,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Mỹ Việt",
   "address": "Số 620 Hoàng Hoa Thám, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0467597,
   "Latitude": 105.8098222
 },
 {
   "STT": 2678,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Liễu Nguyên Đường",
   "address": "Số nhà 35, ngõ 395, đường Lạc Long Quân, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0551027,
   "Latitude": 105.8085153
 },
 {
   "STT": 2679,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hoàng Gia Trực Thuộc Công Ty Cổ Phần Y Tế Hoàng Gia",
   "address": "Số 139A Nguyễn Thái Học, quận Ba Đình, Hà Nội",
   "Longtitude": 21.030554,
   "Latitude": 105.835429
 },
 {
   "STT": 2680,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thu Dũng",
   "address": "Tổ 5, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9812552,
   "Latitude": 105.7849593
 },
 {
   "STT": 2681,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 6, phố Thụy Ứng, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0859166,
   "Latitude": 105.6620836
 },
 {
   "STT": 2682,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Thôn Ngô Đồng, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.7955358,
   "Latitude": 105.7857359
 },
 {
   "STT": 2683,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "P103 tập thể Bệnh viện Việt Xô, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0060039,
   "Latitude": 105.8618616
 },
 {
   "STT": 2684,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 191M phố Minh Khai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.995914,
   "Latitude": 105.8585219
 },
 {
   "STT": 2685,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 105Đ phố Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 2686,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tầng 2, số 95, phố Đội Cấn, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0353818,
   "Latitude": 105.8192757
 },
 {
   "STT": 2687,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 7 ngách 7 ngõ 409 An Dương Vương, tổ 9 cụm 1, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.08927,
   "Latitude": 105.7972616
 },
 {
   "STT": 2688,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Chợ Bái, huyện Phú Xuyên, Hà Nội",
   "Longtitude": 20.680846,
   "Latitude": 105.9571122
 },
 {
   "STT": 2689,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 425A phố Bạch Mai, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 20.9979326,
   "Latitude": 105.8503243
 },
 {
   "STT": 2690,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Dương Khê, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 20.728178,
   "Latitude": 105.7901381
 },
 {
   "STT": 2691,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Mai Xuân Linh",
   "address": "Số 67 phố Cù Chính Lan, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9996748,
   "Latitude": 105.8233472
 },
 {
   "STT": 2692,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Hiền",
   "address": "Số 23 ngõ 54 phố Bùi Xương Trạch, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.99084,
   "Latitude": 105.8169659
 },
 {
   "STT": 2693,
   "Name": "Phòng khám Châm Cứu Bấm Huyệt",
   "address": "Số 226 ngõ chợ Khâm Thiên, quận Đống Đa, Hà Nội",
   "Longtitude": 21.016164,
   "Latitude": 105.837983
 },
 {
   "STT": 2694,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 119, ngõ 66 đường Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.0505892,
   "Latitude": 105.8675355
 },
 {
   "STT": 2695,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiên Phú Đường Trực Thuộc Công Ty Cổ Phần Thiên Phú Đường ",
   "address": "Số 139, phố Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0012748,
   "Latitude": 105.8393044
 },
 {
   "STT": 2696,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thái Lai Đường",
   "address": "Số 33, phố Vạn Phúc Thượng, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0330596,
   "Latitude": 105.8192478
 },
 {
   "STT": 2697,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 5- ngõ 11, đường Luyện Kim, Quán Gánh, , huyện Thường Tín, Hà Nội",
   "Longtitude": 20.8950451,
   "Latitude": 105.8558183
 },
 {
   "STT": 2698,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền An Sinh",
   "address": "Số 238 phố Hoàng Văn Thái, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9836984,
   "Latitude": 105.8636257
 },
 {
   "STT": 2699,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền  Hải Thượng ",
   "address": "Số 70, phố Trần Quốc Toản, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.020538,
   "Latitude": 105.8465224
 },
 {
   "STT": 2700,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đông Y Dược Hùng Vương",
   "address": "Ki ốt 1, nhà CT6A Cầu Bươu, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9606203,
   "Latitude": 105.8004396
 },
 {
   "STT": 2701,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "607 đường Ngọc Hồi, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.941421,
   "Latitude": 105.843986
 },
 {
   "STT": 2702,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 98 phố Ngọc Hà, quận Ba Đình, Hà Nội",
   "Longtitude": 21.037223,
   "Latitude": 105.830984
 },
 {
   "STT": 2703,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Việt Tâm Trực Thuộc Công Ty Cổ Phần Công Nghệ Y Dược Đức An",
   "address": "Số 987 đường Giải Phóng, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9851676,
   "Latitude": 105.8409478
 },
 {
   "STT": 2704,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "107 Bế Văn Đàn, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9701922,
   "Latitude": 105.7738007
 },
 {
   "STT": 2705,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số nhà 5, ngõ 228 phố Lê Trọng Tấn, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9939419,
   "Latitude": 105.8314746
 },
 {
   "STT": 2706,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số nhà 25, phố Mễ Trì Thượng, tổ dân phố Thượng, quận Nam Từ Liêm, Hà Nội",
   "Longtitude": 21.0055267,
   "Latitude": 105.7791327
 },
 {
   "STT": 2707,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thanh Loan",
   "address": "Số 2B, ngõ 115 phố Núi Trúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0295811,
   "Latitude": 105.8240265
 },
 {
   "STT": 2708,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Việt Phương",
   "address": "Khu 7, huyện Mê Linh, Hà Nội",
   "Longtitude": 21.1850027,
   "Latitude": 105.7204476
 },
 {
   "STT": 2709,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đan Phương Ii",
   "address": "Số 42, BT8, khu đô thị Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9741262,
   "Latitude": 105.7894045
 },
 {
   "STT": 2710,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền An Thịnh Đường",
   "address": "Số 25 ngõ 54 phố Bùi Xương Trạch, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.99084,
   "Latitude": 105.8169659
 },
 {
   "STT": 2711,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Đầu Tư Long Biên",
   "address": "Khu Trung đoàn 918, quận Long Biên, Hà Nội",
   "Longtitude": 21.0376249,
   "Latitude": 105.8957718
 },
 {
   "STT": 2712,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 115 ngõ 203 phố Kim Ngưu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025637,
   "Latitude": 105.8617727
 },
 {
   "STT": 2713,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Long Biên",
   "address": "Số 45B, phố Trường Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.06229,
   "Latitude": 105.8981502
 },
 {
   "STT": 2714,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phòng 202 nhà D7 tập thể Trung Tự, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0028128,
   "Latitude": 105.8347862
 },
 {
   "STT": 2715,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vũ Gia Đường",
   "address": "Số 5A, ngõ 122 đường Kim Giang, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9812185,
   "Latitude": 105.8170828
 },
 {
   "STT": 2716,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền An Việt",
   "address": "Số 152, phố Ngọc Khánh, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0255913,
   "Latitude": 105.818218
 },
 {
   "STT": 2717,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Hà Xá, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.7102888,
   "Latitude": 105.760058
 },
 {
   "STT": 2718,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tổ dân phố cụm 14, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0565405,
   "Latitude": 105.7453864
 },
 {
   "STT": 2719,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Cao Lương Đường",
   "address": "Số 463 phố Kim Ngưu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025637,
   "Latitude": 105.8617727
 },
 {
   "STT": 2720,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 77 phố Nguyễn Khuyến, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0282069,
   "Latitude": 105.838835
 },
 {
   "STT": 2721,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "P116 - A4, tập thể Thành Công, quận Ba Đình, Hà Nội",
   "Longtitude": 21.020889,
   "Latitude": 105.8158204
 },
 {
   "STT": 2722,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Điềm Hải",
   "address": "Thôn Ái Nàng, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.6461112,
   "Latitude": 105.7022602
 },
 {
   "STT": 2723,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 1, ngõ 1 đường Lĩnh Nam, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9819356,
   "Latitude": 105.8797073
 },
 {
   "STT": 2724,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tế Sinh Đường",
   "address": "Số 185, phố Tô Hiệu, tổ 81, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.043761,
   "Latitude": 105.797413
 },
 {
   "STT": 2725,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bác sĩ Quỳnh",
   "address": "Tầng 1, số 1 ngách 27/2 Huỳnh Thúc Kháng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0179087,
   "Latitude": 105.8117959
 },
 {
   "STT": 2726,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Dược Phẩm Bạch Mã Vạn Xuân",
   "address": "Số 127, phố Nguyễn Khoái, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0123804,
   "Latitude": 105.8635765
 },
 {
   "STT": 2727,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Đông Y Đồng Nhân Đường",
   "address": "Số nhà 26, ngõ 15, đường An Dương Vương, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.085581,
   "Latitude": 105.8148688
 },
 {
   "STT": 2728,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền - 62",
   "address": "Số 89 phố Thanh Nhàn, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.002791,
   "Latitude": 105.858563
 },
 {
   "STT": 2729,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Tập Đoàn Liên Kết Việt Nam",
   "address": "Lô C16/D21 Khu đô thị mới Cầu Giấy, phố Dịch Vọng Hậu, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0289011,
   "Latitude": 105.7857442
 },
 {
   "STT": 2730,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nguyễn Đắc Lương",
   "address": "Khu 5, huyện Hoài Đức, Hà Nội",
   "Longtitude": 21.064972,
   "Latitude": 105.704941
 },
 {
   "STT": 2731,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 18, ngõ 87, đường Tân Xuân, quận Bắc Từ Liêm, Hà Nội",
   "Longtitude": 21.0804081,
   "Latitude": 105.7849147
 },
 {
   "STT": 2732,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hưng Chi",
   "address": "Số 50 phố Quán Sứ, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.0263829,
   "Latitude": 105.8456339
 },
 {
   "STT": 2733,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 13D, tổ 1 Văn Quán, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9770819,
   "Latitude": 105.7881784
 },
 {
   "STT": 2734,
   "Name": "Phòng khám Gia Truyền Thiên Đức",
   "address": "Số 5 ngõ 133 Thái Hà, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0133519,
   "Latitude": 105.819327
 },
 {
   "STT": 2735,
   "Name": "Đông Y Gia Truyền Thiện Tín 2",
   "address": "Số 19 xóm Kho, Thôn Cổ Điển A, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9411413,
   "Latitude": 105.8547173
 },
 {
   "STT": 2736,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trần Ngọc Chấn",
   "address": "Phòng 4 ngõ 84 phố Hoàng Đạo Thành, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.982868,
   "Latitude": 105.813608
 },
 {
   "STT": 2737,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 35, ngõ 75, phố Vĩnh Phúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0441437,
   "Latitude": 105.8107451
 },
 {
   "STT": 2738,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Nicotex",
   "address": "Số 118 Vũ Xuân Thiều, quận Long Biên, Hà Nội",
   "Longtitude": 21.035837,
   "Latitude": 105.9200803
 },
 {
   "STT": 2739,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tổ dân phố Trung Kiên, quận Hà Đông, Hà Nội",
   "Longtitude": 20.975721,
   "Latitude": 105.745613
 },
 {
   "STT": 2740,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Nhà số 3 ngõ 6, huyện Ứng Hòa, Hà Nội",
   "Longtitude": 21.0277644,
   "Latitude": 105.8341598
 },
 {
   "STT": 2741,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 112, phố Nguyễn Thái Học, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.031752,
   "Latitude": 105.831573
 },
 {
   "STT": 2742,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Nhân Hòa",
   "address": "Số 70 ngõ 116 phố Nhân Hòa, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0034189,
   "Latitude": 105.8070264
 },
 {
   "STT": 2743,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trương Thánh Y Số 7 Trực Thuộc Công Ty Cổ Phần Y Dược Lis",
   "address": "Số 7, ngõ 102 Khuất Duy Tiến, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9968592,
   "Latitude": 105.8000546
 },
 {
   "STT": 2744,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Ích Nhân Đường",
   "address": "Lô 22, khu tập thể B12, tổng cục 5 - Bộ Công An, ngõ 106 đường Hoàng Quốc Việt, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0477757,
   "Latitude": 105.794652
 },
 {
   "STT": 2745,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Bảo Long - Bảo Khoa Đường",
   "address": "Số 317A Thụy Khuê, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.0461361,
   "Latitude": 105.8128972
 },
 {
   "STT": 2746,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Y Dược Nhật Quang",
   "address": "Tầng 3, tầng 4 - Nhà số 89B Vũ Tông Phan, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9978611,
   "Latitude": 105.8171176
 },
 {
   "STT": 2747,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 8 ngõ 116 phố Nhân Hòa, quận Thanh Xuân, Hà Nội",
   "Longtitude": 21.0022937,
   "Latitude": 105.8068638
 },
 {
   "STT": 2748,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phố Nguyễn Thái Học, huyện Đan Phượng, Hà Nội",
   "Longtitude": 21.0887934,
   "Latitude": 105.6564138
 },
 {
   "STT": 2749,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Tổ 02 phố Kim Bài, huyện Thanh Oai, Hà Nội",
   "Longtitude": 20.8559363,
   "Latitude": 105.7673942
 },
 {
   "STT": 2750,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Tnhh Dịch Vụ Y Tế Ích Bảo",
   "address": "Số 452 đường Láng, quận Đống Đa, Hà Nội",
   "Longtitude": 21.009433,
   "Latitude": 105.813274
 },
 {
   "STT": 2751,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Châm Cứu Hà Nội",
   "address": "Số 3A ngõ 134 đường Lê Trọng Tấn, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9939419,
   "Latitude": 105.8314746
 },
 {
   "STT": 2752,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thiên Đức",
   "address": "Số nhà 55, đường Chiến Thắng, tổ 3, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9797044,
   "Latitude": 105.7957259
 },
 {
   "STT": 2753,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Vạn Phát Đường",
   "address": "Số 12C phố Thọ Lão, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0118515,
   "Latitude": 105.858612
 },
 {
   "STT": 2754,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Y Dược Trương Thánh Y",
   "address": "Số 36 đường Phùng Hưng, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9685906,
   "Latitude": 105.7863159
 },
 {
   "STT": 2755,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tuệ Minh Đường",
   "address": "Số 70 phố Trung Liệt, quận Đống Đa, Hà Nội",
   "Longtitude": 21.0108093,
   "Latitude": 105.8233775
 },
 {
   "STT": 2756,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thọ Xuân Đường",
   "address": "Số 15 - thị trấn Thủy Sản, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9959819,
   "Latitude": 105.8097244
 },
 {
   "STT": 2757,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Trực Thuộc Công Ty Cổ Phần Thảo Dược Cổ Phương",
   "address": "Số 371 phố Hoàng Hoa Thám, quận Ba Đình, Hà Nội",
   "Longtitude": 21.042057,
   "Latitude": 105.817086
 },
 {
   "STT": 2758,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Sơn Vũ",
   "address": "Số 156 phố Yên Phụ, quận Tây Hồ, Hà Nội",
   "Longtitude": 21.055153,
   "Latitude": 105.8357787
 },
 {
   "STT": 2759,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phú Xuân Đường",
   "address": "Số 88, tổ 5, khu Chiến Thắng, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.893522,
   "Latitude": 105.571545
 },
 {
   "STT": 2760,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Số 59 phố Lãn Ông, quận Hoàn Kiếm, Hà Nội",
   "Longtitude": 21.035624,
   "Latitude": 105.8487849
 },
 {
   "STT": 2761,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Việt Y",
   "address": "Tầng 7 (một phần), tòa nhà Lancaster - số 20, phố Núi Trúc, quận Ba Đình, Hà Nội",
   "Longtitude": 21.0287997,
   "Latitude": 105.823619
 },
 {
   "STT": 2762,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phùng Gia Đường",
   "address": "Tầng 1, số nhà A3, lô A, khu đô thị Yên Hòa, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0181073,
   "Latitude": 105.7859194
 },
 {
   "STT": 2763,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Tâm Phúc",
   "address": "Số 3 ngõ 39 đường Yên Xá, huyện Thanh Trì, Hà Nội",
   "Longtitude": 20.9682343,
   "Latitude": 105.7939633
 },
 {
   "STT": 2764,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Đại Hữu Đường",
   "address": "101- A1 khu thị trấn 51 phố Cảm Hội, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0113555,
   "Latitude": 105.8610952
 },
 {
   "STT": 2765,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Gia Đạt",
   "address": "Số 281 phố Kim Ngưu, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0025637,
   "Latitude": 105.8617727
 },
 {
   "STT": 2766,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Gia Bảo",
   "address": "Tầng 2, số 65 ngõ 2 phố Phương Mai, quận Đống Đa, Hà Nội",
   "Longtitude": 21.004717,
   "Latitude": 105.840402
 },
 {
   "STT": 2767,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Phần diện tích 72m2, tầng 1, số 4, đường Phạm Hùng, quận Cầu Giấy, Hà Nội",
   "Longtitude": 21.0287108,
   "Latitude": 105.7794437
 },
 {
   "STT": 2768,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 1, ngõ 31 phố Đội Nhân, , quận Ba Đình, Hà Nội",
   "Longtitude": 21.0410125,
   "Latitude": 105.81166
 },
 {
   "STT": 2769,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Minh Quang",
   "address": "Phòng 104 - D3 tập thể 8/3, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0105004,
   "Latitude": 105.8317819
 },
 {
   "STT": 2770,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Phú Vinh, huyện Hoài Đức, Hà Nội",
   "Longtitude": 20.989744,
   "Latitude": 105.724052
 },
 {
   "STT": 2771,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Thế Cường",
   "address": "Thôn Ngãi Cầu, huyện Hoài Đức, Hà Nội",
   "Longtitude": 20.990271,
   "Latitude": 105.7220048
 },
 {
   "STT": 2772,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền ",
   "address": "Nhà LK25-32 Ngô Thì Nhậm, quận Hà Đông, Hà Nội",
   "Longtitude": 20.969517,
   "Latitude": 105.769066
 },
 {
   "STT": 2773,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Gia Lâm Trực Thuộc Công Ty Tnhh Xuất Nhập Khẩu Thiết Bị Y Tế Minh Bang Việt Nam",
   "address": "Số 540 Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.049606,
   "Latitude": 105.880674
 },
 {
   "STT": 2774,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Gia Lâm Trực Thuộc Công Ty Tnhh Xuất Nhập Khẩu Thiết Bị Y Tế Minh Bang Việt Nam",
   "address": "Số 540 Ngọc Lâm, quận Long Biên, Hà Nội",
   "Longtitude": 21.049606,
   "Latitude": 105.880674
 },
 {
   "STT": 2775,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Tân Hội, huyện Chương Mỹ, Hà Nội",
   "Longtitude": 20.8662557,
   "Latitude": 105.6104641
 },
 {
   "STT": 2776,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Thôn Đoan Nữ, huyện Mỹ Đức, Hà Nội",
   "Longtitude": 20.7493267,
   "Latitude": 105.6959921
 },
 {
   "STT": 2777,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền",
   "address": "Số 21, lô 1, khu đô thị Nam La Khê, quận Hà Đông, Hà Nội",
   "Longtitude": 20.9746348,
   "Latitude": 105.7583488
 },
 {
   "STT": 2778,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Hợp Hưng",
   "address": "Nhà 59, ngõ 15, đường Ngọc Hồi, quận Hoàng Mai, Hà Nội",
   "Longtitude": 20.9611334,
   "Latitude": 105.8421339
 },
 {
   "STT": 2779,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Phú An Đường",
   "address": "Số 33 ngõ 358/40 Bùi Xương Trạch, quận Thanh Xuân, Hà Nội",
   "Longtitude": 20.9858035,
   "Latitude": 105.8164087
 },
 {
   "STT": 2780,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Kinh Bắc",
   "address": "Số 13H, phố Cao Bá Quát, quận Ba Đình, Hà Nội",
   "Longtitude": 21.029965,
   "Latitude": 105.839808
 },
 {
   "STT": 2781,
   "Name": "Phòng Chẩn Trị Y Học Cổ Truyền Từ Thiện Chùa Liên Phái",
   "address": "Số 184 ngõ Chùa Liên Phái, quận Hai Bà Trưng, Hà Nội",
   "Longtitude": 21.0050702,
   "Latitude": 105.8496505
 }
];