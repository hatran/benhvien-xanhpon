var datakhoidonvikhac = [
 {
   "STT": 1,
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình",
   "icon": "maps/map-images/cckhhgd.png",
   "address": "22 Phố Lý Thái Tổ, Lý Thái Tổ, Hoàn Kiếm, Hà Nội, Việt Nam",
   "Longtitude": 21.0304196,
   "Latitude": 105.8526007
 },
 {
   "STT": 2,
   "Name": "Chi cục An toàn vệ sinh thực phẩm",
   "icon": "maps/map-images/attp.png",
   "address": "Số 70 Nguyễn Chí Thanh, Q. Đống Đa, Hà Nội",
   "Longtitude": 21.0187492,
   "Latitude": 105.804673
 },
 {
   "STT": 3,
   "Name": "Trung tâm cấp cứu 115",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "11 Phan Chu Trinh, Hoàn Kiếm, Hoàn Kiếm Hà Nội, Việt Nam",
   "Longtitude": 21.0224197,
   "Latitude": 105.8542665
 },
 {
   "STT": 4,
   "Name": "Trung tâm Y tế dự phòng Hà Nội",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "70 Nguyễn Chí Thanh, Láng Thượng, Ba Đình, Hà Nội, Việt Nam",
   "Longtitude": 21.0187492,
   "Latitude": 105.804673
 },
 {
   "STT": 5,
   "Name": "Trung tâm Giám định y khoa Hà Nội",
   "icon": "maps/map-images/gdyk.png",
   "address": "Phương Đình, Đống Đa, Hà Nội, Việt Nam",
   "Longtitude": 21.0011489,
   "Latitude": 105.8360785
 },
 {
   "STT": 6,
   "Name": "Trung tâm Pháp y Hà Nội",
   "icon": "maps/map-images/placeholder-xs1.png",
   "address": "35 Trần Bình, Mai Dịch, Cầu Giấy, Hà Nội, Việt Nam",
   "Longtitude": 21.0337129,
   "Latitude": 105.7766836
 }
];